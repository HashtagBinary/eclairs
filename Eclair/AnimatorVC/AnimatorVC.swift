//
//  AnimatorVC.swift
//  Eclair
//
//  Created by iOS Indigo on 02/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class AnimatorVC: UIViewController {
    
    @IBOutlet weak var animatedVw: UIStackView!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var username: UILabel!
    var name = ""
    var ecode = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.namelbl.text = UserDefaults.standard.string(forKey: "fullName")
        self.username.text = UserDefaults.standard.string(forKey: "eclareName")
        self.fadeViewIn()
        self.profileData(encode: self.ecode)
    }
    
    func profileData(encode : String)
    {
        let parameters: Parameters = [
            "ecode" : encode
        ]
        print("parameter \(parameters)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kProfileImage, parameters: parameters) { (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            if let picture = json?["Picture"].string
            {
                UserDefaults.standard.setValue(picture, forKey: "profileImg")
            }
        }
    }
    @IBAction func skipebtn(_ sender: Any) {
        self.performSegue(withIdentifier: "home", sender: nil)
    }
    func fadeViewIn() {

        UIView.animate(withDuration: 2.0, delay: 0.8, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.animatedVw.transform = CGAffineTransform.identity.scaledBy(x: 1.3, y: 1.3) //
        }) { (finished) in
            
        }
    }
}
