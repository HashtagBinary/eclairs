//
//  bottomShadow.swift
//  Scamlookup
//
//  Created by Developer on 9/9/17.
//  Copyright © 2017 Scamlookup. All rights reserved.
//

import UIKit

class bottomShadow: UIView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 0.0
    }

}
