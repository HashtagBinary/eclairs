//
//  AroundShadowBordered.swift
//  Eclair
//
//  Created by iOS Indigo on 12/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit


class AroundShadowBordered: UIView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
       // self.layer.shadowColor = UIColor.lightGray.cgColor
       // self.layer.shadowOpacity = 0.4
        //self.layer.shadowOffset = CGSize.zero
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(named: "ColorLine")?.cgColor
    }

}
