//
//  LeftRightShadowView.swift
//  AgorzCustomer
//
//  Created by Developer on 01/08/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class LeftRightShadowView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.*/
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize.zero
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
        let shadowRect: CGRect = self.bounds.insetBy(dx: 0, dy: 5)
        self.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
    }
 

}
