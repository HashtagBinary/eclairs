//
//  AroundShadow.swift
//  AgorzCustomer
//
//  Created by Developer on 10/07/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class AroundShadow: UIView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize.zero
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 8
    }

}
