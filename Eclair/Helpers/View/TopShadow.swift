//
//  TopShadow.swift
//  AgorzCustomer
//
//  Created by Developer on 11/06/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class TopShadow: UIView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: -5.0, height: -5.0)
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5.0
    }

}
