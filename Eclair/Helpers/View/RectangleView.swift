//
//  RectangleView.swift
//  Eclair
//
//  Created by iOS Indigo on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit


class RectangleView: UIView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
       
        self.layer.cornerRadius = 5.0
        self.borderColor = UIColor.lightGray
        self.borderWidth = 0.5
        
    }

}
