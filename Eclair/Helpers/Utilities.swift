//
//  Utilities.swift


import UIKit
import AVFoundation
import Foundation
import SystemConfiguration
import NVActivityIndicatorView


class Utilities {

    var ecode = ""
    //MARK:- activityIndicator
    class func showActivityIndicator()
    {
        
        //rgb(255,58,147) .ballBeat
        NVActivityIndicatorView.DEFAULT_TYPE = .lineSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData,NVActivityIndicatorView.DEFAULT_FADE_IN_ANIMATION)
        
    }
    
    //MARK: - hideActivityIndicator
    class func hideActivityIndicator()
    {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(NVActivityIndicatorView.DEFAULT_FADE_OUT_ANIMATION)
        
    }
    class func displayMemberType(ecode : String) -> String
    {
        if ecode.starts(with: "m")
        {
            if Constants.kIsMainMember == true
            {
                return "MAIN MEMBER"
            }
            else
            {
                return "SUB MEMBER"
            }
            
        }
        else if ecode.starts(with: "g")
        {
            return " GUEST"
        }
        else if ecode.starts(with: "e")
        {
            return "EMPLOYEE"
        }
        else if ecode.starts(with: "t")
        {
            if Constants.kIsMainMember == true
            {
                return "MAIN TENANT"
            }
            else
            {
                return "SUB TENANT"
            }
        }
        else
        {
            return ""
        }
    }
    
    // Find Color From Hex Value
    class func UIColorFromHex(hex:String, alpha:Double = 1.0) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
    // Display Alert Message
    class func showAlert(_ title: String,
                         message: String,
                         actions:[UIAlertAction] = [UIAlertAction(title: "OK", style: .cancel, handler: nil)]) {
        if checkAlertExist() == false {
            
            let topView = UIApplication.topViewController()
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            for action in actions {
                alert.addAction(action)
            }
            topView?.present(alert, animated: true, completion: nil)
            
            UILabel.appearance(whenContainedInInstancesOf:
                [UIAlertController.self]).numberOfLines = 0
            
            UILabel.appearance(whenContainedInInstancesOf:
                [UIAlertController.self]).lineBreakMode = .byWordWrapping
            
        }
    }
    
    class func showAlertOnCamera(title: String, message: String, controller: UIViewController, videoPreviewLayer: AVCaptureVideoPreviewLayer) {
        
        if checkAlertExist() == false {
            
            let topView = UIApplication.topViewController()
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) -> Void in
                videoPreviewLayer.connection?.isEnabled = true
            }))
            UILabel.appearance(whenContainedInInstancesOf: [UIAlertController.self]).numberOfLines = 0
            topView?.present(alert, animated: true, completion: nil)
        }
    }
    
    class func checkAlertExist() -> Bool {
        for window: UIWindow in UIApplication.shared.windows {
            if (window.rootViewController?.presentedViewController is UIAlertController) {
                return true
            }
        }
        return false
    }
    
    class func checkInternetConnection() -> Bool {
        
        if(self.isConnectedToNetwork())
        {
            print("Reachable to network")
            return true
        }
        else
        {
            print("Unreachable to network")
            return false
        }
    }
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    //MARK: - store and retrieve value from UserDefault
    
    class func setValueInNSUserDefaults(_ value:AnyObject?, forKey key:String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveValueFromDefault(forKey:String)->String
    {
        return UserDefaults.standard.value(forKey: forKey) as? String ?? ""
    }
    
    class func retriveIntValueFromDefault(forKey:String)->Int
    {
        return UserDefaults.standard.value(forKey: forKey) as? Int ?? 0
    }
    
    class func setObjectToDefault(object:Any,forKey:String)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        UserDefaults.standard.set(data, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveObjectFromDefault(forKey:String)->Any?
    {
        let data = UserDefaults.standard.object(forKey: forKey)
        if data != nil
        {
            let object = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return object
        }
        return nil
    }
    
    class func setBoolValue(value:Bool,forKey:String)
    {
        UserDefaults.standard.set(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveBoolValue(forKey:String)->Bool {
        return UserDefaults.standard.value(forKey: forKey) as? Bool ?? false
    }
    
    class func resetUserDefault(forKey:String)
    {
        UserDefaults.standard.removeObject(forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    // MARK: convert local date & time to UTC
    class func localToUTC(date:String, fromFormat: String, toFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: dt!)
    }
    
    // MARK: Convert UTC date & time to local
    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: dt!)
    }
    
    class func convertDateFormate(forStringDate strDate: String, currentFormate: String, newFormate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentFormate
        let dateObj = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = newFormate
        if dateObj != nil{
            return dateFormatter.string(from: dateObj!)
        }else{
            return ""
        }
    }
    
    class func getDateFromString(strDate: String, currentFormate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentFormate
        let dateObj = dateFormatter.date(from: strDate)
        return dateObj!
    }
    // MARK: Convert Unix To Current Date/Time
    class func UnixToLocal(date : Double, toFormat: String = "") -> String {
        
        let date = Date(timeIntervalSince1970: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:MM a | MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date)
    }
    class func callNumber(phoneNumber:String) {
     
        let MobileNO = phoneNumber.replacingOccurrences(of: ["+", "-", " ", "(", ")"], with: "")
        if let phoneCallURL = URL(string: "tel://\(MobileNO)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }

    class func countryCode(from countryName: String) -> String? {
        return NSLocale.isoCountryCodes.first { (code) -> Bool in
            let name = NSLocale.current.localizedString(forRegionCode: code)
            return name == countryName
        }
    }
}

extension DispatchQueue {

    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }

}
