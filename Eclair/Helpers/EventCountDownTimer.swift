//
//  EventCountDownTimer.swift
//  Eclair IOS
//
//  Created by Sohail on 07/11/2020.
//

import UIKit

@objc class EventCountDownTimer: UIControl {
    
    var timer: Timer?
    public var endTimeMillis: Int64 = 0
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.horizontal
        stackView.distribution  = UIStackView.Distribution.fill
        stackView.alignment = UIStackView.Alignment.fill
        stackView.spacing   = 5
        return stackView
    }()
    
    var d1: FlipperView = FlipperView()
    
    var d2: FlipperView = FlipperView()
    
    var h1: FlipperView = FlipperView()
    
    var h2: FlipperView = FlipperView()
    
    var m1: FlipperView = FlipperView()
    
    var m2: FlipperView = FlipperView()
    
    var s1: FlipperView = FlipperView()
    
    var s2: FlipperView = FlipperView()
    
    @objc required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupViews()
        
        timer?.invalidate()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            
            if self?.endTimeMillis ?? 0 <= 0 {
                return
            }
            
            self?.calculateTime()
        }
    }
    
    func setupViews() {
        for v in stackView.subviews {
            v.removeFromSuperview()
        }
        
        d1 = getFlipperView()
        d2 = getFlipperView()
        h1 = getFlipperView()
        h2 = getFlipperView()
        m1 = getFlipperView()
        m2 = getFlipperView()
        s1 = getFlipperView()
        s2 = getFlipperView()
        
        stackView.addArrangedSubview(d1)
        stackView.addArrangedSubview(d2)
        stackView.addArrangedSubview(UIView())
        stackView.addArrangedSubview(h1)
        stackView.addArrangedSubview(h2)
        stackView.addArrangedSubview(UIView())
        stackView.addArrangedSubview(m1)
        stackView.addArrangedSubview(m2)
        stackView.addArrangedSubview(UIView())
        stackView.addArrangedSubview(s1)
        stackView.addArrangedSubview(s2)
        addSubview(stackView)
    }
    
    func calculateTime() {
        let millisec = endTimeMillis - currentTimeMillis()
        var totalsec = millisec / 1000
        
        let day = Int(floor(Double(millisec) / 86400000.0))
        totalsec = totalsec % (3600 * 24)
        
        let hour = Int(totalsec / 3600 )
        totalsec = totalsec % 3600
        
        let min = Int(totalsec / 60)
        totalsec = totalsec % 60
        
        let sec = totalsec
        
        let seconds = String(format: "%02d", sec)
        let minutes = String(format: "%02d", min)
        let hours = String(format: "%02d", hour)
        let days = String(format: "%02d", day)
        
        d1.updateWithText(String(days.getCharAtIndex(0)))
        d2.updateWithText(String(days.getCharAtIndex(1)))
        
        h1.updateWithText(String(hours.getCharAtIndex(0)))
        h2.updateWithText(String(hours.getCharAtIndex(1)))
        
        m1.updateWithText(String(minutes.getCharAtIndex(0)))
        m2.updateWithText(String(minutes.getCharAtIndex(1)))
        
        s1.updateWithText(String(seconds.getCharAtIndex(0)))
        s2.updateWithText(String(seconds.getCharAtIndex(1)))
        
       // print("Event: \(day) \(hour) \(min) \(sec)")
    }
    
    func getFlipperView() -> FlipperView {
        let flipperView = FlipperView()
        flipperView.layer.cornerRadius = 8
        flipperView.clipsToBounds = true
        flipperView.font = flipperView.font.withSize(22)
        flipperView.textColor = UIColor.white
        flipperView.backgroundColor = UIColor.init(named: "ColorPrimary")
        flipperView.textAlignment = .center
        flipperView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        flipperView.text = "0"
        return flipperView
    }
    
    public override func layoutSubviews() {
        stackView.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height)
    }
    
    public func startTimer(endTime: Int64) {
        self.endTimeMillis = endTime
        setupViews()
    }
    
    func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
}
