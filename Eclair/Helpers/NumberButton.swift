//
//  NumberButton.swift
//  Eclair
//
//  Created by iMac on 13/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit

@objc class NumberButton: UIControl {

    public var currentValue: Int = 0 {
        didSet {
            label.text = "\(currentValue)"
            if oldValue != currentValue {
                self.setupViews()
            }
        }
    }

    public var maxValue: Int = 100
    public var minValue: Int = 0
    public var preValue: Int = 0
    public var type: Int = 0

    var addButtonsFont = UIFont(name: "AvenirNext-Medium", size: 13)
    var buttonsFont = UIFont(name: "AvenirNext-Bold", size: 20.0)
    var labelFont = UIFont(name: "AvenirNext-Medium", size: 18.0)
    var buttonTextColor = UIColor.init(named: "ColorPrimary")
    var buttonBgColor = UIColor.white
    var labelTextColor = UIColor.white
    var labelBgColor = UIColor.init(named: "ColorPrimary")
    var buttonBorderColor = UIColor.init(named: "ColorPrimary")

    lazy var leftButton: UIButton = {
        let button = UIButton()
        button.setTitle("-", for: .normal)
        button.setTitleColor(buttonTextColor, for: .normal)
        button.backgroundColor = buttonBgColor
        button.titleLabel?.font = self.buttonsFont
        button.addTarget(self, action: #selector(NumberButton.leftButtonTouchDown), for: .touchDown)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchUpInside)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchUpOutside)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchCancel)
        return button
    }()

    lazy var rightButton: UIButton = {
        let button = UIButton()
        button.setTitle("+", for: .normal)
        button.setTitleColor(buttonTextColor, for: .normal)
        button.backgroundColor = buttonBgColor
        button.titleLabel?.font = self.buttonsFont
        button.addTarget(self, action: #selector(NumberButton.rightButtonTouchDown), for: .touchDown)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchUpInside)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchUpOutside)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchCancel)
        return button
    }()
    
    lazy var addButton: UIButton = {
        let button = UIButton()
        button.setTitle("ADD", for: .normal)
        button.setTitleColor(buttonTextColor, for: .normal)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = self.addButtonsFont
        button.addTarget(self, action: #selector(NumberButton.addButtonTouchDown), for: .touchDown)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchUpInside)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchUpOutside)
        button.addTarget(self, action: #selector(NumberButton.buttonTouchUp), for: .touchCancel)
        return button
    }()

    @objc dynamic lazy var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = self.labelFont
        label.text = "\(currentValue)"
        label.textColor = labelTextColor
        label.backgroundColor = labelBgColor
        label.layer.masksToBounds = true
        return label
    }()

    @objc required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupViews()
    }
    
    func setupViews() {
        for v in subviews {
           v.removeFromSuperview()
        }
        
        if (currentValue <= 0) {
            addSubview(addButton)
        }
        else {
            addSubview(leftButton)
            addSubview(rightButton)
            addSubview(label)
        }
        
        layer.borderWidth = 1
        layer.borderColor = buttonBorderColor!.cgColor

        backgroundColor =  UIColor.white
        layer.cornerRadius = 8.0
        clipsToBounds = true
    }

    public override func layoutSubviews() {
        let buttonWidth = bounds.size.width * 0.32
        let labelWidth = bounds.size.width * 0.36

        addButton.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height)
        
        label.frame = CGRect(x: buttonWidth, y: 0, width: labelWidth, height: bounds.size.height)
        leftButton.frame = CGRect(x: 0, y: 0, width: buttonWidth, height: bounds.size.height)
        rightButton.frame = CGRect(x: labelWidth + buttonWidth, y: 0, width: buttonWidth, height: bounds.size.height)
    }
    
    public func setValue(newValue: Int) {
        currentValue = newValue
        self.setupViews()
        sendActions(for: .valueChanged)
    }
    
    public func setPreValue() {
        currentValue = preValue
        self.setupViews()
    }
}

// MARK: Button Events
extension NumberButton {
    @objc func addButtonTouchDown(button: UIButton) {

        addButton.backgroundColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.0)

        if currentValue < maxValue {
            type = 1
            preValue = currentValue
            var val = currentValue
            val += 1
            setValue(newValue: val)
        }
    }
    
    @objc func leftButtonTouchDown(button: UIButton) {

        leftButton.backgroundColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.0)

        if currentValue > minValue {
            type = -1
            preValue = currentValue
            var val = currentValue
            val -= 1
            setValue(newValue: val)
        }
    }

    @objc func rightButtonTouchDown(button: UIButton) {

        rightButton.backgroundColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.0)

        if currentValue < maxValue {
            type = 1
            preValue = currentValue
            var val = currentValue
            val += 1
            setValue(newValue: val)
        }
    }

    @objc func buttonTouchUp(button: UIButton) {

        UIView.animate(withDuration: TimeInterval(0.2), animations: {
            self.addButton.backgroundColor = self.buttonBgColor
            self.leftButton.backgroundColor = self.buttonBgColor
            self.rightButton.backgroundColor = self.buttonBgColor
            self.setupViews()
        })
    }
}
