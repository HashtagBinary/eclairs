//
//  Animator.swift
//  Eclair
//
//  Created by Zohaib Naseer on 8/12/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation
import UIKit


final class Animator {
    
    private var hasAnimatedAllCells = false
    private let animation: Animation

    init(animation: @escaping Animation) {
        self.animation = animation
    }

    func animate(cell: UITableViewCell, at indexPath: IndexPath, in tableView: UITableView) {
        guard !hasAnimatedAllCells else {
            return
        }

        animation(cell, indexPath, tableView)

        hasAnimatedAllCells = tableView.isLastVisibleCell(at: indexPath)
    }
    //typealias Animation = (UITableViewCell, IndexPath, UITableView) -> Void
}
