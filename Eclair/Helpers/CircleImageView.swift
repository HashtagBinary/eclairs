//
//  CircleImageView.swift
//  Eclair
//
//  Created by Sohail on 05/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

extension UIImageView {
  public func maskCircle() {
    self.layer.masksToBounds = false
    self.layer.cornerRadius = self.frame.height/2
    self.clipsToBounds = true
  }
}
