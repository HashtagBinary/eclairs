//
//  RadioButton.swift
//  Eclair
//
//  Created by Sohail on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class RadioButton: UIButton {
    
    var id: Int = -1
    
    // Images
    let checkedImage = UIImage(named: "icon_radio_checked")! as UIImage
    let uncheckedImage = UIImage(named: "icon_radio_unchecked")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
        
    override func awakeFromNib() {
        self.isChecked = false
    }
    
    func setValue(isChecked: Bool) {
        self.isChecked = isChecked
    }
}
