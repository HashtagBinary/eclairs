

//

import UIKit
import Alamofire
import SwiftyJSON

class AlamorfireSingleton: NSObject {
    
    
    class func postCall(serviceName:String,parameters: [String:Any]?, completionHandler: @escaping (JSON?, NSError?) -> ()) {
        
        AF.request(serviceName ,method: .post,parameters: parameters ).responseJSON { response in
            // debugPrint("Response: \(response)")
            
            switch response.result{
            
            case let .success(value):
                if response.value != nil
                {
                    let json = try? JSON(data: response.data!)
                    
                    completionHandler(json, nil)
                }
            case .failure(_):
                print("failure")
                completionHandler(nil, response.error as NSError?)
                
            }
        }
    }
    
    class func postCallwithHeader(serviceName:String,parameters: [String:Any]?, completionHandler: @escaping (Data?, NSError?) -> ()) {

        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)"]
        AF.request(serviceName ,method: .post,parameters: parameters , encoding: JSONEncoding.default,headers: Auth_header ).validate(contentType: ["application/json"]).responseJSON { response in
            print("error code is \(response.response?.statusCode ?? -1)")
            if let code = response.response?.statusCode
            {
                if code == 401
                {
                    let topview = UIApplication.topViewController()
                    let refreshAlert = UIAlertController(title: "Authorization", message: "Authorization has been denied for this request.", preferredStyle: UIAlertController.Style.alert)

                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                      print("Handle Ok logic here")
                        
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "RootNavigationController") as! UINavigationController
                        UIApplication.shared.keyWindow?.rootViewController = viewController;
                        
                        topview?.dismiss(animated: false, completion: nil)
                      }))
                    topview?.present(refreshAlert, animated: true, completion: nil)
                }
            }
            
            switch response.result {
            case let .success(value):
                //let json = try? JSON(data: response.data!)
                completionHandler(response.data!, nil)
            case let .failure(error):
                print("error code is")
                print(error.responseCode)
                completionHandler(nil, response.error as NSError?)
            }
        }
    }
    
    class func deleteCall(serviceName:String, completionHandler: @escaping (JSON?, NSError?) -> ()) {
        
        AF.request(serviceName ,method: .delete, encoding: JSONEncoding.default).responseJSON { response in
            //debugPrint("Response: \(response)")
            switch response.result {
            case let .success(value):
                let json = try? JSON(data: response.data!)
                completionHandler(json, nil)
            case let .failure(error):
                print(error)
                completionHandler(nil, response.error as NSError?)
            }
        }
    }
    
    class func getCall(serviceName:String,parameters: [String:Any]?, completionHandler: @escaping (Data?, NSError?) -> ()) {
        
        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        //let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)"]
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)"]
        
        AF.request(serviceName,method: .get,parameters: parameters,headers: Auth_header).responseJSON { response in
            //debugPrint("Response: \(response)")
            print("error code is \(response.response?.statusCode ?? -1)")
            if let code = response.response?.statusCode
            {
                if code == 401
                {
                    let topview = UIApplication.topViewController()
                    let refreshAlert = UIAlertController(title: "Authorization", message: "Authorization has been denied for this request.", preferredStyle: UIAlertController.Style.alert)

                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                      print("Handle Ok logic here")
                        
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "RootNavigationController") as! UINavigationController
                        UIApplication.shared.keyWindow?.rootViewController = viewController;
                        
                        topview?.dismiss(animated: false, completion: nil)
                      }))

                    topview?.present(refreshAlert, animated: true, completion: nil)
                }
            }
            
            switch response.result {
            case let .success(value):
                let json = try? JSON(data: response.data!)
                completionHandler(response.data!, nil)
            case let .failure(error):
                print(error)
                completionHandler(nil, response.error as NSError?)
            }
        }
    }
    
    class func getCallwithoutParameters(serviceName:String, completionHandler: @escaping (Data?, NSError?) -> ()) {
        
        
        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)"]
        AF.request(serviceName ,method: .get,parameters: nil ,headers: Auth_header).responseJSON { response in
            print("error code is \(response.response?.statusCode ?? -1)")
            if let code = response.response?.statusCode
            {
                if code == 401
                {
                    let topview = UIApplication.topViewController()
                    let refreshAlert = UIAlertController(title: "Authorization", message: "Authorization has been denied for this request.", preferredStyle: UIAlertController.Style.alert)

                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                      print("Handle Ok logic here")
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "RootNavigationController") as! UINavigationController
                        UIApplication.shared.keyWindow?.rootViewController = viewController;
                        topview?.dismiss(animated: false, completion: nil)
                      }))
                    topview?.present(refreshAlert, animated: true, completion: nil)
                }
            }
            switch response.result {
            case let .success(value):
                let json = try? JSON(data: response.data!)
                completionHandler(response.data!,nil)
            case let .failure(error):
                print("error code is \(error.responseCode)")
                completionHandler(nil,response.error as? NSError)
                print("status code working \(response.response?.statusCode)")
                print(error)
            }
        }
    }
    
    class func requestWithHeader(serviceName:String,parameters: [String:Any]?, method: HTTPMethod, completionHandler: @escaping (Data?, NSError?) -> ()) {
        
        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)"]
        //let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(token)"]
        AF.request(serviceName ,method: method, parameters: parameters , encoding: JSONEncoding.default,headers: Auth_header ).validate(contentType: ["application/json"]).responseJSON { response in
            //debugPrint("Response: \(response)")
            print("error code is \(response.response?.statusCode ?? -1)")
            if let code = response.response?.statusCode
            {
                if code == 401
                {
                    let topview = UIApplication.topViewController()
                    let refreshAlert = UIAlertController(title: "Authorization", message: "Authorization has been denied for this request.", preferredStyle: UIAlertController.Style.alert)

                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                      print("Handle Ok logic here")
                        
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "RootNavigationController") as! UINavigationController
                        UIApplication.shared.keyWindow?.rootViewController = viewController;
                        
                        topview?.dismiss(animated: false, completion: nil)
                      }))

                    topview?.present(refreshAlert, animated: true, completion: nil)
                }
            }
            switch response.result {
            case let .success(value):
                let json = try? JSON(data: response.data!)
                print(json)
                completionHandler(response.data!, nil)
            case let .failure(error):
                print(error)
                completionHandler(nil, response.error as NSError?)
            }
        }
    }
    
}
