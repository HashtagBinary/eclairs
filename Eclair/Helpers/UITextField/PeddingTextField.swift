//
//  PeddingTextField.swift
//  AgorzCustomer
//
//  Created by Developer on 15/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class PeddingTextField: UITextField {

    override func draw(_ rect: CGRect) {
           self.borderWidth = 1.0
           self.borderColor = Utilities.UIColorFromHex(hex: "#b2b2b2")
           self.cornerRadius = 5.0
           self.paddingLeftCustom = 30
           self.paddingRightCustom = 30
          // self.font = Fonts.FontRobotoSlabRegularWithSize(size: Constants.FontSize.kRegular15)
       }
}
