//
//  BorderTextFieldSmallFont.swift
//  AgorzCustomer
//
//  Created by Developer on 23/07/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class BorderTextFieldSmallFont: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func draw(_ rect: CGRect) {
        self.borderWidth = 1.0
        self.borderColor = Utilities.UIColorFromHex(hex: "#b2b2b2")
        self.cornerRadius = 5.0
        self.paddingLeftCustom = 2
        self.paddingRightCustom = 10
        
    }
}
