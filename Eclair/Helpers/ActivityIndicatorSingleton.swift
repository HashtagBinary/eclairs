
//

import UIKit
import NVActivityIndicatorView

class ActivityIndicatorSingleton: NSObject {

    static var activiity  =   NVActivityIndicatorView.init(frame: CGRect.init(x: 1, y: 1, width: 1, height: 2))
    class func ActivityViewSetup(myView: UIView , mycolor:UIColor)
    {
        /*let frame = CGRect(x: (myView.frame.size.width/2)-150, y: (myView.frame.height/2)-50, width: 300, height: 100)
        activiity  =   NVActivityIndicatorView(frame:frame ,type: NVActivityIndicatorType.ballClipRotate, color: mycolor, padding: NVActivityIndicatorView.DEFAULT_PADDING)*/
    }
    class func StartActivity(myView:UIView)
    {
        /*myView.addSubview(activiity)
        activiity.startAnimating()*/
        NVActivityIndicatorView.DEFAULT_TYPE = .lineSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData,NVActivityIndicatorView.DEFAULT_FADE_IN_ANIMATION)
    }
    class func StartActivityWithDiffColor(myView:UIView,color:UIColor)
    {
        /*activiity.color = color
        myView.addSubview(activiity)*/
        activiity.startAnimating()
    }
    class func StopActivity(myView:UIView)
    {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(NVActivityIndicatorView.DEFAULT_FADE_OUT_ANIMATION)
    }
}
