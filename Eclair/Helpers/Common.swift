//
//  Common.swift
//
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension UIViewController{
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
//    func CalculateDistanceWithGoogleApi(SourceLat:Double,SourceLong:Double,DestinationLat:Double,DestinationLong:Double) -> String
//    {
//        let origin = "\(SourceLat),\(SourceLong)"
//        let destination = "\(DestinationLat),\(DestinationLong)"
//        var distance = ""
//        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=AIzaSyBZb0XTP9n9WFVvdOrVbpn_uSw3eCPQLGo"
//        
//        Alamofire.request(url).responseJSON { response in
//            
//            let json = try? JSON(data: response.data!)
//            
//          print("Result Value is \(json)")
//            let status = json!["status"].stringValue
//            
//            distance = "\(json!["rows"][0]["elements"][0]["distance"]["text"])"
//            print("distance \(distance)")
//            
//        }
//        return distance
//    }
    
    
    
    
    // for convert UTC to local
    
    
    func dateTimeStatus(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let dateval = dateFormatter.date(from: date)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "MM/dd/yyyy -  h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: dateval ?? Date())
            
            return timeStamp
        
        
    
    }
    
   func getMiles(i : Double) -> Double {
           return i*0.000621371192;
      }
    
    
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
}

extension NSMutableAttributedString
{
    func prepare(_ text:String ,color:UIColor, font:UIFont,underLine_Style:Int) -> NSMutableAttributedString
    {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        paragraphStyle.lineSpacing = 0
        
        let attrs = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.paragraphStyle : paragraphStyle,
            NSAttributedString.Key.foregroundColor : color,
            NSAttributedString.Key.underlineStyle : underLine_Style
            ] as [NSAttributedString.Key : Any]
        
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    
    func prepareLink(_ text:String, url : String ,color:UIColor, font:UIFont,underLine_Style:Int) -> NSMutableAttributedString
    {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        paragraphStyle.lineSpacing = 0
        
        let attrs = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.paragraphStyle : paragraphStyle,
            NSAttributedString.Key.foregroundColor : color,
            NSAttributedString.Key.underlineStyle : underLine_Style,
            NSAttributedString.Key.link : url
            ] as [NSAttributedString.Key : Any]
        
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    func prepareCenter(_ text:String ,color:UIColor, font:UIFont, lineSpacing : CGFloat = 15) -> NSMutableAttributedString
    {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        paragraphStyle.lineSpacing = lineSpacing
        
        let attrs = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.paragraphStyle : paragraphStyle,
            NSAttributedString.Key.foregroundColor : color,
            ]
        
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    func prepareLineSpacing(_ text:String ,color:UIColor, font:UIFont, lineSpacing : CGFloat = 15) -> NSMutableAttributedString
    {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        paragraphStyle.lineSpacing = lineSpacing
        
        let attrs = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.paragraphStyle : paragraphStyle,
            NSAttributedString.Key.foregroundColor : color,
            ]
        
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
}
