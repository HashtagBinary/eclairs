//
//  Constants.swift

//

import UIKit
let kAppDelegate                =   UIApplication.shared.delegate as! AppDelegate
let kRootViewController         =   kAppDelegate.window?.rootViewController
let kMainStoryBoard             =   UIStoryboard(name: "Main", bundle: nil)


//"AIzaSyChk-gbHMnRypBwQHF-2uaEzooTbiLRxoU"

struct ServiceTax_Fees {
    static let ResturantFees             =   2.0
    static let DeliveryFees             =   10.0
}

struct PopupValue {
    static let PopupRadiusvalue             =   25.0
}
// MARK:- Screen Size
struct ScreenSize {
    static let SCREEN_WIDTH             =   UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT            =   UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH        =   max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH        =   min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

// MARK:- Device Type
struct DeviceType {
    static let IS_IPHONE_4_OR_LESS      =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5              =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_8              =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_8P             =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X              =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD                  =   UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO              =   UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    static let IS_IPADDEVICE                  = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE                = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA                = UIScreen.main.scale >= 2.0
}
struct comingFrom {
    static let kFromRegisterVC              = "from_signUp_vc"
    static let kFromLoginVC                 = "from_login_vc"
}
struct paymentType {
    static let kStripe               = "Stripe"
    static let kCash                 = "Cash"
    static let kWallet               = "Wallet"
    static let kCheckOut             = "Checkout"
   
}
struct paymentTypeCode {
    static let kStripe               = "2"
    static let kCash                 = "4"
    static let kWallet               = "3"
    static let kCheckOut             = "5"
}
struct paymentStatus {
    static let kCompleted               = "Completed"
    static let kPending                 = "Pending"
}


class Constants: UIViewController {
    
    
    //
    
    static var ecode = ""
    static let CURRENCY_SYMBOL = "SR"
    static let DateFormate = "MMM dd, yyyy hh:mm a"
    static let DateFormateWithoutTime = "MMM dd, yyyy"
    static var ahmsAgreement = false
    
    static let CHECKBOX = 0
    static let COUNTER = 1
    static let CHOICE = 2
    
    static var guestSlots = 0
    static var tenantsSlots = 0
    
    static let kEventEcodePickerNote = "Note: Please enter all ecode else only entered ecode will be registered."
    static let kEventVatLabel = "VAT"
    static let kEventVatPercentage: Float = 15
    
    static let kVoucherCartNote = "Note: Voucher note here."
    static let kVoucherVatLabel = "VAT"
    static let kVoucherVatPercentage: Float = 15
    
    static let kSpaProductCartNote = "Note: Spa product note here."
    static let kSpaProductVatLabel = "VAT"
    static var kSpaProductVatPercentage: Float = 15
    
    static let kAddressNote = "Note: Please take your order from the cashier counter."
    static let kFnBVatLabel = "VAT"
    static var kFnBVatPercentage: Float = 15
    static let kFnBCartNote = "Note: FnB note here."
    static var kIsMainMember = false
    static var kNavTitle = ""
    static var kAccountType = ""
    static var kMemberType = ""
    static let kAppStoreLink = "https://apps.apple.com/us/app/indigobhur/id1548691405"
    static let kSupportFirebaseLink = "https://foodnbeverages-c1c44.firebaseio.com/chat"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    struct GlobalConstants {
         static let APP_NAME:String = "Indigobhur"
         //static  let baseURL = ""
         static  let baseURL = ""
         
    }
    struct FontSize {
        static let kSmall10 : CGFloat = UIDevice().userInterfaceIdiom == .phone ? 10.0 : 10.0
        static let kSmall11 : CGFloat = UIDevice().userInterfaceIdiom == .phone ? 11.0 : 11.0
        static let kSmall12 : CGFloat = UIDevice().userInterfaceIdiom == .phone ? 12.0 : 12.0
        static let kSmall13 : CGFloat = UIDevice().userInterfaceIdiom == .phone ? 13.0 : 13.0
        static let kRegular15 : CGFloat = UIDevice().userInterfaceIdiom == .phone ? 15.0 : 15.0
        static let kRegular17 : CGFloat = UIDevice().userInterfaceIdiom == .phone ? 17.0 : 17.0
        static let kRegular22 : CGFloat = UIDevice().userInterfaceIdiom == .phone ? 22.0 : 22.0
    }
    // MARK:- API Constant
    struct ServerAPI {
        
        // Base URL & Image Upload URL
        //static let FS_FNB_URL = "FNBORDERS"
        static let FS_FNB_URL = "DEMO/APP/FNBORDERS"
        //static let FS_QRCODES_URL = "QRCODES"
        static let FS_QRCODES_URL = "DEMO/APP/QRCODES"
        static let FS_NOTIFICATIONS_URL = "NOTIFICATIONS"
        //static let FS_SPA_REQUESTS_URL = "SPA_REQUESTS"
        static let FS_SPA_REQUESTS_URL = "DEMO/APP/SPA_REQUESTS"
        
        
        static let kBaseURL                       =   "https://api.devhashtag.com/"
        //static let kBaseURL                     =   "https://api.indigobhur.com/"
        
        static let kBaseURLAMS: String       =        "http://ahms.devhashtag.com/"
        //static let kBaseURLAMS: String     =        "https://ams.indigobhur.com/"
        
        
        
        //MARK:- setting for development servers and live servers
        
        
        
        
        
        
        static let kSignIn                        =   kBaseURL + "token"
        static let kChangePassword                =   kBaseURL + "api/profile/changepassword"
        static let kProfileData                   =   kBaseURL + "api/profile/getprofile"
        static let kProfileImage                  =   kBaseURL + "api/profile/getimage"
        static let kProfileChangePic              =   kBaseURL + "api/profile/changepic"
        static let kProfileRemovePic              =   kBaseURL + "api/profile/removepic"
        static let kGetCheckinRatioByMonth        =  kBaseURL + "api/profile/GetTMCheckinRatioByMonth?ecode="
        static let kGetCheckinRatio               =   kBaseURL + "api/profile/GetTMCheckinRatio?ecode="
        //
        
        
        
        
        static let kSpaGetOrderID                   =   kBaseURL + "api/spa/getorderid"
        static let kSpaServices                   =   kBaseURL + "api/spa/getservices"
        static let kSpaGetservicesbyid           =   kBaseURL + "api/spa/getservicesbyid?id="
        static let kSpaGetSlots                   =   kBaseURL + "api/spa/getslots?date="
        static let kSpaGetCoupon                  =   kBaseURL + "api/spa/getcoupon"
        static let kSpaGetHistory                  =   kBaseURL + "api/spa/getreq"
        static let kSpaSaveRequest                   =   kBaseURL + "api/spa/savereq"
        static let kSpaProducts                   =   kBaseURL + "api/spa/getproducts"
        static let kSpaProductSave                =   kBaseURL + "api/spa/saveproduct"
        static let kSunbedGetAllByUser            =   kBaseURL + "api/sunbed/getallbyuser"
        
        static let kEvent = kBaseURL + "api/event/getevent"
        static let kEventSave = kBaseURL + "api/event/save"
        static let kVerifyEcode = kBaseURL + "api/profile/checkinfo"
        
        static let kGetVouchers = kBaseURL + "api/spa/getvoucher"
        static let kVoucherSave = kBaseURL + "api/spa/savevoucher"
        
        static let kGetlevels = kBaseURL + "api/anr/seasonapi/getseasonbyecode"
        static let kGetTokens = kBaseURL + "api/anr/MarketPlaceApi/GetTokensByEcode"
        static let kGetTokensforfilter = kBaseURL + "api/anr/MarketPlaceApi/GetTokens"
        static let kSignAgreement = kBaseURL + "api/anr/MarketPlaceApi/AhmsAgreement"
        
        
        //
        static let kAddressTables = kBaseURL + "api/fnb/tablesapi/gettablesbydepartmentid"
        static let kGetFnBItems = kBaseURL + "api/fnb/OrdersApi/GetTrends"
        static let kGetFnBMenuItems = kBaseURL + "api/fnb/DepartmentsApi/GetDepartments"
        static let kGetFnBCategoriesById = kBaseURL + "api/fnb/menucategoriesapi/getcategorybyid"
        static let kGetFnBItemsById = kBaseURL + "api/fnb/menuitemsapi/getmenuitemsbyid"
        static let kGetFnBJuiceIngredients = kBaseURL + "api/fnb/juiceapi/GetJuiceIngredients"
        static let kFnBGetNewOrderId = kBaseURL + "api/Checkout/PostCheckout"
        static let kFnBVerifyVoucher = kBaseURL + "api/FOGate/CheckVoucher"
        static let kFnBGetClosedOrders = kBaseURL + "api/fnb/ordersapi/GetOrderHistoryByEcode"
        static let kFnBGetGlasses = kBaseURL + "api/fnb/JuiceApi/GetJuiceGlass"
        static let kFnBRemoveGlass = kBaseURL + "api/fnb/JuiceApi/RemoveCustomGlass"
        static let kFnBSaveGlass = kBaseURL + "api/fnb/JuiceApi/PostCustomGlass"
        static let kFnBGetVoucherCode = kBaseURL + "api/fnb/ordersapi/fnbcheckinfo"
        
    
        //fnb/ordersapi/fnbcheckinfo
        //
        
        //MARK:- AHMS APIs
        
        static let kGetAuctions = kBaseURLAMS + "api/auctionsapi?ecode="
        static let kGetActiveAuctions = kBaseURLAMS + "api/GetAuctionsByEcode?skip=0&take=10&ecode="
        static let kGetSoldAuctions = kBaseURLAMS + "api/GetSoldAuctions?skip=0&take=10&ecode="
        static let kGetUnSoldAuctions = kBaseURLAMS + "api/GetUnsoldAuctions?skip=0&take=10&ecode="
        static let kGetActiveBids = kBaseURLAMS + "api/GetActiveBids?ecode="
        static let kGetDashboard = kBaseURLAMS + "api/GetDashboardByEcode?ecode="
        static let kAddAuctions = kBaseURLAMS + "api/auctionsapi"
        static let kgetBids = kBaseURLAMS + "api/BidsAPI?id="
        static let kBuyOut = kBaseURLAMS + "api/BuyOut"
        static let kCancelAuction = kBaseURLAMS + "api/CancelAuction"
        static let kPlaceBid = kBaseURLAMS + "api/BidsApi"
        static let kgetTimeList = kBaseURLAMS + "api/GetTimeList"
        static let kReAuction = kBaseURLAMS + "api/RePostAuctions"
        static let kDeleteAuctionById = kBaseURLAMS + "api/DeleteAuctionByAuctionId"
        
        //MARK:- Title API
        static let kGetTitlesList = kBaseURL + "api/anr/ANRProfileApi/GetTitlesByEcode"
        static let kSetCurrenttitle = kBaseURL + "api/anr/ANRProfileApi/SetCurrentTitleByEcode"
        static let kGetLeaderBoardList = kBaseURL + "api/anr/ANRProfileApi/GetLeaderboard"
        
        
        
        // Customer
        
        static let ksaveRequestForm = kBaseURL + "api/request/saverequest"
        static let ksaveRe_InvitationForm = kBaseURL + "api/request/saveri"
        static let kGetMaintananceFormItems = kBaseURL + "api/maintainance/getItems"
        static let kGetMaintananceHistory = kBaseURL + "api/maintainance/getrequest"
        static let ksaveMaintananceForm = kBaseURL + "api/maintainance/saverequest"
        static let kGetGuestList = kBaseURL + "api/request/getguestlist?id="
        static let kGetTenantsList = kBaseURL + "api/request/getsublist?id="
        static let ksaveGuestRequestForm = kBaseURL + "api/request/saveguestreq"
        static let ksaveTMRequestForm = kBaseURL + "api/request/savetmreq"
        static let ksaveLostNFound = kBaseURL + "api/FOGate/PostComplain"
        static let kHistoryLostNFound = kBaseURL + "api/FOGate/GetComplainsByEcode"
        static let kHistoryReInvitation = kBaseURL + "api/request/getri?id="
        static let kHistoryGuest = kBaseURL + "api/request/getguestreq?id="
        static let kHistoryT_M = kBaseURL + "api/request/gettmreq?id="
        //
        
        
        //MARK:- xpBoost APIs
        
        static let kGetXpBoost = kBaseURL + "api/anr/MarketPlaceApi/GetXpBoost"
        static let kGetQuestsByEcode = kBaseURL + "api/anr/SeasonApi/GetQuestsByEcode"
        static let kBuyXPByEcode = kBaseURL + "api/anr/MarketPlaceApi/Purchase"
        
        static let kGetWalletDashboard = kBaseURL + "api/ewallet/dashboarddata"
        static let kGetWalletHistory = kBaseURL + "api/ewallet/getlist"
        static let kWalletTransferCoin = kBaseURL + "api/ewallet/transferfund"
        static let kWalletFilterHistory = kBaseURL + "api/ewallet/getTransactionHistory"
        static let kWalletGenrateOTP = kBaseURL + "api/ewallet/generateotp"
        static let kWalletVerifyOTP = kBaseURL + "api/ewallet/verifyotp"
        //
    }
    
    // MARK:- Pop Up Error Message
    struct ErrorMessage {
        static let kTitle                       = "Error!"
        static let kNoInternetConnectionMessage = "Could not connect to the internet. Please check your network.\nYou must connect to Wi-Fi or a Cellular Network to get online again"
        static let kCommanErrorMessage          = "Please try again later...!!!"
        static let kServerErrorMessage          = "Internal server error"
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
// MARK:- Structure for User Defaults Key
struct UDKey {
    static let kUserInfo                 =   "userInfo"
    static let kLatitude                 =   "latitude"
   
}
