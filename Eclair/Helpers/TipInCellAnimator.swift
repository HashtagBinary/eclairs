//
//  TipInCellAnimator.swift
//  AgorzCustomer
//
//  Created by admin on 3/1/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class TipInCellAnimator {
    // placeholder for things to come -- only fades in for now
    class func animate(cell:UITableViewCell) {
        let view = cell.contentView
        view.layer.opacity = 0.1
        UIView.animate(withDuration: 2.0) {
            view.layer.opacity = 1
        }
    }
}
