//
//  JuiceGlass.swift
//  Eclair IOS
//
//  Created by Sohail on 09/11/2020.
//

import UIKit

class JuiceGlass: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var sugarImg: UIImageView!
    @IBOutlet weak var iceImg: UIImageView!
    @IBOutlet weak var flavorContainerStackView: UIStackView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
//    func commonInit() {
//        Bundle.main.loadNibNamed("JuiceGlass", owner: self, options: nil)
//        contentView.fixInView(self)
//        flavorContainerStackView.clipsToBounds = true
//        flavorContainerStackView.translatesAutoresizingMaskIntoConstraints = false
//
//        //Text Label
//        let view1 = UIView()
//        view1.backgroundColor = UIColor.yellow
//        view1.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
//
//        let view2 = UIView()
//        view2.backgroundColor = UIColor.red
//        view2.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
//
//        let view3 = UIView()
//        view3.backgroundColor = UIColor.blue
//        view3.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
//
//        flavorContainerStackView.addArrangedSubview(view1)
//        view1.heightAnchor.constraint(equalTo: self.flavorContainerStackView.heightAnchor, multiplier: 0.2).isActive = true
//
//        flavorContainerStackView.addArrangedSubview(view2)
//        view2.heightAnchor.constraint(equalTo: self.flavorContainerStackView.heightAnchor, multiplier: 0.2).isActive = true
//
//        flavorContainerStackView.addArrangedSubview(view3)
//        view3.heightAnchor.constraint(equalTo: self.flavorContainerStackView.heightAnchor, multiplier: 0.2).isActive = true
//
//        Timer.scheduledTimer(timeInterval: 2,
//            target: self,
//            selector: #selector(self.adjustmentBestSongBpmHeartRate(_:)),
//            userInfo: nil,
//            repeats: false)
//
//
//    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("JuiceGlass", owner: self, options: nil)
        contentView.fixInView(self)
        flavorContainerStackView.clipsToBounds = true
        flavorContainerStackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func UpdateFlavors() {
        
        flavorContainerStackView.removeAllArrangedSubviews()
        let equalWeight: Double = 1 / Double(juiceSelectedFlavours.count)
        var arrayPerc = [Double]()
        var remainingPerc: Double = 0
        juiceSelectedFlavours.forEach { (flavor) in
            var perc = (equalWeight * flavor.percentage!) / 100
            if (perc > equalWeight) {
                perc = equalWeight
            }
            let remaining = equalWeight - perc
            arrayPerc.append(perc)
            remainingPerc += remaining
        }
        
        let remainingWeight = remainingPerc / Double(juiceSelectedFlavours.count)
        for (index, flavor) in juiceSelectedFlavours.enumerated() {
            let view1 = UIView()
            view1.backgroundColor = Constants.hexStringToUIColor(hex: flavor.colorCode ?? "FFFFFF")
            view1.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
            flavorContainerStackView.addArrangedSubview(view1)
            view1.heightAnchor.constraint(equalTo: self.flavorContainerStackView.heightAnchor, multiplier: CGFloat(arrayPerc[index] + remainingWeight)).isActive = true
        }
    }
    
//    @objc func adjustmentBestSongBpmHeartRate(_ timer: Timer) {
////        let view = self.flavorContainerStackView.arrangedSubviews[2]
////
////        UIView.animate(
////            withDuration: 0.5,
////            delay: 0.0,
////            options: [.curveLinear],
////            animations: {
////                view.isHidden = true
////        })
//
//        let view3 = UIView()
//        view3.backgroundColor = UIColor.purple
//        view3.isHidden = true
//        self.flavorContainerStackView.insertArrangedSubview(view3, at: 0)
//        view3.heightAnchor.constraint(equalTo: self.flavorContainerStackView.heightAnchor, multiplier: 0.5).isActive = true
//
//        UIView.animate(
//            withDuration: 0.5,
//            delay: 0.0,
//            options: [.curveLinear],
//            animations: {
//                view3.isHidden = false
//        })
//     }
}
