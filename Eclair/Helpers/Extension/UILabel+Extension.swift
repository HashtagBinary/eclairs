//
//  UILabel+Extension.swift
//  AgorzCustomer
//
//  Created by admin on 6/11/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Foundation


extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: rect.inset(by: insets))
        } else {
            self.drawText(in: rect)
        }
    }
    
    func MakeTextBlur()
    {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.bounds
        self.addSubview(blurredEffectView)
    }
    func RemoveBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
      
        blurredEffectView.removeFromSuperview()
    }
    

    func startBlink() {
        UIView.animate(withDuration: 0.6,
                       delay:0.0,
                      options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                      animations: { self.alpha = 0
                    
                      },
                      completion: nil)
    }

    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
    
}
