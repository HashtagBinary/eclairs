//
//  TextField+Extension.swift
//  AgorzCustomer
//
//  Created by admin on 6/11/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Foundation

extension UITextField {
    
    
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.black.cgColor
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)
        
    }
    var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }
    
    var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
    
    
    func setLeftView(image: UIImage) {
      let iconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25)) // set your Own size
      iconView.image = image
      let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 45))
      iconContainerView.addSubview(iconView)
      leftView = iconContainerView
      leftViewMode = .always
      self.tintColor = .lightGray
    }
    
    
    func setRightView(image: UIImage) {
      let iconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25)) // set your Own size
      iconView.image = image
      let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 45))
      iconContainerView.addSubview(iconView)
      rightView = iconContainerView
      rightViewMode = .always
      self.tintColor = .lightGray
    }
    
}

