//
//  UITableview+Extension.swift
//  AgorzCustomer
//
//  Created by admin on 7/22/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

extension UITableViewCell {
    func applyConfig(for indexPath: IndexPath, numberOfCellsInSection: Int) {
        switch indexPath.row {
        case numberOfCellsInSection - 1:
            // This is the case when the cell is last in the section,
            // so we round to bottom corners
            self.clipsToBounds = true
            self.layer.cornerRadius = 15
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            
            
            self.layer.shadowOpacity = 5
            self.layer.shadowRadius = 5
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 3, height: 3)
            
            
            
            // However, if it's the only one, round all four
            if numberOfCellsInSection == 1 {
                
                
                self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
                self.clipsToBounds = true
                self.layer.cornerRadius = 15
                self.layer.shadowOpacity = 5
                self.layer.shadowRadius = 5
                self.layer.shadowColor = UIColor.black.cgColor
                self.layer.shadowOffset = CGSize(width: 3, height: 3)
                
            }
            
        case 0:
            // If the cell is first in the section, round the top corners
            self.clipsToBounds = true
            self.layer.cornerRadius = 15
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            self.layer.shadowOpacity = 5
            self.layer.shadowRadius = 5
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 3, height: 3)
            
            //
            
        default:
            // If it's somewhere in the middle, round no corners
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
            self.clipsToBounds = true
            self.layer.cornerRadius = 0
        }
    }
}
extension UITableView {

    func reloadWithAnimation() {
        self.reloadData()
        let tableViewHeight = self.bounds.size.height
        let cells = self.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.6, delay: 0.08 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else {
            return false
        }

        return lastIndexPath == indexPath
    }
}
