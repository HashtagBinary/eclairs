//
//  JuiceGlassSmall.swift
//  Eclair
//
//  Created by Sohail on 16/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class JuiceGlassSmall: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var flavorContainerStackView: UIStackView!
    
    var juiceFlavors = [FnBJuiceFlavor]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("JuiceGlassSmall", owner: self, options: nil)
        contentView.fixInView(self)
        flavorContainerStackView.clipsToBounds = true
        flavorContainerStackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func UpdateFlavors() {
        
        flavorContainerStackView.removeAllArrangedSubviews()
        
        let equalWeight: Double = 1 / Double(juiceFlavors.count)
        
        var arrayPerc = [Double]()
        var remainingPerc: Double = 0
        
        juiceFlavors.forEach { (flavor) in
            var perc = (equalWeight * flavor.percentage!) / 100
            if (perc > equalWeight) {
                perc = equalWeight
            }
            
            let remaining = equalWeight - perc
            
            arrayPerc.append(perc)
            remainingPerc += remaining
        }
        
        let remainingWeight = remainingPerc / Double(juiceFlavors.count)
        
        for (index, flavor) in juiceFlavors.enumerated() {
            let view1 = UIView()
            view1.backgroundColor = Constants.hexStringToUIColor(hex: flavor.colorCode ?? "FFFFFF")
            view1.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
            
            flavorContainerStackView.addArrangedSubview(view1)
            view1.heightAnchor.constraint(equalTo: self.flavorContainerStackView.heightAnchor, multiplier: CGFloat(arrayPerc[index] + remainingWeight)).isActive = true
        }
    }
}
