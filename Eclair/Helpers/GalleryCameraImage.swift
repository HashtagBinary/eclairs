//
//  GalleryCameraImage.swift
//  Triarch LLC
//  Created by Ankita Thakur on 13/02/18.
//  Copyright © 2018 Ankita Thakur. All rights reserved.

import UIKit
import Foundation
protocol passImageDelegate {
    func passSelectedimage(selectImage: UIImage)
}
var galleryCameraImageObj:passImageDelegate?

class GalleryCameraImage: NSObject,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK:- variable Decleration
    var ImagePicker = UIImagePickerController()
    var imageTapped = Int()
    var clickImage = UIImage()
    //Mark:- Choose Image Method
    func customActionSheet() {
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        myActionSheet.view.tintColor = UIColor.black
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
        })
        let cmaeraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        myActionSheet.addAction(galleryAction)
        myActionSheet.addAction(cmaeraAction)
        myActionSheet.addAction(cancelAction)
        
        UIApplication.topViewController()!.present(myActionSheet, animated: true, completion: nil)
    }
    //MARK:- Open Image Camera
    func openCamera() {
        DispatchQueue.main.async {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
                self.ImagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.ImagePicker.delegate = self
                //self.ImagePicker.allowsEditing = false
                UIApplication.topViewController()!.present(self.ImagePicker, animated: true, completion: nil)
                
            } else {
                let alert = UIAlertController(title: "Alert", message: "Camera is not supported", preferredStyle: UIAlertController.Style.alert)
                let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(okAction)
                UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
            }
        }
    }
   //MARK:- Open Image Gallery
    func openGallary() {
        ImagePicker.delegate = self
        //ImagePicker.allowsEditing = false
        ImagePicker.sourceType = .photoLibrary
        UIApplication.topViewController()!.present(ImagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if(picker.sourceType == UIImagePickerController.SourceType.camera) {
                if let objImagePick = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.setSelectedimage(objImagePick)
                }
             }else{
                if let objImagePick = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.setSelectedimage(objImagePick)
                }
            }
        }
    }

    //MARK:- Selectedimage
    func setSelectedimage(_ image: UIImage) {
        clickImage = image
        galleryCameraImageObj?.passSelectedimage(selectImage: clickImage)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        UIApplication.topViewController()!.dismiss(animated: true, completion: nil)
    }
}
extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in:CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage;
    }
    /// Fix image orientaton to protrait up
    func fixedOrientation() -> UIImage? {
        guard imageOrientation != UIImage.Orientation.up else {
                //This is default orientation, don't need to do anything
                return self.copy() as? UIImage
            }
            
            guard let cgImage = self.cgImage else {
                //CGImage is not available
                return nil
            }

            guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
                return nil //Not able to create CGContext
            }
            
            var transform: CGAffineTransform = CGAffineTransform.identity
            
            switch imageOrientation {
            case .down, .downMirrored:
                transform = transform.translatedBy(x: size.width, y: size.height)
                transform = transform.rotated(by: CGFloat.pi)
                break
            case .left, .leftMirrored:
                transform = transform.translatedBy(x: size.width, y: 0)
                transform = transform.rotated(by: CGFloat.pi / 2.0)
                break
            case .right, .rightMirrored:
                transform = transform.translatedBy(x: 0, y: size.height)
                transform = transform.rotated(by: CGFloat.pi / -2.0)
                break
            case .up, .upMirrored:
                break
            }
            
            //Flip image one more time if needed to, this is to prevent flipped image
            switch imageOrientation {
            case .upMirrored, .downMirrored:
                transform.translatedBy(x: size.width, y: 0)
                transform.scaledBy(x: -1, y: 1)
                break
            case .leftMirrored, .rightMirrored:
                transform.translatedBy(x: size.height, y: 0)
                transform.scaledBy(x: -1, y: 1)
            case .up, .down, .left, .right:
                break
            }
            
            ctx.concatenate(transform)
            
            switch imageOrientation {
            case .left, .leftMirrored, .right, .rightMirrored:
                ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            default:
                ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                break
            }
            
            guard let newCGImage = ctx.makeImage() else { return nil }
            return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
        }
}
