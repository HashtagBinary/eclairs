//
//  settings.swift
//  Eclair
//
//  Created by Indigo on 14/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class settings: UIViewController {

    @IBOutlet weak var changeVw: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        changeVw.addGestureRecognizer(tap)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.performSegue(withIdentifier: "change", sender: self)
    }
}
