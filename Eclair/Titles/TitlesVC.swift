//
//  TitlesVC.swift
//  Eclair
//
//  Created by Zohaib Naseer on 8/12/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON

class TitlesVC: UIViewController {

    @IBOutlet weak var tbleVw: UITableView!
    var titleList = [AllTitlesList]()
    var currentTitle = ""
    var acheivedTitleId = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewWillAppear(_ animated: Bool) {
        getTitlesList()
    }
    func getTitlesList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kGetTitlesList, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json of crystal history \(json)")
            do{
                let model = try JSONDecoder().decode(TitleModel.self, from: data!)
                if model.response == false
                {
                    Utilities.showAlert("", message: model.error ?? "")
                    return
                }
                else
                {
                    self.currentTitle = model.currentTitle ?? ""
                    self.titleList = model.allTitlesList ?? []
                    self.tbleVw.reloadData()
                    if model.titlesList?.isEmpty == false
                    {
                        let achivedTitles = model.titlesList?.sorted(by: { $0.titleId ?? -1 > $1.titleId ?? -1 })
                        self.acheivedTitleId = achivedTitles?[0].titleId ?? -1
                    }
                }
            }
            catch{
                print("error is \(error)")
                Utilities.showAlert("Error", message: error.localizedDescription)
            }
        }
    }
    
    func setCurrentTitle(selectedId:Int) {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
            "titleId" : selectedId
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kSetCurrenttitle, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json of crystal history \(json)")
            let response = json?["response"].bool ?? false
            if response == false
            {
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert("", message: json?["error"].string ?? "Server Error")
                return
            }
            else
            {
                self.getTitlesList()
            }
        }
    }
}
extension TitlesVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! titleCell
        cell.titlelbl.text = titleList[indexPath.row].title ?? ""
        
        if acheivedTitleId < titleList[indexPath.row].titleId ?? -1
        {
            cell.containerVw.backgroundColor = .gray
            cell.tickMark.isHidden = true
        }
        else
        {
            cell.containerVw.backgroundColor = .white
            if titleList[indexPath.row].title == currentTitle
            {
                cell.tickMark.isHidden = false
            }
            else
            {
                cell.tickMark.isHidden = true
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let id = titleList[indexPath.row].titleId ?? -1
        let mytitle = titleList[indexPath.row].title ?? ""
        if mytitle != currentTitle
        {
            if id <= acheivedTitleId
            {
                self.alertView(id: id)
            }
        }
    }
    
    func alertView(id:Int)
    {
        let refreshAlert = UIAlertController(title: "Confirmation", message: "Are you sure you want to set as current title?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.setCurrentTitle(selectedId: id)
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
}

