//
//  titleCell.swift
//  Eclair
//
//  Created by Zohaib Naseer on 8/13/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class titleCell: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var containerVw: AroundShadow!
    @IBOutlet weak var tickMark: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
