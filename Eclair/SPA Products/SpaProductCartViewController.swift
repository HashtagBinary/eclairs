//
//  SpaProductCartViewController.swift
//  Eclair
//
//  Created by Sohail on 29/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SpaProductCartViewController: UIViewController, SubItemDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dbHelper = DBHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 44.0;
        
        tableView.register(UINib(nibName: "SpaProductCartItemsTableViewCell", bundle: nil), forCellReuseIdentifier: "SpaProductCartItemsTableViewCell")
        tableView.register(UINib(nibName: "SpaProductSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "SpaProductSummaryTableViewCell")
        
    }
    
    func getCartList() -> [SpaProductItem] {
        var cartList = [SpaProductItem]()
        
        dbHelper.getAllSpaProductItems()?.forEach({ (obj) in
            let id = obj.value(forKey: "id") as? Int
            let catId = obj.value(forKey: "categoryId") as? Int
            
            let category = spaProducts.first { $0.id == catId }
            if (category != nil) {
                var item = category?.spaItems.first{ $0.id == id }
                if (item != nil) {
                    item!.qty = obj.value(forKey: "quantity") as? Int
                    item!.catId = catId
                    cartList.append(item!)
                }
            }
        })
        
        return cartList
    }
    
    func action(type: Int, data: Any) {
        if (type == 1) {
            let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to remove this item.", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                if let item = data as? SpaProductItem {
                    self.dbHelper.updateSpaProductItem(id: item.id ?? -1, quantity: 0, catId: item.catId ?? -1)
                    self.tableView.reloadData()
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func clearAll(sender: UIButton) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to remove all items from the cart.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.dbHelper.emptySpaProductCart()
            self.navigationController!.popViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func checkOut(sender: UIButton)
    {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to checkout?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.checkoutApiCall()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func checkoutApiCall()
    {
        
        var itemCount = 0
        var subtotal: Float = 0
        
        var itemList = [SpaProductItem]()
        dbHelper.getAllSpaProductItems()?.forEach({ (obj) in
            let id = obj.value(forKey: "id") as? Int
            let catId = obj.value(forKey: "categoryId") as? Int
            
            let category = spaProducts.first { $0.id == catId }
            if (category != nil) {
                var item = category?.spaItems.first{ $0.id == id }
                if (item != nil) {
                    item!.qty = obj.value(forKey: "quantity") as? Int
                    item!.catId = catId
                    itemList.append(item!)
                    
                    itemCount += item!.qty ?? 0
                    subtotal += ((item!.price ?? 0) * Float(item!.qty ?? 0))
                }
            }
        })
        
        // Tax Calculation
        let vtax = subtotal * Constants.kVoucherVatPercentage / 100
        let total = vtax + subtotal
        
        if itemCount <= 0 {
            Utilities.showAlert("Checkout Error", message: "Please select atleast one voucher to checkout!")
            return
        }
        
        let jsonData = try! JSONEncoder().encode(itemList)
        let itemsJson = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
        let parameters = [
            "ecode" : Constants.ecode,
            "vat" : vtax,
            "totalprice" : total,
            "paymentType" : "payment",
            "ItemList" : itemsJson,
        ] as [String : Any]
        
        print("parameter \(parameters)")
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kSpaProductSave, parameters: parameters) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(SpaProductResponse.self, from: data!)
                if (model.response!) {
                    let alert = UIAlertController(title: "Success", message: model.message ?? "", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                        self.dbHelper.emptySpaProductCart()
                        self.navigationController!.popToRootViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    Utilities.showAlert("Response", message: model.message ?? "")
                }
                self.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }
    }
}


extension SpaProductCartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpaProductCartItemsTableViewCell", for: indexPath) as! SpaProductCartItemsTableViewCell
            
            cell.delegate = self
            
            cell.cartList = getCartList()
            cell.tableView.reloadData()
            cell.tableViewHeightContraint.constant = cell.tableView.contentSize.height
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpaProductSummaryTableViewCell", for: indexPath) as! SpaProductSummaryTableViewCell
            
            var itemCount = 0
            var subtotal: Float = 0
            
            getCartList().forEach { (obj) in
                itemCount += obj.qty ?? 0
                subtotal += ((obj.price ?? 0) * Float(obj.qty ?? 0))
            }
            
            // Tax Calculation
            let vtax = subtotal * Constants.kSpaProductVatPercentage / 100
            let total = vtax + subtotal
            
            cell.totalItemsLabel.text = String(format: "%02d", itemCount)
            cell.subAmountLabel.text = "\(subtotal) \(Constants.CURRENCY_SYMBOL)"
            cell.vatTitleLabel.text = "\(Constants.kSpaProductVatLabel) (\(Constants.kSpaProductVatPercentage)%)"
            cell.vatLabel.text = "\(vtax) \(Constants.CURRENCY_SYMBOL)"
            cell.totalAmountLabel.text = "\(total) \(Constants.CURRENCY_SYMBOL)"
            
            cell.clearButton.addTarget(self, action: #selector(clearAll(sender:)), for: .touchUpInside)
            cell.checkOutButton.addTarget(self, action: #selector(checkOut(sender:)), for: .touchUpInside)
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}
