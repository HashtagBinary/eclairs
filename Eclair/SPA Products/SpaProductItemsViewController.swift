//
//  SpaProductItemsViewController.swift
//  Eclair
//
//  Created by Sohail on 29/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SpaProductItemsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var summaryBoxView: SummaryBoxView!
    
    var spaProductCategory: SpaProductCategory!
    
    var dbHelper = DBHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSummaryBoxClick))
        summaryBoxView.addGestureRecognizer(tap)
        summaryBoxView.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        updateCartSummaryBox()
    }
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    func setupViews() {
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    func updateCartSummaryBox() {
        
        var itemCount = 0
        var total: Float = 0

        dbHelper.getAllSpaProductItems()?.forEach({ (obj) in
            let id = obj.value(forKey: "id") as? Int
            let catId = obj.value(forKey: "categoryId") as? Int
            
            let category = spaProducts.first { $0.id == catId }
            if (category != nil) {
                let item = category?.spaItems.first{ $0.id == id }
                if (item != nil) {
                    itemCount += (obj.value(forKey: "quantity") as? Int) ?? 0
                    total += item?.price ?? 0
                }
            }
        })

        summaryBoxView.updateValue(count: itemCount, total: total)
        summaryBoxView.isHidden = (itemCount <= 0) ? true : false
    }
    
    @objc func onSummaryBoxClick(sender: SummaryBoxView) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpaProductCartViewController") as? SpaProductCartViewController

        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    @objc func addToCart(sender: NumberButton) {
        dbHelper.updateSpaProductItem(id: sender.tag, quantity: sender.currentValue, catId: self.spaProductCategory.id!)
        updateCartSummaryBox()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.spaProductCategory.spaItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SpaProductItemTableViewCell
        
        cell.titleLabel.text = self.spaProductCategory.spaItems[indexPath.row].name
        cell.descriptionLabel.text = self.spaProductCategory.spaItems[indexPath.row].description
        cell.priceLabel.text = "\(self.spaProductCategory.spaItems[indexPath.row].price ?? 0) \(Constants.CURRENCY_SYMBOL)"
        
        let cartItem = dbHelper.getSpaProductItemById(id: self.spaProductCategory.spaItems[indexPath.row].id!,
                                                      catId: self.spaProductCategory.id!)
        if (cartItem != nil) {
            cell.numberButton.setValue(newValue: cartItem?.value(forKey: "quantity") as! Int)
        }
        else {
            cell.numberButton.setValue(newValue: 0)
        }
        
        cell.numberButton.tag = self.spaProductCategory.spaItems[indexPath.row].id ?? -1
        cell.numberButton.addTarget(self, action: #selector(addToCart(sender:)), for: .valueChanged)
        
        return cell
    }
}
