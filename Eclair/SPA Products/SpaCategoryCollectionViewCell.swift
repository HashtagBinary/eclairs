//
//  SpaCategoryCollectionViewCell.swift
//  Eclair
//
//  Created by Sohail on 25/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SpaCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
    
    func setupViews() {
        cardView.cornerRadius = 8
        cardView.shadowColor = UIColor.black
        cardView.shadowOffset = CGSize(width: 1, height: 1)
        cardView.shadowOpacity = 0.2
        cardView.shadowRadius = 2
        cardView.maskToBounds = false
        
        imageView.cornerRadius = 8
    }
}
