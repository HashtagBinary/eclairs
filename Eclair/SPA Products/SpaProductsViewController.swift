//
//  SpaProductsViewController.swift
//  Eclair
//
//  Created by Sohail on 24/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

var spaProducts = [SpaProductCategory]()

class SpaProductsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
                layout.minimumLineSpacing = 0
                layout.minimumInteritemSpacing = 4
                layout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
                let size = CGSize(width:(collectionView!.bounds.width - 20)/2, height: 200)
                layout.itemSize = size
                layout.invalidateLayout()
        }
        
        
        fetchData()
    }
    
    func fetchData()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kSpaProducts, parameters: nil, method: .get) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(SpaProducts.self, from: data!)
                spaProducts = model.data
                self.collectionView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }
    }
}

extension SpaProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        spaProducts.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SpaCategoryCollectionViewCell
        
        cell.titleLabel.text = spaProducts[indexPath.row].category
        
        //cell.imageView.sd_setImage(with: URL(string: "https://www.indigobhur.com/Images/items/1563881349_Male_avatar.png"), placeholderImage: UIImage(named: "image_loading.jpg"))
        cell.imageView.image = UIImage.init(named: "image_loading")
        let url = spaProducts[indexPath.row].img ?? "image_loading"
        cell.imageView.downloadImage(with: url)
        { (data:UIImage?, error:NSError?) in
            cell.imageView.image = data
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpaProductItemsViewController") as? SpaProductItemsViewController
        
        vc?.spaProductCategory = spaProducts[indexPath.row]
        self.navigationController!.pushViewController(vc!, animated: true)
    }
}
