//
//  SpaProductCartItemsTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 29/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SpaProductCartItemsTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightContraint: NSLayoutConstraint!
    
    var delegate : SubItemDelegate?
    
    var cartList = [SpaProductItem]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "SpaProductCartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "SpaProductCartItemTableViewCell")
    }

    @objc func removeItem(sender: UIButton) {
        delegate?.action(type: 1, data: cartList[sender.tag])
    }
}

extension SpaProductCartItemsTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpaProductCartItemTableViewCell", for: indexPath) as! SpaProductCartItemTableViewCell
        
        cell.titleLabel.text = cartList[indexPath.row].name
        cell.descriptionLabel.text = cartList[indexPath.row].description
        cell.priceLabel.text = "\(cartList[indexPath.row].price ?? 0) \(Constants.CURRENCY_SYMBOL)"
        cell.quantityLabel.text = String(cartList[indexPath.row].qty ?? 0)
        
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(removeItem(sender:)), for: .touchUpInside)
        
        return cell
    }
}
