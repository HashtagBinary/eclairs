//
//  SpaProductItemTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 29/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SpaProductItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var numberButton: NumberButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
