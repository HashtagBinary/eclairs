//
//  SpaProductSummaryTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 29/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SpaProductSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var totalItemsLabel: UILabel!
    @IBOutlet weak var subAmountLabel: UILabel!
    @IBOutlet weak var vatTitleLabel: UILabel!
    @IBOutlet weak var vatLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var note: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
        
        note.text = Constants.kSpaProductCartNote
    }
    
    func setupViews() {
        
        clearButton.borderWidth = 1
        clearButton.cornerRadius = 8
        clearButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        checkOutButton.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
