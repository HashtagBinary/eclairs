//
//  TablevHeaderVw.swift
//  Eclair
//
//  Created by Indigo on 02/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class HeaderTableView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var secondImg: UIImageView!
    @IBOutlet weak var firstImg: UIImageView!
    @IBOutlet weak var thirdImg: UIImageView!
    @IBOutlet weak var firstBarHeight: NSLayoutConstraint!
    @IBOutlet weak var firstBarWidth: NSLayoutConstraint!
    @IBOutlet weak var thirdBarWidth: NSLayoutConstraint!
    @IBOutlet weak var thirdBarHeight: NSLayoutConstraint!
    @IBOutlet weak var secondBarHeight: NSLayoutConstraint!
    @IBOutlet weak var secondBarWidth: NSLayoutConstraint!
    
    @IBOutlet weak var firstImgWidth: NSLayoutConstraint!
    @IBOutlet weak var firstImgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var secondImgWidth: NSLayoutConstraint!
    @IBOutlet weak var secondImgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var thirdImgWidth: NSLayoutConstraint!
    @IBOutlet weak var thirdImgHeight: NSLayoutConstraint!
    @IBOutlet weak var firstBadgeWidth: NSLayoutConstraint!
    @IBOutlet weak var firstBadgeHeight: NSLayoutConstraint!
    @IBOutlet weak var secondBadgeWidth: NSLayoutConstraint!
    @IBOutlet weak var secondBadgeHeight: NSLayoutConstraint!
    @IBOutlet weak var thirdBadgeWidth: NSLayoutConstraint!
    @IBOutlet weak var thirdBadgeHeight: NSLayoutConstraint!
    @IBOutlet weak var ecodeFirstlbl: UILabel!
    @IBOutlet weak var levelFirstlbl: UILabel!
    @IBOutlet weak var xpFirstlbl: UILabel!
    
    @IBOutlet weak var ecodeSecondlbl: UILabel!
    @IBOutlet weak var levelSecondlbl: UILabel!
    @IBOutlet weak var xpSecondlbl: UILabel!
    
    @IBOutlet weak var ecodeThirdlbl: UILabel!
    @IBOutlet weak var levelThirdlbl: UILabel!
    @IBOutlet weak var xpThirdlbl: UILabel!
    @IBOutlet weak var currentTitleLabelSecond: UILabel!
    @IBOutlet weak var currentTitleLabelFirst: UILabel!
    @IBOutlet weak var currentTitleLabelThird: UILabel!
    @IBOutlet weak var leaderBarSecond: UIImageView!
    @IBOutlet weak var leaderBarFirst: UIImageView!
    @IBOutlet weak var leaderBarThird: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        leaderBarFirst.tintColor = UIColor.init(named: "ColorPurple")
        leaderBarSecond.tintColor = UIColor.init(named: "ColorPurple")
        leaderBarThird.tintColor = UIColor.init(named: "ColorPurple")
        
        ecodeFirstlbl.textColor = .white
        levelFirstlbl.textColor = .white
        xpFirstlbl.textColor = .white
        currentTitleLabelFirst.textColor = .white
        
        ecodeSecondlbl.textColor = .white
        levelSecondlbl.textColor = .white
        xpSecondlbl.textColor = .white
        currentTitleLabelSecond.textColor = .white
        
        ecodeThirdlbl.textColor = .white
        levelThirdlbl.textColor = .white
        xpThirdlbl.textColor = .white
        currentTitleLabelThird.textColor = .white
    }

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
