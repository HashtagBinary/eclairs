//
//  LeaderBoardVC.swift
//  Eclair
//
//  Created by Indigo on 02/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LeaderBoardVC: UIViewController {

    @IBOutlet weak var tblvw: UITableView!
    var leadersList = [Leaderboard]()
    var fistEcode = ""
    var secondEcode = ""
    var thirdEcode = ""
    var levelFirst = ""
    var levelSecond = ""
    var levelThird = ""
    var xpFirst = ""
    var xpSecond = ""
    var xpThird = ""
    var firstImage : UIImage?
    var secondImage : UIImage?
    var thirdImage : UIImage?
    var firstBarColor : UIColor?
    var secondBarColor : UIColor?
    var thirdBarColor : UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "HeaderTableView", bundle: nil)
        tblvw.register(nib, forHeaderFooterViewReuseIdentifier: "HeaderTableView")

        setupView()
    }
    func setupView()
    {
        tblvw.delegate = self
        tblvw.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getListOfTitles()
    }
    
    //MARK:- API CAlling
    func getListOfTitles()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
            "skip" : 0,
            "take" : 100
         ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kGetLeaderBoardList, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json of crystal history \(json)")
            do{
                let model = try JSONDecoder().decode(LeaderboardModel.self, from: data!)
                if model.response == false
                {
                    Utilities.showAlert("", message: model.error ?? "")
                    return
                }
                else
                {
                    self.leadersList = model.leaderboards ?? []
                    self.tblvw.reloadData()
                    if self.leadersList.count > 2
                    {
                        self.fistEcode = self.leadersList[0].ecode?.uppercased() ?? "-"
                        self.levelFirst = "Level \(self.leadersList[0].anrLevel ?? 0)"
                        self.xpFirst = "\(self.leadersList[0].xp ?? 0) XP"
                        self.firstBarColor = UIColor.init(named: "ColorPurple")
                        
    
                        self.secondEcode = self.leadersList[1].ecode?.uppercased() ?? "-"
                        self.levelSecond = "Level \(self.leadersList[1].anrLevel ?? 0)"
                        self.xpSecond = "\(self.leadersList[1].xp ?? 0) XP"
                        self.secondBarColor = UIColor.init(named: "ColorPurple")
                        
                        self.thirdEcode = self.leadersList[2].ecode?.uppercased() ?? "-"
                        self.levelThird = "Level \(self.leadersList[2].anrLevel ?? 0)"
                        self.xpThird = "\(self.leadersList[2].xp ?? 0) XP"
                        self.thirdBarColor = UIColor.init(named: "ColorPurple")
                        
                        if Constants.ecode.lowercased() == self.fistEcode.lowercased()
                        {
                            self.firstBarColor = .white
                        }
                        else if Constants.ecode.lowercased() == self.secondEcode.lowercased()
                        {
                            self.secondBarColor = .white
                        }
                        else if Constants.ecode.lowercased() == self.thirdEcode.lowercased()
                        {
                            self.thirdBarColor = .white
                        }
                        
                        
                        self.tblvw.reloadData()
                        
                        for i in 3..<self.leadersList.count
                        {
                            if Constants.ecode.lowercased() == self.leadersList[i].ecode?.lowercased()
                            {
                                if i < self.leadersList.count - 3
                                {
                                    let indexPath = IndexPath(row: i, section: 0)
                                    self.tblvw.scrollToRow(at: indexPath, at: .top, animated: true)
                                }
                                else
                                {
                                    let indexPath = IndexPath(row: i - 3 , section: 0)
                                    self.tblvw.scrollToRow(at: indexPath, at: .top, animated: true)
                                }
                            }
                        }
                        
                        
                        //self.tblvw.reloadData()
                    }
                }
            }
            catch{
                Utilities.showAlert("Error", message: error.localizedDescription)
            }
        }
    }
}
extension LeaderBoardVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leadersList.count - 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBoardCell") as! LeaderBoardCell
        
        cell.indexlbl.text = "\(indexPath.row + 4)"
        cell.indexlbl.cornerRadius = 15
        cell.userImg.image = UIImage(named: "userimg")
        cell.userImg.circleCorner = true
        cell.userImg.borderWidth = 1
        //cell.userImg.image = UIImage(data: leadersList[indexPath.row + 4].img ?? Data()) ?? UIImage(named: "userimg")
        //Constants.ecode.lowercased()
        if Constants.ecode.lowercased() == leadersList[indexPath.row + 3].ecode?.lowercased()
        {
            cell.containerVw.backgroundColor = Utilities.UIColorFromHex(hex: "#4B03B0")
            cell.userImg.borderColor = .white
            cell.indexlbl.backgroundColor = .white
            cell.indexlbl.textColor = .black
            cell.ecodelbl.textColor = .white
            cell.levelLbl.textColor = .white
            cell.levelTitle.textColor = .white
            cell.currentTitlesTit.textColor = .white
            cell.xplbl.textColor = .white
            cell.userImg.borderColor = .white
        }
        else
        {
            cell.userImg.borderColor = UIColor.init(named: "colorTextDark")
            cell.containerVw.backgroundColor = .white
            cell.indexlbl.backgroundColor = Utilities.UIColorFromHex(hex: "#4B03B0")
            cell.indexlbl.textColor = .white
            cell.ecodelbl.textColor = .black
            cell.levelLbl.textColor = .black
            cell.levelTitle.textColor = .black
            cell.currentTitlesTit.textColor = .black
            cell.xplbl.textColor = .black
        }
        
        cell.ecodelbl.text = leadersList[indexPath.row + 3].ecode?.uppercased() ?? "-"
        cell.levelLbl.text = "\(leadersList[indexPath.row + 3].anrLevel ?? 0)"
        cell.xplbl.text = "\(leadersList[indexPath.row + 3].xp ?? 0) XP"
        
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let vw = self.tblvw.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView")  as! HeaderTableView
        vw.backgroundColor = .clear
        //MARK:- constraints settings
        
        let screenHeight = UIScreen.main.bounds.height / 3
        let screenWidth = UIScreen.main.bounds.width
        
        
        vw.firstBarWidth.constant = screenWidth / 3 - 30
        vw.secondBarWidth.constant = screenWidth / 3 - 30
        vw.thirdBarWidth.constant = screenWidth / 3 - 30
        vw.firstBarHeight.constant = screenHeight - 80
        vw.secondBarHeight.constant = screenHeight - 100
        vw.thirdBarHeight.constant = screenHeight - 120
        vw.firstImgWidth.constant = vw.firstBarWidth.constant - 30
        vw.firstImgHeight.constant = vw.firstBarWidth.constant - 30
        vw.secondImgWidth.constant = vw.firstBarWidth.constant - 30
        vw.secondImgHeight.constant = vw.firstBarWidth.constant - 30
        vw.thirdImgWidth.constant = vw.firstBarWidth.constant - 30
        vw.thirdImgHeight.constant = vw.firstBarWidth.constant - 30
        
        vw.firstImg.cornerRadius = vw.firstImgWidth.constant / 2
        vw.secondImg.cornerRadius = vw.secondImgWidth.constant / 2
        vw.thirdImg.cornerRadius = vw.thirdImgWidth.constant / 2
        vw.firstImg.borderColor = .white
        vw.firstImg.borderWidth = 1
        vw.secondImg.borderColor = .white
        vw.secondImg.borderWidth = 1
        vw.thirdImg.borderColor = .white
        vw.thirdImg.borderWidth = 1
        
        vw.firstBadgeWidth.constant = vw.firstImgWidth.constant - 40
        vw.firstBadgeHeight.constant = vw.firstImgWidth.constant - 40
        vw.secondBadgeWidth.constant = vw.firstImgWidth.constant - 40
        vw.secondBadgeHeight.constant = vw.firstImgWidth.constant - 40
        vw.thirdBadgeWidth.constant = vw.firstImgWidth.constant - 40
        vw.thirdBadgeHeight.constant = vw.firstImgWidth.constant - 40
        
        //MARK:- First Position
        vw.ecodeFirstlbl.text = self.fistEcode
        
        vw.levelFirstlbl.text = self.levelFirst
        vw.xpFirstlbl.text = self.xpFirst
        vw.leaderBarFirst.tintColor = self.firstBarColor
        if self.firstBarColor == .white
        {
            vw.ecodeFirstlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.levelFirstlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.xpFirstlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.currentTitleLabelFirst.textColor = UIColor.init(named: "ColorPurple")
        }
        
        //MARK:- Second Position
        vw.ecodeSecondlbl.text = self.secondEcode
        vw.levelSecondlbl.text = self.levelSecond
        vw.xpSecondlbl.text = self.xpSecond
        vw.leaderBarSecond.tintColor = self.secondBarColor
        if self.secondBarColor == .white
        {
            vw.ecodeSecondlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.levelSecondlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.xpSecondlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.currentTitleLabelSecond.textColor = UIColor.init(named: "ColorPurple")
        }
        
        
        //MARK:- Third Position
        vw.ecodeThirdlbl.text = self.thirdEcode
        vw.levelThirdlbl.text = self.levelThird
        vw.xpThirdlbl.text = self.xpThird
        vw.leaderBarThird.tintColor = self.thirdBarColor
        if self.thirdBarColor == .white
        {
            vw.ecodeThirdlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.levelThirdlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.xpThirdlbl.textColor = UIColor.init(named: "ColorPurple")
            vw.currentTitleLabelThird.textColor = UIColor.init(named: "ColorPurple")
        }
        
        return vw
        }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.view.frame.height / 2.4
    }
}
