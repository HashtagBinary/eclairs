//
//  LeaderBoardCell.swift
//  Eclair
//
//  Created by Indigo on 02/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class LeaderBoardCell: UITableViewCell {

    @IBOutlet weak var indexlbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var ecodelbl: UILabel!
    @IBOutlet weak var levelLbl: UILabel!
    @IBOutlet weak var xplbl: UILabel!
    @IBOutlet weak var containerVw: UIView!
    @IBOutlet weak var currentTitlesTit: UILabel!
    @IBOutlet weak var levelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
