//
//  ChangePasswordViewController.swift
//  Eclair
//
//  Created by iMac on 07/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var currentPasswordtxt: UITextField!
    @IBOutlet weak var newPasstxt: UITextField!
    @IBOutlet weak var confirmPasswordtxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

   
    @IBAction func updatebtn(_ sender: Any) {
        if currentPasswordtxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please enter your current password")
            return
        }
        else if newPasstxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please enter your new password")
            return
        }
        else if confirmPasswordtxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please enter confirm password")
            return
        }
        else if newPasstxt.text != confirmPasswordtxt.text
        {
            Utilities.showAlert("", message: "Your password does not match")
            return
        }
        else
        {
           // changePassword()
            self.alert(title: "", message: "Are you sure you want to change the password?")
        }
    }
    
    func alert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
             let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                self.changePassword()
                
             })
             alert.addAction(ok)
             let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
             })
             alert.addAction(cancel)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    
    func changePassword()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let params : Parameters = ["ecode": Constants.ecode,
                                   "oldpassword":self.currentPasswordtxt.text ?? "",
                                   "password":newPasstxt.text ?? ""]
        print("Params \(params)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kChangePassword, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            if json?["response"].bool ?? false == false
            {
                Utilities.showAlert("", message: json?["msg"].string ?? "Server Error")
                return
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
                Utilities.showAlert("", message: json?["msg"].string ?? "Server Error")
            }
     }
   }
}
