//
//  TokenNexpiryVC.swift
//  Eclair
//
//  Created by iOS Indigo on 30/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TokenNexpiryVC: UIViewController {

    @IBOutlet weak var tokenTbl: UITableView!
    @IBOutlet weak var expiryTbl: UITableView!
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var titlelbl: UILabel!
    var tokens = [GetTokensList]()
    var timeList = [JSON]()
    var listType = ""
    var delegate : gettokenNtime?
    var titleText = ""
    
    var selectedTokenTitle = "Select Token"
    var selectedTokenId = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titlelbl.text = titleText
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if listType == "tokens"
        {
            self.mainStack.arrangedSubviews[0].isHidden = false
            self.mainStack.arrangedSubviews[1].isHidden = true
            self.tokenList()
        }
        else
        {
            self.mainStack.arrangedSubviews[1].isHidden = false
            self.mainStack.arrangedSubviews[0].isHidden = true
            self.getTimeList()
        }
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectbtn(_ sender: Any) {
        self.delegate?.getData(id: self.selectedTokenId, title: self.selectedTokenTitle, type: "tokens")
        self.dismiss(animated: true, completion: nil)
    }
    func tokenList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params :Parameters = [
            "ecode" : Constants.ecode
        ]
        print("url \(Constants.ServerAPI.kGetTokens)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kGetTokens, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(GetTokens.self, from: data!)
     
             if(model?.response == true)
             {
                self.tokens = model?.tokensList ?? []
                self.tokenTbl.reloadData()
             }
             else
             {
                 Utilities.showAlert("", message: "Error: \(model?.error ?? "server Error")")
             }
      }
}
    
    func getTimeList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params : Parameters = ["ecode":Constants.ecode]
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kgetTimeList, parameters: params) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            do {
                let json = try? JSON(data : data!)
                self.timeList = json?.array ?? []
                self.expiryTbl.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }
    }
}
extension TokenNexpiryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tokenTbl
        {
            return tokens.count
        }
        else
        {
           return timeList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tokenTbl
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TokenCell
            cell.titlelbl.text = tokens[indexPath.row].tokenType ?? ""
            cell.descriptionLbl.text = "\(tokens[indexPath.row].quantity ?? 0) Quantity"
            let url = tokens[indexPath.row].imageUrl ?? "image_loading"
            cell.tokenImg.image = UIImage.init(named: "image_loading")
            cell.tokenImg.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.tokenImg.image = data
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ExpiryCell
            cell.titlelbl.text = timeList[indexPath.row]["closingHours"].string ?? ""
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tokenTbl
        {
            self.selectedTokenTitle = self.tokens[indexPath.row].tokenType ?? ""
            self.selectedTokenId = self.tokens[indexPath.row].tokenId ?? -1
            let cell = tableView.cellForRow(at: indexPath) as! TokenCell
            cell.selectedImg.image = UIImage(named: "icon_success")
        }
        else
        {
            let title = self.timeList[indexPath.row]["closingHours"].string ?? ""
            let id = self.timeList[indexPath.row]["id"].int ?? -1
            self.delegate?.getData(id: id, title: title, type: "")
            self.dismiss(animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == tokenTbl
        {
            let cell = tableView.cellForRow(at: indexPath) as! TokenCell
            cell.selectedImg.image = UIImage(named: "")
        }
    }
}
