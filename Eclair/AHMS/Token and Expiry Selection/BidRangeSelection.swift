//
//  BidRangeSelection.swift
//  Eclair
//
//  Created by iOS Indigo on 30/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftRangeSlider
import SwiftyJSON
import Alamofire

class BidRangeSelection: UIViewController {
    struct token {
        var title:String
        var id:Int
        var flag:Bool
    }
    @IBOutlet weak var tablevw: UITableView!
    @IBOutlet weak var minlbl: UILabel!
    @IBOutlet weak var maxlbl: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var sliderVw: RangeSlider!
    var minbid = -1
    var maxbid = -1
    var ids = [Int]()
    var tokensArray = [token]()
    var delegate : bidFilterDelegates?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resetButton.borderWidth = 1
        self.getTokenList()
    }
    @IBAction func cancelbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func applyfilterbtn(_ sender: Any) {
        let formattedArray = (ids.map{String($0)}).joined(separator: ",")

        self.minbid = Int(self.sliderVw.lowerValue)
        self.maxbid = Int(self.sliderVw.upperValue)
        
        self.delegate?.getfilters(minBid:minbid , maxBid: maxbid, tokenIds: "\(formattedArray)")
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func resetbtn(_ sender: Any) {
        self.delegate?.getfilters(minBid:-1 , maxBid: -1, tokenIds: "")
        self.dismiss(animated: true, completion: nil)
    }
    func getTokenList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kGetTokensforfilter, parameters: nil) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            do {
                let json = try? JSON(data : data!)
                let Array = json?["tokensList"].array ?? []
                for i in 0..<Array.count
                {
                    self.tokensArray.append(token.init(title: Array[i]["type"].string ?? "", id: Array[i]["tokenId"].int ?? -1, flag: false))
                }
                self.tablevw.reloadData()
                } catch let error as NSError {
                print(error)
            }
        }
    }
    //
}
extension BidRangeSelection:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tokensArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! bidFilterCell
        
        cell.titleLbl.text = tokensArray[indexPath.row].title
        
        let item = ids.first {$0 == tokensArray[indexPath.row].id}
        if item != nil
        {
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        addId(id: tokensArray[indexPath.row].id)
        tablevw.reloadData()
    }
    
    func addId(id: Int) {
        let index = ids.firstIndex { $0 == id}
        if (index == nil) {
            ids.append(id)
        }
        else {
            removeId(id: id)
        }
    }
    
    func removeId(id: Int) {
        let index = ids.firstIndex { $0 == id}
        if (index != nil) {
            ids.remove(at: index!)
        }
    }
}
