//
//  HMSBidsViewController.swift
//  Eclair
//
//  Created by iOS Indigo on 11/12/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import FLAnimatedImage
import Kingfisher
import SwiftSignalRClient

class HMSBidsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var url : String = ""
    
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    @IBOutlet weak var collectionview: UICollectionView!
    var type: Int = 0
    var bids_list = [BidsApiResponse]()
    var currentAuction : AuctionsAPIModel = AuctionsAPIModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initActivityIndicator()
        self.collectionview.delegate = self
        self.collectionview.dataSource = self
        self.collectionview.register(UINib(nibName: "HMSBidViewCell", bundle: nil), forCellWithReuseIdentifier: "HMSBidViewCell")
        apiCall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionview!.contentInset = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: (self.collectionview.frame.size.width), height: 127)
            layout.minimumLineSpacing = 6;
            layout.minimumInteritemSpacing = 5;
            self.collectionview.setCollectionViewLayout(layout, animated: true)
        }
    }
    private func handleMessage(_ message: String, from user: String) {
        
        NSLog("message from BidListener \(message)");
    }
    
    func apiCall()
    {
        bids_list = [BidsApiResponse]()
        activityIndicator.startAnimating()
        var urlToExecute = URL(string: Constants.ServerAPI.kBaseURLAMS + "api/GetActiveBids?ecode=\(Constants.ecode)") //temporary url by default
        
        if(self.type == 1)
        {
          var auctionId: Int = self.currentAuction.auctionId ?? 0
            urlToExecute = URL(string: Constants.ServerAPI.kBaseURLAMS + "api/GetBidsByEcode?id=\(auctionId)&ecode=\(Constants.ecode)")
        }
        else if(self.type == 2)
        {
            urlToExecute = URL(string: Constants.ServerAPI.kBaseURLAMS + "api/GetActiveBids?ecode=\(Constants.ecode)")
        }

        var request   = URLRequest(url: urlToExecute!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        AF.request(request).responseJSON{ (response) in
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode([BidsApiResponse].self, from: response.data!)
                self.activityIndicator.stopAnimating()
                NSLog("bids count\(model.count)")
                self.bids_list = model;
                self.reload()
            }
            catch {
                print(error)
            }
        }
    }
    
    func reload()
    {
        self.collectionview.reloadData()
    }
    
    @IBAction func onReloadClick(_ sender: UIButton) {
        apiCall()
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bids_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionview.dequeueReusableCell(withReuseIdentifier: "HMSBidViewCell", for: indexPath) as! HMSBidViewCell
        
        cell.title_label.text = bids_list[indexPath.row].auctionId?.description
        cell.currentBid_label.text = (bids_list[indexPath.row].currentBid?.description ?? "0") + " IC"
        cell.bidCount_label.text = (bids_list[indexPath.row].count?.description ?? "0") + " bids"
        cell.startingBid_label.text = (bids_list[indexPath.row].currentBid?.description ?? "0") + " IC"
        cell.endingBid_label.text = (bids_list[indexPath.row].timestamp?.description ?? "0") + " IC"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

    }
    
    func initActivityIndicator()
    {
        activityIndicator.center   = self.view.center
        activityIndicator.hidesWhenStopped = true
        
        if #available(iOS 13.0, *) {
            activityIndicator.style = .large
        } else {
            activityIndicator.style = .gray
        }
        view.addSubview(activityIndicator)
 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
}

