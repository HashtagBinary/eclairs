//
//  CountDownAuction.swift
//  Eclair
//
//  Created by Indigo on 14/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class CountDownAuction: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var stackContainerVw: UIView!
    @IBOutlet weak var stackVw: UIStackView!
    @IBOutlet weak var imageContainerVw: UIView!
    @IBOutlet weak var hVw: UIView!
    @IBOutlet weak var hlbl: UILabel!
    @IBOutlet weak var mVw: UIView!
    @IBOutlet weak var mlbl: UILabel!
    @IBOutlet weak var sVw: UIView!
    @IBOutlet weak var slbl: UILabel!
    @IBOutlet weak var hhBottomLbl: UILabel!
    
    var timer: Timer?
    public var endTimeMillis: Int64 = 0
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
            commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("CountDownAuction", owner: self, options: nil)
        contentView.fixInView(self)
        stackContainerVw.clipsToBounds = true
        stackVw.translatesAutoresizingMaskIntoConstraints = false
        timer?.invalidate()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            if self?.endTimeMillis ?? 0 <= 0 {
                return
            }
            self?.calculateTime()
        }
    }
    func calculateTime() {
        
        let millisec = endTimeMillis - currentTimeMillis()
        var totalsec = millisec / 1000
        
        
        let day = Int(floor(Double(millisec) / 86400000.0))
        totalsec = totalsec % (3600 * 24)
        
        var hour = Int(totalsec / 3600 )
        totalsec = totalsec % 3600
        
        let min = Int(totalsec / 60)
        totalsec = totalsec % 60
        
        let sec = totalsec
        if day > 0
        {
            hour = (day * 24) + hour
        }
        let seconds = String(format: "%02d", sec)
        let minutes = String(format: "%02d", min)
        let hours = String(format: "%02d", hour)
        let days = String(format: "%02d", day)
        
       // print("time value in auction count down \(days),\(hour),\(minutes),\(seconds)")
        
        if millisec <= 0
        {
            hlbl.stopBlink()
            mlbl.stopBlink()
            slbl.stopBlink()
            
            stackVw.arrangedSubviews[0].isHidden = true
            stackVw.arrangedSubviews[1].isHidden = false
            stackVw.arrangedSubviews[2].isHidden = true
            stackVw.arrangedSubviews[3].isHidden = true
            stackVw.arrangedSubviews[4].isHidden = true
            stackVw.arrangedSubviews[5].isHidden = true
            self.hhBottomLbl.isHidden = true
            
            hlbl.text = "Expired"
            hlbl.font = UIFont.boldSystemFont(ofSize: 17)
            mlbl.text = "00"
            slbl.text = "00"
            hlbl.adjustsFontSizeToFitWidth = true
            hlbl.textColor = .red
            mlbl.textColor = .black
            slbl.textColor = .black
            imageContainerVw.isHidden = true
            return
            
        }
        else if millisec/1000 <= 25 && millisec/1000 > 0
        {
            hlbl.startBlink()
            mlbl.startBlink()
            slbl.startBlink()
            hlbl.textColor = .red
            mlbl.textColor = .red
            slbl.textColor = .red
            hlbl.text = hours
            mlbl.text = minutes
            slbl.text = seconds
            hlbl.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            imageContainerVw.isHidden = false
            hlbl.adjustsFontSizeToFitWidth = false
            
            stackVw.arrangedSubviews[0].isHidden = false
            stackVw.arrangedSubviews[1].isHidden = false
            stackVw.arrangedSubviews[2].isHidden = false
            stackVw.arrangedSubviews[3].isHidden = false
            stackVw.arrangedSubviews[4].isHidden = false
            stackVw.arrangedSubviews[5].isHidden = false
            self.hhBottomLbl.isHidden = false
            
        }
        else
        {
            hlbl.textColor = .black
            mlbl.textColor = .black
            slbl.textColor = .black
            hlbl.text = hours
            mlbl.text = minutes
            slbl.text = seconds
            hlbl.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            imageContainerVw.isHidden = false
            hlbl.adjustsFontSizeToFitWidth = false
            stackVw.arrangedSubviews[0].isHidden = false
            stackVw.arrangedSubviews[1].isHidden = false
            stackVw.arrangedSubviews[2].isHidden = false
            stackVw.arrangedSubviews[3].isHidden = false
            stackVw.arrangedSubviews[4].isHidden = false
            stackVw.arrangedSubviews[5].isHidden = false
            self.hhBottomLbl.isHidden = false
        }
    }
    public func startTimer(endTime: Int64) {
        self.endTimeMillis = endTime
        //setupViews()
    }
    public func stopTimer()
    {
        timer?.invalidate()
    }
    func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
}
