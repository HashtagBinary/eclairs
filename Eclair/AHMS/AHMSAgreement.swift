//
//  AHMSAgreement.swift
//  Eclair
//
//  Created by iOS Indigo on 31/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AHMSAgreement: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    @IBOutlet weak var tblvw: UITableView!
    @IBOutlet weak var logoVw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        logoVw.circleCorner = true
        logoVw.borderWidth = 5
        logoVw.borderColor = .white
        tblvw.rowHeight = UITableView.automaticDimension
    }
    @IBAction func disagreeBtn(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToHome"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func agreebtn(_ sender: Any) {
        
        self.acceptAgreement()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! agreementCell
        return cell
    }
   
    
    //MARK:- Agreement Api Calling
    func acceptAgreement()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params : Parameters = [
            "ecode" : Constants.ecode
            ]
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kSignAgreement, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let response = json?["response"].bool ?? false
            if response == false
            {
                Utilities.showAlert("", message: json?["message"].string ?? "Server Error")
                return
            }
            else
            {
                let refreshAlert = UIAlertController(title: "Agreement", message: json?["message"].string ?? "", preferredStyle: UIAlertController.Style.alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    Constants.ahmsAgreement = true
                    self.dismiss(animated: true, completion: nil)
                  }))
                self.present(refreshAlert, animated: true, completion: nil)
            }
      }
}
    
}
class agreementCell : UITableViewCell
{
    
}
