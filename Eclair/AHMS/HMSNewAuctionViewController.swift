//
//  HMSNewAuctionViewController.swift
//  Eclair
//
//  Created by Bilal on 11/7/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import FLAnimatedImage
import Kingfisher
import CoreData
//import DropDown
//import DatePickerDialog
import SwiftyJSON

class HMSNewAuctionViewController: UIViewController {


    @IBOutlet var selectToken_label: UILabel!
    @IBOutlet weak var dateVw: UIView!
    //let selectToken_dropDown = DropDown()
    var token_list = [String]()
    var tokenType = ""
    var segueType = ""
    var titleText = ""
    var timeID = -1
    var tokenid = -1
    var auctionId = -1
    
    @IBOutlet weak var tokenVw: UIView!
    @IBOutlet var bidRangeMin_textField: UITextField!
    @IBOutlet var bidRangeMax_textField: UITextField!
    @IBOutlet var closingDate_label: UILabel!
    var closingDate : Int64 = 0


    var isUnsold = false
    var minBid = ""
    var maxBid = ""
    var tokenStr = ""
    var timeStr = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        dateVw.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap1(_:)))
        tokenVw.addGestureRecognizer(tap1)
        
        
        if isUnsold == true
        {
            selectToken_label.text = self.tokenStr
            bidRangeMin_textField.text = self.minBid
            bidRangeMax_textField.text = self.maxBid
        }
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        //
        self.titleText = "Select"
        self.segueType = ""
        self.performSegue(withIdentifier: "tokenNtime", sender: self)
    }
    @objc func handleTap1(_ sender: UITapGestureRecognizer? = nil) {
        self.segueType = "tokens"
        self.titleText = "Select Token"
        self.performSegue(withIdentifier: "tokenNtime", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tokenNtime"
        {
            let vc = segue.destination as! TokenNexpiryVC
            vc.listType = self.segueType
            vc.delegate = self
            vc.titleText = self.titleText
        }
    }
    func addAuctions()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let bidRangeMin : Double = Double(bidRangeMin_textField.text ?? "") ?? 0
        let bidRangeMax : Double = Double(bidRangeMax_textField.text ?? "") ?? 0
        let params : Parameters = [
               "timeId" : self.timeID,
               "tokenId":self.tokenid,
               "bidRangeMin":bidRangeMin ,
               "bidRangeMax":bidRangeMax ,
               "ecode": Constants.ecode
            ]
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kAddAuctions, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(AuctionsAPIResponseModel.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: "Unable to add Auction to Market Place.")
            }
            else
            {
                if(model?.auctionId != nil && model?.auctionId! ?? -1 >= 0)
                {
                    self.navigationController?.popViewController(animated: true)
                    Utilities.showAlert("", message: "Auction Added to Market Place.")
                }
                else
                {
                    Utilities.showAlert("", message: "Unable to add Auction to Market Place.")
                }
         }
      }
   }
    
    func reAuction()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let bidRangeMin : Double = Double(bidRangeMin_textField.text ?? "0") ?? 0
        let bidRangeMax : Double = Double(bidRangeMax_textField.text ?? "0") ?? 0
        let params : Parameters = [
               "timeId" : self.timeID,
               "tokenId":self.tokenid,
               "bidRangeMin":bidRangeMin ,
               "bidRangeMax":bidRangeMax ,
               "ecode": Constants.ecode,
               "auctionId" : auctionId
            ]
        print("params \(params)")
        
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kReAuction, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(AuctionsAPIResponseModel.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: "Unable to Re Auction to Market Place.")
            }
            else
            {
                if(model?.auctionId != nil && model?.auctionId! ?? -1 >= 0)
                {
                    //self.navigationController?.popViewController(animated: true)
                    self.navigationController?.popToRootViewController(animated: true)
                    Utilities.showAlert("", message: "Auction Added to Market Place.")
                }
                else
                {
                    Utilities.showAlert("", message: "Unable to Re Auction to Market Place.")
                }
         }
      }
}
    
    
    @IBAction func addAuction_touchUpInside(_ sender: Any) {
        if(isValidInput())
        {
            if isUnsold == true
            {
                alert(message: "Are you sure you want to Re-auction this token?")
            }
            
            else
            {
                alert(message: "Are you sure you want to auction this token?")
            }
        }
    }
    
    func alert(message : String) {
    
        let refreshAlert = UIAlertController(title: "Auction", message: message, preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
            if self.isUnsold == true
            {
                self.reAuction()
            }
            else
            {
                self.addAuctions()
            }
        }))

        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func isValidInput() -> Bool
    {
        if(tokenid == -1)
        {
            Utilities.showAlert("", message: "Please select token type.")
            return false
        }
        if((self.bidRangeMin_textField.text?.description.trimString().elementsEqual("") == true))
        {
            Utilities.showAlert("", message: "Please enter minimum bid value.")
            return false
        }
        if((self.bidRangeMax_textField.text?.description.trimString().elementsEqual("") == true))
        {
            Utilities.showAlert("", message: "Please enter maximum bid value.")
            return false
        }
        if((self.bidRangeMin_textField.text ?? "").toDouble() ?? 0 >= (self.bidRangeMax_textField.text ?? "").toDouble() ?? 0)
        {
            Utilities.showAlert("", message: "Bid minimum range should be less than maximum range")
            return false
        }
        if self.bidRangeMin_textField.text?.isEmpty == false
        {
            let amount = Double(self.bidRangeMin_textField.text ?? "0") ?? 0
            if amount < 100
            {
                Utilities.showAlert("", message: "Minimum bid should be greater than 100")
                return false
            }
        }
        if timeID == -1
        {
        Utilities.showAlert("", message: "Please select time")
        return false
        }
        return true
    }
    
    @IBAction func cancelAuction_touchUpInside(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension HMSNewAuctionViewController : gettokenNtime{
    func getData(id: Int, title: String, type: String) {
        if type == "tokens"
        {
            self.selectToken_label.text = title
            self.tokenid = id
        }
        else
        {
            self.closingDate_label.text = title
            self.timeID = id
        }
    }
}

protocol gettokenNtime {
    func getData(id:Int,title:String,type:String)
}
