//
//  TokenCell.swift
//  Eclair
//
//  Created by iOS Indigo on 30/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class TokenCell: UITableViewCell {

    @IBOutlet weak var tokenImg: UIImageView!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var selectedImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var descriptionLbl: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
