//
//  HMSAuctionViewCell.swift
//  Eclair
//
//  Created by Bilal on 11/17/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit

class HMSAuctionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tokenImg: UIImageView!
    @IBOutlet var title_label: UILabel!
    @IBOutlet var detail_imageView: UIImageView!
    @IBOutlet var bidCount_label: UILabel!
    
    @IBOutlet var endingBid_label: UILabel!
    @IBOutlet var startingBid_label: UILabel!
    @IBOutlet var currentBid_label: UILabel!
    
    @IBOutlet weak var countDownVw: CountDownAuction!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}
