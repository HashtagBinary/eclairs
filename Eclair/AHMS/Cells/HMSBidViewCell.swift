//
//  HMSBidViewCell.swift
//  Eclair
//
//  Created by iOS Indigo on 07/01/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class HMSBidViewCell: UICollectionViewCell {

    @IBOutlet var title_label: UILabel!
    @IBOutlet var detail_imageView: UIImageView!
    @IBOutlet var bidCount_label: UILabel!
    
    @IBOutlet var endingBid_label: UILabel!
    @IBOutlet var startingBid_label: UILabel!
    @IBOutlet var currentBid_label: UILabel!
    
    @IBOutlet weak var next_imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
