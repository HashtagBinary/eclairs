//
//  HMSBidDetailViewController.swift
//  Eclair
//
//  Created by Bilal on 11/18/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSignalRClient
import SwiftyJSON

class HMSAuctionDetailViewController: UIViewController {

    private var connection: HubConnection?
    @IBOutlet var title_label: UILabel!
    @IBOutlet var startingBid_label: UILabel!
    @IBOutlet var tokenType_label: UILabel!
    @IBOutlet var buyOutPrice_label: UILabel!
    @IBOutlet var currentBid_label: UILabel!
    @IBOutlet var count_label: UILabel!
    @IBOutlet var lastBid_label: UILabel!
    @IBOutlet var closingDate_label: UILabel!
    @IBOutlet weak var bidButton: UIButton!
    @IBOutlet weak var buyNCancelVw: UIView!
    @IBOutlet weak var tokenImg: UIImageView!
    @IBOutlet weak var buttonsStack: UIStackView!
    @IBOutlet weak var countdownVw: CountDownAuction!
    
    var currentAuction : AuctionsAPIModel = AuctionsAPIModel()
    
    var myLastBid: Float = 0.0
    var auctionId : Int = -1;
    var ecode : NSString = "";
    var type : Int?
    var closingUTC = ""
    var bidTimer = Timer()
    var isExpired = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bidButton.borderColor = UIColor.init(named: "ColorTextDark")
        bidButton.cornerRadius = 10
        bidButton.borderWidth = 1
        
        initSingalR()
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.bidTimer.invalidate()
    }
    override func viewWillAppear(_ animated: Bool) {
        buyNCancelVw.isHidden = false
        getBidsData()
        
        buttonsStack.arrangedSubviews[0].isHidden = true
        buttonsStack.arrangedSubviews[1].isHidden = false
        buttonsStack.arrangedSubviews[2].isHidden = false
            
        if isExpired == true
        {
            if type == 4
            {
                buttonsStack.arrangedSubviews[0].isHidden = false
                buttonsStack.arrangedSubviews[1].isHidden = true
                buttonsStack.arrangedSubviews[2].isHidden = true
            }
            else
            {
                buttonsStack.arrangedSubviews[0].isHidden = true
                buttonsStack.arrangedSubviews[1].isHidden = true
                buttonsStack.arrangedSubviews[2].isHidden = true
            }
        }
    }
    func initData()
    {
        
        let startingBid : Date = Date(timeIntervalSince1970: TimeInterval(self.currentAuction.startingTime ?? 0) / 1000)
        let closingBid : Date = Date(timeIntervalSince1970: TimeInterval(self.currentAuction.closingTime ?? 0) / 1000)

        if let closingUTCTime = self.currentAuction.closingTimeUTC
        {
            self.closingUTC = closingUTCTime
            let inMilies = Utilities.getDateFromString(strDate: self.closingUTC, currentFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            let endTimeMillis = inMilies.millisecondsSince1970
            self.countdownVw.startTimer(endTime: endTimeMillis)
        }
        
                
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy' at 'h:mm a "
        var closingBidString = dateFormatterPrint.string(from: closingBid)
        title_label.text = currentAuction.title
        startingBid_label.text = (currentAuction.bidRangeMin?.description ?? "0.0") + " IC"
        buyOutPrice_label.text = (currentAuction.bidRangeMax?.description ?? "0.0") + " IC"
        title_label.text = currentAuction.tokenType
        currentBid_label.text = (currentAuction.currentBid?.description ?? "0.0") + " IC"

        closingDate_label.text = closingBidString
        count_label.text = "\(currentAuction.bidsCount ?? 0) Bids"
        lastBid_label.text = (myLastBid.description ?? "0.0") + " IC"
        tokenImg.image = UIImage.init(named: "image_loading")
        let url = currentAuction.imageUrl ?? "image_loading"
        tokenImg.downloadImage(with: url)
        { (data:UIImage?, error:NSError?) in
            self.tokenImg.image = data
        }
    }
    
    func initSingalR()
    {
        let url = URL(string: Constants.ServerAPI.kBaseURLAMS + "movehub")!
        connection = HubConnectionBuilder(url: url).withLogging(minLogLevel: .error).build()
        connection?.on(method: "BidListener", callback: { (model: BidsApiResponse) in
            do {
                if(model.auctionId == self.currentAuction.auctionId)
                {
                    self.currentAuction.bidsCount = model.count ?? 0
                    self.currentAuction.currentBid = model.currentBid ?? 0.0
                    self.myLastBid = model.myLastBid ?? 0.0
                    self.count_label.text = "\(self.currentAuction.bidsCount ?? 0) Bids"
                    self.currentBid_label.text = "\(self.currentAuction.currentBid ?? 0) IC"
                    self.lastBid_label.text = "\(model.myLastBid ?? 0) IC"
                    print("signal R test \(model.ecode)")
                    print("signal R model \(model)")
                }

            } catch {
                print(error)
            }
        })
        
        connection?.start()
    }
    
    func getBidsData()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kgetBids+"\(self.currentAuction.auctionId!)&ecode=\(Constants.ecode)")
        { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(BidsApiResponse.self, from: data!)
             self.currentAuction.bidsCount = model?.count ?? 0
             self.currentAuction.currentBid = model?.currentBid ?? 0
             self.myLastBid = model?.myLastBid ?? 0
             self.initData()
      }
}
    
    func BuyOut()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params :Parameters = [
            "auctionId" : currentAuction.auctionId!,
            "ecode" : Constants.ecode
        ]
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kBuyOut, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(BidsApiResponse.self, from: data!)
             NSLog("Auctions title\(model?.bidId ?? 0) : \(model?.myLastBid ?? 0)")
             if(model?.response == true)
             {
                 self.navigationController?.popToRootViewController(animated: true)
                 Utilities.showAlert("", message: "Token successfully bought")
             }
             else
             {
                 Utilities.showAlert("", message: "Error: \(model?.error ?? "server Error")")
             }
       }
}
    
 
    
    func cancelAuction()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)

        let params : Parameters = [
            "auctionId" : currentAuction.auctionId ?? -1
            ]
          
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kCancelAuction, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(AuctionsAPIResponseModel.self, from: data!)
            NSLog("Auctions title\(model?.title ?? "no title") : \(model?.auctionId ?? 0)")
            if(model?.response == true)
            {
              self.navigationController?.popViewController(animated: true)
              Utilities.showAlert("", message: "Auction successfully Cacelled!")
            }
            else
            {
              Utilities.showAlert("", message: "Error: \(model?.error ?? "")")
            }
      }
}

    @IBAction func buyOut_touchUpInside(_ sender: Any) {
        self.alert()
    }
    func alert() {
    
        let refreshAlert = UIAlertController(title: "Auction", message: "Are you sure you want to buyout this token?", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
            self.BuyOut()
        }))

        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func bid_touchUpInside(_ sender: Any) {

        performSegue(withIdentifier: "AuctionDetailToBidSegue", sender: self)
    }
       
    @IBAction func viewBidders_touchUpInside(_ sender: Any) {
        //
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HMSNewAuctionViewController") as! HMSNewAuctionViewController
        vc.isUnsold = true
        vc.tokenStr = currentAuction.tokenType ?? ""
        vc.tokenid = currentAuction.tokenId ?? -1
        vc.auctionId = self.currentAuction.auctionId ?? -1
        vc.minBid = "\(currentAuction.bidRangeMin ?? 0)"
        vc.maxBid = "\(currentAuction.bidRangeMax ?? 0)"
        
        if currentAuction.timeId == 0
        {
            vc.timeID = -1
        }
        else
        {
            vc.timeID = currentAuction.timeId ?? -1
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if((segue.identifier?.elementsEqual("AuctionDetailToBidSegue"))!)
        {
            let intent = segue.destination as! HMSBidViewController
            intent.currentAuction = self.currentAuction
            intent.delegate = self
        }
        else if((segue.identifier?.elementsEqual("AuctionDetailtoBidsSegue"))!)
        {
            let intent = segue.destination as! HMSBidsViewController
            intent.currentAuction = self.currentAuction
            intent.type = 1
        }
     }
}
protocol updateBidDelegate {
    func updateData()
}
extension HMSAuctionDetailViewController : updateBidDelegate
{
    func updateData() {
        self.getBidsData()
    }
}
