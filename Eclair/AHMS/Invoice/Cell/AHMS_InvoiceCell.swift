//
//  AHMS_InvoiceCell.swift
//  Eclair
//
//  Created by Indigo on 06/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class AHMS_InvoiceCell: UITableViewCell {

    @IBOutlet weak var containerVw: UIView!
    @IBOutlet weak var invoiceIdlbl: UILabel!
    @IBOutlet weak var dateTimelbl: UILabel!
    @IBOutlet weak var itemNamelbl: UILabel!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var totalLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewDetailButton.borderColor = UIColor.init(named: "ColorTextDark")
        viewDetailButton.borderWidth = 1
        viewDetailButton.cornerRadius = 12
        containerVw.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
