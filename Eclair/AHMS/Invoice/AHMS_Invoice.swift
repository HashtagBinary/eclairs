//
//  AHMS_Invoice.swift
//  Eclair
//
//  Created by Indigo on 06/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class AHMS_Invoice: UIViewController {

    @IBOutlet weak var tblvw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        // Do any additional setup after loading the view.
        setupView()
    }
    func setupView()
    {
        tblvw.register(UINib(nibName: "AHMS_InvoiceCell", bundle: nil), forCellReuseIdentifier: "AHMS_InvoiceCell")
    }
}
extension AHMS_Invoice : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AHMS_InvoiceCell") as! AHMS_InvoiceCell
        return cell
    }
    
    
}
