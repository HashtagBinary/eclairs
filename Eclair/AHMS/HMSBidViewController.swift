//
//  HMSBidViewController.swift
//  Eclair
//
//  Created by Bilal on 11/28/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HMSBidViewController: UIViewController {

    @IBOutlet var bid_label: UILabel!
    @IBOutlet var bid_textField: UITextField!
    @IBOutlet weak var cancelVw: UIView!
    
    var currentAuction : AuctionsAPIModel = AuctionsAPIModel()
    
    var delegate : updateBidDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelVw.cornerRadius = 12
        cancelVw.borderColor = UIColor.init(named: "ColorTextDark")
        cancelVw.borderWidth = 1
        bid_label.text  = "Current Bid: "+(self.currentAuction.currentBid?.description ?? "0.0") + " IC"
    }
    
    @IBAction func cancel_touchUpInside(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func placeBid_touchUpInside(_ sender: Any) {

        if((self.bid_textField.text?.description.trimString().elementsEqual("") == false) && (self.bid_textField.text?.description as! NSString).floatValue > 0.0)
        {
            let currentBid = Float(self.bid_textField.text ?? "0.0") ?? 0.0
            let last_Bid = self.currentAuction.currentBid ?? 0.0
            
            if currentBid <= 100
            {
                Utilities.showAlert("", message: "Bid must be greater than 100")
                return
            }
            else if currentBid <= last_Bid || currentBid <= self.currentAuction.bidRangeMin ?? 0
            {
                Utilities.showAlert("", message: "Bid amount should be greater than starting bid & current bid")
                return
            }
            else if currentBid > last_Bid 
            {
                self.alert()
            }
        }
        else
        {
            Utilities.showAlert("", message: "Please enter you bid amount.")
        }
    }
    func alert() {
    
        let refreshAlert = UIAlertController(title: "Auction", message: "Are you sure you want to bid on this token?", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
            self.placeBid()
        }))

        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }
    func placeBid()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        var amount : Int = 0
        amount = Int((bid_textField.text as! NSString).intValue ?? 0)
        let params : Parameters = [
            "ecode" : Constants.ecode,
            "auctionId" : currentAuction.auctionId ?? -1,
            "amount" : amount
            ]
          
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kPlaceBid, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(BidsApiResponse.self, from: data!)
            NSLog("Auctions title\(model?.bidId ?? 0) : \(model?.myLastBid ?? 0)")
            if(model?.response == true)
            {
            self.alertforOk(message: "Bid successfully placed!", tag: "1")
            }
            else
            {
                self.alertforOk(message: "Error: \(model?.error ?? "Server Error")", tag: "")
            }
      }
}
    func alertforOk(message : String,tag:String) {
        let refreshAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
            if tag == "1"
            {
                self.delegate?.updateData()
                self.dismiss(animated: true, completion: nil)
            }
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
}
