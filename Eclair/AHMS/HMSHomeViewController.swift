//
//  HMSHomeViewController.swift
//  Eclair
//
//  Created by Bilal on 11/7/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import FLAnimatedImage
import Kingfisher
import SwiftSignalRClient
import SwiftyJSON



class HMSHomeViewController: UIViewController {

    @IBOutlet var activeAuctionCount_label: UILabel!
    @IBOutlet var activeBidCount_label: UILabel!
    @IBOutlet var soldItemsCount_label: UILabel!
    @IBOutlet var unsoldItemCount_label: UILabel!
    @IBOutlet weak var firstStack: UIStackView!
    @IBOutlet weak var secondStack: UIStackView!
    @IBOutlet weak var totalActiveAuctionLbl: UILabel!
    
    var selectedAuctionType = 1 //defalult value for all auctions segue
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        print("agreement \(Constants.ahmsAgreement)")
        if Constants.ahmsAgreement == false
        {
            self.performSegue(withIdentifier: "popup", sender: self)
        }
        else
        {
            setupVw()
            getDashboard()
        }
    }

    func setupVw()
    {
        navigationController?.navigationBar.topItem?.title = "AHMS"
        firstStack.arrangedSubviews[0].tag = 1
        firstStack.arrangedSubviews[1].tag = 2
        
        secondStack.arrangedSubviews[0].tag = 3
        secondStack.arrangedSubviews[1].tag = 4
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        firstStack.arrangedSubviews[0].addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        firstStack.arrangedSubviews[1].addGestureRecognizer(tap2)
        
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        secondStack.arrangedSubviews[0].addGestureRecognizer(tap3)
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        secondStack.arrangedSubviews[1].addGestureRecognizer(tap4)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        let tag = sender?.view?.tag ?? 0
        self.selectedAuctionType = tag
        performSegue(withIdentifier: "HomeToAuctionsSegue", sender: self)
    }
    
    func getDashboard()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kGetDashboard+Constants.ecode)
        { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            
            let decoder = JSONDecoder()
            let model = try? decoder.decode(GetDashboardByEcode.self, from: data ?? Data())
            self.activeAuctionCount_label.text = model?.activeAuctions!.description
            self.activeBidCount_label.text = model?.activeBids!.description
            self.soldItemsCount_label.text = model?.soldItems!.description
            self.unsoldItemCount_label.text = model?.unsoldItems!.description
            self.totalActiveAuctionLbl.text = model?.totalActiveAuctions?.description
      }
}
    
    
    func apiCall()
    {
        guard let urlToExecute = URL(string: Constants.ServerAPI.kGetDashboard+Constants.ecode) else {
            return
        }
        
        var request = URLRequest(url: urlToExecute)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        AF.request(request).responseJSON{ (response) in
            do {
                
                NSLog("URL to execute \(urlToExecute)")
                NSLog("URL response \(response.data?.description)")
                let json = try JSON(data: response.data!)
                print("json Data \(json)")
                let decoder = JSONDecoder()
                let model = try decoder.decode(GetDashboardByEcode.self, from: response.data!)
                self.activeAuctionCount_label.text = model.activeAuctions!.description
                self.activeBidCount_label.text = model.activeBids!.description
                self.soldItemsCount_label.text = model.soldItems!.description
                self.unsoldItemCount_label.text = model.unsoldItems!.description
            
            }
            catch {
                print(error)
            }
        }
    }
    
    @IBAction func marketPlace_touchUpInside(_ sender: Any) {
        self.selectedAuctionType = 0
        performSegue(withIdentifier: "HomeToAuctionsSegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if(segue != nil && segue.identifier != nil && (segue.identifier?.elementsEqual("HomeToAuctionsSegue"))!)
     {
        let intent = segue.destination as! HMSAuctionMarketViewController
        intent.type = self.selectedAuctionType
         
     }
    }
}
