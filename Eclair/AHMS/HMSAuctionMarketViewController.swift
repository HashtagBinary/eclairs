//
//  HMSAuctionMarketViewController.swift
//  Eclair
//
//  Created by Bilal on 11/17/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import FLAnimatedImage
import Kingfisher
import SwiftSignalRClient
//import ListPlaceholder
import SwiftyJSON

class HMSAuctionMarketViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var url : String = ""
    private var connection: HubConnection?
   // private let apiClient = ApiClient()
    @IBOutlet weak var collectionview: UICollectionView!

    var type: Int = 0 // 1: all 2: active 3: sold 4:unsold
    var auction_list = [AuctionsAPIModel]()
    var selectedAuction : AuctionsAPIModel = AuctionsAPIModel()
    var take = 30
    var skip = 0
    var minBid = -1
    var maxBid = -1
    var tokenId = ""
    var isExpired = false
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        if type == 0
        {
            let edit = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(playTapped))
            navigationItem.rightBarButtonItems = [edit]
        }

        self.collectionview.delegate = self
        self.collectionview.dataSource = self
        self.collectionview.register(UINib(nibName: "HMSAuctionViewCell", bundle: nil), forCellWithReuseIdentifier: "HMSAuctionViewCell")
        self.collectionview.refreshControl = self.refreshControl
        self.collectionview.alwaysBounceVertical = true
        
        initSingalR()  
    }

   
    override func viewWillAppear(_ animated: Bool) {
        self.skip = 0
        self.isExpired = false
        self.auction_list.removeAll()
        self.collectionview.reloadData()
        getAuctions()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            self.collectionview!.contentInset = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: (self.collectionview.frame.size.width), height: 210)
            layout.minimumLineSpacing = 6;
            layout.minimumInteritemSpacing = 5;
            self.collectionview.setCollectionViewLayout(layout, animated: true)
        }
    }
    @objc func playTapped(_ sender: UITapGestureRecognizer? = nil) {
        
        self.performSegue(withIdentifier: "bidFilter", sender: self)
    }
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        self.skip = 0
        self.auction_list.removeAll()
        self.collectionview.reloadData()
        self.refreshControl.endRefreshing()
        self.getAuctions()
    }
    
    func initSingalR()
    {
        let url = URL(string: Constants.ServerAPI.kBaseURLAMS + "movehub")!
        connection = HubConnectionBuilder(url: url).withLogging(minLogLevel: .error).build()
        connection?.on(method: "AuctionListener", callback: { (model: AuctionsAPIModel) in
            do {
                if(model.ecode != Constants.ecode)//i.e this auction is not of current logged in user
                {
                    self.auction_list.insert(model, at: 0)
                    self.reload()
                    NSLog("SignalR Title: \(model.title)");
                }
            } catch {
                print(error)
            }
        })
        connection?.start()
    }
    
    
    private func handleMessage(_ message: String, from user: String) {
        
        NSLog("message from BidListener \(message)");
    }
    
    
    
    func getAuctions()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        var url = ""
        let ecode = Constants.ecode
        if type == 1 // activeAuction
        {
            url = Constants.ServerAPI.kGetActiveAuctions+ecode
        }
        else if type == 2
        {
            url = Constants.ServerAPI.kGetActiveBids+ecode
        }
        else if type == 3
        {
            url = Constants.ServerAPI.kGetSoldAuctions+ecode
        }
        else if type == 4
        {
            url = Constants.ServerAPI.kGetUnSoldAuctions+ecode
        }
        else
        {
            url = Constants.ServerAPI.kGetAuctions+ecode+"&skip=\(skip)&take=\(take)&minBid=\(minBid)&maxBid=\(maxBid)&tokenIds=\(tokenId)"
        }
        print("url \(url)")
        
        AlamorfireSingleton.getCallwithoutParameters(serviceName: url)
        { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                print("error code is \(error)")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("json data \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode([AuctionsAPIModel].self, from: data ?? Data())
            
            self.auction_list += model ?? []
            
            if self.auction_list.count <= 0 && self.skip <= 0
            {
                if self.skip == 0 && model?.count ?? 0 <= 0
                {
                    Utilities.showAlert("", message: "No Data Found")
                    return
                }
                Utilities.showAlert("", message: json?["error"].string ?? "Response error")
                return
            }
            else if model?.count ?? -1 <= 0 && self.skip > 0
            {
                return
            }
            else
            {
                self.collectionview.reloadData()
            }
      }
}
    
    func reload()
    {
        self.collectionview.reloadData()
        if(self.auction_list.count>0)
        {
          self.collectionview.setContentOffset(CGPoint(x:0,y:0), animated: false)
        }
    }
    
    @IBAction func onReloadClick(_ sender: UIButton) {

        getAuctions()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.auction_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionview.dequeueReusableCell(withReuseIdentifier: "HMSAuctionViewCell", for: indexPath) as! HMSAuctionViewCell
                
        if auction_list[indexPath.row].tokenType?.isEmpty == true
        {
            cell.title_label.text = auction_list[indexPath.row].type
        }
        else
        {
            cell.title_label.text = auction_list[indexPath.row].tokenType
        }
        
        cell.currentBid_label.text = (auction_list[indexPath.row].currentBid?.description ?? "0") + " IC"
        cell.startingBid_label.text = (auction_list[indexPath.row].bidRangeMin?.description ?? "0") + " IC"
        cell.endingBid_label.text = (auction_list[indexPath.row].bidRangeMax?.description ?? "0") + " IC"
        cell.bidCount_label.text = ""
        
        
        
        let inMilies = Utilities.getDateFromString(strDate: auction_list[indexPath.row].closingTimeUTC ?? "", currentFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")

        let endTimeMillis = inMilies.millisecondsSince1970
        cell.countDownVw.startTimer(endTime: endTimeMillis)
   
        
        cell.detail_imageView.tag = indexPath.row
        cell.tokenImg.image = UIImage.init(named: "image_loading")
        let url = auction_list[indexPath.row].imageUrl ?? "image_loading"
        cell.tokenImg.downloadImage(with: url)
        { (data:UIImage?, error:NSError?) in
            cell.tokenImg.image = data
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let inMilies = Utilities.getDateFromString(strDate: auction_list[indexPath.row].closingTimeUTC ?? "", currentFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        let endTimeMillis = inMilies.millisecondsSince1970
        let currentTimeToMilies = Int64((NSDate().timeIntervalSince1970) * 1000)
        let mili = endTimeMillis - currentTimeToMilies

        if mili <= 0
        {
            self.isExpired = true
        }
        self.selectedAuction = self.auction_list[indexPath.row]
        performSegue(withIdentifier: "HMSMarketToAuctionDetailSegue", sender: self)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if indexPath.row == auction_list.count - 1 {  //numberofitem count
                skip += take
            }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "HMSMarketToAuctionDetailSegue"
        {
            let intent = segue.destination as! HMSAuctionDetailViewController
            intent.currentAuction = self.selectedAuction
            intent.isExpired = self.isExpired
            if(self.type == 0) || (self.type == 2)
            {
                intent.type = 0
            }
            else if(self.type == 3 )//started from user dashboard
            {
                intent.type = 2
            }
            else if self.type == 4
            {
                intent.type = 4
            }
        }
        else if segue.identifier == "bidFilter"
        {
            let vc = segue.destination as! BidRangeSelection
            vc.delegate = self
        }
    }
}
extension HMSAuctionMarketViewController : bidFilterDelegates
{
    func getfilters(minBid: Int, maxBid: Int, tokenIds: String) {
        self.minBid = minBid
        self.maxBid = maxBid
        tokenId = tokenIds
        self.skip = 0
        self.auction_list.removeAll()
        self.collectionview.reloadData()
        self.getAuctions()
    }  
}
protocol bidFilterDelegates {
    func getfilters(minBid:Int,maxBid:Int,tokenIds:String)
}
