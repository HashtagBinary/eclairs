//
//  VoucherProductsViewController.swift
//  Eclair IOS
//
//  Created by Sohail on 10/11/2020.
//

import UIKit

class VoucherProductsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var summaryBoxView: SummaryBoxView!
    
    var voucherList = [VoucherModel]()
    
    var dbHelper = DBHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSummaryBoxClick))
        summaryBoxView.addGestureRecognizer(tap)
        summaryBoxView.isHidden = true
        fetchVouchers()
    }
    override func viewWillAppear(_ animated: Bool) {
        updateCartSummaryBox()
    }
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
        
    }
    
    func setupViews() {
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    func fetchVouchers()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kGetVouchers, parameters: nil) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                self.voucherList = try JSONDecoder().decode([VoucherModel].self, from: data!)
                if self.voucherList.isEmpty == true
                {
                    Utilities.showAlert("", message: "No Data Found")
                    return
                }
            
                self.tableView.reloadData()
                
                self.updateCartSummaryBox()
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func updateCartSummaryBox() {
        
        var itemCount = 0
        var total: Float = 0
        
        dbHelper.getAllVoucherItems()?.forEach({ (obj) in
            let id = obj.value(forKey: "id") as? Int
            let voucher = self.voucherList.first{ $0.voucherId == id }
            if (voucher != nil) {
                itemCount += (obj.value(forKey: "quantity") as? Int) ?? 0
                total += voucher?.amount ?? 0
            }
        })
        
        summaryBoxView.updateValue(count: itemCount, total: total)
        summaryBoxView.isHidden = (itemCount <= 0) ? true : false
    }
    
    @objc func onSummaryBoxClick(sender: SummaryBoxView) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "VoucherCartViewController") as? VoucherCartViewController
        
        vc?.voucherList = self.voucherList
        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    @objc func addToCart(sender: NumberButton) {
        dbHelper.updateVoucherItem(id: sender.tag, quantity: sender.currentValue)
        updateCartSummaryBox()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.voucherList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherTableViewCell", for: indexPath) as! VoucherTableViewCell
        
        cell.titleLabel.text = self.voucherList[indexPath.row].voucherName
        cell.descriptionLabel.text = self.voucherList[indexPath.row].items
        cell.priceLabel.text = "\(self.voucherList[indexPath.row].amount) \(Constants.CURRENCY_SYMBOL)"
        
        let cartItem = dbHelper.getVoucherItemById(id: self.voucherList[indexPath.row].voucherId)
        if (cartItem != nil) {
            cell.numberButton.setValue(newValue: cartItem?.value(forKey: "quantity") as! Int)
        }
        else {
            cell.numberButton.setValue(newValue: 0)
        }
        
        cell.numberButton.tag = self.voucherList[indexPath.row].voucherId
        cell.numberButton.addTarget(self, action: #selector(addToCart(sender:)), for: .valueChanged)
        
        return cell
    }
}
