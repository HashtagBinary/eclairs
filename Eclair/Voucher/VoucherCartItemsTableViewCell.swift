//
//  VoucherCartItemsTableViewCell.swift
//  Eclair IOS
//
//  Created by Sohail on 14/11/2020.
//

import UIKit

class VoucherCartItemsTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightContraint: NSLayoutConstraint!
    
    var cartList = [VoucherModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "VoucherCartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherCartItemTableViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension VoucherCartItemsTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherCartItemTableViewCell", for: indexPath) as! VoucherCartItemTableViewCell
        
        cell.titleLabel.text = cartList[indexPath.row].voucherName
        cell.descriptionLabel.text = cartList[indexPath.row].items
        cell.priceLabel.text = "\(cartList[indexPath.row].amount) \(Constants.CURRENCY_SYMBOL)"
        cell.quantityLabel.text = String(cartList[indexPath.row].quantity ?? 0)
        
        return cell
    }
}

class NestedTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }

    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }

    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}
