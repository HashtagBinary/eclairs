//
//  VoucherCartViewController.swift
//  Eclair IOS
//
//  Created by Sohail on 14/11/2020.
//

import UIKit

class VoucherCartViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var voucherList = [VoucherModel]()
    var dbHelper = DBHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 44.0;
        
        tableView.register(UINib(nibName: "VoucherCartItemsTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherCartItemsTableViewCell")
        tableView.register(UINib(nibName: "VoucherSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherSummaryTableViewCell")
    }
    
    func getCartList() -> [VoucherModel] {
        var cartList = [VoucherModel]()
        
        dbHelper.getAllVoucherItems()?.forEach({ (obj) in
            let id = obj.value(forKey: "id") as? Int
            var voucher = self.voucherList.first{ $0.voucherId == id }
            if (voucher != nil) {
                voucher!.quantity = obj.value(forKey: "quantity") as? Int
                cartList.append(voucher!)
            }
        })
        
        return cartList
    }
    
    @objc func clearAll(sender: UIButton) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to remove all vouchers from the cart.", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.dbHelper.emptyVoucherCart()
            self.navigationController!.popViewController(animated: true)
          }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func checkOut(sender: UIButton) {
        
        var itemCount = 0
        var subtotal: Float = 0
        var voucherIds = ""
        
        getCartList().forEach { (obj) in
            voucherIds += String(format: "%d,", obj.voucherId)
            itemCount += obj.quantity ?? 0
            subtotal += (obj.amount * Float(obj.quantity ?? 0))
        }
        voucherIds = String(voucherIds.dropLast())
        
        // Tax Calculation
        let vtax = subtotal * Constants.kVoucherVatPercentage / 100
        let total = vtax + subtotal
        
        if itemCount <= 0 {
            Utilities.showAlert("Checkout Error", message: "Please select atleast one voucher to checkout!")
            return
        }
        
        let parameters = [
            "voucherId" : voucherIds,
            "paymentType" : "payment",
            "vat" : vtax,
            "total" : total,
            "ecode" : Constants.ecode,
        ] as [String : Any]
        
        print("parameter \(parameters)")
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kVoucherSave, parameters: parameters) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(EventResponseModel.self, from: data!)
                if (model.response) {
                    let alert = UIAlertController(title: "Success", message: model.message ?? "", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                        self.dbHelper.emptyVoucherCart()
                        self.navigationController!.popToRootViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    Utilities.showAlert("Response", message: model.message ?? "")
                }
                
                self.tableView.reloadData()
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
}

extension VoucherCartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherCartItemsTableViewCell", for: indexPath) as! VoucherCartItemsTableViewCell
            
            cell.cartList = getCartList()
            cell.tableView.reloadData()
            cell.tableViewHeightContraint.constant = cell.tableView.contentSize.height
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherSummaryTableViewCell", for: indexPath) as! VoucherSummaryTableViewCell
            
            var itemCount = 0
            var subtotal: Float = 0
            
            getCartList().forEach { (obj) in
                itemCount += obj.quantity ?? 0
                subtotal += (obj.amount * Float(obj.quantity ?? 0))
            }
            
            // Tax Calculation
            let vtax = subtotal * Constants.kVoucherVatPercentage / 100
            let total = vtax + subtotal

            cell.totalItemsLabel.text = String(format: "%02d", itemCount)
            cell.subAmountLabel.text = "\(subtotal) \(Constants.CURRENCY_SYMBOL)"
            cell.vatTitleLabel.text = "\(Constants.kVoucherVatLabel) (\(Constants.kVoucherVatPercentage)%)"
            cell.vatLabel.text = "\(vtax) \(Constants.CURRENCY_SYMBOL)"
            cell.totalAmountLabel.text = "\(total) \(Constants.CURRENCY_SYMBOL)"
            
            cell.clearButton.addTarget(self, action: #selector(clearAll(sender:)), for: .touchUpInside)
            cell.checkOutButton.addTarget(self, action: #selector(checkOut(sender:)), for: .touchUpInside)
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}
