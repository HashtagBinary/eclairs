//
//  VoucherTableViewCell.swift
//  Eclair IOS
//
//  Created by Sohail on 10/11/2020.
//

import UIKit

class VoucherTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var numberButton: NumberButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        numberButton.maxValue = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
