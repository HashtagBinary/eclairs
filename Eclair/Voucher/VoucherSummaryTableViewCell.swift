//
//  VoucherSummaryTableViewCell.swift
//  Eclair IOS
//
//  Created by Sohail on 14/11/2020.
//

import UIKit

class VoucherSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var totalItemsLabel: UILabel!
    @IBOutlet weak var subAmountLabel: UILabel!
    @IBOutlet weak var vatTitleLabel: UILabel!
    @IBOutlet weak var vatLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var note: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
        
        note.text = Constants.kVoucherCartNote
    }
    
    func setupViews() {
        
        clearButton.borderWidth = 1
        clearButton.cornerRadius = 8
        clearButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        checkOutButton.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
