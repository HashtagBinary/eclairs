//
//  ApiClient.swift
//  Eclair
//
//  Created by iMac on 10/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import Foundation
import Alamofire

class ApiClient {
    
    typealias WebServiceResponse = ([[String: Any]]?, Error?) -> Void
    
    func execute(_ url: URL, completion: @escaping WebServiceResponse) {
        
        AF.request(url).validate().responseJSON { response in
            if let error = response.error {
                completion(nil, error)
            }
            else if let jsonArray = response.data as? [[String: Any]] {
                completion(jsonArray, nil)
            }
            else if let jsonDict = response.data as? [String: Any] {
                completion([jsonDict], nil)
            }
        }
    }
}
