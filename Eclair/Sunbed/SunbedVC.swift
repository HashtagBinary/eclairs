//
//  SunbedVC.swift
//  Eclair
//
//  Created by iOS Indigo on 19/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SunbedVC: UIViewController {

    @IBOutlet weak var typeCollection: UICollectionView!
   
    @IBOutlet weak var subValuesCollection: UICollectionView!
    var selectedId = -1
    var sunbedmodelData = [SunbedModel]()
    var subSunbedDataList = [SunbedModelSlotsList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSunbedData()
        // Do any additional setup after loading the view.
    }
    func getSunbedData()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
      
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kSunbedGetAllByUser, parameters: nil) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
//            let json = try? JSON(data: data!)
//            if json?.isEmpty == true
//            {
//                Utilities.showAlert("", message: "Data not found")
//                return
//            }
            self.selectedId = 0
            let decoder = JSONDecoder()
            let model = try? decoder.decode([SunbedModel].self, from: data ?? Data())
            self.sunbedmodelData = model ?? []
            self.typeCollection.reloadData()
    }
}
}

extension SunbedVC: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.typeCollection
        {
            return self.sunbedmodelData.count
        }
        else
        {
            return self.subSunbedDataList.count
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.typeCollection
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SunbedTypeCell
            cell.alphaLbl.text = self.sunbedmodelData[indexPath.item].SunbedNo ?? ""
            cell.valueLbl.text = self.sunbedmodelData[indexPath.item].Name ?? ""
            cell.valueLbl.adjustsFontSizeToFitWidth = true
            cell.containerVw.cornerRadius = 12
            if indexPath.item == selectedId
            {
                cell.containerVw.backgroundColor = Utilities.UIColorFromHex(hex: "#333333")
                cell.alphaLbl.textColor = UIColor.white
                cell.valueLbl.textColor = UIColor.white
                self.subSunbedDataList = self.sunbedmodelData[selectedId].SlotsList ?? []
                self.subValuesCollection.reloadData()
            }
            else
            {
                cell.containerVw.backgroundColor = Utilities.UIColorFromHex(hex: "#DBDBDB")
                cell.alphaLbl.textColor = UIColor.black
                cell.valueLbl.textColor = UIColor.black
            }
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SunbedCategoryCell
            cell.containerVw.cornerRadius = 12
            cell.titlelbl.text = self.subSunbedDataList[indexPath.item].Slot ?? ""
            cell.titlelbl.adjustsFontSizeToFitWidth = true
            return cell
        }
    }
}
extension SunbedVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.typeCollection
        {
            selectedId = indexPath.item
            collectionView.reloadData()
        }
    }
}
extension SunbedVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.typeCollection
        {
            return CGSize(width: UIScreen.main.bounds.width / 2.5 - 10, height: 70)
        }
        else
        {
            return CGSize(width: UIScreen.main.bounds.width / 4 - 20, height: UIScreen.main.bounds.width / 4 - 20)
        }
    }

//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) //.zero
//    }
//
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
}
