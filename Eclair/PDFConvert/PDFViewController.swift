//
//  PDFViewController.swift
//  Eclair
//
//  Created by Zohaib Naseer on 6/15/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import PDFKit

class PDFViewController: UIViewController {
    
    @IBOutlet weak var pdfView: PDFView!
   
    var pdfURL: URL!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        if let document = PDFDocument(url: pdfURL) {
            pdfView.document = document
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+13) {
          //  self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func optionbtn(_ sender: Any) {
        if let documet = PDFDocument(url: pdfURL)
        {
            sharePDF(documet)
        }
        
    }
    func sharePDF(_ filePDF: PDFDocument) {
        if let pdfData = filePDF.dataRepresentation() {
            let objectsToShare = [pdfData]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            //activityVC.popoverPresentationController?.sourceView = self.button

            self.present(activityVC, animated: true, completion: nil)
        }
    }
}
