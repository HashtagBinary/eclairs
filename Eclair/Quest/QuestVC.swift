//
//  QuestVC.swift
//  Eclair
//
//  Created by iOS Indigo on 20/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class QuestVC: UIViewController {

    @IBOutlet weak var tableVw: UITableView!
    var questList = [QuestModelQuest]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getQuest()
    }
    func getQuest()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let params : Parameters = ["ecode":Constants.ecode]
        print("Params \(params)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kGetQuestsByEcode, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(QuestModel.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: model?.message ?? "Data Not Found")
                return
            }
            self.questList = model?.quests ?? []
            self.tableVw.reloadData()
    }
}
}
extension QuestVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! QuestCell
        cell.descriptoinlbl.text = questList[indexPath.row].description ?? ""
        cell.titlelbl.text = "\(questList[indexPath.row].title ?? 0)"
        return cell
    }
    
    
}
