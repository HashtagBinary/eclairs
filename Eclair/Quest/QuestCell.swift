//
//  QuestCell.swift
//  Eclair
//
//  Created by iOS Indigo on 20/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class QuestCell: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var descriptoinlbl: UILabel!
    @IBOutlet weak var playPauseBtn: UIButton!
    @IBOutlet weak var timerlbl: UILabel!
    @IBOutlet weak var startPauselbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
