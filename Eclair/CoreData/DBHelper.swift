//
//  DBHelper.swift
//  Eclair IOS
//
//  Created by Sohail on 15/11/2020.
//

import UIKit
import CoreData

class DBHelper {
    
    var managedContext: NSManagedObjectContext? = nil
    init() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate?.persistentContainer.viewContext
    }
    
    // Voucher methods
    func updateVoucherItem(id: Int, quantity: Int) {
        if (managedContext == nil) { return }
        let entity = NSEntityDescription.entity(forEntityName: "Voucher", in: managedContext!)!
        let cartItem = getVoucherItemById(id: id)
        if (cartItem != nil) {
            if (quantity <= 0) {
                managedContext!.delete(cartItem!)
            }
            else {
                cartItem!.setValue(quantity, forKeyPath: "quantity")
            }
        }
        else if quantity > 0 {
            let voucher = NSManagedObject(entity: entity, insertInto: managedContext)
            voucher.setValue(id, forKeyPath: "id")
            voucher.setValue(quantity, forKeyPath: "quantity")
            voucher.setValue(Constants.ecode, forKeyPath: "ecode")
        }

        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func getAllVoucherItems() -> [NSManagedObject]? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Voucher")
        fetchRequest.predicate = NSPredicate(format: "ecode == %@", Constants.ecode)

        do {
            let voucherCart = try managedContext!.fetch(fetchRequest)
            return voucherCart
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }

    func getVoucherItemById(id: Int) -> NSManagedObject? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Voucher")
        fetchRequest.predicate = NSPredicate(format: "id == %@ && ecode == %@", NSNumber(value: id), Constants.ecode)
        do {
            let voucherCart = try managedContext!.fetch(fetchRequest)

            if (voucherCart.count > 0) {
                return voucherCart[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }

    func emptyVoucherCart() {
        if (managedContext == nil) { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Voucher")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedContext!.execute(batchDeleteRequest)
        } catch {
            print("Could not fetch. \(error)")
        }
    }
    
    // Spa Product methods ---------------------------------------------------
    func updateSpaProductItem(id: Int, quantity: Int, catId: Int) {
        if (managedContext == nil) { return }
        
        let entity = NSEntityDescription.entity(forEntityName: "SpaProduct", in: managedContext!)!
        
        let cartItem = getSpaProductItemById(id: id, catId: catId)
        if (cartItem != nil) {
            if (quantity <= 0) {
                managedContext!.delete(cartItem!)
            }
            else {
                cartItem!.setValue(quantity, forKeyPath: "quantity")
            }
        }
        else if quantity > 0 {
            let obj = NSManagedObject(entity: entity, insertInto: managedContext)
            obj.setValue(id, forKeyPath: "id")
            obj.setValue(quantity, forKeyPath: "quantity")
            obj.setValue(catId, forKeyPath: "categoryId")
            obj.setValue(Constants.ecode, forKeyPath: "ecode")
        }
        
        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getAllSpaProductItems() -> [NSManagedObject]? {
        if (managedContext == nil) { return nil }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SpaProduct")
        fetchRequest.predicate = NSPredicate(format: "ecode == %@", Constants.ecode)
        
        do {
            let cartItem = try managedContext!.fetch(fetchRequest)
            return cartItem
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    func getSpaProductItemById(id: Int, catId: Int) -> NSManagedObject? {
        if (managedContext == nil) { return nil }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SpaProduct")
        fetchRequest.predicate = NSPredicate(format: "id == %@ && categoryId == %@ && ecode == %@", NSNumber(value: id), NSNumber(value: catId), Constants.ecode)
        
        do {
            let cartItem = try managedContext!.fetch(fetchRequest)
            
            if (cartItem.count > 0) {
                return cartItem[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    func emptySpaProductCart() {
        if (managedContext == nil) { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SpaProduct")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try managedContext!.execute(batchDeleteRequest)

        } catch {
            print("Could not fetch. \(error)")
        }
    }
}
