//
//  DBHelper.swift
//  Eclair IOS
//
//  Created by Sohail on 15/11/2020.
//

import UIKit
import CoreData

class DBHelperFnB {
    
    var managedContext: NSManagedObjectContext? = nil
    init() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate?.persistentContainer.viewContext
    }
    
    func updateFnbItem(id: Int, catId: Int, value: Int) {
        if (managedContext == nil) { return }

        let entity = NSEntityDescription.entity(forEntityName: "FnBItem", in: managedContext!)!

        let cartItem = getFnbItemById(id: id)
        if (cartItem != nil) {
            if (value <= 0) {
                managedContext!.delete(cartItem!)
            }
            else {
                cartItem!.setValue(value, forKeyPath: "value")
            }
        }
        else if value > 0 {
            let item = NSManagedObject(entity: entity, insertInto: managedContext)
            item.setValue(id, forKeyPath: "id")
            item.setValue(catId, forKeyPath: "catId")
            item.setValue(value, forKeyPath: "value")
            item.setValue(Constants.ecode, forKeyPath: "ecode")
        }

        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func getAllFnbItems() -> [NSManagedObject]? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBItem")
        fetchRequest.predicate = NSPredicate(format: "ecode == %@", Constants.ecode)

        do {
            let item = try managedContext!.fetch(fetchRequest)
            return item
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }

    func getFnbItemById(id: Int) -> NSManagedObject? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBItem")
        fetchRequest.predicate = NSPredicate(format: "id == %@ && ecode == %@", NSNumber(value: id), Constants.ecode)

        do {
            let item = try managedContext!.fetch(fetchRequest)

            if (item.count > 0) {
                return item[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }
    
    func deleteFnBItemById(id: Int) {
        if (managedContext == nil) { return }

        let cartItem = getFnbItemById(id: id)
        if (cartItem != nil) {
            managedContext!.delete(cartItem!)
            
            do {
                try managedContext!.save()
            }
            catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func updateFnbIngredient(id: Int, itemId: Int, quantityId: Int, value: Int) {
        if (managedContext == nil) { return }

        let entity = NSEntityDescription.entity(forEntityName: "FnBIngredient", in: managedContext!)!

        let cartItem = getFnbIngredientById(id: id, quantityId: quantityId)
        if (cartItem != nil) {
            cartItem!.setValue(value, forKeyPath: "value")
        }
        else {
            let item = NSManagedObject(entity: entity, insertInto: managedContext)
            item.setValue(id, forKeyPath: "id")
            item.setValue(itemId, forKeyPath: "itemId")
            item.setValue(quantityId, forKeyPath: "quantityId")
            item.setValue(value, forKeyPath: "value")
            item.setValue(Constants.ecode, forKeyPath: "ecode")
        }

        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getFnbIngredientById(id: Int, quantityId: Int) -> NSManagedObject? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBIngredient")
        fetchRequest.predicate = NSPredicate(format: "id == %@ && quantityId == %@ && ecode == %@", NSNumber(value: id), NSNumber(value: quantityId), Constants.ecode)

        do {
            let item = try managedContext!.fetch(fetchRequest)

            if (item.count > 0) {
                return item[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }
    
    func deleteFnbIngredientById(id: Int, quantityId: Int) {
        if (managedContext == nil) { return }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBIngredient")
        fetchRequest.predicate = NSPredicate(format: "id == %@ && quantityId == %@ && ecode == %@", NSNumber(value: id), NSNumber(value: quantityId), Constants.ecode)
        
        do {
            let items = try managedContext!.fetch(fetchRequest)
            
            if (items.count > 0) {
                managedContext!.delete(items[0])
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func setDepartment(id: Int, isSheesha: Bool, isJuice: Bool) {
        if (managedContext == nil) { return }

        let entity = NSEntityDescription.entity(forEntityName: "FnBDepartment", in: managedContext!)!

        let item = NSManagedObject(entity: entity, insertInto: managedContext)
        item.setValue(id, forKeyPath: "id")
        item.setValue(isSheesha, forKeyPath: "isSheesha")
        item.setValue(isJuice, forKeyPath: "isJuice")
        item.setValue(Constants.ecode, forKeyPath: "ecode")

        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getDepartment() -> NSManagedObject? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBDepartment")
        fetchRequest.predicate = NSPredicate(format: "ecode == %@", Constants.ecode)

        do {
            let item = try managedContext!.fetch(fetchRequest)
            
            if (item.count > 0) {
                return item[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }
    
    func getAllJuiceItems() -> [NSManagedObject]? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBJuice")
        fetchRequest.predicate = NSPredicate(format: "ecode == %@", Constants.ecode)

        do {
            let item = try managedContext!.fetch(fetchRequest)
            return item
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }
    
    func setJuice(id: Int, json: String) {
        if (managedContext == nil) { return }

        let entity = NSEntityDescription.entity(forEntityName: "FnBJuice", in: managedContext!)!

        let item = NSManagedObject(entity: entity, insertInto: managedContext)
        item.setValue(id, forKeyPath: "id")
        item.setValue(json, forKeyPath: "json")
        item.setValue(Constants.ecode, forKeyPath: "ecode")

        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func updateJuice(id: Int, json: String) {
        if (managedContext == nil) { return }
        let juiceItem = getJuiceItemById(id: id)
        if (juiceItem != nil) {

            juiceItem!.setValue(json, forKeyPath: "json")
        }
        else
        {
            print("juice item not found")
        }

        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getJuiceItemById(id: Int) -> NSManagedObject? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBJuice")
        fetchRequest.predicate = NSPredicate(format: "id == %@", NSNumber(value: id))

        do {
            let item = try managedContext!.fetch(fetchRequest)

            if (item.count > 0) {
                return item[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }
    
    
    func removeJuice(id: Int) {
        if (managedContext == nil) { return }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBJuice")
        fetchRequest.predicate = NSPredicate(format:"id == %@ && ecode == %@", NSNumber(value: id), Constants.ecode)
        
        do {
            let items = try managedContext!.fetch(fetchRequest)
            
            if (items.count > 0) {
                managedContext!.delete(items[0])
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func setInstructions(data: String) {
        if (managedContext == nil) { return }

        let entity = NSEntityDescription.entity(forEntityName: "FnBInstructions", in: managedContext!)!

        let cartItem = getInstructionsById()
        if (cartItem != nil) {
            cartItem!.setValue(data, forKeyPath: "data")
        }
        else {
            let item = NSManagedObject(entity: entity, insertInto: managedContext)
            item.setValue(data, forKeyPath: "data")
            item.setValue(Constants.ecode, forKeyPath: "ecode")
        }
        
        do {
            try managedContext!.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getInstructionsById() -> NSManagedObject? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBInstructions")
        fetchRequest.predicate = NSPredicate(format: "ecode == %@", Constants.ecode)

        do {
            let item = try managedContext!.fetch(fetchRequest)

            if (item.count > 0) {
                return item[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }
    
    func getInstructions() -> NSManagedObject? {
        if (managedContext == nil) { return nil }

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FnBInstructions")
        fetchRequest.predicate = NSPredicate(format: "ecode == %@", Constants.ecode)

        do {
            let item = try managedContext!.fetch(fetchRequest)
            
            if (item.count > 0) {
                return item[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        return nil
    }

    func emptyFnbCart() {
        if (managedContext == nil) { return }
        
        juiceSelectedFlavours = []
        juiceSelectedGlassId = -1
        juiceSelectedBlendId = -1
        juiceSelectedSweetenerId = -1
        juiceSelectedIceId = -1
        
        GselectedTableId = -1
        GselectedAddress = nil
        GselectedPaymentType = nil
        GselectedPreOrderTime = nil

        // empty by ecode implementation remaining!

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FnBItem")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        let fetchRequest1 = NSFetchRequest<NSFetchRequestResult>(entityName: "FnBIngredient")
        let batchDeleteRequest1 = NSBatchDeleteRequest(fetchRequest: fetchRequest1)
        
        let fetchRequest2 = NSFetchRequest<NSFetchRequestResult>(entityName: "FnBDepartment")
        let batchDeleteRequest2 = NSBatchDeleteRequest(fetchRequest: fetchRequest2)
        
        let fetchRequest3 = NSFetchRequest<NSFetchRequestResult>(entityName: "FnBJuice")
        let batchDeleteRequest3 = NSBatchDeleteRequest(fetchRequest: fetchRequest3)

        do {
            try managedContext!.execute(batchDeleteRequest)
            try managedContext!.execute(batchDeleteRequest1)
            try managedContext!.execute(batchDeleteRequest2)
            try managedContext!.execute(batchDeleteRequest3)

        } catch {
            print("Could not fetch. \(error)")
        }
    }
}
