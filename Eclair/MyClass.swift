//
//  MyClass.swift
//  Eclair
//
//  Created by iMac on 13/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import Foundation

class MyClass {
//    public static let hostname: String = "http://eclair.obhursolutions.com/"
    public static let hostname: String = "https://api.indigobhur.com/"
    public static let hostname_fnb: String = "https://api.indigobhur.com/api/fnb/"
    public static let hostname_anr: String = "https://api.indigobhur.com/api/anr/"
    public static let hostname_1: String = "https://indigobhur.com/MW/api/"
    public static let hostname_signalR: String = "https://ams.indigobhur.com/"
//    movehub
//    public static let hostname_signalR_1: String = "http://192.168.1.6:5000/movehub"
    
    
    public static var eventecodelist: Dictionary = [Int : VerifyUser]()
    public static var eventextraecodelist: Dictionary = [Int : VerifyUser]()
    
    public static func resetEventEcodeList() {
        eventecodelist = [Int : VerifyUser]()
    }
    
    public static func resetEventExtraEcodeList() {
        eventextraecodelist = [Int : VerifyUser]()
    }
}
