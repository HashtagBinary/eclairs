//
//  SupportViewController.swift
//  Eclair
//
//  Created by Sohail on 22/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import MobileCoreServices
import FirebaseDatabase
import FirebaseStorage
import SafariServices

class SupportViewController: UIViewController {

    @IBOutlet weak var tblvw: UITableView!
    @IBOutlet weak var galleryImgVw: UIImageView!
    @IBOutlet weak var fileImageVw: UIImageView!
    @IBOutlet weak var bottomVw: UIView!
    @IBOutlet weak var messagetxt: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypePDF),String(kUTTypeInkText),String(kUTTypePlainText)], in: .import)
    var imagePicker = UIImagePickerController()
    
    var ref: DatabaseReference!
    let storage = Storage.storage()
    var suportList = [supportModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupVw()
    }
    func setupVw()
    {
        tblvw.register(UINib(nibName: "AdminCell", bundle: nil), forCellReuseIdentifier: "AdminCell")
        tblvw.register(UINib(nibName: "ClientCell", bundle: nil), forCellReuseIdentifier: "ClientCell")
        
        let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(tap))
        galleryImgVw.addGestureRecognizer(gestureRecognizerOne)

        let gestureRecognizerTwo = UITapGestureRecognizer(target: self, action: #selector(fileTap))
        fileImageVw.addGestureRecognizer(gestureRecognizerTwo)
        fileImageVw.isUserInteractionEnabled = true
        galleryImgVw.isUserInteractionEnabled = true
        bottomVw.circleCorner = true
        messagetxt.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        getDataFromFirebase()
    }
    @IBAction func sendbutton(_ sender: Any) {
        self.ref.child("chat").child(Constants.ecode).childByAutoId().setValue(["image": "none",
             "created_date":ServerValue.timestamp(),
             "media":"none",
             "message":self.messagetxt.text ?? "",
             "status":1,
             "attended_by":""])
        self.messagetxt.text = ""
    }
    func getDataFromFirebase()
    {
        self.suportList.removeAll()
        ref = Database.database().reference()
        self.ref.child("chat").child(Constants.ecode).observe(.value, with: { snapshot in
            
            if let snap = snapshot.children.allObjects as? [DataSnapshot]{
                print(snap)
                
                for (index,val) in snap.enumerated(){
                    
                    let data = val.value as? Dictionary<String, Any>
                    
                    var dataModel = supportModel()
                    dataModel.attended_by = data?["attended_by"] as? String
                    dataModel.image = data?["image"] as? String
                    dataModel.media = data?["media"] as? String
                    dataModel.message = data?["message"] as? String
                    dataModel.status = data?["status"] as? Int
                    dataModel.url = data?["url"] as? String
                    if let time = data?["created_date"] as? TimeInterval
                    {
                        let dateobj = NSDate(timeIntervalSince1970: time/1000)
                        let strDate = Utilities.convertDateFormate(forStringDate: "\(dateobj)", currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: Constants.DateFormate)
                        dataModel.created_date = strDate
                    }
                    self.suportList.append(dataModel)
                    self.tblvw.reloadData()
                    self.tblvw.scrollToRow(at: IndexPath(row: self.suportList.count - 1, section: 0), at: .bottom, animated: true)
                }
            }
        }) { error in
            print(error.localizedDescription)
        }
    }
    @objc func tap(_ sender:AnyObject){
            
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func fileTap(_ sender:AnyObject){
         self.openDocumentPicker()
    }
    @objc func openfile(_ sender:UITapGestureRecognizer){
        
        let tag = sender.view?.tag ?? -1
        let url = suportList[tag].url ?? ""
        guard let url = URL(string: url) else { return }
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    func openDocumentPicker()
    {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true)
    }
    
    @objc func imageTapped(sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        newImageView.enableZoom()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(sender:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }

   @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
}

extension SupportViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suportList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if suportList[indexPath.row].status == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminCell") as! AdminCell
            cell.imageFileImg.isHidden = true
            cell.lbl.isHidden = true
            if suportList[indexPath.row].message?.isEmpty == false
            {
                cell.lbl.text = suportList[indexPath.row].message ?? "-"
                cell.lbl.isHidden = false
               // tableView.rowHeight = UITableView.automaticDimension
                cell.lbl.setContentHuggingPriority(.defaultHigh, for: .horizontal)
            }
            if suportList[indexPath.row].image != "none"
            {
                cell.imageFileImg.isHidden = false
                
                let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(imageTapped(sender:)))
                cell.imageFileImg.addGestureRecognizer(gestureRecognizerOne)
                cell.imageFileImg.isUserInteractionEnabled = true
                
                let url = suportList[indexPath.row].image ?? "image_loading"
                
                //cell.imageFileImg.image = UIImage.init(named: "image_loading")
                cell.lbl.setContentHuggingPriority(.defaultLow, for: .horizontal)
                //tableView.rowHeight = UITableView.automaticDimension
                cell.imageFileImg.downloadImage(with: url)
                { (data:UIImage?, error:NSError?) in
                    cell.imageFileImg.image = data
                    if error != nil
                    {   tableView.rowHeight = 200
                        cell.imageFileImg.image = UIImage(named: "no_Preview_Available")
                        cell.imageFileImg.contentMode = .scaleToFill
                        cell.setContentHuggingPriority(.defaultHigh, for: .vertical)
                    }
                    else
                    {
                        tableView.rowHeight = 400
                    }
                }
            }
            else if suportList[indexPath.row].media != "none"
            {
                //
                tableView.rowHeight = 300
                cell.imageFileImg.isHidden = false
                cell.imageFileImg.image = UIImage(named: "fileImage")
                cell.imageFileImg.tag = indexPath.row
                let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(openfile(_:)))
                cell.imageFileImg.addGestureRecognizer(gestureRecognizerOne)
                cell.imageFileImg.tag = indexPath.row
            }
            else if suportList[indexPath.row].image == "none" && suportList[indexPath.row].media == "none"
            {
                cell.imageFileImg.isHidden = true
                tblvw.rowHeight = UITableView.automaticDimension
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClientCell") as! ClientCell
            
            cell.imgFileImg.isHidden = true
            cell.lbl.isHidden = true
            if suportList[indexPath.row].message?.isEmpty == false
            {
                cell.lbl.text = suportList[indexPath.row].message ?? "-"
                cell.lbl.isHidden = false
                cell.lbl.setContentHuggingPriority(.defaultHigh, for: .horizontal)
                tableView.rowHeight = UITableView.automaticDimension
            }
            if suportList[indexPath.row].image != "none"
            {
                cell.lbl.setContentHuggingPriority(.defaultLow, for: .horizontal)
                let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(imageTapped(sender:)))
                cell.imgFileImg.addGestureRecognizer(gestureRecognizerOne)
                cell.imgFileImg.isHidden = false
                cell.imgFileImg.isUserInteractionEnabled = true
                let url = suportList[indexPath.row].image ?? "no_Preview_Available"
                
                //tableView.rowHeight = UITableView.automaticDimension
                cell.imgFileImg.downloadImage(with: url)
                { (data:UIImage?, error:NSError?) in
                    cell.imgFileImg.image = data
                    if error != nil
                    {
                        //tableView.rowHeight = UITableView.automaticDimension
                        tableView.rowHeight = 200
                        cell.imgFileImg.image = UIImage(named: "no_Preview_Available")
                        cell.setContentHuggingPriority(.defaultHigh, for: .vertical)
                        cell.imgFileImg.tag = indexPath.row
                        cell.imgFileImg.contentMode = .scaleToFill
                        let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(self.openfile(_:)))
                        cell.imgFileImg.addGestureRecognizer(gestureRecognizerOne)
                        cell.imgFileImg.tag = indexPath.row
                    }
                    else
                    {
                        tableView.rowHeight = 400
                        
                    }
                }
            }
            else if suportList[indexPath.row].media != "none"
            {
                //
                cell.lbl.setContentHuggingPriority(.defaultLow, for: .horizontal)
                tableView.rowHeight = 300
                cell.imgFileImg.isHidden = false
                cell.imgFileImg.image = UIImage(named: "fileImage")
                cell.imgFileImg.tag = indexPath.row
                let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(openfile(_:)))
                cell.imgFileImg.addGestureRecognizer(gestureRecognizerOne)
                cell.imgFileImg.tag = indexPath.row
            }
            else if suportList[indexPath.row].image == "none" && suportList[indexPath.row].media == "none"
            {
                cell.imgFileImg.isHidden = true
                tableView.rowHeight = UITableView.automaticDimension
            }
            return cell
        }
    }
}
extension SupportViewController:UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate
{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls)
        guard let myURL = urls.first else {
                return
            }
        uploadMediaFileOnFirebase(mediaURL: myURL)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

     guard let img: UIImage = info[.originalImage] as? UIImage else { return }
     guard let imgdata: Data = img.jpegData(compressionQuality: 0.1) else { return }
     dismiss(animated: true)
     uploadImageOnFirebase(imageData: imgdata)

  }
    func uploadImageOnFirebase(imageData:Data)
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let md = StorageMetadata()
        md.contentType = "image/jpg"

        let ref = Storage.storage().reference().child("chat").child("images").child("\(NSDate().timeIntervalSince1970)-img.png")
        ref.putData(imageData, metadata: md) { (metadata, error) in
         if error == nil {
             ref.downloadURL(completion: { (url, error) in
                 print("Done, url is \(String(describing: url))")
                if let myURL = url
                {
                    self.ref.child("chat").child(Constants.ecode).childByAutoId().setValue(["image": "\(myURL)",
                         "created_date":ServerValue.timestamp(),
                         "media":"none",
                         "message":"test",
                         "status":1,
                         "attended_by":"",
                         "url":"\(myURL)"])
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                }
             })
         }else{
             print("error \(String(describing: error))")
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
         }
     }
    }
    
    
    func uploadMediaFileOnFirebase(mediaURL:URL)
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let ref = Storage.storage().reference().child("chat").child("media").child("\(NSDate().timeIntervalSince1970)-sample.pdf")
        
        ref.putFile(from: mediaURL,metadata: nil) { (metadata, error) in
        if error == nil {
            ref.downloadURL(completion: { (url, error) in
                print("Done, url is \(String(describing: url))")
                if let myURL = url
                {
                    self.ref.child("chat").child(Constants.ecode).childByAutoId().setValue(["image": "none",
                        "created_date":ServerValue.timestamp(),
                        "media":"\(myURL)",
                        "message":"",
                        "status":1,
                        "attended_by":"",
                        "url":"\(myURL)"])
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                }
            })
        }else{
            print("error \(String(describing: error))")
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
        }
    }
  }
}
extension SupportViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count ?? 0 < 1
        {
            sendButton.isEnabled = false
        }
        else
        {
            sendButton.isEnabled = true
        }
        return true
    }
}
