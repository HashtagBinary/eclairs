//
//  ViewController.swift
//  Eclair
//
//  Created by iMac on 25/02/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit

var baseVC: BaseViewController?

class BaseViewController: UIViewController {
  
    var count = 1
    var counter = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        baseVC = self
        self.definesPresentationContext = true
    }

    @IBAction func sidemenubtn(_ sender: Any) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    func someBackgroundTask(timer:Timer) {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).async {
            print("do some background task")
            self.counter = self.counter + 1
            
            DispatchQueue.main.async {
                self.navigationItem.prompt = "Navigation prompts appear at the top. \(self.counter)"
            }
        }
    }
}
