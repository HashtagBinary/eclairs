//
//  monthPercentageCell.swift
//  Eclair
//
//  Created by iOS Indigo on 03/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class monthPercentageCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
}
