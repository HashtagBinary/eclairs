//
//  ClientCell.swift
//  Eclair
//
//  Created by Indigo on 08/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class ClientCell: UITableViewCell {

    @IBOutlet weak var imgFileImg: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
