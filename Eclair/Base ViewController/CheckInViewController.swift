//
//  CheckInViewController.swift
//  Eclair
//
//  Created by Sohail on 16/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import QRCode
import Firebase
import Shimmer

class CheckInViewController: UIViewController {

    @IBOutlet weak var success_view: UIView!
    @IBOutlet weak var successTick_imageView: UIImageView!
    @IBOutlet weak var successScan_imageView: UIImageView!
    @IBOutlet weak var qrcode_view: UIView!
    @IBOutlet weak var qrExpiredInfo_view: UIView!
    @IBOutlet weak var toShimmer_view: UIView!
    @IBOutlet weak var shimmeringView_view: FBShimmeringView!
    @IBOutlet weak var generateQRButton_view: UIView!
    @IBOutlet weak var qrCode_imageView: UIImageView!
//    let firebase_ref = Database.database().reference(withPath: "grocery-items")
    @IBOutlet weak var timerTime_label: UILabel!
    var firebase_ref: DatabaseReference!
    let db = Firestore.firestore()
    var timer:Timer?
    var timeLeft = 60
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shimmeringView_view.isShimmering = true
        shimmeringView_view.contentView = toShimmer_view
        firebase_ref = Database.database().reference()
        self.getLastQRCode()
        
        let clickGesture = UITapGestureRecognizer(target: self, action:  #selector (self.generateQRClicked (_:)))
        self.generateQRButton_view.addGestureRecognizer(clickGesture)
        self.qrcode_view.isHidden = true
        self.qrExpiredInfo_view.isHidden = true
        success_view.isHidden = true

        let dbColl = db.collection("DEMO/APP/QRCODES").document(Constants.ecode)
        dbColl.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                print("Document data: \(dataDescription)")
            } else {
                print("Document does not exist")
                self.showQRDialog()
                self.showHideUI(option: 2)
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        shimmeringView_view.isShimmering = true
        shimmeringView_view.contentView = toShimmer_view
        firebase_ref = Database.database().reference()
        self.getLastQRCode()
        let clickGesture = UITapGestureRecognizer(target: self, action:  #selector (self.generateQRClicked (_:)))
        self.generateQRButton_view.addGestureRecognizer(clickGesture)
        self.qrcode_view.isHidden = true
        self.qrExpiredInfo_view.isHidden = true
        success_view.isHidden = true

        let dbColl = db.collection("DEMO/APP/QRCODES").document(Constants.ecode)
        dbColl.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                print("Document data: \(dataDescription)")
            } else {
                print("Document does not exist")
                self.showQRDialog()
                self.showHideUI(option: 2)
            }
        }
    }
    func dateCalculated() -> Date?
    {
         let date = Date()
            print(DateFormatter.localizedString(
                from: date,
                dateStyle: .medium,
                timeStyle: .medium))
        print("Date calculated")
        return date
    }
    
    func observeFireBase()
    {
        let dbColl = db.collection("DEMO/APP/QRCODES").document(Constants.ecode)
        dbColl.addSnapshotListener(includeMetadataChanges: false) { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data() as [String : AnyObject]? ?? [:]
                print("Document data: \(dataDescription)")
            } else {
                print("Document does not exist")
                self.showHideUI(option: 3)
            }
        }
    }
    
    // or for Swift 4
    @objc func generateQRClicked(_ sender:UITapGestureRecognizer){
        // do other task
        self.showHideUI(option: 1)
        self.generateNewQR()
    }
    
    
    func showHideUI(option: Int)
    {
        if(option == 1)
        {
            qrcode_view.isHidden = false
            qrExpiredInfo_view.isHidden = true
            success_view.isHidden = true
        }
        else if(option == 2)
        {
            qrcode_view.isHidden = true
            qrExpiredInfo_view.isHidden = false
            success_view.isHidden = true
        }
        else if(option == 3)
        {
            let gif_qr_scanned = UIImage.gifImageWithName("gif_qr_scanned")
            let gif_success = UIImage.gifImageWithName("gif_success")
            
            successScan_imageView.image = gif_qr_scanned
            successTick_imageView.image = gif_success
            
            qrcode_view.isHidden = true
            qrExpiredInfo_view.isHidden = true
            success_view.isHidden = false
        }
    }
    
    func getLastQRCode()
    {
        
        let dbColl = db.collection("DEMO/APP/QRCODES").document(Constants.ecode)
        dbColl.getDocument { (document, error) in
            if let document = document, document.exists {
            // Get user value
                let value = document.data() as NSDictionary?
            let qCode = value?["qcode"] as? String ?? ""
            let end_datetime = value?["datetime_new"] as? Double ?? 0.0
            let currentDate = Date()
            var expireDate = Date(timeIntervalSince1970: (end_datetime / 1000.0))
            if currentDate>expireDate {
                self.showQRDialog()
                self.showHideUI(option: 2)
            }
            else//qr already active
            {
                self.showHideUI(option: 1)
                self.createQR(qCode: qCode, expireDate: expireDate)
            }
        }
      }
    }
    
    func showQRDialog()
    {
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "QR Expired", message: "Your QR has expired. Please request a new one.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            self.showHideUI(option: 2)
        })
        dialogMessage.addAction(ok)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    
    func generateNewQR()
    {
        //                let currentDate = Date()
        let currentTime = Int64(Date().timeIntervalSince1970 * 1000)
        let expireDate = Date().addingTimeInterval(120)
        let expireTime = expireDate.timeIntervalSince1970*1000
        let randomString = "\(currentTime)_\(String(Int.random(in: 0 ..< 100)))_\(String(Int.random(in: 0 ..< 100)))";
        let getDate = dateCalculated() ?? Date()
        
        let dbColl = db.collection("DEMO/APP/QRCODES").document(Constants.ecode)
        dbColl.setData(["qcode":randomString,"device":"IOS","datetime_new":Timestamp(date: getDate)])
         self.createQR(qCode: randomString, expireDate: expireDate)
    }
  
    func createQR(qCode : String, expireDate: Date)
    {
        let currentDate = Date()
        var expireTime = expireDate.timeIntervalSince1970
        qrCode_imageView.image = {
            var qrCode = QRCode(qCode)!
            qrCode.size = self.qrCode_imageView.bounds.size
            qrCode.errorCorrection = .High
            return qrCode.image
        }()
        
        self.showHideUI(option: 1)
        var dateDiff = Calendar.current.dateComponents([.second, .minute], from: currentDate, to: expireDate)
        var timerTimeInSec = ((dateDiff.minute ?? 0) * 60) + (dateDiff.second ?? 0)
        self.setTimer(time: timerTimeInSec)
        self.observeFireBase();
    }
    
    func setTimer(time: Int)
    {
        timeLeft = time;
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    }
    
    @objc func onTimerFires()
    {
        timeLeft -= 1
        let time = secondsToHoursMinutesSeconds(seconds: Int(timeLeft))
        timerTime_label.text = String(format: "%02d min : %02d sec",time.1,time.2)
        
        if timeLeft <= 0 {
            timer?.invalidate()
            timer = nil
            self.showQRDialog()
        }
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
