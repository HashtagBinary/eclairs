//
//  HomeViewController.swift
//  Eclair
//
//  Created by Sohail on 22/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import NavigationDrawer
import FirebaseFirestore

class HomeViewController: UIViewController, UITabBarDelegate {
    
    @IBOutlet weak var tabbar: UITabBar!
    var interactor = Interactor()
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabbar.delegate = self
        goToHome()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.homeTab(_:)), name: NSNotification.Name(rawValue: "goToHome"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.profileTab(_:)), name: NSNotification.Name(rawValue: "goToProfile"), object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        snapShotForFS()
    }
    func snapShotForFS()
    {
        //MARK:- snapshot listener for every change
        let docRef = db.collection(Constants.ServerAPI.FS_NOTIFICATIONS_URL).document("\(Constants.ecode)")//.collection("notificationCount")
        .addSnapshotListener(includeMetadataChanges: true)
        { querySnapshot, error in
                        // ...
            guard let snapshot = querySnapshot else {
                        print("Error fetching snapshots: \(error!)")
                        ActivityIndicatorSingleton.StopActivity(myView: self.view)
                        return
                    }
            snapshot.data()?.forEach{ diff in
                print("value is \(diff.value)")
                if let tabItems =   self.tabbar.items {
                    let tabItem = tabItems[3]
                    
                    // In this case we want to modify the badge number of the third tab:
                    let intValue = diff.value as? Int
                    if intValue ?? 0 > 0 && intValue ?? 0 < 10
                    {
                        
                        tabItem.badgeValue = "\(diff.value)"
                    }
                    else if intValue ?? 0 > 9
                    {
                        tabItem.badgeValue = "9+"
                    }
                    else
                    {
                        
                        tabItem.badgeValue = nil
                    }
                    
                    //text coloring
                    if #available(iOS 10.0, *) {
                        tabItem.badgeColor = UIColor.init(named: "ColorAmber")
                        tabItem.setBadgeTextAttributes([
                            NSAttributedString.Key.foregroundColor: UIColor.init(named: "ColorPrimary")
                                    ], for: .normal)
                            }
                    
                    
                    
                    
                }
            }
            
         }
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 0 {
            performSegue(withIdentifier: "showNavigationDrawer", sender: nil)
        }
        else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": item.tag])
        }
    }
    
    @objc func homeTab(_ notification: NSNotification) {
        goToHome()
    }
    
    @objc func profileTab(_ notification: NSNotification) {
        tabbar.selectedItem = tabbar.items![1]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 1])
    }
    
    func goToHome() {
        tabbar.selectedItem = tabbar.items![2]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 2])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? NavigationDrawerController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = self.interactor
        }
    }
    
    @IBAction func edgePanGesture(sender: UIScreenEdgePanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translationInView: translation, viewBounds: view.bounds, direction: .Right)
        
        MenuHelper.mapGestureStateToInteractor(
            gestureState: sender.state,
            progress: progress,
            interactor: interactor) {
            self.performSegue(withIdentifier: "showNavigationDrawer", sender: nil)
        }
    }
}

extension HomeViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator(direction: .Left)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}
