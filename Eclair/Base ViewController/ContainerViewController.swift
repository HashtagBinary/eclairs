//
//  ContainerViewController.swift
//  temp
//
//  Created by Sohail on 08/11/2020.
//

import UIKit

class ContainerViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeTab(_:)), name: NSNotification.Name(rawValue: "changeTab"), object: nil)
    }
    
    
    var newsViewController: NewsViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
        return viewController
    }()
    
    var profileNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ProfileNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Profile".uppercased()
        return viewController
    }()
    
    var notificationNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "NotificationNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Notifications".uppercased()
        return viewController
    }()
    
    var supportNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "SupportNavigationController") as! UINavigationController
//        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Support".uppercased()
        return viewController
    }()
    
    var checkInNavigationController: UINavigationController = {
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "CheckInNavigationController") as! UINavigationController
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Check In/Out QR".uppercased();
        return viewController
    }()
    
    var fnbNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "FnB", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "FnBNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Food & Beverages".uppercased();
        return viewController
    }()
    
    var eventNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "EventNavigationController") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Event Reservation".uppercased();
        return viewController
    }()
    
    
    
    var eventHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "eventHistoryNavigationController") as! UINavigationController
        //let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Event Reservation History".uppercased();
        return viewController
    }()
    
    var voucherNavigationController: UINavigationController = {
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "VoucherNavigationController") as! UINavigationController
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Vouchers".uppercased();
        return viewController
    }()
    
    var GardenNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "VoucherNavigationController") as! UINavigationController
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Garden".uppercased();
        return viewController
    }()
    
    var spaProductNavigationController: UINavigationController = {
   //     let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "SpaProductNavigationController") as! UINavigationController
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "SPA Products".uppercased();
        return viewController
    }()
    
    var spaServiceNavigationController: UINavigationController = {
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "SpaServiceNavigationController") as! UINavigationController
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "SPA".uppercased();
        return viewController
    }()
    
    var sunbedNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "SunbedNavigationController") as! UINavigationController
//        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sunbed".uppercased();
        return viewController
    }()
    var sunbedBevaragesNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
       // var viewController = storyboard.instantiateViewController(withIdentifier: "SunbedNavigationController") as! UINavigationController
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sunbed Beverages".uppercased();
        return viewController
    }()
    var levelingNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "LevelingNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Level".uppercased();
        return viewController
    }()
    
    var memberRequestFormNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MemberRequestFormNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sub Tenant Form".uppercased();
        return viewController
    }()
    
    var subMemberRequestFormNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MemberRequestFormNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sub Member Form".uppercased();
        return viewController
    }()
    
    var guestRequestFormNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "GuestRequestFormNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Guest Form".uppercased();
        return viewController
    }()
    
    var maintainanceFormNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MaintainanceFormNavigationController") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Villa Maintainance".uppercased();
        return viewController
    }()
    
    var guestListNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "GuestListNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Guest List".uppercased();
        return viewController
    }()
    
    var memberListNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MemberListNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sub Tenant List".uppercased();
        return viewController
    }()
    var subMemberListNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MemberListNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sub Member List".uppercased();
        return viewController
    }()
    var ewalletNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "EwalletNavigationController") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Crystal SAC".uppercased();
        return viewController
    }()
    
    var reinvitationFormNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ReinvitationFormNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Invite Guest".uppercased();
        return viewController
    }()
    
    var lostnFoundNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "LostnFoundNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Lost & Found".uppercased();
        return viewController
    }()
    
    var waterActivityNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
//        var viewController = storyboard.instantiateViewController(withIdentifier: "MarinaNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Marina".uppercased();
        return viewController
    }()
    
    var levelBoostNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "LevelBoostNavigationController") as! UINavigationController
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "XP Boost".uppercased();
        return viewController
    }()
    
    var questNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "QuestNavigationController") as! UINavigationController
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Quest".uppercased();
        return viewController
    }()
    var botiqueNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        //var viewController = storyboard.instantiateViewController(withIdentifier: "VoucherNavigationController") as! UINavigationController
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Boutique".uppercased();
        return viewController
    }()
    
    var AHMSNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "AHMSNavigationController") as! UINavigationController
//        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        
        viewController.navigationBar.topItem?.title = "AHMS".uppercased();
        return viewController
    }()
    var PlatinumGongNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        //var viewController = storyboard.instantiateViewController(withIdentifier: "AHMSNavigationController") as! UINavigationController
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        
        viewController.navigationBar.topItem?.title = "Platinum Gong".uppercased();
        return viewController
    }()
    var InventoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        //var viewController = storyboard.instantiateViewController(withIdentifier: "AHMSNavigationController") as! UINavigationController
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        
        viewController.navigationBar.topItem?.title = "Inventory".uppercased();
        return viewController
    }()
    var TitlesNavigationController: UINavigationController = {

        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "TitlesNavigationController") as! UINavigationController
//        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        
        viewController.navigationBar.topItem?.title = "Titles".uppercased();
        return viewController
    }()
    var LeaderBoardNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "leaderBoardNavController") as! UINavigationController
//        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "AvailableLateNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Leaderboard".uppercased();
        return viewController
    }()
    
    //
    var TokenNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MyTokenNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Tokens".uppercased();
        return viewController
    }()
    //settingsNavigationController
    var settingsNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "settingsNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Settings".uppercased();
        return viewController
    }()
    
    
    var fnBHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "FnB", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "fnbHistoryNavigationController") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "F&B History".uppercased();
        return viewController
    }()
    var L_FHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "lostNfoundHistoryNavCon") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Lost & Found History".uppercased();
        return viewController
    }()
    
    var villaMaintenaceHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "lostNfoundHistoryNavCon") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Villa Maintenance History".uppercased();
        return viewController
    }()
    
    var ReInviteHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "reInvitaionHistoryNav") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Re-Invitation History".uppercased();
        return viewController
    }()
    var TenantsHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "maintenanceHistoryNav") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sub Tenants History".uppercased();
        return viewController
    }()
    var MemberHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "maintenanceHistoryNav") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Sub Members History".uppercased();
        return viewController
    }()
    
    var GuestHistoryNavigationController: UINavigationController = {
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "maintenanceHistoryNav") as! UINavigationController
        //var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "Guest History".uppercased();
        return viewController
    }()
    
    var SpaHistoryNavigationController: UINavigationController = {
      //  let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "SpaHistoryNav") as! UINavigationController
        let storyboard = UIStoryboard(name: "Forms", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ComingSoonNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "SPA History".uppercased();
        return viewController
    }()
    var AHMSInvoice: UINavigationController = {
      //  let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "SpaHistoryNav") as! UINavigationController
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "AHMSInvoiceNav") as! UINavigationController
        viewController.navigationBar.topItem?.title = "AHMS Invoice".uppercased();
        return viewController
    }()
    
    var Trader: UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "traderNavigationController") as! UINavigationController
        viewController.navigationBar.topItem?.title = "The Trader".uppercased();
        return viewController
    }()
    //
    
    func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        removeAllViews()
        addChild(viewController)
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func removeAllViews() {
        view.subviews.forEach { $0.removeFromSuperview() }
    }
    
    // handle notification
    @objc func changeTab(_ notification: NSNotification) {
        removeAllViews()
        
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["id"] as? Int {
                removeAllViews()
                switch id {
                case 1:
                    add(asChildViewController: profileNavigationController)
                case 2:
                    add(asChildViewController: newsViewController)
                case 3:
                    add(asChildViewController: notificationNavigationController)
                case 4:
                    add(asChildViewController: supportNavigationController)
                case 5:
                    add(asChildViewController: checkInNavigationController)
                case 6:
                    add(asChildViewController: ewalletNavigationController)
                case 7:
                    add(asChildViewController: fnbNavigationController)
                case 8:
                    add(asChildViewController: spaServiceNavigationController)
                case 9:
                    add(asChildViewController: spaProductNavigationController)
                case 10:
                    add(asChildViewController: sunbedNavigationController)
                case 11:
                    add(asChildViewController: sunbedBevaragesNavigationController)
                case 12:
                    add(asChildViewController: eventNavigationController)
                case 13:
                    add(asChildViewController: waterActivityNavigationController)
                case 14:
                    add(asChildViewController: memberListNavigationController)
                case 15:
                    add(asChildViewController: guestListNavigationController)
                case 16:
                    add(asChildViewController: memberRequestFormNavigationController)
                case 17:
                    add(asChildViewController: guestRequestFormNavigationController)
                case 18:
                    add(asChildViewController: reinvitationFormNavigationController)
                case 19:
                    add(asChildViewController: maintainanceFormNavigationController)
                case 20:
                    add(asChildViewController: lostnFoundNavigationController)
                case 21:
                    add(asChildViewController: levelingNavigationController)
                case 22:
                    add(asChildViewController: questNavigationController)
                case 23:
                    //UserDefaults.standard.setValue(true, forKey: "popup")
                    add(asChildViewController: AHMSNavigationController)
                case 24:
                    add(asChildViewController: voucherNavigationController)
                case 25:
                    add(asChildViewController: levelBoostNavigationController)
                case 26:
                    add(asChildViewController: botiqueNavigationController)
                case 27:
                    add(asChildViewController: fnBHistoryNavigationController)
                case 28:
                    add(asChildViewController: PlatinumGongNavigationController)
                case 29:
                    add(asChildViewController: InventoryNavigationController)
                case 30:
                    add(asChildViewController: TitlesNavigationController)
                case 31:
                    add(asChildViewController: TokenNavigationController)
                case 32:
                    add(asChildViewController: GardenNavigationController)
                case 33:
                    add(asChildViewController: settingsNavigationController)
                case 34:
                    add(asChildViewController: subMemberRequestFormNavigationController)
                case 35:
                    add(asChildViewController: subMemberListNavigationController)
                case 36:
                    add(asChildViewController: LeaderBoardNavigationController)
                case 37:
                    UserDefaults.standard.set(false, forKey: "isVilla")
                    add(asChildViewController: L_FHistoryNavigationController)
                case 38:
                    add(asChildViewController: ReInviteHistoryNavigationController)
                case 39:
                    UserDefaults.standard.setValue(false, forKey: "isGuest")
                    add(asChildViewController: TenantsHistoryNavigationController)
                case 40:
                    UserDefaults.standard.setValue(false, forKey: "isGuest")
                    add(asChildViewController: MemberHistoryNavigationController)
                case 41:
                    UserDefaults.standard.setValue(true, forKey: "isGuest")
                    add(asChildViewController: GuestHistoryNavigationController)
                case 42:
                    add(asChildViewController: SpaHistoryNavigationController)
                case 43:
                    UserDefaults.standard.set(true, forKey: "isVilla")
                    add(asChildViewController: villaMaintenaceHistoryNavigationController)
                case 44:
                    add(asChildViewController: eventHistoryNavigationController)
                case 45:
                    add(asChildViewController: AHMSInvoice)
                case 46:
                    add(asChildViewController: Trader)
                    //
                default:
                    break
                }
            }
        }
        
        //    private func remove(asChildViewController viewController: UIViewController) {
        //        // Notify Child View Controller
        //        viewController.willMove(toParentViewController: nil)
        //
        //        // Remove Child View From Superview
        //        viewController.view.removeFromSuperview()
        //
        //        // Notify Child View Controller
        //        viewController.removeFromParentViewController()
        //    }
        //
        //    private func updateView() {
        //
        //        add(asChildViewController: registerViewController)
        //
        //        /*
        //         if segmentedControl.selectedSegmentIndex == 0 {
        //         remove(asChildViewController: sessionsViewController)
        //         add(asChildViewController: summaryViewController)
        //         } else {
        //         remove(asChildViewController: summaryViewController)
        //         add(asChildViewController: sessionsViewController)
        //         }
        //         */
        //    }
        
    }
}
