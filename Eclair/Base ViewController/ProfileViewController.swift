//
//  ProfileViewController.swift
//  Eclair
//
//  Created by Sohail on 03/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var tracker_bg_view: UIView!
    @IBOutlet weak var ftt_view: UIView!
    @IBOutlet weak var changepass_button: UIView!
    @IBOutlet weak var requestTrackerButton: UIView!
    @IBOutlet weak var buttonsStack: UIStackView!
    @IBOutlet weak var profile_btn: UIView!
    @IBOutlet weak var ecode_label: UILabel!
    @IBOutlet weak var image_imageView: UIImageView!
    @IBOutlet weak var fullName_label: UILabel!
    @IBOutlet weak var eclairName_label: UILabel!
    @IBOutlet weak var type_label: UILabel!
    @IBOutlet weak var startDate_label: UILabel!
    @IBOutlet weak var endDate_label: UILabel!
    @IBOutlet weak var daysRemaining_label: UILabel!
    @IBOutlet weak var accountType: UILabel!
    @IBOutlet weak var maleRatiolbl: UILabel!
    @IBOutlet weak var femailRatioLbl: UILabel!
    @IBOutlet weak var gftt_view: UIView!
    
    
    @IBOutlet weak var contractVw: UIView!
    @IBOutlet weak var genralRatioMetor: UIView!
    @IBOutlet weak var coll: UICollectionView!
    @IBOutlet weak var weekStack: UIStackView!
    @IBOutlet weak var femailVwWidth: NSLayoutConstraint!
    @IBOutlet weak var genralRatioWidth: NSLayoutConstraint!
    @IBOutlet weak var genralRatioStack: UIStackView!
    
    @IBOutlet weak var GMaleRatiolbl: UILabel!
    @IBOutlet weak var gfmailratiolbl: UILabel!
    
    var monthArray = [JSON]()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        
        
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleChangePassword(sender:)))
        changepass_button.addGestureRecognizer(tapGesture)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfileButton(sender:)))
        profile_btn.addGestureRecognizer(tapGesture)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTrackerRequest(sender:)))
        requestTrackerButton.addGestureRecognizer(tapGesture)
        
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleGenralRequest(sender:)))
        genralRatioMetor.addGestureRecognizer(tapGesture)
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        self.buttonsStack.arrangedSubviews[0].isHidden = true
//        self.buttonsStack.arrangedSubviews[1].isHidden = true
//        self.buttonsStack.arrangedSubviews[2].isHidden = false
//        self.buttonsStack.arrangedSubviews[3].isHidden = false
//        self.buttonsStack.arrangedSubviews[4].isHidden = false
        
        
        self.accountType.circleCorner = true
        self.accountType.borderWidth = 2
        self.accountType.borderColor = UIColor.init(named: "ColorPrimary")
        let type = Constants.ecode.prefix(1)
        print("type \(type)")
        if type == "g"
        {
            contractVw.isHidden = true
            buttonsStack.arrangedSubviews[2].isHidden = true
            buttonsStack.arrangedSubviews[3].isHidden = true
            buttonsStack.arrangedSubviews[4].isHidden = true
        }
        profileData(ecode: Constants.ecode)
    }
    
    func setupViews() {
        ecode_label.roundCorners(corners: [.topRight, .bottomRight], radius: 20)
        self.image_imageView.circleCorner = true
        
        self.ecode_label.text = ""
        self.fullName_label.text = ""
        self.eclairName_label.text = ""
        self.type_label.text = ""
        self.startDate_label.text = ""
        self.endDate_label.text = ""
        self.daysRemaining_label.text = ""
        coll.delegate = self
        coll.dataSource = self
    }
    
    @objc func handleChangePassword(sender: UITapGestureRecognizer) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController
        
        self.navigationController!.pushViewController(vc!, animated: true)
    }
    @objc func handleTrackerRequest(sender: UITapGestureRecognizer) {
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        self.getTMRatioByMonth()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired
            self.buttonsStack.arrangedSubviews[0].isHidden = false
            self.buttonsStack.arrangedSubviews[1].isHidden = true
            self.buttonsStack.arrangedSubviews[2].isHidden = false
            self.buttonsStack.arrangedSubviews[3].isHidden = false
            self.buttonsStack.arrangedSubviews[4].isHidden = true
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
        }
    }
    @objc func handleGenralRequest(sender: UITapGestureRecognizer) {
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        self.getTMRatio()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired
            self.buttonsStack.arrangedSubviews[0].isHidden = true
            self.buttonsStack.arrangedSubviews[1].isHidden = false
            self.buttonsStack.arrangedSubviews[2].isHidden = true
            self.buttonsStack.arrangedSubviews[3].isHidden = false
            self.buttonsStack.arrangedSubviews[4].isHidden = false
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
        }
    }
    @objc func handleProfileButton(sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Profile Picture Settings", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Edit Profile Picture", style: UIAlertAction.Style.default, handler: { (action) in
            self.showGalleryOpt()
        }))
        alert.addAction(UIAlertAction(title: "Remove Profile Picture", style: UIAlertAction.Style.default, handler: { (action) in
            self.alert(title: "Remove Picture", message: "Are you sure you want to remove profile picture?")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func profileData(ecode : String)
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters: Parameters = [
            "ecode" : ecode
        ]
        print("parameter \(parameters)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kProfileData, parameters: parameters) { (data:Data?, error:NSError?) in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                
                let json = try? JSON(data : data!)
                print("json \(json)")
                do{
                    let model = try JSONDecoder().decode(UserResponse.self, from: data!)
                    print("model data \(model)")
                    if model.response == true
                    {
                        
                        Constants.kIsMainMember = model.data?.isMain ?? false
                        self.ecode_label.text = model.data?.ecode?.uppercased()
                        self.fullName_label.text = model.data?.fullName
                        self.eclairName_label.text = model.data?.eclareName
                        self.type_label.text = Utilities.displayMemberType(ecode: model.data?.ecode ?? "")
                        self.accountType.text = model.data?.accountType
                        self.startDate_label.text = model.data?.contractStart
                        self.endDate_label.text = model.data?.contractEnd
                        self.daysRemaining_label.text = "\(model.data?.remainDays ?? 0) Days Remaining"
                        self.fetchProfileImage(ecode: model.data?.ecode ?? "")
                    }
                    else
                    {
                        Utilities.showAlert("", message: model.error ?? "Server Error")
                    }
                    
                }
                catch  {
                           print(error)
                    }

                
            }
        }
        
    }
    
    
    
    func removeProfileImg()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters: Parameters = [
            "ecode" : Constants.ecode
        ]
        print("parameter \(parameters)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kProfileRemovePic, parameters: parameters) { (data:Data?, error:NSError?) in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                let json = try JSON(data : data!)
                print("json \(json)")
                let response = json["response"].bool ?? false
                if response == true
                {
                    UserDefaults.standard.setValue("", forKey: "profileImg")
                    self.image_imageView.image = UIImage(named: "userimg")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "profilePicUpdate"), object: nil)
                    Utilities.showAlert("", message: json["msg"].string ?? "")
                    return
                }
                //
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    func alert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
             
             let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
             })
             alert.addAction(cancel)
        
        
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.removeProfileImg()             })
            alert.addAction(ok)
        
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    func updateProfilePic(image:UIImage){
       
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)",
                                          "Content-type": "multipart/form-data",
                                          "Content-Disposition" : "form-data"
        ]
        let parameters: Parameters = [
            "ecode":Constants.ecode
        ]
            AF.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                let ImgData = image.jpegData(compressionQuality: 0.1)
                if let data = ImgData{
                    multipartFormData.append(data, withName: "image", fileName: "imagename.jpg", mimeType: "image/jpeg")
                }

            }, to:Constants.ServerAPI.kProfileChangePic,headers: Auth_header).response{ response in
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                if(response.data != nil){
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)

                    let json = try? JSON(data: response.data!)
                    print("Json \(json)")
                    let respns = json?["response"].bool ?? false
                    if respns == false
                    {
                     let message = json?["msg"].string ?? ""
                     Utilities.showAlert("", message: message)
                     return
                    }
                    else
                    {
                        //
                        if let picture = json?["image"].string
                        {
                            if let decodedData = Data(base64Encoded: picture, options: .ignoreUnknownCharacters) {
                                let image = UIImage(data: decodedData)
                                self.image_imageView.image = image
                                UserDefaults.standard.setValue(picture, forKey: "profileImg")
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "profilePicUpdate"), object: nil)
                            }
                        }
                        else
                        {
                            let message = json?["msg"].string ?? "Internal server error"
                            Utilities.showAlert("", message: message)
                            return
                        }
                    }
                    print("Json data for saving images \(json)")
                }
                else{
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                     print(response.error?.localizedDescription)
                }
            }

        }
    
    
    
    func showGalleryOpt(){
        
        let alert = UIAlertController(title: "Upload Picture", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
            self.getPhotoFromLib()
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.getPhotoFromCam()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getPhotoFromLib(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func getPhotoFromCam(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    
    func fetchProfileImage(ecode : String)
    {
        let parameters: Parameters = [
            "ecode" : ecode
        ]
        print("parameter \(parameters)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kProfileImage, parameters: parameters) { (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                return
            }

            let json = try? JSON(data: data!)
            print("json value\(json!)")
            if let picture = json?["Picture"].string
            {
                if let decodedData = Data(base64Encoded: picture, options: .ignoreUnknownCharacters) {
                    let image = UIImage(data: decodedData)
                    self.image_imageView.image = image
                }
            }
        }
    }
    
    
    func getTMRatioByMonth()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kGetCheckinRatioByMonth+Constants.ecode, parameters: nil) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                let jsonofMonth = try? JSON(data : data!)
                print("json \(jsonofMonth)")
                let response = jsonofMonth?["response"].bool ?? false
                if response == true
                {
                    //MARK:- Ratio metor working
                    
                    let femaleRatio = jsonofMonth?["femaleRatio"].int ?? 0
                    let maleRatio = jsonofMonth?["maleRatio"].int ?? 0
                    self.maleRatiolbl.text = "\(maleRatio)%"
                    self.femailRatioLbl.text = "\(femaleRatio)%"
                    let fttVwWidth = Int(self.ftt_view.frame.width)
                    print("width \(fttVwWidth)")
                    let percent = fttVwWidth * femaleRatio
                    let actualWidth = percent / 100
                    print("width \(actualWidth)")
                    self.femailVwWidth.constant = CGFloat(actualWidth)
                    self.weekStack.arrangedSubviews[1].isHidden = false
                    self.monthArray = jsonofMonth?["ptmlist"].array ?? []
                    self.coll.reloadData()
                    if self.monthArray.count == 0
                    {
                        Utilities.showAlert("", message: "Data not Found!")
                        return
                    }
                }
                else
                {
                    Utilities.showAlert("", message: jsonofMonth?["error"].string ?? "Server Error")
                    return
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    func getTMRatio()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kGetCheckinRatio+Constants.ecode, parameters: nil) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                let jsonofMonth = try? JSON(data : data!)
                print("json \(jsonofMonth)")
                let response = jsonofMonth?["response"].bool ?? false
                if response == true
                {
                    
                    //MARK:- Ratio metor working
                    
                    let femaleRatio = jsonofMonth?["femaleRatio"].int ?? 0
                    let maleRatio = jsonofMonth?["maleRatio"].int ?? 0
                    
                    self.GMaleRatiolbl.text = "\(maleRatio)%"
                    self.gfmailratiolbl.text = "\(femaleRatio)%"
                    
                    
                    let fttVwWidth = Int(self.gftt_view.frame.width)
                    print("width \(fttVwWidth)")
                    let percent = fttVwWidth * femaleRatio
                    let actualWidth = percent / 100
                    print("width \(actualWidth)")
                    self.genralRatioWidth.constant = CGFloat(actualWidth)
                    self.genralRatioStack.arrangedSubviews[1].isHidden = false
                    
                }
                else
                {
                    Utilities.showAlert("", message: jsonofMonth?["error"].string ?? "Server Error")
                    return
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
}

extension UIView {
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = self.bounds
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
}

extension ProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage{
           
            //self.image_imageView.image = image
            self.updateProfilePic(image: image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
extension ProfileViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return monthArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RatioMetorByMonthCell
        let num = monthArray[indexPath.row]["months"].int ?? 1
        let monthName = DateFormatter().shortMonthSymbols[num - 1]
        cell.titlelbl.text = monthName
        if monthArray[indexPath.row]["color"].string ?? "" == "purple"
        {
            let img = UIImage.gifImageWithName("gif_profile_dot_purple")
            cell.img.image = img
        }
        else if monthArray[indexPath.row]["color"].string ?? "" == "orange"
        {
            let img = UIImage.gifImageWithName("gif_profile_dot_amber")
            cell.img.image = img
        }
        else if monthArray[indexPath.row]["color"].string ?? "" == "darkGrey"
        {
            let img = UIImage(named: "darkGrayPercentage")
            cell.img.image = img
        }
        else if monthArray[indexPath.row]["color"].string ?? "" == "grey"
        {
            let img = UIImage(named: "defaultPercentage")
            cell.img.image = img
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let collectionWidth = view.bounds.width
        return CGSize(width: collectionWidth/13, height: 55)

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0

    }
    
}
