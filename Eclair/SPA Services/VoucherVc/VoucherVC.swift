//
//  VoucherVC.swift
//  Eclair
//
//  Created by iOS Indigo on 16/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class VoucherVC: UIViewController {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var logoOmg: UIImageView!
    @IBOutlet weak var ecodeTxt: BorderTextField!
    var delegate : VoucherDelegate?
    var isFnb = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.cornerRadius = 5
        submitButton.cornerRadius = 5
        logoOmg.circleCorner = true
        ecodeTxt.text = Gvoucher_Code
    }
    @IBAction func cancelbtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButton(_ sender: Any) {
        if self.ecodeTxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please write promo code first")
            return
        }
        else
        {
//            if isFnb == false
//            {
//                self.getCoupon(id: self.ecodeTxt.text ?? "")
//            }
//            else
//            {
                self.getCouponForFnb(id: self.ecodeTxt.text ?? "")
            //}
        }
        
    }
    
    
    func getCoupon(id:String)
    {
        let params : Parameters = ["id":id]
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kSpaGetCoupon, parameters: params){ (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json ?? [])")
            if json?["response"].bool ?? false == false
            {
                Utilities.showAlert("", message: json?["message"].string ?? "Server Error")
                return
            }
            else
            {
                let data = json?["data"] ?? []
                let amount = data["Amount"]
                print("amount \(amount)")
                self.delegate?.getVoucherCode(amount: "\(amount)", voucherCode: self.ecodeTxt.text ?? "")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func getCouponForFnb(id:String)
    {
        let params : Parameters = ["voucherCode":id]
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kFnBVerifyVoucher, parameters: params){ (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json ?? [])")
            if json?["response"].bool ?? false == false
            {
                Utilities.showAlert("", message: json?["error"].string ?? "Server Error")
                return
            }
            else
            {
                let amount = json?["amount"].int ?? 0
                print("amount \(amount)")
                self.delegate?.getVoucherCode(amount: "\(amount)", voucherCode: self.ecodeTxt.text ?? "")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
protocol VoucherDelegate {
    func getVoucherCode(amount:String,voucherCode:String)
}

