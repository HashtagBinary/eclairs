//
//  SpaSubValuesTblCell.swift
//  Eclair
//
//  Created by iOS Indigo on 11/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SpaSubValuesTblCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var subheading: UILabel!
    @IBOutlet weak var bottomLbl: UILabel!
    @IBOutlet weak var titlelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
