//
//  SpaServicesDetail.swift
//  Eclair
//
//  Created by iOS Indigo on 11/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class SpaServicesDetail: UIViewController {
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var tableVw: UITableView!
    
    var spaCategory: SpaServiceModelSpaCategory?
    var spaServiceItem = [SpaServiceItemData]()
    var selectedId = 0
    var spaTime = ""
    var spaPrice = ""
    var itemId = ""
    var slotsRequired = 0
    var additionalRate = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.spaCategory?.name ?? ""
        
        self.tableVw.delegate = self
        self.tableVw.dataSource = self
        
        if (spaCategory!.spaSubCategories.count > 0) {
            self.getSpaServices(id: "\(spaCategory!.spaSubCategories[0].id ?? 0)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "checkout"
        {
            let vc = segue.destination as! SpaCheckout
            vc.timeSlaps = self.spaTime
            vc.price = self.spaPrice
            vc.itemId = self.itemId
        }
    }
    
    func getSpaServices(id:String)
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kSpaGetservicesbyid+id) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("json value\(json!)")
            if let response = json?["Response"].int 
            {
                if response == 1
                {
                    Utilities.showAlert("", message: json?["Message"].string ?? Constants.ErrorMessage.kCommanErrorMessage)
                    return
                }
                else
                {
                    let decoder = JSONDecoder()
                    let model = try? decoder.decode(SpaServiceItem.self, from: data ?? Data())
                    self.spaServiceItem = model?.data ?? []
                    self.tableVw.reloadData()
                }
            }
            else
            {
                Utilities.showAlert("", message: "Internal server error")
                return
            } 
    }
  }
    
}
extension SpaServicesDetail: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.spaServiceItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SpaSubValuesTblCell
        cell.img.circleCorner = true
        cell.titlelbl.text = self.spaServiceItem[indexPath.row].name ?? ""
        cell.subheading.text = self.spaServiceItem[indexPath.row].duration ?? ""
        cell.bottomLbl.text = "\(self.spaServiceItem[indexPath.row].price ?? 0.0)"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let newStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let VC = newStoryBoard.instantiateViewController(withIdentifier: "SpaCheckout") as! SpaCheckout
        VC.titleStr = self.spaServiceItem[indexPath.row].name ?? ""
        VC.timeSlaps = self.spaServiceItem[indexPath.row].duration ?? ""
        VC.price = "\(self.spaServiceItem[indexPath.row].price ?? 0.0)"
        VC.itemId = "\(self.spaServiceItem[indexPath.row].id ?? 0)"
        VC.additionalTime = self.spaServiceItem[indexPath.row].additionalrate ?? 0
        VC.slotsRequired = self.spaServiceItem[indexPath.row].slotrequired ?? 0
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}
extension SpaServicesDetail: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return spaCategory!.spaSubCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CategoryCollectionCell
        cell?.titleLbl.text = self.spaCategory!.spaSubCategories[indexPath.item].name ?? ""
        cell?.containerVw.circleCorner = true
        cell?.titleLbl.adjustsFontSizeToFitWidth = true
        if indexPath.item == selectedId
        {
            cell?.containerVw.backgroundColor = Utilities.UIColorFromHex(hex: "#333333")
            cell?.titleLbl.textColor = UIColor.white
        }
        else
        {
            cell?.containerVw.backgroundColor = Utilities.UIColorFromHex(hex: "#DBDBDB")
            cell?.titleLbl.textColor = UIColor.black
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedId = indexPath.item
        collection.reloadData()
        self.getSpaServices(id: "\(self.spaCategory!.spaSubCategories[indexPath.item].id ?? 0)")
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: UIScreen.main.bounds.width / 3 - 10, height: 50)
        
    }
}
