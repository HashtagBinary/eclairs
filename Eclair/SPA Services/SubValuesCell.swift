//
//  SubValuesCell.swift
//  Eclair
//
//  Created by iOS Indigo on 04/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SubValuesCell: UICollectionViewCell {
    
    @IBOutlet weak var subVw: AroundShadow!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var viewDetailBtn: UIButton!
}
