//
//  SpaServices.swift
//  Eclair
//
//  Created by iOS Indigo on 04/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class SpaServices: UIViewController {

    @IBOutlet weak var typeCollection: UICollectionView!
    @IBOutlet weak var subvaluesCollection: UICollectionView!
    @IBOutlet weak var subVw: UIView!
    var spaServiceData = [SpaServiceModelData]()
    var subService = [SpaServiceModelSpaCategory]()
    var selectedId = -1
    var spaDetail = [SpaServiceModelSpaSubCategory]()
    var titleStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.subVw.layer.cornerRadius = 12
        self.subVw.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        getSpaServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    //MARK:- API Call
    
    func getSpaServices()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kSpaServices) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("json value\(json!)")
            if let response = json?["Response"].int
            {
                if response == 1
                {
                    Utilities.showAlert("", message: json?["Message"].string ?? Constants.ErrorMessage.kCommanErrorMessage)
                    return
                }
                else
                {
                    let decoder = JSONDecoder()
                    let model = try? decoder.decode(SpaServiceModel.self, from: data ?? Data())
                    self.spaServiceData = model?.data ?? []
                    print("API Response data \(self.spaServiceData)")
                    self.selectedId = 0
                    self.typeCollection.reloadData()
                }
                
            }
            else
            {
                Utilities.showAlert("", message: "Internal server error")
                return
            } 
        }
  }
    @objc func btnTapped(_ sender: UIButton) {
        self.spaDetail = self.subService[sender.tag].spaSubCategories
        //self.navigationController?.viewControllers.removeAll()
        let newStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let VC = newStoryBoard.instantiateViewController(withIdentifier: "SpaServicesDetail") as! SpaServicesDetail 
        VC.spaCategory = self.subService[sender.tag]
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

extension SpaServices: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.typeCollection
        {
            return self.spaServiceData.count
        }
        else
        {
            return self.subService.count
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.typeCollection
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCollectionCell
            cell.titleLbl.text = self.spaServiceData[indexPath.item].type ?? ""
            cell.titleLbl.adjustsFontSizeToFitWidth = true
            cell.containerVw.circleCorner = true
            if indexPath.item == selectedId
            {
                cell.containerVw.backgroundColor = Utilities.UIColorFromHex(hex: "#333333")
                cell.titleLbl.textColor = UIColor.white
                self.subService = self.spaServiceData[selectedId].spaCategories ?? []
                self.subvaluesCollection.reloadData()
            }
            else
            {
                cell.containerVw.backgroundColor = Utilities.UIColorFromHex(hex: "#DBDBDB")
                cell.titleLbl.textColor = UIColor.black
            }
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SubValuesCell
            cell.titleLBL.text = self.subService[indexPath.item].name ?? ""
            cell.titleLBL.adjustsFontSizeToFitWidth = true
            cell.image.cornerRadius = 5
            cell.viewDetailBtn.tag = indexPath.item
            cell.viewDetailBtn.addTarget(self, action: #selector(btnTapped(_:)), for: .touchUpInside)
            return cell
        }
    }
}

extension SpaServices: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.typeCollection
        {
            selectedId = indexPath.item
            self.titleStr =  self.spaServiceData[selectedId].type ?? ""
            collectionView.reloadData()
        }
    }
}

extension SpaServices: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.typeCollection
        {
            return CGSize(width: UIScreen.main.bounds.width / 3 - 10, height: 60)
        }
        else
        {
            return CGSize(width: UIScreen.main.bounds.width / 2 - 10, height: UIScreen.main.bounds.width / 2 - 10)
        }
    }

//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) //.zero
//    }
//
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
}
