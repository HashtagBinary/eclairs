//
//  SaveSpaOrderVc.swift
//  Eclair
//
//  Created by iOS Indigo on 18/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SaveSpaOrderVc: UIViewController {

    @IBOutlet weak var gifimg: UIImageView!
    @IBOutlet weak var doneButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let jeremyGif = UIImage.gifImageWithName("gif_thanks")
        self.gifimg.image = jeremyGif
        self.doneButton.cornerRadius = 5.0
    }

    @IBAction func donbtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
