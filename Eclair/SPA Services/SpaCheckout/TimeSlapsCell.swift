//
//  TimeSlapsCell.swift
//  Eclair
//
//  Created by iOS Indigo on 14/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class TimeSlapsCell: UICollectionViewCell {
    
    @IBOutlet weak var timeSlaps: UILabel!
    @IBOutlet weak var userLBL: UILabel!
    @IBOutlet weak var containerVw: UIView!
}
