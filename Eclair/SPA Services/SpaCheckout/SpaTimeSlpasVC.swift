//
//  SpaTimeSlpasVC.swift
//  Eclair
//
//  Created by iOS Indigo on 14/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol timeSlapsDelegate {
    func selectedvalues(list : [JSON])
}

class SpaTimeSlpasVC: UIViewController {
    @IBOutlet weak var slapsCollection: UICollectionView!
    var delegate : timeSlapsDelegate?
    var slotsList = [JSON]()
    var selectedSlot = ""
    var slotsRequired = 0
    var selectedListValues : JSON?
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSlots()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okbtn(_ sender: Any) {
        if selectedIndex == -1
        {
            Utilities.showAlert("", message: "Please select any time")
            return
        }
        else
        {
            let total = (selectedIndex + slotsRequired) - 1
            var selectedSlots = [JSON]()
            for i in selectedIndex...total
            {
                selectedSlots.append(slotsList[i])
            }
            self.delegate?.selectedvalues(list: selectedSlots)
            self.dismiss(animated: true, completion: nil)
        }
    }
    //MARK:- API Calling
    
    func getSlots()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kSpaGetSlots) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            self.slotsList = json?.arrayValue ?? []
            print("Slots Array \(self.slotsList)")
            self.slapsCollection.reloadData()
    }
  }
    
}
extension SpaTimeSlpasVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slotsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TimeSlapsCell
        cell.timeSlaps.text = slotsList[indexPath.item]["value"].string ?? ""
        cell.userLBL.text = slotsList[indexPath.row]["description"].string ?? "...."
        
        
        if selectedIndex != -1
        {
            let selectedIndexes = (self.selectedIndex + slotsRequired) - 1
            if selectedIndexes < slotsList.count
            {
                if indexPath.row >= selectedIndex && indexPath.row <= selectedIndexes
                {
                    cell.containerVw.borderColor = .black
                    cell.containerVw.borderWidth = 1
                    cell.containerVw.backgroundColor = .lightGray
                }
                else
                {
                    cell.containerVw.borderColor = .white
                    cell.containerVw.borderWidth = 0
                    cell.containerVw.backgroundColor = .white
                }
            }
            else
            {
                cell.containerVw.borderColor = .white
                cell.containerVw.borderWidth = 0
                cell.containerVw.backgroundColor = .white
            }
        }
        else
        {
            cell.containerVw.borderColor = .white
            cell.containerVw.borderWidth = 0
            cell.containerVw.backgroundColor = .white
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedListValues = slotsList[indexPath.row]
//        delegate?.selectedvalues(key: slotsList[indexPath.item]["key"].int ?? 0, value: slotsList[indexPath.item]["value"].string ?? "", list: slotsList)
//        self.dismiss(animated: true, completion: nil)
        
    
        let selectedIndexes = (indexPath.row + slotsRequired) - 1
        if selectedIndexes >= slotsList.count
        {
            selectedIndex = -1
            collectionView.reloadData()
            Utilities.showAlert("", message: "Sorry slots can't be selected")
        }
        else
        {
            selectedIndex = indexPath.row
            collectionView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
    return CGSize(width: UIScreen.main.bounds.width / 3 - 30, height: 130)
    }
}
