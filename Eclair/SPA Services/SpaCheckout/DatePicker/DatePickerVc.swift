//
//  DatePickerVc.swift
//  Eclair
//
//  Created by iOS Indigo on 16/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class DatePickerVc: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var delegate : GetDateDelegate?
    var selectedate = Date()
    var pickerType = ""
    var minimumFromCurrent = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        okButton.cornerRadius = 5
        
        datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        if #available(iOS 14, *) {
                datePicker.preferredDatePickerStyle = .inline
        }
        else{
            if #available(iOS 13.4, *) {
                datePicker.preferredDatePickerStyle = .wheels
            }
        }
        if pickerType == "Time&Date"
        {
            datePicker.datePickerMode = .dateAndTime
        }
        let date = Date()
        
        if self.minimumFromCurrent == "greater"
        {
            datePicker.minimumDate = date
        }
        else if self.minimumFromCurrent == "Less"
        {
            datePicker.maximumDate = date
        }
    }
    @IBAction func cancelbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okbtn(_ sender: Any) {
        
        
        delegate?.getSelectedDate(date: "\(selectedate)")
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        print(datePicker.date)
        self.selectedate = datePicker.date
        //delegate?.getSelectedDate(date: "\(datePicker.date)")
    }

}
protocol GetDateDelegate {
    func getSelectedDate(date : String)
}
