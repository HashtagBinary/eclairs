//
//  SpaCheckout.swift
//  Eclair
//
//  Created by iOS Indigo on 12/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import FirebaseFirestore

class SpaCheckout: UIViewController, UITextViewDelegate {
    
    struct timeSlot : Codable {
        var id : Int
        var value : String
    }
    
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var selectedTimeLbl: UILabel!
    @IBOutlet weak var dateView: AroundShadow!
    @IBOutlet weak var timeVw: AroundShadow!
    @IBOutlet weak var dateContainerVw: UIView!
    @IBOutlet weak var dateStack: UIStackView!
    @IBOutlet weak var datecontainerHeight: NSLayoutConstraint!
    @IBOutlet weak var counterButton: NumberButton!
    @IBOutlet weak var titleVw: UIView!
    @IBOutlet weak var slotsDescriptionLbl: UILabel!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var haveVoucherButton: UIButton!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var singleRoomButton: UIButton!
    @IBOutlet weak var separateRoomButton: UIButton!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var amountStack: UIStackView!
    @IBOutlet weak var removeVoucherButton: UIButton!
    @IBOutlet weak var discountAmountLbl: UILabel!
    @IBOutlet weak var bottomVwHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentVw: UIView!
    @IBOutlet weak var paymentTypelbl: UILabel!
    @IBOutlet weak var tipLbl: UILabel!
    @IBOutlet weak var tipVw: UIView!
    @IBOutlet weak var removeTipButton: UIButton!
    @IBOutlet weak var addTipButton: UIButton!
    @IBOutlet weak var voucherButtonsVw: UIView!
    @IBOutlet weak var vatTitle: UILabel!
    @IBOutlet weak var voucherDiscountVw: UIView!
    
    var gendar = "male"
    var roomType = "single room"
    var titleStr = ""
    var price = "0"
    var itemId = "-1"
    var timeSlaps = ""
    var selectedDate = ""
    var statusTitle = ""
    var voucherCode = ""
    var vatAmount = "0"
    var total = "0"
    var slotsRequired = 0
    var additionalTime = 0
    var selectedTimeKeys = ""
    var selectTimeValues = ""
    var paymentType = ""
    var tipAmuount : Float = 0.0
    var voucherAmount = 0.0
    var timeSlotsJson = [timeSlot]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setupVw()
    }
    func setupVw()
    {
        let Datetap = UITapGestureRecognizer(target: self, action: #selector(self.handleDateTap(_:)))
        dateView.addGestureRecognizer(Datetap)
        let Timetap = UITapGestureRecognizer(target: self, action: #selector(self.handleTimeTap(_:)))
        timeVw.addGestureRecognizer(Timetap)
        let paymentTap = UITapGestureRecognizer(target: self, action: #selector(self.handlePaymentTap(_:)))
        paymentVw.addGestureRecognizer(paymentTap)
        self.dateStack.arrangedSubviews[2].isHidden = true
        self.dateStack.arrangedSubviews[3].isHidden = true
        self.tipVw.isHidden = true
        self.removeTipButton.isHidden = true
        self.datecontainerHeight.constant = 140
        self.descriptionTxt.delegate = self
        descriptionTxt.text = "Write Description"
        vatTitle.text = "VAT \(Constants.kSpaProductVatPercentage)%"
        descriptionTxt.textColor = UIColor.lightGray
        self.haveVoucherButton.circleCorner = true
        self.haveVoucherButton.borderWidth = 2
        self.haveVoucherButton.borderColor = Utilities.UIColorFromHex(hex: "#000000")
        self.removeVoucherButton.circleCorner = true
        self.removeVoucherButton.borderWidth = 2
        self.removeVoucherButton.borderColor = Utilities.UIColorFromHex(hex: "#000000")
        self.voucherButtonsVw.isHidden = true
        self.removeTipButton.circleCorner = true
        self.removeTipButton.borderWidth = 2
        self.removeTipButton.borderColor = Utilities.UIColorFromHex(hex: "#000000")
        self.addTipButton.circleCorner = true
        self.addTipButton.borderWidth = 2
        self.addTipButton.borderColor = Utilities.UIColorFromHex(hex: "#000000")
        self.namelbl.text = self.titleStr
        
        self.pricelbl.text = self.price
        self.timelbl.text = self.timeSlaps
        self.checkoutButton.cornerRadius = 5
        self.cancelButton.cornerRadius = 5
        self.cancelButton.borderColor = Utilities.UIColorFromHex(hex: "#333333")
        self.cancelButton.borderWidth = 1
        self.voucherDiscountVw.isHidden = true
        self.bottomVwHeight.constant = 530
        if additionalTime <= 0
        {
            self.counterButton.maxValue = additionalTime
        }
        self.getVoucher()
    }
   
    @objc func handleDateTap(_ sender: UITapGestureRecognizer? = nil) {
        self.performSegue(withIdentifier: "date", sender: self)
    }
    @objc func handlePaymentTap(_ sender: UITapGestureRecognizer? = nil) {
        self.performSegue(withIdentifier: "payment", sender: self)
    }
    @objc func handleTimeTap(_ sender: UITapGestureRecognizer? = nil) {
        if self.selectedDate.isEmpty == true
        {
            Utilities.showAlert("", message: "Please select date first")
            return
        }
        self.performSegue(withIdentifier: "time", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "date"
        {
            let VC = segue.destination as! DatePickerVc
            VC.delegate = self
            VC.minimumFromCurrent = "greater"
        }
        else if segue.identifier == "time"
        {
            let VC = segue.destination as! SpaTimeSlpasVC
            VC.delegate = self
            VC.slotsRequired = self.slotsRequired
        }
        else if segue.identifier == "payment"
        {
            let vc = segue.destination as! PaymentVc
            vc.delegate = self
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent
        {
            Gvoucher_Code = ""
            GisVoucher = false
        }
    }
    @IBAction func malebtn(_ sender: Any) {
        self.gendar = "male"
        UIView.transition(with: maleButton!, duration: 1.5, options: .transitionCrossDissolve, animations: {
            self.maleButton.setImage(UIImage(named: "radioChecked"), for: .normal)
            }, completion: nil)
        self.femaleButton.setImage(UIImage(named: "radioUnchecked"), for: .normal)
    }
    
    @IBAction func cancelbtn(_ sender: Any) {
        
        let alert = UIAlertController(title: "Cancel", message: "Are you sure you want to cancel this order?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func voucherbtn(_ sender: Any) {
        
        if self.selectedDate.isEmpty == true
        {
            Utilities.showAlert("", message: "Please select date first")
            return
        }
        let newStoryBoard : UIStoryboard = UIStoryboard(name: "FnB", bundle:nil)
        let VC = newStoryBoard.instantiateViewController(withIdentifier: "VoucherVC") as! VoucherVC
        VC.delegate = self
        self.present(VC, animated: true, completion: nil)
    }
    @IBAction func femalebtn(_ sender: Any) {
        self.gendar = "female"
        UIView.transition(with: femaleButton!, duration: 1.5, options: .transitionCrossDissolve, animations: {
            self.femaleButton.setImage(UIImage(named: "radioChecked"), for: .normal)
                }, completion: nil)
        self.maleButton.setImage(UIImage(named: "radioUnchecked"), for: .normal)
    }
    @IBAction func singleRoombtn(_ sender: Any) {
        //Single
        self.roomType = "single room"
        UIView.transition(with: singleRoomButton!, duration: 1.5, options: .transitionCrossDissolve, animations: {
            self.singleRoomButton.setImage(UIImage(named: "radioChecked"), for: .normal)
                }, completion: nil)
        self.separateRoomButton.setImage(UIImage(named: "radioUnchecked"), for: .normal)
    }
    
    @IBAction func removetip(_ sender: Any){
        
        let tot = self.total.toDouble() ?? 0.0
        let totWithoutTip = tot - Double(self.tipAmuount)
        self.total = "\(totWithoutTip)"
        self.totalLbl.text = "\(totWithoutTip) \(Constants.CURRENCY_SYMBOL)"
        self.tipAmuount = 0.0
        self.tipLbl.text = "\(0.0) \(Constants.CURRENCY_SYMBOL)"
        self.tipVw.isHidden = true
        self.removeTipButton.isHidden = true
        self.addTipButton.isHidden = false
    }
    
    @IBAction func addtipbtn(_ sender: Any) {
        let newStoryBoard : UIStoryboard = UIStoryboard(name: "FnB", bundle:nil)
        let VC = newStoryBoard.instantiateViewController(withIdentifier: "TipViewController") as! TipViewController
        VC.delegate = self
        self.present(VC, animated: true, completion: nil)
    }
    @IBAction func separateRoombtn(_ sender: Any) {
        self.roomType = "couple room"
        UIView.transition(with: separateRoomButton!, duration: 1.5, options: .transitionCrossDissolve, animations: {
            self.separateRoomButton.setImage(UIImage(named: "radioChecked"), for: .normal)
                }, completion: nil)
        self.singleRoomButton.setImage(UIImage(named: "radioUnchecked"), for: .normal)
    }
    
    func getVoucher() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let parameters = [
            "ecode" : Constants.ecode
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBGetVoucherCode, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let json = try? JSON(data:data!)
            let response = json?["response"].string ?? "false"
            if response == "true"
            {
                let voucherCode = json?["voucherCode"].string ?? ""
                if voucherCode.isEmpty == false
                {
                    Gvoucher_Code = voucherCode
                    GisVoucher = true
                    self.voucherButtonsVw.isHidden = false
                    self.haveVoucherButton.isHidden = false
                    self.removeVoucherButton.isHidden = true
                }
                else
                {
                    Gvoucher_Code = ""
                    GisVoucher = false
                    self.voucherButtonsVw.isHidden = true
                }
            }
        }
    }
    func getOrderID() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)

        let parameters = [
            "date" : ""
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kSpaGetOrderID, parameters: nil, method: .get) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let json = try? JSON(data:data!)
            let response = json?["response"].string ?? "false"
            
            if response == "true"
            {
                
            }
        }
    }
    
    
    
    func submitOrder(order_id: Int) {
        var desc = self.descriptionTxt.text ?? ""
        let jsonData = try? JSONEncoder().encode(self.timeSlotsJson)
        let jsonObject = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if desc == "Write Description"
        {
            desc = ""
        }
        let map = [
            "ecode":  Constants.ecode,
            "item_id": self.itemId,
            "booking_date": self.selectedDate,
            "timeslot":jsonObject as Any,
            "additional_time": "\(self.counterButton.currentValue)",
            "addtional_rate" : "",
            "gender":self.gendar,
            "room_type": self.roomType,
            "description": desc,
            "vouchercode": self.voucherCode,
            "vat" : self.vatAmount,
            "subtotal": self.price,
            "total": self.total,
            "payment_type": self.paymentType,
            "datetime_new": FieldValue.serverTimestamp(),
            "status": "n",
            "device": "Iphone",
            "tip":self.tipAmuount
        ] as [String : Any]
        
        Firestore.firestore().collection(Constants.ServerAPI.FS_SPA_REQUESTS_URL).document(String(order_id)).setData(map) { err in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                
                let alert = UIAlertController(title: "Success", message: "Your Order has been submitted successfully!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                    //self.dbHelper.emptyFnbCart()
                    self.navigationController!.popToRootViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func removeVoucherbtn(_ sender: Any) {
        self.voucherDiscountVw.isHidden = true
        self.voucherAmount = 0.0
        self.CalculateTotalAmount()
        self.getVoucher()
    }
    
    @IBAction func checkoutbtn(_ sender: Any) {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to checkout?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.checkoutApi()
            //self.submitOrder(order_id: 2)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    //MARK:- API Calling
    
//    func submitAPI()
//    {
//        ActivityIndicatorSingleton.StartActivity(myView: self.view)
//        var desc = self.descriptionTxt.text ?? ""
//        if desc == "Write Description"
//        {
//            desc = ""
//        }
//
//
//      let parameters: Parameters = [
//
//        "ecode": Constants.ecode,
//        "itemid": self.itemId,
//        "date": self.selectedDate,
//        "timeslot":self.selectedTimeKeys,
//        "additionaltime": "\(self.counterButton.currentValue)",
//        "timeslotValues":self.selectTimeValues,
//        "gender":self.gendar,
//        "roomtype": self.roomType,
//        "description": desc,
//        "vouchercode": self.voucherCode,
//        "vat" : self.vatAmount,
//        "subtotal": self.price,
//        "total": self.total,
//        "paymenttype": self.paymentType
//                    ]
//        print("parameter \(parameters)")
//        AlamorfireSingleton.postCall(serviceName: Constants.ServerAPI.kSpaSaveRequest, parameters: parameters) { (json:JSON?, error:NSError?) in
//            if error != nil {
//                   print(error?.localizedDescription ?? "Error")
//                ActivityIndicatorSingleton.StopActivity(myView: self.view)
//                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
//                return
//            }
//            ActivityIndicatorSingleton.StopActivity(myView: self.view)
//            print("json value\(json!)")
//            let response = json?["response"].bool ?? false
//            if response == false
//            {
//                Utilities.showAlert("", message: json?["message"].string ?? Constants.ErrorMessage.kCommanErrorMessage)
//                return
//            }
//            else
//            {
//                let newStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                let VC = newStoryBoard.instantiateViewController(withIdentifier: "SaveSpaOrderVc") as! SaveSpaOrderVc
//                self.present(VC, animated: true, completion: nil)
//            }
//
//       }
//   }
    func checkoutApi()
    {
        if (selectedDate.isEmpty == true) {
            Utilities.showAlert("", message: "Date is not selected!")
            return
        }
        if self.selectTimeValues.isEmpty == true
        {
            Utilities.showAlert("", message: "Time slots not selected!")
            return
        }
        if self.paymentType.isEmpty == true
        {
            Utilities.showAlert("", message: "Payment Type is not selected!")
            return
        }
        var desc = self.descriptionTxt.text ?? ""
        if desc == "Write Description"
        {
            desc = ""
        }
        
        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        jsonObject.setValue(Constants.ecode, forKey: "ecode")
        jsonObject.setValue(self.itemId, forKey: "itemid")
        jsonObject.setValue(self.selectedDate, forKey: "date")
        jsonObject.setValue(self.selectedTimeKeys, forKey: "timeslot")
        jsonObject.setValue("\(self.counterButton.currentValue)", forKey: "additionaltime")
        jsonObject.setValue(self.selectTimeValues, forKey: "timeslotValues")
        jsonObject.setValue(self.gendar, forKey: "gender")
        jsonObject.setValue(self.roomType, forKey: "roomtype")
        jsonObject.setValue(desc, forKey: "description")
        jsonObject.setValue(self.voucherCode, forKey: "vouchercode")
        jsonObject.setValue(self.vatAmount, forKey: "vat")
        jsonObject.setValue(self.price, forKey: "subtotal")
        jsonObject.setValue(self.total, forKey: "total")
        jsonObject.setValue(self.tipAmuount, forKey: "tip")
        jsonObject.setValue(self.paymentType, forKey: "paymenttype")
        
        
        let jsonData: NSData
        var jsonString = String()

        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String

        } catch _ {
            print ("JSON Failure")
        }
        
        var accountType = ""
        if self.paymentType == "Settle to Villa"
        {
            accountType = Constants.kAccountType
        }
        let parameters = [
            "ecode" : Constants.ecode,
            "paymentType" : self.paymentType,
            "subType":accountType,
            "checkoutData" : jsonString,
            "checkoutFrom" : "spa",
            "totalBill" : self.total,
            "vat":self.vatAmount,
            "discount": 0,
            "totalNetBill":self.price,
            "voucherCode":self.voucherCode,
            "address": "",
            "tip":self.tipAmuount,
            "transactionId":""
        ] as [String: Any]
        
        print("parameter \(parameters)")
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kFnBGetNewOrderId, parameters: parameters) {  (data:Data?, error:NSError?)  in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                let model = try JSONDecoder().decode(FnBCheckoutResponse.self, from: data!)
                if (model.response) {
                    Gvoucher_Code = ""
                    GisVoucher = false
                    let alert = UIAlertController(title: "Success", message: "Your SPA reservation has been submitted successfully!", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                        self.submitOrder(order_id: model.orderId ?? 0)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                    Utilities.showAlert("Response Error", message: model.error ?? "")
                }
            } catch let error as NSError {
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                print(error)
            }
        }
    }
    func CalculateTotalAmount()
    {
        let subTot = self.price.double ?? 0.0
        let discount = self.voucherAmount
        let netAmount = subTot - discount
        let percentage = netAmount * (Double(Constants.kSpaProductVatPercentage)/100)
        self.vatAmount = "\(percentage)"
        let totalAmt = netAmount + percentage
        self.vatLbl.text = "\(percentage) \(Constants.CURRENCY_SYMBOL)"
        let totWithTip = totalAmt + Double(self.tipAmuount)
        self.total = "\(totWithTip)"
        self.totalLbl.text = "\(self.total) \(Constants.CURRENCY_SYMBOL)"
    }
}

extension SpaCheckout:timeSlapsDelegate
{
    func selectedvalues(list: [JSON]) {
        var text = [String]()
        var keys = [String]()
        for i in 0..<list.count
        {
            text.append(list[i]["value"].string ?? "")
            keys.append("\(list[i]["key"])")
            self.timeSlotsJson.append(timeSlot.init(id: list[i]["key"].int ?? -1, value: list[i]["value"].string ?? ""))
        }
        self.selectedTimeLbl.numberOfLines = 0
        self.selectedTimeLbl.text = text.joined(separator: ",")
        self.selectedTimeKeys = keys.joined(separator: ",")
        self.selectTimeValues = self.selectedTimeLbl.text ?? ""
    }
}
extension SpaCheckout : GetDateDelegate{
    func getSelectedDate(date: String) {
        self.selectedDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "dd - MM - yyyy")
        let lblDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: Constants.DateFormate)
        self.datelbl.text = "Date : " + lblDate
        self.subTotalLbl.text = self.price + " \(Constants.CURRENCY_SYMBOL)"
        
//        let price = self.price.toDouble() ?? 0.0
//        let percentage = price * (Double(Constants.kSpaProductVatPercentage)/100)
        
//        let totalAmt = price + percentage
//        self.vatAmount = "\(percentage)"
//        self.vatLbl.text = "\(percentage) \(Constants.CURRENCY_SYMBOL)"
//        self.totalLbl.text = "\(totalAmt) \(Constants.CURRENCY_SYMBOL)"
//        self.total = "\(totalAmt)"
        self.CalculateTotalAmount()
    }
}
extension SpaCheckout : VoucherDelegate{
    func getVoucherCode(amount: String, voucherCode: String) {
        
        self.voucherAmount = Double(amount) ?? 0.0
        self.voucherCode = voucherCode
        self.discountAmountLbl.text = "- "+amount + " \(Constants.CURRENCY_SYMBOL)"
        self.voucherDiscountVw.isHidden = false
        self.removeVoucherButton.isHidden = false
        self.haveVoucherButton.isHidden = true
//        self.subTotalLbl.text = self.price + " \(Constants.CURRENCY_SYMBOL)"
//        let price = self.price.toDouble() ?? 0.0
//        let discount = amount.toDouble() ?? 0.0
//        let netAmount = price - discount
//        let percentage = netAmount * (Double(Constants.kSpaProductVatPercentage)/100)
//        self.vatAmount = "\(percentage)"
//        let totalAmt = netAmount + percentage
//        self.vatLbl.text = "\(percentage) \(Constants.CURRENCY_SYMBOL)"
//        self.totalLbl.text = "\(totalAmt) \(Constants.CURRENCY_SYMBOL)"
//        self.total = "\(totalAmt)"
        self.CalculateTotalAmount()
    }
}
extension SpaCheckout : PaymentDelegate
{
    func getType(type: String, id: Int, tag: Int) {
        print("type \(type),id \(id) tag \(tag)")
        self.paymentType = type
        self.paymentTypelbl.text = type
    }
}
extension SpaCheckout : TipDelegate
{
    func addTip(amount: Float) {
        print("Tip amount \(amount)")
        self.tipAmuount = amount
        self.tipLbl.text = "\(amount) \(Constants.CURRENCY_SYMBOL)"
        self.tipVw.isHidden = false
        self.removeTipButton.isHidden = false
        self.addTipButton.isHidden = true
//        let total = self.total.toDouble() ?? 0.0
//        let totWithTip = total + Double(amount)
//        self.total = "\(totWithTip)"
//        self.totalLbl.text = "\(self.total) \(Constants.CURRENCY_SYMBOL)"
        self.CalculateTotalAmount()
    }
}
