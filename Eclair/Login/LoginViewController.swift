//
//  LoginViewController.swift
//  Eclair
//
//  Created by iOS Indigo on 16/09/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import SwiftyJSON

class LoginViewController: UIViewController {
    
    @IBOutlet weak var eCode_textField: UITextField!
    @IBOutlet weak var videoVw: UIView!
    @IBOutlet weak var fieldsStack: UIStackView!
    @IBOutlet weak var password_textField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var stackContainerVw: UIView!
    @IBOutlet weak var poweredLabel: UILabel!
    @IBOutlet weak var passButton: UIButton!
    
    var player = AVPlayer()
    fileprivate var playerObserver: Any?
    
    var iconClick = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        if Constants.ServerAPI.kBaseURL == "https://api.devhashtag.com/"
        {
            eCode_textField.text = "m00497"
            password_textField.text = "abc@123"
        }        

        let tap = UITapGestureRecognizer(target: self, action: #selector(onPoweredBy))
        poweredLabel.isUserInteractionEnabled = true
        poweredLabel.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func willEnterForeground() {
        self.player.play()
    }
    
    func setupView()
    {
        self.playVideo(from: "loginVideo.mp4")
        self.stackContainerVw.cornerRadius = 8
        self.stackContainerVw.borderWidth = 1
        self.stackContainerVw.borderColor = UIColor.white
        
        eCode_textField.attributedPlaceholder = NSAttributedString(string: "Indigo Code", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        password_textField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        loginButton.cornerRadius = 5
    }
    
    @objc func onPoweredBy(sender: UILabel) {
        let myUrl = "https://aintechnology.com"
        if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func showHideAction(_ sender: Any) {
        
        if(iconClick == true) {
            password_textField.isSecureTextEntry = false
            self.passButton.setImage(UIImage(named: "openEye"), for: .normal)
            //closeEye  openEye
        } else
        {
           password_textField.isSecureTextEntry = true
            self.passButton.setImage(UIImage(named: "closeEye"), for: .normal)
        }

            iconClick = !iconClick
    }
    @IBAction func login_touchUpInside(_ sender: Any) {
        if((eCode_textField.text ?? "").trimString().elementsEqual(""))
        {
            Utilities.showAlert("", message: "Please enter ecode")
            return
        }
        
        else if((password_textField.text ?? "").trimString().elementsEqual(""))
        {
            Utilities.showAlert("", message: "Please enter password")
            return
        }
        else{
            self.loginAPI()
        }
    }
    //MARK:- API Calling
    
    func loginAPI()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters: Parameters = [
            "grant_type" : "password",
            "type" : "tm",
            "username" : self.eCode_textField.text ?? "",
            "password" : self.password_textField.text ?? "",
        ]
        print("parameter \(parameters)")
        AlamorfireSingleton.postCall(serviceName: Constants.ServerAPI.kSignIn, parameters: parameters) { (json:JSON?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            // ActivityIndicatorSingleton.StopActivity(myView: self.view)
            print("json value\(json!)")
            let error = json?["error"].string ?? ""
            if error.isEmpty == false
            {
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert("", message: json?["error_description"].string ?? Constants.ErrorMessage.kCommanErrorMessage)
                return
            }
            else
            {
                let token = json?["access_token"].string ?? ""
                UserDefaults.standard.set(token, forKey: "token")
                Constants.ecode = self.eCode_textField.text ?? ""
                self.profileData(ecode: Constants.ecode)
            }
        }
    }
    
    func profileData(ecode : String)
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters: Parameters = [
            "ecode" : ecode
        ]
        print("parameter \(parameters)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kProfileData, parameters: parameters) { (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("json value\(json!)")
            let response = json?["response"].bool ?? false
            
                if response == true
                {
                    let data = json?["data"] ?? []
                    let config = json?["config"] ?? []
                    let fullname = data["fullName"].string ?? ""
                    let memberType = data["type"].string ?? ""
                    let isMain = data["isMain"].bool ?? false
                    let tenantsSlots = config["tenantSlots"].int ?? 0
                    let guestsSlots = config["guestSlots"].int ?? 0
                    let fnbVat = config["fnbVat"].float ?? 0
                    let spaVat = config["spaVat"].float ?? 0
                    let isBlackList = data["isBlackList"].bool ?? false
                    let isDisable = data["isDisable"].bool ?? false
                    let accountType = data["accountType"].string?.trimString() ?? ""
                    let ahmsAgreement = json?["ahmsAgreement"].bool ?? false
                    let serverVersion = config["iosVersion"].string ?? "1.0.0"
                    let isForceUpdate = config["isForceUpdate"].bool ?? false
                    
                    Constants.kIsMainMember = isMain
                    Constants.kFnBVatPercentage = fnbVat
                    Constants.kSpaProductVatPercentage = spaVat
                    Constants.tenantsSlots = tenantsSlots
                    Constants.guestSlots = guestsSlots
                    Constants.kAccountType = accountType
                    Constants.kMemberType = memberType
                    Constants.ahmsAgreement = ahmsAgreement
                    
                    UserDefaults.standard.set(fullname, forKey: "fullName")
                    UserDefaults.standard.set(data["eclareName"].string ?? "", forKey: "eclareName")
                    UserDefaults.standard.set(memberType, forKey: "memberType")
                    
                    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
                    
                    let serverVersionNo = Int(serverVersion.replacingOccurrences(of: ".", with: "")) ?? 100
                    let appVersionNo = Int(appVersion.replacingOccurrences(of: ".", with: "")) ?? 101
                    //self.performSegue(withIdentifier: "animator", sender: nil)
                    if appVersionNo < serverVersionNo
                    {
                        let alert = UIAlertController(title: "Update Available", message: "A new version of Indigobhur is available. Please update to version \(serverVersion) now.", preferredStyle: .alert)

                        if isForceUpdate == false
                        {
                            alert.addAction(UIAlertAction(title: "Skip", style: .default, handler: { action in

                                if isBlackList == false && isDisable == false
                                {
                                    self.performSegue(withIdentifier: "animator", sender: nil)
                                }
                                else
                                {
                                    if isDisable == true
                                    {
                                        Utilities.showAlert("", message: "Your account is disabled.Kindly contact to admin")
                                        return
                                    }
                                    if isBlackList == true
                                    {
                                        Utilities.showAlert("", message: "Your account is blacklisted.Kindly contact to admin")
                                        return
                                    }
                                }
                            }))
                            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { action in

                                if let url = URL(string: Constants.kAppStoreLink) {
                                        if #available(iOS 10, *){
                                            UIApplication.shared.open(url)
                                        }else{
                                            UIApplication.shared.openURL(url)
                                        }
                                    }
                             }))
                        }
                        else
                        {
                            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { action in

                                if let url = URL(string: Constants.kAppStoreLink) {
                                        if #available(iOS 10, *){
                                            UIApplication.shared.open(url)
                                        }else{
                                            UIApplication.shared.openURL(url)
                                        }
                                    }
                             }))
                        }
                           self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        if isBlackList == false && isDisable == false
                        {
                            self.performSegue(withIdentifier: "animator", sender: nil)
                        }
                        else
                        {
                            if isDisable == true
                            {
                                Utilities.showAlert("", message: "Your account is disabled.Kindly contact to admin")
                                return
                            }
                            if isBlackList == true
                            {
                                Utilities.showAlert("", message: "Your account is blacklisted.Kindly contact to admin")
                                return
                            }
                        }
                    }
                }
                else
                {
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                    Utilities.showAlert("", message: json?["msg"].string ?? Constants.ErrorMessage.kServerErrorMessage)
                    return
                }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "animator"
        {
            let vc = segue.destination as! AnimatorVC
            vc.ecode = self.eCode_textField.text ?? ""
        }
    }
    //MARK:- video methods
    private func playVideo(from file:String)
    {
        let file = file.components(separatedBy: ".")
        
        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            debugPrint( "\(file.joined(separator: ".")) not found")
            return
        }
        
        self.player = AVPlayer(url: URL(fileURLWithPath: path))
        self.player.isMuted = true
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        playerLayer.videoGravity = .resizeAspectFill
        self.videoVw.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.videoVw.layer.addSublayer(playerLayer)
        self.player.play()
        self.loopVideo(videoPlayer: self.player)
    }
    func loopVideo(videoPlayer: AVPlayer) {
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            videoPlayer.seek(to: CMTime.zero)
            videoPlayer.play()
        }
    }
}
