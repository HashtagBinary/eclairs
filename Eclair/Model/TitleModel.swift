//
//  TitleModel.swift
//  Eclair
//
//  Created by Zohaib Naseer on 8/13/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation


struct TitleModel : Codable {

    let titlesList : [TitlesList]?
    let message : String?
    let response : Bool?
    let error : String?
    let currentTitle : String?
    let allTitlesList : [AllTitlesList]?
}
struct TitlesList : Codable {

    let titleId : Int?
    let title : String?
    let ecode : String?
}
struct AllTitlesList : Codable {

    let titleId : Int?
    let title : String?
    var isAchieve : Bool?
}
