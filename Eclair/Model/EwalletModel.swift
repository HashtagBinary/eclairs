//
//  EwalletModel.swift
//  Eclair
//
//  Created by Sohail on 22/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

struct EwalletModel : Decodable {
    var response: Bool?
    var error: String?
    
    var totalCoins: Double?
    var lastCredit: Double?
    var lastDebit: Double?
    var totalGrains: Int?
}
