//
//  SpaHistoryModel.swift
//  Eclair
//
//  Created by Indigo on 13/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct SpaHistoryModel : Codable {

    let list : [SpaHistoryModelList]?
    let message : String?
    let response : Bool?
}
struct SpaHistoryModelList : Codable {

    let additionaltime : String?
    let bookingdate : String?
    let date : String?
    let description : String?
    let ecode : String?
    let gender : String?
    let id : Int?
    let itemid : Int?
    let items : SpaHistoryModelItem?
    let paymenttype : String?
    let roomtype : String?
    let subtotal : Int?
    let timeslot : String?
    let total : Float?
    let vat : Float?
    let vouchercode : String?
}
struct SpaHistoryModelItem : Codable {

    let additionalrate : Int?
    let catname : String?
    let description : String?
    let id : Int?
    let itemname : String?
    let price : Int?
    let subcatname : String?
    let type : String?
}
