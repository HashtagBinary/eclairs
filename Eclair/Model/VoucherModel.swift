//
//  VoucherModel.swift
//  Eclair IOS
//
//  Created by Sohail on 11/11/2020.
//

struct VoucherModel: Decodable {
    var voucherId: Int
    var voucherName: String
    var expiryDate: String
    var description: String
    var amount: Float
    var items: String
    var quantity: Int?
}
