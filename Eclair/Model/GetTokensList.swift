//
//  GetTokensList.swift
//  Eclair
//
//  Created by iOS Indigo on 30/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct GetTokens : Codable {

    let error : String?
    let message : String?
    let response : Bool?
    let tokensList : [GetTokensList]?
    let totalTokens : Int?

}
struct GetTokensList : Codable {

    let imageUrl : String?
    let quantity : Int?
    let tokenId : Int?
    let tokenType : String?

}
