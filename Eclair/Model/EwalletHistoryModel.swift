//
//  EwalletHistoryModel.swift
//  Eclair
//
//  Created by Indigo on 27/04/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation


struct EwalletHistoryModel : Decodable {
    
    var TransactionId: String?
    var TotalBalance: Double?
    var Status: String?
    var RefId: String?
    var Description: String?
    var TimeStamp: String?
    var CTID: Int?
    var Credit: Double?
    var Ecode: String?
    var Date: String?
    var `Type`: String?
    var Debit: Double?
}

struct EwalletFilterModel : Decodable {
   var response: Bool?
   var message: String?
   var error: String?
   var detail: String?
   var list: [EwalletFilterList]
}
struct EwalletFilterList : Decodable
{
    var TransactionId: String?
    var TotalBalance: Double?
    var Status: String?
    var RefId: String?
    var Description: String?
    var TimeStamp: String?
    var CTID: Int?
    var Credit: Double?
    var Ecode: String?
    var Date: String?
    var `Type`: String?
    var Debit: Double?
    var StrDate : String?
}
