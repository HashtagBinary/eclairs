//
//  Lost_FoundHistoryModel.swift
//  Eclair
//
//  Created by Indigo on 19/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct lostNfoundHistoryModel : Codable {

    let error : String?
    let response : Bool?
    let message : String?
    let complaints : [complaints]?
}
struct complaints : Codable {
    let Id : Int?
    let Photo :String?
    let ReturnBy : String?
    let Title : String?
    let SubmitDate : String?
    let Ecode : String?
    let Status : String?
    let Description : String?
    let Note : String?
    let FoundDate : String?
    let StrDate : String?
}
