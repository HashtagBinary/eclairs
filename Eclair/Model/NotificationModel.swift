//
//  NotificationModel.swift
//  Eclair
//
//  Created by Sohail on 05/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Firebase


struct NotificationModel : Codable {
    var documentID : String?
    var body : String?
    var title : String?
    var id :String?
    var datetime : Date?
    var type : Int?
    var is_seen: Bool?
}
