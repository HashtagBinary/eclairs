//
//  AuctionsAPI.swift
//  Eclair
//
//  Created by Bilal on 11/23/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import Foundation


struct AuctionListenerModel: Codable
{
    var auctionId: Int
    var title: String
    var tokenType: String
    var bidRangeMin: Float
    var bidRangeMax: Float
    var startingTime: Double
    var closingTime: Double
    var bidsCount: Int
    var currentBid: Float
    var ecode: String
}


struct AuctionsAPIResponseModel: Codable
{
    var response : Bool?
    var error: String?
    var auctionId: Int?
    var title: String?
    var tokenType: String?
    var bidRangeMin: Int?
    var bidRangeMax: Int?
    var startingTime: CLong?
    var closingTime: CLong?
    var bidsCount: Int?
    var currentBid: Int?
    var isActive: Bool?
    var isSold: Bool?
    var ecode: String?
    
    init()
    {
        response  = false
        error = ""
        auctionId = 0
        title = ""
        tokenType = ""
        bidRangeMin = 0
        bidRangeMax = 0
        startingTime = 0
        closingTime = 0
        bidsCount = 0
        currentBid = 0
        isActive = false
        isSold = false
        ecode = ""
    }
    
}
struct GetAllAuctions: Codable {
    var auctions : [AuctionsAPIModel]
    
    init()
    {
        auctions = [AuctionsAPIModel]()
    }
}
struct AuctionsAPIModel: Codable {
    var response : Bool?
    var error : String?
    var auctionId : Int?
    var imageUrl : String?
    var title : String?
    var tokenType : String?
    var type : String?
    var bidRangeMin : Float?
    var bidRangeMax : Float?
    var startingTime : CLong?
    var closingTime : CLong?
    var bidsCount : Int?
    var currentBid : Float?
    var isActive : Bool?
    var isSold : Bool?
    var ecode : String?
    var timeId: Int?
    var tokenSerialNo: String?
    var houseCut : Int?
    var tokenId : Int?
    var isBuyout : Bool?
    var closingTimeUTC : String?
    var startingTimeUTC : String?
    
    init()
    {
        response = false
        error = ""
        auctionId = 0
        title = ""
        tokenType = ""
        type = ""
        bidRangeMin = 0.0
        bidRangeMax = 0.0
        startingTime = 0
        closingTime = 0
        bidsCount = 0
        currentBid = 0.0
        isActive = false
        isSold = false
        ecode = ""
        timeId = -1
        tokenSerialNo = ""
        houseCut = 0
        tokenId = -1
        isBuyout = false
        startingTimeUTC = ""
        closingTimeUTC = ""
    }
}



