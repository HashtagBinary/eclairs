//
//  XpBoostModel.swift
//  Eclair
//
//  Created by iOS Indigo on 17/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct XpBoostModel : Codable {

    let currentXp : Int?
    let currentlevelId : Int?
    let error : String?
    let message : String?
    let response : Bool?
    let superTokenRequirments : XpBoostModelSuperTokenRequirment?
    let tokensList : [XpBoostModelTokensList]?
    let totalTokens : Int?
    let xpBoost : [XpBoostModelXpBoost]?
}
struct XpBoostModelXpBoost : Codable {

    let discountRate : Int?
    let isDiscountOffer : Bool?
    let rate : Double?
    let xpBoostId : Int?
    let xps : Int?
}
struct XpBoostModelTokensList : Codable {

    let quantity : Int?
    let tokenId : Int?
    let type : String?
}
struct XpBoostModelSuperTokenRequirment : Codable {

    let imageUrl : String?
    let tokenId : Int?
    let tokenType : String?
    let tokens : [XpBoostModelToken]?
}
struct XpBoostModelToken : Codable {

    let `Type` : String?
    let imageUrl : String?
    let tokenId : Int?
}
