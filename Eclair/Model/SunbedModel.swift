//
//  SunbedModel.swift
//  Eclair
//
//  Created by iOS Indigo on 19/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct SunbedModel : Codable {

    let IsAvailable : Bool?
    let Name : String?
    let SlotsList : [SunbedModelSlotsList]?
    let SunbedNo : String?

}

struct SunbedModelSlotsList : Codable {

    let ID : Int?
    let IsAvailable : Bool?
    let Slot : String?
    let SunbedId : String?


}
