//
//  FnbFirestoreModel.swift
//  Eclair
//
//  Created by Indigo on 12/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct FnbFirestoreModel : Codable {

    let address : String?
    let department_id : Int?
    let paymenttype : String?
    let instructions : String?
    let preordertime : String?
    let isdepartment_sheesha : Bool?
    let vatpercentage : Int?
    let device : String?
    let tip : Int?
    let status : String?
    let datetime_new : String?
    let ispreorder : Bool?
    let subtotal : Int?
    let vat : Int?
    let ecode : String?
    let datetime_preparing : String?
    let item_count : Int?
    let total : Int?
    let menuitems : [menuitemsList]?

}
struct menuitemsList : Codable {

    let id : Int?
    let quantity : Int?
    let totalprice : Int?
    let menu_ingredients_list : [menu_ingredients_list]?

}
struct menu_ingredients_list : Codable {

    let totalprice : Int?
    let menuIngredients : [menuIngredients]?

}

struct menuIngredients : Codable {

    let id : Int?
    let totalprice : Int?
    let quantity : Int?

}
