//
//  MaintenanceHistoryModel.swift
//  Eclair
//
//  Created by Indigo on 04/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct maintenanceHistoryModel : Codable {
    
    let response : Bool?
    let msg : String?
    let data : [maintenanceHistoryData]?
}
struct maintenanceHistoryData : Codable {
    let ID : Int?
    let `Type` :String?
    let FullName : String?
    let Address : String?
    let ContactNo : String?
    let Nationality : String?
    let IdentityType : String?
    let CardNo : String?
    let Gender : String?
    let CardPic : String?
    let EmailAddress : String?
    let Description : String?
    let SubmitedBy : String?
    let Status : String?
    let SubmitDate : String?
    let ApproveDate : String?
    let DOB: String?
    let FamilyID : String?
    let Relation:String?
    let Reason: String?
    
    
}
