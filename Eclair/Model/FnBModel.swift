//
//  FnBTrendsModel.swift
//  Eclair
//
//  Created by iOS Indigo on 25/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//


import Foundation

struct FnBTrendsModel : Codable {
    var response : Bool?
    var error : String?
    var dayTrend : [FnBMenuItem] = []
    var monthTrend : [FnBMenuItem] = []
    var weekTrend : [FnBMenuItem] = []
}

struct FnBMenuDepartments : Codable {
    var response : Bool?
    var error : String?
    var depts : [FnBMenuDepartment] = []
    var menuItems : [FnBMenuItem] = []
    var juiceGlasses : [FnBJuiceGlass] = []
    var juiceIces : [FnBJuiceIce] = []
    var juiceFlavours : [FnBJuiceFlavor] = []
    var juiceSweetners : [FnBJuiceSweetner] = []
    var juiceBlendTimes : [FnBJuiceBlendTime] = []
    var juiceSyrups : [FnBJuiceSyrups] = []
}

struct FnBMenuDepartment : Codable {
    var id: Int?
    var title: String?
    var image: String?
    var isSheeshaAvailable: Bool?
    var isJuice: Bool?
}

struct FnBMenuCategories : Codable {
    var response: Bool?
    var error: String?
    var categories: [FnBMenuCategory] = []
}

struct FnBMenuCategory : Codable {
    var id: Int?
    var title: String?
    var moto: String?
    var image: String?
}

struct FnBMenuItems : Codable {
    var response: Bool?
    var error: String?
    var menuItems: [FnBMenuItem] = []
}

struct FnBMenuItem : Codable {
    var available : Bool?
    var calories : Float?
    var categoryId : Int?
    var departmentId : Int?
    var description : String?
    var id : Int?
    var image : String?
    var isSheeshaAvailable : Bool?
    var menu_ingredients : [FnBMenuIngredient] = []
    var moto : String?
    var price : Float?
    var rating : Float?
    var title : String?
    var qty: Int?
}

struct FnBMenuIngredient : Codable {
    var id : Int?
    var title : String?
    var type : Int?
    var retailPrice : Float?
    var menuItemId : Int?
    var defaultValue: Int?
    var price : Float?
    var max : Int?
    var value: Int?
    var image: Int?
    var currentValue: Int?
    var quantity : Int?
    var calories : Int?
    var flag : Bool?
    var totalPrice : Float?
    var unit:String?
    var subIngredients : [FnBSubMenuIngredient] = []
}

struct FnBSubMenuIngredient : Codable {
    var subIngId : Int?
    var title : String?
    var price : Float?
    var MenuIngredient : String?
    var ingredientId :Int?
    
}

struct JuiceIngredients : Codable {
    var response: Bool?
    var error: String?
    var juiceGlass = [FnBJuiceGlass]()
    var juiceIce = [FnBJuiceIce]()
    var juiceFlavours = [FnBJuiceFlavor]()
    var juiceSweetner = [FnBJuiceSweetner]()
    var juiceBlendTimes = [FnBJuiceBlendTime]()
    var juiceSyrups = [FnBJuiceSyrups]()
}

struct FnBJuiceFlavor : Codable {
    var id: Int?
    var flavour: String?
    var percentage: Double?
    var colorCode: String?
   // var departmentId : Int?
}

struct FnBJuiceGlass: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var price: Float?
    var departmentId : Int?
}

struct FnBJuiceIce: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var departmentId : Int?
}

struct FnBJuiceSweetner: Codable {
    var id: Int?
    var title: String?
    var description: String?
}

struct FnBJuiceBlendTime: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var departmentId : Int?
}
struct FnBJuiceSyrups: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var departmentId : Int?
    var color : String?
}
struct FnbJuiceGlass :Codable {
//    var response : Bool?
//    var error : String?
//    var glass : [FnbJuiceGlassObj]
    
    let error : String?
    let glass : [FnbJuiceGlassObj]?
    let response : Bool?

    
}

struct FnbJuiceGlassObj :Codable {
//    var id : Int?
//    var ecode : String?
//    var glass : FnBJuiceCartItem?
//
    let ecode : String?
    let glass : FnBJuiceCartItem?
    let id : Int?
}


struct FnBJuiceCartItem: Codable {
    var id: Int = -1
    var glass_id: Int = -1
    var ice_id: Int = -1
    var sweetner_id: Int = -1
    var blend_id: Int = -1
    var syrup_id :Int = -1
    var flavours = [FnBJuiceFlavor]()
    var totalprice: Double = 0
}
struct FnBJuiceGlassItem: Codable {
    var glass_id: Int = -1
    var ice_id: Int = -1
    var sweetner_id: Int = -1
    var blend_id: Int = -1
    var syrup_id :Int = -1
    var flavours = [FnBJuiceFlavor]()
    var totalprice: Double = 0
}

struct FnBCheckoutResponse: Codable {
    var response: Bool
    var errorCode: Int?
    var orderId: Int?
    var error: String?
}

struct FnBCheckOutMenuItem: Codable {
    var id: Int
    var quantity: Int
    var totalprice: Float
    var menu_ingredients_list = [FnBCheckOutMenuIngredients]()
}

struct FnBCheckOutMenuIngredients: Codable {
    var totalprice: Float
    var menuIngredients = [FnBCheckOutMenuIngredient]()
}

struct FnBCheckOutMenuIngredient: Codable {
    var id: Int
    var quantity: Int
    var totalprice: Float
}

class CartFnbItem {
    var menuitemId: Int = 0
    var menuitemQuantity: Int = 0
    var ingredientLists = [CartIngredientList]()
    
    init(menuitemId: Int, menuitemQuantity: Int) {
        self.menuitemId = menuitemId
        self.menuitemQuantity = menuitemQuantity
    }
    
    func removeItem(position: Int) {
        if (ingredientLists.count > position) {
            ingredientLists.remove(at: position)
        
        }
        
        menuitemQuantity = menuitemQuantity - 1
    }
}

struct CartIngredientList {
    var ingredientList = [CartIngredient]()
}

struct CartIngredient {
    var quantityId: Int?
    var ingredientId: Int?
    var ingredientQuantity: Int?
}
