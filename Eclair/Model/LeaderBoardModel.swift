//
//  LeaderBoardModel.swift
//  Eclair
//
//  Created by Indigo on 03/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct LeaderboardModel: Codable {
    let response: Bool?
    let leaderboards: [Leaderboard]?
    let error: String?
    let message: String?
}

// MARK: - Leaderboard
struct Leaderboard: Codable {
    var img : Data?
    let ecode: String?
    let xp, totalToken: Int?
    let currentTitle: String?
    let anrLevel: Int?
}
