//
//  SupportChatModel.swift
//  Eclair
//
//  Created by Indigo on 16/09/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct supportModel {
    
    var attended_by :String?
    var created_date : String?
    var image : String?
    var media :String?
    var message :String?
    var status : Int?
    var url : String?

}
