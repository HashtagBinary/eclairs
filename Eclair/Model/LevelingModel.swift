//
//  LevelingModel.swift
//  Eclair
//
//  Created by iOS Indigo on 23/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct LevelingModel : Codable {

    let currentXp : Int?
    let currentlevelId : Int?
    let error : String?
    let levelProgress : Int?
    let response : Bool?
    let seasons : [LevelingModelSeason]?
    let totalTokens : Int?
    let currentProgressXp: Int?
    let header: String?
    let remainingProgressXp : Int?
    let rewardsSettings : [LevelingRewardSettings]?
}

struct LevelingModelSeason : Codable {

    let endDate : String?
    let isGuest : Bool?
    let levels : [LevelingModelLevel]?
    let seasonId : Int?
    let startDate : String?
    let title : String?
}

struct LevelingModelLevel : Codable {

    let description : String?
    let levelId : Int?
    let requiredXP : Int?
    let rewards : [LevelingModelReward]?
    let seasonId : Int?
    let timestamp : String?
    let title : Int?
}

struct LevelingModelReward : Codable {

    let quantity : Int?
    let id : Int?
    let title : String?
    let levelNo : Int?
}
struct LevelingRewardSettings : Codable {

    let perk : String?
    let id : Int?
    let title : String?
    let levelNo : Int?
    let isGuestSeason : Bool?
}
