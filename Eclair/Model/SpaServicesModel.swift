//
//  SpaServicesModel.swift
//  Eclair
//
//  Created by iOS Indigo on 10/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//




import Foundation

struct SpaServiceModel : Codable {
    let response : Int?
    let data : [SpaServiceModelData]?
}

struct SpaServiceModelData : Codable {
    let spaCategories : [SpaServiceModelSpaCategory]?
    let type : String?
}

struct SpaServiceModelSpaCategory : Codable {
    let descriptionField : String?
    let id : Int?
    let name : String?
    var spaSubCategories : [SpaServiceModelSpaSubCategory] = []
}

struct SpaServiceModelSpaSubCategory : Codable {
    let catid : Int?
    let id : Int?
    let name : String?
    let spaItemslists : String?
}

struct SpaServiceItem : Codable {
    let response : Int?
    let data : [SpaServiceItemData]?
}

struct SpaServiceItemData : Codable {
    let descriptionField : String?
    let duration : String?
    let id : Int?
    let name : String?
    let price : Double?
    let subid : Int?
    let slotrequired : Int?
    let additionalrate : Int?
}
