//
//  mainFormsItems.swift
//  Eclair
//
//  Created by iOS Indigo on 15/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct mainFormsItems : Codable {

    let Departments : [mainFormsItemsDepartment]?
    let Response : Bool?
}

struct mainFormsItemsDepartment : Codable {

    let category : String?
    let deptId : Int?
    let deptName : String?
    let items : [mainFormsItemsSubItem]?
}
struct mainFormsItemsSubItem : Codable {

    let ID : Int?
    let Name : String?

}
