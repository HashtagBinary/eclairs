//
//  News.swift
//  Eclair
//
//  Created by iMac on 19/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import Foundation

struct News: Codable {
    let key: String?
    let datetime: UInt64?
    let description: String?
    let headline: String?
    let image: String?
    
    let isliked: Bool?
    let likescount: Int?
}

struct Comment: Codable {
    let key: String?
    let datetime: UInt64?
    let txt: String?
    
    let replycount: Int?
}
