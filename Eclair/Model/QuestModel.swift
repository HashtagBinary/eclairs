//
//  QuestModel.swift
//  Eclair
//
//  Created by iOS Indigo on 20/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct QuestModel : Codable {

    let currentQuest : Int?
    let currentXp : Int?
    let currentlevel : Int?
    let error : String?
    let message : String?
    let quests : [QuestModelQuest]?
    let response : Bool?
    let totalTokens : Int?


}
struct QuestModelQuest : Codable {

    let description : String?
    let levelup : Int?
    let questId : Int?
    let questTime : Int?
    let reward : Int?
    let seasonId : Int?
    let title : Int?
    let type : String?


}
