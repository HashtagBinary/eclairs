//
//  TenantsListModel.swift
//  Eclair
//
//  Created by iOS Indigo on 27/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct TenantsListModel : Codable {

    let data : [TenantsListModelData]?
    let msg : String?
    let response : Bool?


}
struct TenantsListModelData : Codable {

    let EclairCode : String?
    let FullName : String?
    let Gender : String?
    let Nationality : String?
    let Relate : String?

}
