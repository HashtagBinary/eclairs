//
//  BidsAPI.swift
//  Eclair
//
//  Created by Bilal on 12/1/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import Foundation

struct BidsApiResponse : Codable
{
    var response : Bool?
    var error : String?
    var bidId : Int?
    var ecode : String?
    var amount : Double?
    var timestamp : CLong?
    var auctionId : Int?
    var currentBid : Float?
    var myLastBid : Float?
    var count : Int?
    
    
    init()
    {
        response = false
        error = ""
        bidId = 0
        ecode = ""
        amount = 0.0
        timestamp = 0
        auctionId = 0
        currentBid = 0.0
        myLastBid  = 0.0
        count = 0
        
    }
}
