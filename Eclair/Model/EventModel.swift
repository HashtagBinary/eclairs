//
//  EventModel.swift
//  Eclair IOS
//
//  Created by Sohail on 02/11/2020.
//

struct EventResponseModel: Decodable {
    var response : Bool
    var msg : String?
    var message : String?
    var data : EventModel?
}

struct EventModel: Decodable {
    var isEligiable: Bool?
    var eventId: Int?
    var mapImage: String?
    var eventName: String?
    var windowDelay: Int?
    var eventStart: String?
    var eventEnd: String?
    var startTime: String?
    var endTime: String?
    var isActive: Bool?
    var extraUnitsAmount: Float?
    var vat: Float?
    var totalAmount: Float?
    var unitList: [EventUnitModel]
}

struct EventUnitModel : Decodable {
    var id: Int?
    var name: String?
    var quantity: Int?
    var rate: Float?
    var extra: Int?
    var extraRate: Float?
}

struct EventEcodeModel: Codable {
    var code: String?
    var name: String?
}
