//
//  villaMaintenanceModel.swift
//  Eclair
//
//  Created by Indigo on 17/03/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct villaMaintenanceModel : Codable {

    let data : [villaMaintenanceModelData]?
    let msg : String?
    let response : Bool?
}
struct villaMaintenanceModelData : Codable {

    let AvailabilityDate : String?
    let CategoryName : String?
    let CreateDate : String?
    let DeptId : Int?
    let DeptName : String?
    let Description : String?
    let ECode : String?
    let ID : Int?
    let ItemId : Int?
    let ItemName : String?
    let Picture : String?
    let Status : String?
    let SubmitDate : String?


}
