//
//  UserModel.swift
//  Eclair
//
//  Created by Sohail on 16/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//


struct UserResponse: Codable {
        var response: Bool?
        var msg: String?
        var name: String?
        var data: UserModel?
        var error : String?
}

struct UserModel: Codable {
        var ecode: String?
        var fullName: String?
        var gender: String?
        var contactNo: String?
        var address: String?
        var rank: String?
        var nationality: String?
        var eclareName: String?
        var cardNo: String?
        var isAvailable: Bool?
        var isBlackList: String?
        var contractStart: String?
        var contractEnd: String?
        var levelTitle: String?
        var remainDays: Int?
        var isMain : Bool?
//        var startMiliSeconds: Int32?
//        var endMiliSeconds: Int32?
        var type: String?
        var accountType: String?
}
