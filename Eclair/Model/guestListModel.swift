//
//  guestListModel.swift
//  Eclair
//
//  Created by iOS Indigo on 27/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import Foundation

struct guestListModel : Codable {

    let data : [guestListModelData]?
    let msg : String?
    let response : Bool?
}
struct guestListModelData : Codable {

    let ecode : String?
    let name : String?
    let checkInDate : String?
    let checkinTime : String?
    let checkoutDate : String?
    let checkoutTime : String?
    let isAvailable : Bool?
}
