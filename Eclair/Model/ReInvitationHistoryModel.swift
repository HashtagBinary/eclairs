//
//  ReInvitationHistoryModel.swift
//  Eclair
//
//  Created by Indigo on 04/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import Foundation

struct reInvitationHistoryModel : Codable {
    
    let response : Bool?
    let msg : String?
    let data : [reInHistoryData]?
}
struct reInHistoryData : Codable {
    let ID : Int?
    let `Type` :String?
    let Ecode : String?
    let SubmitedBy : String?
    let SubmitDate : String?
    let ApproveDate : String?
    let Date : String?
    let status : String?
    let Reason : String?
    let StrDate : String?
}
