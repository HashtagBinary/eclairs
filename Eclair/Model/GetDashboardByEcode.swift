//
//  GetDashboardByEcode.swift
//  Eclair
//
//  Created by Bilal on 11/27/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import Foundation

struct GetDashboardByEcode : Codable
{
    var activeAuctions : Int?
    var activeBids : Int?
    var soldItems : Int?
    var unsoldItems : Int?
    var totalActiveAuctions : Int?
    
    init()
    {
        activeAuctions = 0
        activeBids = 0
        soldItems = 0
        unsoldItems = 0
        totalActiveAuctions = 0
    }
}
