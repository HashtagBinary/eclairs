//
//  SpaProductModel.swift
//  Eclair
//
//  Created by Sohail on 24/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

struct SpaProductResponse: Decodable {
    var response: Bool?
    var msg : String?
    var message : String?
}

struct SpaProducts: Decodable {
    var Response: Int?
    var data: [SpaProductCategory] = []
}

struct SpaProductCategory: Decodable {
    var id: Int?
    var category: String?
    var img: String?
    var spaItems: [SpaProductItem]
}

struct SpaProductItem: Codable {
    var id: Int?
    var name: String?
    var price: Float?
    var img: String?
    var expiry: Int64?
    var brandName: String?
    var qty: Int?
    var description: String?
    var catId: Int?
}
