//
//  Connectivity.swift
//  Eclair
//
//  Created by iMac on 16/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
