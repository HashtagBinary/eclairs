//
//  NewsHeaderView.swift
//  Eclair
//
//  Created by iMac on 18/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import ImageSlideshow

class NewsHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userType: UILabel!
    
    override func awakeFromNib() {
        slideshow.slideshowInterval = 3
        slideshow.contentScaleMode = .scaleToFill
        profilePic.circleCorner = true
    }
}
