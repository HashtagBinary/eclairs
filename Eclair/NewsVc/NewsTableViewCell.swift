//
//  NewsTableViewCell.swift
//  Eclair
//
//  Created by iMac on 18/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import FaveButton


class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headline_lbl: UILabel!
    @IBOutlet weak var description_lbl: UILabel!
    @IBOutlet weak var like_count_lbl: UILabel!
    @IBOutlet weak var comment_count_lbl: UILabel!
    @IBOutlet weak var datetime_lbl: UILabel!
    @IBOutlet weak var newsimage: UIImageView!
    @IBOutlet weak var likeview: UIView!
    @IBOutlet weak var commentview: UIView!
    @IBOutlet weak var shareview: UIView!
    
    @IBOutlet weak var likebtn: FaveButton!
    @IBOutlet weak var likelabel: UILabel!
    
    var isLiked: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setUnLiked() {
        isLiked = false
        likebtn.setSelected(selected: false, animated: true)
        likelabel.textColor = UIColor(red:0.33, green:0.33, blue:0.33, alpha:1.0) //Dark Gray
    }
    
    public func setLiked() {
        isLiked = true
        likebtn.setSelected(selected: true, animated: true)
        likelabel.textColor = UIColor(red:0.11, green:0.39, blue:0.85, alpha:1.0)
    }
}

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
}
