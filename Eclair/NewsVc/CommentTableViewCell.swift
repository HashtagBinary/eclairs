//
//  CommentTableViewCell.swift
//  Eclair
//
//  Created by iMac on 21/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var txt_lbl: UILabel!
    @IBOutlet weak var datetime_lbl: UILabel!
    @IBOutlet weak var replybtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func onReplyClick(_ sender: UIButton) {
    }
}
