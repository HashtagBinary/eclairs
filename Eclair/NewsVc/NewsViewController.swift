//
//  NewsViewController.swift
//  Eclair
//
//  Created by iMac on 18/03/2019.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit
import Firebase
import ImageSlideshow
import Kingfisher

class NewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableview: UITableView!
    var ref: DatabaseReference!
    var newsList = [News]()
    var currentKey: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.dataSource = self
        tableview.delegate = self
        
        ref = Database.database().reference()
        
        let headerNib = UINib.init(nibName: "NewsHeaderView", bundle: Bundle.main)
        self.tableview.register(headerNib, forHeaderFooterViewReuseIdentifier: "NewsHeaderView")
        
        initData()
    }
    override func viewWillAppear(_ animated: Bool) {
        tableview.reloadData()
    }
    func initData()
    {
        ref.child("news").observe(.childAdded, with: { (snapshot) -> Void in
            
            let key = snapshot.key
            let value = snapshot.value as? NSDictionary
            let datetime = (value?["datetime"] as? UInt64)!
            let description = (value?["description"] as? String)!
            let headline = (value?["headline"] as? String)!
            let image = (value?["image"] as? String)!
            
            self.ref.child("news").child(snapshot.key).child("likes").observeSingleEvent(of: .value, with: { (snapshot) in
                
                var likescount: Int = Int(snapshot.childrenCount)
                var isliked: Bool = false
                
                if snapshot.hasChild(Constants.ecode) {
                    isliked = true
                    likescount = likescount - 1
                }
               
                self.newsList.insert( News(key: key, datetime: datetime, description: description, headline: headline, image: image,
                         isliked: isliked, likescount: likescount), at: 0)
                self.tableview.reloadData()
            })
        })
        
        ref.child("news").observe(.childChanged, with: { (snapshot) -> Void in
            
            var index: Int = 0
            
            for (i, element) in self.newsList.enumerated() {
                
                if snapshot.key == element.key {
                    index = i
                }
            }
            
            let key = snapshot.key
            let value = snapshot.value as? NSDictionary
            let datetime = (value?["datetime"] as? UInt64)!
            let description = (value?["description"] as? String)!
            let headline = (value?["headline"] as? String)!
            let image = (value?["image"] as? String)!
            
            self.ref.child("news").child(snapshot.key).child("likes").observeSingleEvent(of: .value, with: { (snapshot) in
                
                var likescount: Int = Int(snapshot.childrenCount)
                var isliked: Bool = false
                
                if snapshot.hasChild(Constants.ecode) {
                    isliked = true
                    likescount = likescount - 1
                }
                
                self.newsList[index] = News(key: key, datetime: datetime, description: description, headline: headline, image: image,
                                           isliked: isliked, likescount: likescount)
                
                let offset = self.tableview.contentOffset
                self.tableview.reloadData()
                self.tableview.layoutIfNeeded()
                self.tableview.setContentOffset(offset, animated: false)
            })
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "NewsHeaderView") as! NewsHeaderView
        headerView.username.text = Constants.ecode
        headerView.userType.text = Utilities.displayMemberType(ecode: Constants.ecode)
        let picture = UserDefaults.standard.string(forKey: "profileImg") ?? ""
        if picture.isEmpty == true
        {
            headerView.profilePic.image = UIImage(named: "userimg")
        }
        else
        {
            headerView.profilePic.circleCorner = true
            if let decodedData = Data(base64Encoded: picture, options: .ignoreUnknownCharacters) {
            let image = UIImage(data: decodedData)
                headerView.profilePic.image = image
            }
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        headerView.profilePic.isUserInteractionEnabled = true
        headerView.profilePic.addGestureRecognizer(tapGestureRecognizer)
        headerView.profilePic.borderWidth = 1
        
        
        
        
        
        
        headerView.slideshow.setImageInputs([
//            SDWebImageSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080", placeholder: UIImage(named: "image_loading"))!,
//            SDWebImageSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080", placeholder: UIImage(named: "image_loading"))!
            ])
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 425
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let milisecond = self.newsList[indexPath.row].datetime
        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond!)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        
        let cell = Bundle.main.loadNibNamed("NewsTableViewCell", owner: self, options: nil)?.first as! NewsTableViewCell
        cell.headline_lbl.text = self.newsList[indexPath.row].headline
        cell.description_lbl.text = self.newsList[indexPath.row].description
        cell.datetime_lbl.text = dateFormatter.string(from: dateVar)

        cell.newsimage.image = UIImage.init(named: "image_loading")
        let url = newsList[indexPath.row].image ?? "image_loading"
        cell.newsimage.downloadImage(with: url)
        { (data:UIImage?, error:NSError?) in
            cell.newsimage.image = data
        }
        
        if self.newsList[indexPath.row].isliked! && newsList[indexPath.row].likescount! > 0 {
            cell.like_count_lbl.text = "You and \(newsList[indexPath.row].likescount ?? 0) other liked this"
        }
        else if self.newsList[indexPath.row].isliked! {
            cell.like_count_lbl.text = "You liked this"
        }
        else if newsList[indexPath.row].likescount! > 0 {
            cell.like_count_lbl.text = "\(newsList[indexPath.row].likescount ?? 0) Likes"
        }
        else {
            cell.like_count_lbl.text = "No Like"
        }
        
        cell.setLiked()
        
        if !self.newsList[indexPath.row].isliked!
        {
            cell.setUnLiked()
        }
        
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleLike(sender:)))
        cell.likeview.addGestureRecognizer(tapGesture)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleComment(sender:)))
        cell.commentview.addGestureRecognizer(tapGesture)
        
        return cell
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToProfile"), object: nil)
    }
    
    @objc func handleLike(sender: UITapGestureRecognizer) {
        
        let touch = sender.location(in: tableview)
        if let indexPath = tableview.indexPathForRow(at: touch) {
            if self.newsList[indexPath.row].isliked! {
                ref.child("news").child(self.newsList[indexPath.row].key!).child("likes").child(Constants.ecode).removeValue()
            }
            else {
                ref.child("news").child(self.newsList[indexPath.row].key!).child("likes").child(Constants.ecode).setValue(Constants.ecode)
            }
        }
    }
    
    @objc func handleComment(sender: UITapGestureRecognizer) {
        
        let touch = sender.location(in: tableview)
        if let indexPath = tableview.indexPathForRow(at: touch) {
            currentKey = self.newsList[indexPath.row].key
            performSegue(withIdentifier: "commentSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let intent = segue.destination as! CommentViewController
        intent.newskey = currentKey
    }
}
