//
//  CommentViewController.swift
//  Eclair
//
//  Created by Sohail on 16/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Firebase

class CommentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

     @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var sendbtn: UIButton!
    
    var ref: DatabaseReference!
    
    var commentList = [Comment]()
    var newskey: String!
    var currentKey: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        
        ref = Database.database().reference()

        sendbtn.isEnabled = false
        sendbtn.alpha = 0.2
        
        initData()
    }
    
    func initData()
    {
        ref.child("news/" + newskey + "/comments/").observe(.childAdded, with: { (snapshot) -> Void in
            
            let key = snapshot.key
            let value = snapshot.value as? NSDictionary
            let datetime = (value?["datetime"] as? UInt64)!
            let txt = (value?["txt"] as? String)!
            
            self.ref.child("news/" + self.newskey + "/replies/" + key).observeSingleEvent(of: .value, with: { (snapshot) in
                
                let replycount: Int = Int(snapshot.childrenCount)
                
                self.commentList.append( Comment(key: key, datetime: datetime, txt: txt, replycount: replycount) )
                self.tableview.reloadData()
            })
        })
        
    }

    @IBAction func onSendClick(_ sender: UIButton) {
        
        if (textfield.text?.trimString().isEmpty)! {
            return
        }
        
        let map = [
            "datetime":  Date().toMillis(),
            "txt": textfield.text!
            ] as [String : Any]
        
        ref.child("news/" + newskey + "/comments/").childByAutoId().setValue(map) { (error, ref) in
            if let error = error {
                
                let alertController = UIAlertController(title: "Comment Send Error", message: error as? String, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "Close", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
            else {
                self.textfield.text = ""
                self.sendbtn.isEnabled = false
                self.sendbtn.alpha = 0.2
                self.scrollToBottom()
            }
        }
    }
    
    @IBAction func onTextChanged(_ sender: UITextField) {
        
        if (textfield.text?.trimString().isEmpty)! {
            sendbtn.isEnabled = false
            sendbtn.alpha = 0.2
        }
        else {
            sendbtn.isEnabled = true
            sendbtn.alpha = 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let milisecond = self.commentList[indexPath.row].datetime
        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond!)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy | hh:mm:ss"
        
        let cell = Bundle.main.loadNibNamed("CommentTableViewCell", owner: self, options: nil)?.first as! CommentTableViewCell
        
        cell.txt_lbl.text = self.commentList[indexPath.row].txt
        cell.datetime_lbl.text = dateFormatter.string(from: dateVar)
        
        if self.commentList[indexPath.row].replycount == 0 {
            cell.replybtn.setTitle("no reply",for: .normal)
        }
        else {
            cell.replybtn.setTitle("\(self.commentList[indexPath.row].replycount ?? 0) reply",for: .normal)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleReply(sender:)))
        cell.replybtn.addGestureRecognizer(tapGesture)
        
        return cell
    }
    
    @objc func handleReply(sender: UITapGestureRecognizer) {
        
        let touch = sender.location(in: tableview)
        if let indexPath = tableview.indexPathForRow(at: touch) {
            
            currentKey = self.commentList[indexPath.row].key
            performSegue(withIdentifier: "replysegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let intent = segue.destination as! ReplyViewController
        intent.newskey = newskey
        intent.commentkey = currentKey
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.commentList.count - 1, section: 0)
            self.tableview.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
}
