//
//  NotificationViewController.swift
//  Eclair
//
//  Created by Sohail on 22/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import FirebaseFirestore

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    let db = Firestore.firestore()
    var arrayList : [NotificationModel?] = []
    var filterType = -1
    var filterArray : [NotificationModel?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        let edit = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(editTapped))
        navigationItem.rightBarButtonItems = [edit]
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.arrayList.removeAll()
        self.tableView.reloadData()
        self.snapShotForFS()
        self.updateNotificationCounter()
    }
    override func viewDidDisappear(_ animated: Bool) {
            self.updateOnFb()
    }
    func updateOnFb()
    {
        let docRef = db.collection(Constants.ServerAPI.FS_NOTIFICATIONS_URL).document("\(Constants.ecode)").collection("DATA")
            docRef.order(by: "datetime",descending: true).addSnapshotListener(includeMetadataChanges: true)
          { querySnapshot, error in
                        // ...
          guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots: \(error!)")
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                    return
                }
             snapshot.documentChanges.forEach { diff in
                let json = diff.document.data()
                let status = json["is_seen"] as? Bool
                if status == false
                {
                    diff.document.reference.updateData(["is_seen":true])
                }
        }
                
      }
    }
    
    func snapShotForFS()
    {
        //MARK:- snapshot listener for every change
        let docRef = db.collection(Constants.ServerAPI.FS_NOTIFICATIONS_URL).document("\(Constants.ecode)").collection("DATA")
        docRef.order(by: "datetime",descending: true).addSnapshotListener(includeMetadataChanges: true)
        { querySnapshot, error in
                        // ...
            guard let snapshot = querySnapshot else {
                        print("Error fetching snapshots: \(error!)")
                        ActivityIndicatorSingleton.StopActivity(myView: self.view)
                        return
                    }
            snapshot.documentChanges.forEach { diff in
                let josnData = diff.document.data()
                if diff.type == .added
                {
                    let date = josnData["datetime"] as? FirebaseFirestore.Timestamp
                    var item = NotificationModel()
                    item.documentID = diff.document.documentID
                    item.id = josnData["id"] as? String
                    item.body = josnData["body"] as? String
                    item.datetime = date?.dateValue()
                    item.is_seen = josnData["is_seen"] as? Bool
                    item.title = josnData["title"] as? String
                    item.type = josnData["type"] as? Int
                    if self.filterType != -1
                    {
                        if self.filterType == item.type
                        {
                            self.arrayList.append(item)
                        }
                    }
                    else
                    {
                        self.arrayList.append(item)
                    }
                     self.arrayList.sort(by: {$0?.datetime!.compare(($1?.datetime)!) == .orderedDescending})
                }
                else if diff.type == .removed
                {
                    let docId = diff.document.documentID
                    if let indexOfNotificataionToRemove = self.arrayList.firstIndex(where: { $0?.documentID == docId} ) {
                        self.arrayList.remove(at: indexOfNotificataionToRemove)
                        print("removed: \(docId)")
                    }
                }
                else if diff.type == .modified
                {
                    let date = josnData["datetime"] as? FirebaseFirestore.Timestamp
                    var item = NotificationModel()
                    item.id = josnData["id"] as? String
                    item.body = josnData["body"] as? String
                    item.datetime = date?.dateValue()
                    item.is_seen = josnData["is_seen"] as? Bool
                    item.title = josnData["title"] as? String
                    item.type = josnData["type"] as? Int
                    
                    for i in 0..<self.arrayList.count
                    {
                        if self.arrayList[i]?.documentID == diff.document.documentID
                        {
                            self.arrayList[i]?.id = item.id
                            self.arrayList[i]?.body = item.body
                            self.arrayList[i]?.datetime = item.datetime
                            self.arrayList[i]?.is_seen = item.is_seen
                            self.arrayList[i]?.title = item.title
                            self.arrayList[i]?.type = item.type
                        }
                    }
                }
                self.tableView.reloadData()
             }
         }
        self.tableView.reloadData()
    }
    
    
    func updateNotificationCounter()
    {
        //MARK:- snapshot listener for every change
        let docRef = db.collection(Constants.ServerAPI.FS_NOTIFICATIONS_URL).document("\(Constants.ecode)")//.collection("notificationCount")
        .getDocument()
        { querySnapshot, error in
                        // ...
            guard let document = querySnapshot else {
                        print("Error fetching snapshots: \(error!)")
                        ActivityIndicatorSingleton.StopActivity(myView: self.view)
                        return
                    }
            document.reference.updateData(["notificationCount":0])
         }
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filter"
        {
            let vc = segue.destination as! NotificationFilterVC
            vc.delegate = self
        }
    }
    @objc func editTapped(sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "filter", sender: self)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell

            cell.backgroundColor = (arrayList[indexPath.row]?.is_seen ?? false) ? UIColor.white : UIColor(named: "ColorUnseen")
            cell.titleLabel.text = arrayList[indexPath.row]?.title
            cell.msgLabel.text = arrayList[indexPath.row]?.body
        
        
        switch arrayList[indexPath.row]?.type {
        case 0:
            cell.imgView.image = UIImage(named: "noti_eclair")
        case 1:
            cell.imgView.image = UIImage(named: "noti_Fnb")
        case 2:
            cell.imgView.image = UIImage(named: "noti_spa")
        default:
            cell.imgView.image = UIImage(named: "image_loading")
        }
            
            if self.arrayList[indexPath.row]?.datetime != nil
            {
                let currentDate = Date()
                let dayMonth = currentDate.getMonthDayHours(recent: currentDate, previous: self.arrayList[indexPath.row]!.datetime!)
                if dayMonth.day ?? 0 < 1 && dayMonth.hour ?? 0 > 0
                {
                    cell.datetimeLabel.text = "\(dayMonth.hour ?? 0) hours ago"
                }
                else if dayMonth.hour ?? 0 < 1 && dayMonth.minute ?? 0 >= 1
                {
                    cell.datetimeLabel.text = "\(dayMonth.minute ?? 0) minutes ago"
                }
                else if dayMonth.second ?? 0 < 60 && dayMonth.day ?? 0 <= 0 && dayMonth.hour ?? 0 <= 0 && dayMonth.minute ?? 0 < 1
                {
                    cell.datetimeLabel.text = "Just Now"
                }
                else if dayMonth.day ?? 0 >= 1
                {
                    cell.datetimeLabel.text = Utilities.convertDateFormate(forStringDate: "\(self.arrayList[indexPath.row]!.datetime!)", currentFormate: "yyyy-MM-dd' 'HH:mm:ss Z", newFormate: "MMM dd,yyyy' at 'HH:mm")
                }
            }
            else
            {
                cell.datetimeLabel.text = ""
            }
            return cell
    }
}
extension NotificationViewController : NotificationFilter
{
    func filterType(type: Int) {
        self.filterType = type
        self.arrayList.removeAll()
        self.snapShotForFS()
    }
}
