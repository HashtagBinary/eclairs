//
//  NotificationFilterVC.swift
//  Eclair
//
//  Created by Indigo on 19/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class NotificationFilterVC: UIViewController {

    let filters = ["Indigo","Food & Beverages","SPA"]
    var delegate : NotificationFilter?
    var selectedTypeOfFilter = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clearbtn(_ sender: Any) {
        self.selectedTypeOfFilter = -1
        self.delegate?.filterType(type: self.selectedTypeOfFilter)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func applybtn(_ sender: Any) {
        self.delegate?.filterType(type: self.selectedTypeOfFilter)
        self.dismiss(animated: true, completion: nil)
    }
}
extension NotificationFilterVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NotificationFilterCell
        cell.titlelbl.text = filters[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell =  tableView.cellForRow(at: indexPath) as? NotificationFilterCell
        cell?.accessoryType = .checkmark
        self.selectedTypeOfFilter = indexPath.row
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell =  tableView.cellForRow(at: indexPath) as? NotificationFilterCell
        cell?.accessoryType = .none
    }
}
protocol NotificationFilter {
    func filterType(type:Int)
}
