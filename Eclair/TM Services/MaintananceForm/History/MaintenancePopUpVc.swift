//
//  MaintenancePopUpVc.swift
//  Eclair
//
//  Created by Indigo on 05/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class MaintenancePopUpVc: UIViewController {
    @IBOutlet weak var stackVw: UIStackView!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var addresslbl: UILabel!
    @IBOutlet weak var contactlbl: UILabel!
    @IBOutlet weak var typelbl: UILabel!
    @IBOutlet weak var nationalitylbl: UILabel!
    @IBOutlet weak var identityTypeLbl: UILabel!
    @IBOutlet weak var cardNolbl: UILabel!
    @IBOutlet weak var CnicImg: UIImageView!
    @IBOutlet weak var genderlbl: UILabel!
    @IBOutlet weak var emaillbl: UILabel!
    @IBOutlet weak var submittedDatelbl: UILabel!
    @IBOutlet weak var doblbl: UILabel!
    @IBOutlet weak var relationlbl: UILabel!
    @IBOutlet weak var familyPic: UIImageView!
    @IBOutlet weak var descriptionlbl: UILabel!
    @IBOutlet weak var cardNoTitlelbl: UILabel!
    @IBOutlet weak var cnicPicTitlelbl: UILabel!
    @IBOutlet weak var familyCardVw: UIView!
    @IBOutlet weak var cnicPicVw: UIView!
    @IBOutlet weak var relationVw: UIView!
    
    @IBOutlet weak var identityVw: UIView!
    @IBOutlet weak var cardVw: UIView!
    
    var selectedmaintenanceHistoryData : maintenanceHistoryData?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let isGuest = UserDefaults.standard.bool(forKey: "isGuest") 
        if isGuest == true
        {
            familyCardVw.isHidden = true
            relationVw.isHidden = true
        }
        
        namelbl.text = selectedmaintenanceHistoryData?.FullName ?? "-"
        addresslbl.text = selectedmaintenanceHistoryData?.Address ?? "-"
        contactlbl.text = selectedmaintenanceHistoryData?.ContactNo ?? "-"
        typelbl.text = selectedmaintenanceHistoryData?.Type ?? "-"
        nationalitylbl.text = selectedmaintenanceHistoryData?.Nationality ?? "-"
        identityTypeLbl.text = selectedmaintenanceHistoryData?.IdentityType ?? "-"
        genderlbl.text = selectedmaintenanceHistoryData?.Gender ?? "-"
        submittedDatelbl.text = selectedmaintenanceHistoryData?.SubmitDate ?? "-"
        doblbl.text = selectedmaintenanceHistoryData?.DOB ?? "-"
        let submitDate = selectedmaintenanceHistoryData?.SubmitDate ?? ""
        let dob = selectedmaintenanceHistoryData?.DOB ?? ""
//        if isGuest == true
//        {
//            if submitDate.isEmpty == false && dob.isEmpty == false
//            {
//                let subDate = Utilities.getDateFromString(strDate: submitDate, currentFormate: "MM/dd/yyyy")
//                let dobDate = Utilities.getDateFromString(strDate: dob, currentFormate: "dd/MM/yyyy")
//                let year = yearsBetweenDate(startDate: dobDate, endDate: subDate)
//                print("years \(year)")
//                
//                if year < 12
//                {
//                    identityVw.isHidden = true
//                    cardVw.isHidden = true
//                    cnicPicVw.isHidden = true
//                }
//            }
//        }
        
        
        
        
        relationlbl.text = selectedmaintenanceHistoryData?.Relation ?? "-"
        descriptionlbl.text = selectedmaintenanceHistoryData?.Description ?? "-"
        cardNolbl.text = selectedmaintenanceHistoryData?.CardNo ?? "-"
        emaillbl.text = selectedmaintenanceHistoryData?.EmailAddress ?? "-"
        if selectedmaintenanceHistoryData?.IdentityType == "NIC"
        {
            cardNoTitlelbl.text = "CNIC No"
            cnicPicTitlelbl.text = "CNIC Pic"
        }
        else if selectedmaintenanceHistoryData?.IdentityType == "Passport No"
        {
            cardNoTitlelbl.text = "Passport No"
            cnicPicTitlelbl.text = "Passport Pic"
        }
        
        
        let cardPic = selectedmaintenanceHistoryData?.CardPic ?? "image_loading"
        self.CnicImg.image = UIImage.init(named: "image_loading")
        self.CnicImg.downloadImage(with: cardPic)
        { (data:UIImage?, error:NSError?) in
            self.CnicImg.image = data
        }
        
        
        let familyPic = selectedmaintenanceHistoryData?.FamilyID ?? "image_loading"
        self.familyPic.image = UIImage.init(named: "image_loading")
        self.familyPic.downloadImage(with: familyPic)
        { (data:UIImage?, error:NSError?) in
            self.familyPic.image = data
        }  
    }
    @IBAction func okbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func yearsBetweenDate(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year], from: startDate, to: endDate)
        return components.year ?? 0
    }
}
