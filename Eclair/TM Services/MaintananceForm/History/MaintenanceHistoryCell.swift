//
//  MaintenanceHistoryCell.swift
//  Eclair
//
//  Created by Indigo on 04/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class MaintenanceHistoryCell: UITableViewCell {

    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var typelbl: UILabel!
    @IBOutlet weak var contactlbl: UILabel!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var statuslbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
