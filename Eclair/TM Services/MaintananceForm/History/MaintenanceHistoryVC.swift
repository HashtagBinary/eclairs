//
//  MaintenanceHistoryVC.swift
//  Eclair
//
//  Created by Indigo on 04/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MaintenanceHistoryVC: UIViewController {

    @IBOutlet weak var tblvw: UITableView!
    var historyData = [maintenanceHistoryData]()
    var selectedData : maintenanceHistoryData?
    var isGuest = false
    private let refreshControl = UIRefreshControl()
    var skip = 0
    var limit = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        // Do any additional setup after loading the view.
    }
    @objc private func refreshData(_ sender: Any) {
        
        self.skip = 0
        self.historyData.removeAll()
        self.tblvw.reloadData()
        self.refreshControl.endRefreshing()
        self.HistoryList()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.skip = 0
        isGuest = UserDefaults.standard.bool(forKey: "isGuest")
        HistoryList()
    }
    func HistoryList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params : Parameters = ["id": Constants.ecode,
                                   "skip":self.skip,
                                   "limit":self.limit]
        var url = ""
        if isGuest == false
        {
            url = Constants.ServerAPI.kHistoryT_M+"\(Constants.ecode)&skip=\(self.skip)&limit=\(self.limit)"
        }
        else
        {
            url = Constants.ServerAPI.kHistoryGuest+"\(Constants.ecode)&skip=\(self.skip)&limit=\(self.limit)"
        }
        
        AlamorfireSingleton.getCall(serviceName: url, parameters: nil) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(maintenanceHistoryModel.self, from: data!)

            if(model?.response == true)
            {
                if model?.data?.count ?? -1 <= 0 && self.skip <= 0
                {
                    Utilities.showAlert("", message: "No Data Found")
                    return
                }
                else if model?.data?.count ?? -1 <= 0 && self.skip > 0
                {
                    return
                }
                else
                {
                    self.historyData += model?.data ?? []
                    self.tblvw.reloadData()
                }
             }
             else
             {
                 Utilities.showAlert("", message: "Error: \(model?.msg ?? "server Error")")
             }
      }
}
    @objc func Click(sender: UIButton){
        let buttonTag = sender.tag
        self.selectedData = historyData[buttonTag]
        self.performSegue(withIdentifier: "detail", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail"
        {
            let vc = segue.destination as! MaintenancePopUpVc
            vc.selectedmaintenanceHistoryData = self.selectedData
        }
    }
}
extension MaintenanceHistoryVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        let cell = tblvw.dequeueReusableCell(withIdentifier: "cell") as! MaintenanceHistoryCell
        cell.namelbl.text = historyData[indexPath.row].FullName ?? ""
        cell.contactlbl.text = historyData[indexPath.row].ContactNo ?? ""
        cell.typelbl.text = historyData[indexPath.row].Nationality ?? ""
        let status = historyData[indexPath.row].Status ?? ""
        var updateStatus = ""
        if status == "a"
        {
            updateStatus = "Approved"
        }
        else if status == "s"
        {
            updateStatus = "Submitted"
        }
        else if status == "c"
        {
            updateStatus = "Cancelled"
        }
        else if status == "p"
        {
            updateStatus = "Pending"
        }
        else if status == "r"
        {
            updateStatus = "Rejected"
        }
        else
        {
            updateStatus = "Not Defined"
        }
        cell.statuslbl.text = updateStatus
        cell.detailButton.circleCorner = true
        cell.detailButton.borderWidth = 1
        if #available(iOS 13.0, *) {
            cell.detailButton.borderColor = .link
        } else {
            // Fallback on earlier versions
            cell.detailButton.borderColor = .blue
        }
        cell.detailButton.tag = indexPath.row
        cell.detailButton.addTarget(self, action: #selector(Click(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
            if indexPath.row == self.historyData.count - 1 {
                    skip += limit
                    self.HistoryList()
            }
      }
}
