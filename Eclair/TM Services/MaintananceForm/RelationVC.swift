//
//  RelationVC.swift
//  Eclair
//
//  Created by Indigo on 18/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class RelationVC: UIViewController {

    
    let relations = ["Spouse","Daugther","Son","Mother","Father","Sister","Firends","Brother","Sister_In_Law","Brother_In_Law","First_Cousin","Direct_Aunt","Direct_Uncle","Niece","Nephew","Daugher_Of_Spouse","Son_Of_Spouse","Mother_Of_Spouse","Father_Of_Spouse","Sister_Of_Spouse","Brother_Of_Spouse","First_Cousin_Of_Spouse","Direct_Aunt_Of_Spouse","Direct_Uncle_Of_Spouse","Niece_Of_Spouse","Nephew_Of_Spouse","Sons_Wife","Daughters_Husband","Wife_Of_Son_Of_Spouse"]
    
    var delegate:relationDelegate?
    var selected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func cancelbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func okBtn(_ sender: Any) {
        self.delegate?.getRelation(selectedRelation: selected)
        self.dismiss(animated: true, completion: nil)
    }
}
extension RelationVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return relations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = relations[indexPath.row]
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        self.selected = relations[indexPath.row]
        cell?.accessoryType = .checkmark
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .none
    }
    
}
protocol relationDelegate {
    func getRelation(selectedRelation:String)
}
