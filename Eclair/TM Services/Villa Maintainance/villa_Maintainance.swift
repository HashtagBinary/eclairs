//
//  villa_Maintainance.swift
//  Eclair
//
//  Created by iOS Indigo on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class villa_Maintainance: UIViewController ,UITextFieldDelegate,UITextViewDelegate{

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var selectImgVw: RectangleView!
    @IBOutlet weak var imgContainerVw: RectangleView!
    @IBOutlet weak var selectedimg: UIImageView!
    @IBOutlet weak var departmentlbl: UILabel!
    @IBOutlet weak var issuetxt: UITextField!
    @IBOutlet weak var deptTxt: UITextField!
    @IBOutlet weak var descriptiontxt: UITextView!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var dateNtimeView: RectangleView!
    var issues = [mainFormsItemsSubItem]()
    var departments = [mainFormsItemsDepartment]()
    var issuePicker = UIPickerView()
    var departmentPicker = UIPickerView()
    var descriptionPlaceHolder = "Enter Description"
    var selectedDate = ""
    var dateForSending = ""
    var selectedItemId = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupview()
        getItems()
    }
    func setupview()
    {
        submitButton.cornerRadius = 5
        departmentPicker.tag = 1
        issuePicker.tag = 2
        imgContainerVw.isHidden = true
        let imgTap = UITapGestureRecognizer(target: self, action: #selector(self.handleImageTap(_:)))
        selectImgVw.addGestureRecognizer(imgTap)
        let dateTap = UITapGestureRecognizer(target: self, action: #selector(self.handleDateTap(_:)))
        dateNtimeView.addGestureRecognizer(dateTap)
        deptTxt.delegate = self
        issuetxt.delegate = self
        departmentPicker.delegate = self
        issuePicker.delegate = self
        descriptiontxt.text = descriptionPlaceHolder
        descriptiontxt.textColor = UIColor.lightGray
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == deptTxt
        {
            if departments.count == 0
            {
                textField.resignFirstResponder()
                return
            }
            textField.inputView = departmentPicker
        }
        else
        {
            if issues.count == 0
            {
                textField.resignFirstResponder()
                return
            }
            textField.inputView = issuePicker
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = descriptionPlaceHolder
            textView.textColor = UIColor.lightGray
        }
    }
    @objc func handleImageTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
      showGalleryOpt()
    }
    @objc func handleDateTap(_ sender: UITapGestureRecognizer? = nil) {
        self.performSegue(withIdentifier: "date", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "date"
        {
            let vc = segue.destination as!DatePickerVc
            vc.delegate = self
            vc.pickerType = "Time&Date"
        }
    }
    func showGalleryOpt(){
        
        let alert = UIAlertController(title: "Upload Picture", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
            self.getPhotoFromLib()
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.getPhotoFromCam()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getPhotoFromLib(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func getPhotoFromCam(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func submitbtn(_ sender: Any) {
        
        if selectedimg.image == nil
        {
            Utilities.showAlert("", message: "Please select any image first")
            return
        }
        
        else if self.selectedItemId == -1
        {
            Utilities.showAlert("", message: "Please select any Item first")
            return
        }
        
        else if datelbl.text == "Select Data & Time"
        {
            Utilities.showAlert("", message: "Please select any date and time")
            return
        }
       else if descriptiontxt.text == descriptionPlaceHolder
        {
            submitAPI(desc: "")
        }
        else if descriptiontxt.text != descriptionPlaceHolder
        {
            //submitAPI(desc: self.descriptiontxt.text ?? "")
            self.alert(title: "", message: "Are you sure you want to submit this request?")
        }
    }
    func alert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
             
             let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
             })
             alert.addAction(cancel)
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
               self.submitAPI(desc: self.descriptiontxt.text ?? "")             })
            alert.addAction(ok)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    //MARK:- api calling
    
    func getItems()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
     
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kGetMaintananceFormItems, parameters: nil) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            
            let decoder = JSONDecoder()
            let model = try? decoder.decode(mainFormsItems.self, from: data ?? Data())
            if model?.Response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            print("model data \(model)")
            self.departments = model?.Departments ?? []
            if self.departments.isEmpty == false
            {
                self.departmentlbl.text = "Department: \(self.departments[0].deptName ?? "")"
                self.deptTxt.text = self.departments[0].category ?? ""
                if self.departments[0].items?.isEmpty == false
                {
                    self.issuetxt.text = self.departments[0].items?[0].Name ?? ""
                    self.selectedItemId = self.departments[0].items?[0].ID ?? -1
                    self.issues = self.departments[0].items ?? []
                }
            }
      }
}
    
    
    func submitAPI(desc : String){
       
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)",
                                          "Content-type": "multipart/form-data",
                                          "Content-Disposition" : "form-data"
        ]
        let parameters: Parameters = [
            "ItemID": self.selectedItemId,
            "Description":desc,
            "DateNTime":self.dateForSending,
            "ECode":Constants.ecode
        ]
            AF.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                let ImgData = self.selectedimg.image?.jpegData(compressionQuality: 1)
                if let data = ImgData{
                    multipartFormData.append(data, withName: "Picture", fileName: "imagename.jpg", mimeType: "image/jpeg")
                }

            }, to:Constants.ServerAPI.ksaveMaintananceForm,headers: Auth_header).response{ response in
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                if(response.data != nil){
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)

                    let json = try? JSON(data: response.data!)
                    let respns = json?["response"].bool ?? false
                    if respns == false
                    {
                      let message = json?["msg"].string ?? ""
                      Utilities.showAlert("", message: message)
                      return
                    }
                    else
                    {
                      let message = json?["msg"].string ?? ""
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToHome"), object: nil)
                      Utilities.showAlert("", message: message)
                      return
                    }
                      print("Json data for saving images \(json)")
                }
                else{
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                     print(response.error?.localizedDescription)
                }
            }
        }
}
extension villa_Maintainance: UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // assign rows in pickerview
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        // Set Rows Count
        switch pickerView.tag {
        case 1:
            return self.departments.count
        case 2:
            return self.issues.count
        default:
            return 0
        }
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // rows title like male,female
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //Setting Row Data
        switch pickerView.tag {
        case 1:
            return self.departments[row].category ?? ""
        case 2:
            return self.issues[row].Name ?? ""
        default:
            return "N/A"
        }
    }
    
    // action on each row select
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Setting Row Data
        switch pickerView.tag {
        case 1:
            
            departmentlbl.text = "Department: \(self.departments[row].deptName ?? "")"
            deptTxt.text = self.departments[row].category ?? ""
            self.issues = self.departments[row].items ?? []
            issuetxt.text = ""
            self.selectedItemId = -1
        
        case 2:
            issuetxt.text = self.issues[row].Name ?? ""
            self.selectedItemId = self.issues[row].ID ?? -1

        default:
            return
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage{
           
                self.selectedimg.image = image
                self.imgContainerVw.isHidden = false
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

}
extension villa_Maintainance : GetDateDelegate{
    func getSelectedDate(date: String) {
        self.selectedDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "dd - MM - yyyy HH:mm")
        self.dateForSending = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "dd-MM-yyyy HH:mm")
        self.datelbl.text = "Date " + "\(self.selectedDate)"
    }
}
