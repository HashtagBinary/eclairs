//
//  GuestRequestFormVC.swift
//  Eclair
//
//  Created by iOS Indigo on 14/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CountryPickerView

class GuestRequestFormVC: UIViewController {
    
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var address: UITextField!
    
    @IBOutlet weak var guestTxt: UITextField!
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var nationalityText: UITextField!
    @IBOutlet weak var nic: UITextField!
    @IBOutlet weak var emaillAddress: UITextField!
    @IBOutlet weak var descriptionTxt: UITextField!
    @IBOutlet weak var gendertext: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var idCardPicVw: UIView!
    @IBOutlet weak var relationVw: UIView!
    @IBOutlet weak var nicVw: UIView!
    @IBOutlet weak var passportVw: UIView!
    @IBOutlet weak var nicRadio: UIImageView!
    @IBOutlet weak var passportRadio: UIImageView!
    @IBOutlet weak var nicNpassportlbl: UILabel!
    @IBOutlet weak var nicImg: UIImageView!
    @IBOutlet weak var dateVw: UIView!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var relationtxt: UITextField!
    @IBOutlet weak var stackVw: UIStackView!
    @IBOutlet weak var selectIdCardPicVw: UIView!
    @IBOutlet weak var agelbl: UILabel!
    @IBOutlet weak var agevwHeight: NSLayoutConstraint!
    var dob: Int?
    var cardPicture: UIImage!
    var selectedDate = ""
    let relationPicker = UIPickerView()
    let genderPickerView =  UIPickerView()
    let guestPicker =  UIPickerView()
    let gender = ["Male", "Female"]
    let relation = ["Brother","Sister"]
    let guests = ["Single","Couple","Companion (Voucher)","Companion (Weekend)","Female (Voucher)"]
    var imageFor = ""
    var identityType = "NIC"
    var dateForSending = ""
    var selectedCountry = "Afghanistan"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createToolbar()
        setupLayout()
        setupVw()
        // Do any additional setup after loading the view.
    }
    func setupVw()
    {
        //MARK:- Gesture working
        let nicTap = UITapGestureRecognizer(target: self, action: #selector(self.handleNicTap(_:)))
        nicVw.addGestureRecognizer(nicTap)
        let passportTap = UITapGestureRecognizer(target: self, action: #selector(self.handlePassPortTap(_:)))
        passportVw.addGestureRecognizer(passportTap)
        
        let dateTap = UITapGestureRecognizer(target: self, action: #selector(self.handleDateTap(_:)))
        dateVw.addGestureRecognizer(dateTap)
        
        //let IdTap = UITapGestureRecognizer(target: self, action: #selector(self.handleIdCardTap(_:)))
        //selectIdCardPicVw.addGestureRecognizer(IdTap)
        
        //
        
        //MARK:- stackview working
        
        //familyVw.isHidden = true
        idCardPicVw.isHidden = true
        //relationVw.isHidden = true
        
        
        //MARK:- picker view working
        
        gendertext.inputView = genderPickerView
        relationtxt.inputView = relationPicker
        guestTxt.inputView = guestPicker
        guestPicker.delegate = self
        relationPicker.delegate = self
        relationPicker.dataSource = self
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        nic.delegate = self
        
        relationPicker.tag = 1
        genderPickerView.tag = 2
        guestPicker.tag = 3
        //MARK:- Country list working
        
        
        
        let cpv = CountryPickerView(frame: CGRect(x: 10, y: 0, width: self.nationalityText.frame.width, height: self.nationalityText.frame.height))
               // nationalityText.leftView = cpv
               // nationalityText.leftViewMode = .always
    
        cpv.delegate = self
        cpv.showCountryCodeInView = false
        cpv.showCountryNameInView = true
        cpv.showPhoneCodeInView = false
        nationalityText.addSubview(cpv)
        self.selectedCountry = cpv.selectedCountry.name
        
        stackVw.arrangedSubviews[6].isHidden = true
        stackVw.arrangedSubviews[7].isHidden = true
        stackVw.arrangedSubviews[8].isHidden = true
        stackVw.arrangedSubviews[9].isHidden = true
        self.agevwHeight.constant = 100
        
    }
    @objc func handleNicTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        //radioChecked , radioUnchecked
        nicRadio.image = UIImage(named: "radioChecked")
        passportRadio.image = UIImage(named: "radioUnchecked")
        nicNpassportlbl.text = "National Identity Card Number (Required)"
        nic.placeholder = "Enter NIC Number"
        identityType = "NIC"
        nic.text = ""
    }
    @objc func handlePassPortTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        nicRadio.image = UIImage(named: "radioUnchecked")
        passportRadio.image = UIImage(named: "radioChecked")
        nicNpassportlbl.text = "Passport Number (Required)"
        nic.placeholder = "Enter Passport Number"
        identityType = "Passport No"
        nic.text = ""
    }
    @objc func handleDateTap(_ sender: UITapGestureRecognizer? = nil) {
        self.performSegue(withIdentifier: "date", sender: self)
    }
    @objc func handleIdCardTap(_ sender: UITapGestureRecognizer? = nil) {
        self.imageFor = "card"
        self.showGalleryOpt()
    }
    func getflag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
   
    @objc func datePickerChanged(picker: UIDatePicker) {
        let dateFormatr = DateFormatter()
        dateFormatr.dateFormat = "dd-MM-yyyy"
        let date = dateFormatr.string(from: (picker.date))
        //dob = date
        //        print(dob as Any)
    }
    // Create Toolbar For each pickerview
    func createToolbar() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButton(sender:)))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        //nationalityText.inputAccessoryView = toolBar
        gendertext.inputAccessoryView = toolBar
    }
    // .done button action
    @objc func doneButton(sender: Any) {
        view.endEditing(true)
    }
    
    
    func setupLayout(){
        submitBtn.layer.cornerRadius = 8
    }
    
    @IBAction func selectCamraOptn(_ sender: Any) {
        self.imageFor = "idcard"
        showGalleryOpt()
    }
    func showGalleryOpt(){
        
        let alert = UIAlertController(title: "Upload Picture", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
            self.getPhotoFromLib()
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.getPhotoFromCam()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getPhotoFromLib(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func getPhotoFromCam(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func submitForm(_ sender: Any) {
        if fullName.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please Enter Name")
            return
        }
        else if contactNumber.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please Enter Contact No.")
            return
        }
        else if datelbl.text == "Select Date"
        {
            Utilities.showAlert("", message: "Please Select Date")
            return
        }
        
        else if dob ?? 0 >= 12
        {
            if nic.text?.isEmpty == true
            {
                Utilities.showAlert("", message: "Please Enter \(identityType).")
                return
            }
            else if nicImg.image == nil
            {
                Utilities.showAlert("", message: "Please Select \(identityType) Image")
                return
            }
            else
            {
                self.alert(title: "", message: "Are you sure you want to submit this request?")
            }
            
        }
        else
        {
            //self.submitAPI()
            self.alert(title: "", message: "Are you sure you want to submit this request?")
        }
    }
    
    //MARK:- api calling
    func alert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
             let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
             })
             alert.addAction(cancel)
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
               self.submitAPI()
            })
            alert.addAction(ok)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }

    func submitAPI(){
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)",
                                          "Content-type": "multipart/form-data",
                                          "Content-Disposition" : "form-data"
        ]
        let parameters: [String: String] = [
            "Description": descriptionTxt.text ?? "",
            "Address": address.text ?? "",
            "ContactNo": contactNumber.text ?? "",
            "CardNo": nic.text ?? "",
            "FullName": fullName.text ?? "",
            "Type":self.guestTxt.text ?? "",
            "Gender": gendertext.text ?? "",
            "IdentityType":self.identityType,
            "Nationality": self.selectedCountry,
            "DOB": self.dateForSending,
            "EmailAddress": emaillAddress.text ?? "",
            "SubmitedBy":Constants.ecode,
            "Relation":self.relationtxt.text ?? ""
        ]
        print("paramerter \(parameters)")
            AF.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                let NicImgData = self.nicImg.image?.jpegData(compressionQuality: 1)
                if let data = NicImgData{
                    multipartFormData.append(data, withName: "CardPic", fileName: "imagename.jpg", mimeType: "image/jpeg")
                }

            }, to:Constants.ServerAPI.ksaveGuestRequestForm,headers: Auth_header).response{ response in
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                if(response.data != nil){
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                    let jsonData = response.data!
                    let json = try? JSON(data: jsonData)
                    let respns = json?["response"].bool ?? false
                    if respns == false
                    {
                     let message = json?["msg"].string ?? ""
                     Utilities.showAlert("", message: message)
                     return
                    }
                    else
                    {
                     let message = json?["msg"].string ?? ""
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToHome"), object: nil)
                     Utilities.showAlert("", message: message)
                     return
                    }
                    print("Json data for saving images \(json)")
                }
                else{
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                     print(response.error?.localizedDescription)
                }
            }

        }
    
    
}
extension GuestRequestFormVC: UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // assign rows in pickerview
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        // Set Rows Count
        switch pickerView.tag {
        case 1:
            return relation.count
        case 2:
            return gender.count
        case 3:
            return guests.count
        default:
            return 1
        }
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // rows title like male,female
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //Setting Row Data
        switch pickerView.tag {
        case 1:
            return relation[row]
        case 2:
            return self.gender[row]
        case 3:
            return guests[row]
        default:
            return "N/A"
        }
        
        
    }
    
    // action on each row select
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Setting Row Data
        switch pickerView.tag {
        case 1:
            relationtxt.text = relation[row]
        //nationalityText.resignFirstResponder()
        case 2:
            gendertext.text = self.gender[row]
        case 3:
            guestTxt.text = guests[row]
        default:
            return
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage{
            
            cardPicture = image
            
                self.nicImg.image = image
                self.idCardPicVw.isHidden = false
            
            print(cardPicture!)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "date"
        {
            let vc =  segue.destination as! DatePickerVc
            vc.delegate = self
            vc.minimumFromCurrent = "Less"
        }
    }
}

extension GuestRequestFormVC : GetDateDelegate,UITextFieldDelegate{
    func getSelectedDate(date: String) {
        self.selectedDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "dd - MM - yyyy")
        self.dateForSending = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "MM/dd/yyyy")
        self.datelbl.text = "Date " + "\(self.selectedDate)"
        
        let getDate = Utilities.getDateFromString(strDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z")
        let years = yearsBetweenDate(startDate: getDate, endDate: Date())
        self.dob = years
        self.agelbl.text = "\(years) years old"
        if years < 2
        {
            self.agelbl.text = "\(years) year old"
        }
        
        self.agelbl.isHidden = false
        self.agevwHeight.constant = 130
        print("years \(years)")
        if years < 12
        {
            stackVw.arrangedSubviews[6].isHidden = true
            stackVw.arrangedSubviews[7].isHidden = true
            stackVw.arrangedSubviews[8].isHidden = true
            stackVw.arrangedSubviews[9].isHidden = true
        }
        else
        {
            stackVw.arrangedSubviews[6].isHidden = false
            stackVw.arrangedSubviews[7].isHidden = false
            stackVw.arrangedSubviews[8].isHidden = false
        }

    }
    func yearsBetweenDate(startDate: Date, endDate: Date) -> Int {

        let calendar = Calendar.current

        let components = calendar.dateComponents([.year], from: startDate, to: endDate)

        return components.year ?? 0
    }
}
extension GuestRequestFormVC : CountryPickerViewDelegate,CountryPickerViewDataSource
{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print("country \(country)")
        self.selectedCountry = country.name
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == contactNumber
        {
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
                textField.text = format(with: "XXXXX-XXXXX-XXXX", phone: newString)
                return false
        }
        else if textField == nic
        {
            if identityType == "NIC"
            {
                let maxLength = 10
                let currentString: NSString = (textField.text ?? "") as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            else
            {
                let maxLength = 9
                let currentString: NSString = (textField.text ?? "") as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
        }
        else if textField == fullName
        {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let Regex = "[a-z A-Z ]+"
                let predicate = NSPredicate.init(format: "SELF MATCHES %@", Regex)
                if predicate.evaluate(with: text) || string == ""
                {
                    return true
                }
                else
                {
                    return false
                }
          }
        
        return true
    }
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator

        // iterate over the mask characters until the iterator of numbers ends
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                // mask requires a number in this place, so take the next one
                result.append(numbers[index])

                // move numbers iterator to the next index
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }

}
