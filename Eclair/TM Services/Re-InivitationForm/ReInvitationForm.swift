//
//  ReInvitationForm.swift
//  Eclair
//
//  Created by iOS Indigo on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ReInvitationForm: UIViewController {

    @IBOutlet weak var ecodeTxt: UITextField!
    @IBOutlet weak var ecodeLbl: UILabel!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var datetxt: UITextField!
    var isEcodeValid = false
    var guestEcode = ""
    var selectedDate = ""
    var dateForSending = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        submitbtn.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func submitbuton(_ sender: Any) {
        if ecodeTxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Enter Ecode please.")
            return
        }
        else if isEcodeValid == false
        {
            Utilities.showAlert("", message: "Ecode you entered is not valid")
            return
        }
        else if datetxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Select Invite Date Please")
            return
        }
        else
        {
            //self.submitAPI()
           // self.alert(title: "", message: "Are you sure you want to submit this request?", tag: "1")
            self.alert(title: "", message: "Are you sure you want to submit this request?", tag: "1", ok: "Yes", cancel: "No")
        }
        
    }
    func alert(title:String,message:String,tag:String,ok:String,cancel:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
             
             let canceled = UIAlertAction(title: cancel, style: .default, handler: { action in
             })
            if tag == "1"
            {
                alert.addAction(canceled)
            }
            let ok = UIAlertAction(title: ok, style: .default, handler: { action in
               if tag == "1"
               {
                   self.submitAPI()
               }
               else
               {
                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToHome"), object: nil)
               }
                            })
            alert.addAction(ok)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
   //MARK:- api calling
    
    func verifyEcode(ecode: String) {
        
        let parameters = [
            "ecode" : ecode,
        ]
        print("parameter \(parameters)")
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kVerifyEcode, parameters: parameters) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            if (data == nil) {
                return
            }
            
            do {
                let json = try? JSON(data: data!)
                print("jsonValue \(json ?? [])")
                if let response = json?["response"].boolValue, response == true {
                    self.isEcodeValid = true
                    self.guestEcode = ecode
                    self.ecodeLbl.text = json?["name"].string ?? ""
                    self.ecodeLbl.isHidden = false
                }
                else {
                    Utilities.showAlert("Ecode Error", message: "Invalid ecode!")
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }

    
    func submitAPI() {
        
        let type = UserDefaults.standard.string(forKey: "memberType") ?? ""
        let parameters = [
            "Ecode" : self.guestEcode,
            "Type":type,
            "SubmitedBy":Constants.ecode,
            "Date":self.dateForSending
        ]
        print("parameter \(parameters)")
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.ksaveRe_InvitationForm, parameters: parameters) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            if (data == nil) {
                return
            }
            
            do {
                let json = try? JSON(data: data!)
                print("jsonValue \(json ?? [])")
                let response = json?["response"].bool ?? false
                let msg = json?["msg"].string ?? ""
                if response == true
                {
                    self.alert(title: "", message: "Saved Successfully", tag: "", ok: "Ok", cancel: "")
                }
                else
                {
                    Utilities.showAlert("", message: msg)
                    return
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
}
extension ReInvitationForm : GetDateDelegate,UITextFieldDelegate{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "date"
        {
            let vc = segue.destination as? DatePickerVc
            vc?.delegate = self
            vc?.minimumFromCurrent = "greater"
        }
    }
    
    
    func getSelectedDate(date: String) {
        self.selectedDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "dd - MM - yyyy")
        self.dateForSending = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "MM/dd/yyyy HH:mm:ss")
        self.datetxt.text = "Date " + "\(self.selectedDate)"
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.datetxt
        {
            textField.resignFirstResponder()
            self.performSegue(withIdentifier: "date", sender: self)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.ecodeTxt
        {
            if (textField.text != nil && !textField.text!.isEmpty) {
                
                let text = textField.text ?? ""
                if text.count > 0
                {
                    let type = text.prefix(1)
                    
                    if type == "g"
                    {
                        verifyEcode(ecode: textField.text!)
                    }
                    else
                    {
                        Utilities.showAlert("", message: "Please enter guest ecode!")
                        return
                    }
                }
            }
        }
    }
}
