//
//  InvitationHistoryCell.swift
//  Eclair
//
//  Created by Indigo on 04/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class InvitationHistoryCell: UITableViewCell {

    @IBOutlet weak var requestIDlbl: UILabel!
    @IBOutlet weak var typelbl: UILabel!
    @IBOutlet weak var ecodelbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var statuslbl: UILabel!
    @IBOutlet weak var inviteDatelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
