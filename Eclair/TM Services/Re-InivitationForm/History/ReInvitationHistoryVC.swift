//
//  ReInvitationHistoryVC.swift
//  Eclair
//
//  Created by Indigo on 04/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReInvitationHistoryVC: UIViewController {

    @IBOutlet weak var tblVw: UITableView!
    var historyData = [reInHistoryData]()
    private let refreshControl = UIRefreshControl()
    var skip = 0
    var limit = 10
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        // Do any additional setup after loading the view.
        tblVw.delegate = self
        tblVw.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.skip = 0
        HistoryList()
    }
    @objc private func refreshData(_ sender: Any) {
        self.skip = 0
        historyData.removeAll()
        self.tblVw.reloadData()
        self.HistoryList()
        self.refreshControl.endRefreshing()
    }
    func HistoryList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kHistoryReInvitation+"\(Constants.ecode)&skip=\(skip)&limit=\(limit)", parameters: nil) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(reInvitationHistoryModel.self, from: data!)
             if(model?.response == true)
             {
                if model?.data?.count ?? -1 <= 0 && self.skip <= 0
                {
                    Utilities.showAlert("", message: "No Data Found")
                    return
                }
                else if model?.data?.count ?? -1 <= 0 && self.skip > 0
                {
                    return
                }
                else
                {   self.historyData += model?.data ?? []
                    self.tblVw.reloadData()
                }
             }
             else
             {
                 Utilities.showAlert("", message: "Error: \(model?.msg ?? "server Error")")
             }
      }
}
}
extension ReInvitationHistoryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! InvitationHistoryCell
        cell.requestIDlbl.text = "\(historyData[indexPath.row].ID ?? 0)"
        cell.typelbl.text = historyData[indexPath.row].Type
        cell.ecodelbl.text = historyData[indexPath.row].Ecode
        cell.datelbl.text = ""
        let status = historyData[indexPath.row].status ?? ""
        var updatedStatus = ""
        if status == "a"
        {
            updatedStatus = "Approved"
        }
        else if status == "c"
        {
            updatedStatus = "Cancelled"
        }
        else if status == "p"
        {
            updatedStatus = "Pending"
        }
        else if status == "s"
        {
            updatedStatus = "Submitted"
        }
        else if status == "r"
        {
            updatedStatus = "Rejected"
        }
        else
        {
            updatedStatus = "Not Defined"
        }
        cell.statuslbl.text = updatedStatus
        let getDate = historyData[indexPath.row].StrDate ?? ""
        cell.datelbl.text = getDate
        cell.inviteDatelbl.text = historyData[indexPath.row].Date ?? ""
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
            if indexPath.row == self.historyData.count - 1 {
                    skip += limit
                    self.HistoryList()
            }
     }
}
