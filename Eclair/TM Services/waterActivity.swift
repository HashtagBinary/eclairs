//
//  waterActivity.swift
//  Eclair
//
//  Created by iOS Indigo on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class waterActivity: UIViewController {

    @IBOutlet weak var boatTxt: UITextField!
    @IBOutlet weak var portTxt: UITextField!
    @IBOutlet weak var remainingSeatsLbl: UILabel!
    @IBOutlet weak var timeFromTxt: UITextField!
    @IBOutlet weak var timeToTx: UITextField!
    @IBOutlet weak var gameTypeTxt: UITextField!
    @IBOutlet weak var rideTypeTxt: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    var boats = ["one","Two"]
    var ports = ["Gwandar","Karachi"]
    var timeFrom = ["6:00","7:00","8:00","9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00"]
    var timeTo = ["6:00","7:00","8:00","9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00"]
    var games = ["Football","Cricket"]
    var rides = ["Local","Special"]
    
    var boatPicker = UIPickerView()
    var portPicker = UIPickerView()
    var timeFromPicker = UIPickerView()
    var timeToPicker = UIPickerView()
    var gamePicker = UIPickerView()
    var ridePicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupVw()
    }
func setupVw()
{
    boatPicker.delegate = self
    portPicker.delegate = self
    timeFromPicker.delegate = self
    timeToPicker.delegate = self
    gamePicker.delegate = self
    ridePicker.delegate = self
    
    boatPicker.tag = 1
    portPicker.tag = 2
    timeFromPicker.tag = 3
    timeToPicker.tag = 4
    gamePicker.tag = 5
    ridePicker.tag = 6
    submitButton.cornerRadius = 10
     
}
}
extension waterActivity: UIPickerViewDataSource, UIPickerViewDelegate,UITextFieldDelegate {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == boatTxt
        {
            textField.inputView = boatPicker
            textField.text = boats[0]
        }
        else if textField == portTxt
        {
            textField.inputView = portPicker
            textField.text = ports[0]
        }
        else if textField == timeFromTxt
        {
            textField.inputView = timeFromPicker
            textField.text = timeFrom[0]
        }
        else if textField == timeToTx
        {
            textField.inputView = timeToPicker
            textField.text = timeTo[0]
        }
        else if textField == gameTypeTxt
        {
            textField.inputView = gamePicker
            textField.text = games[0]
        }
        else if textField == rideTypeTxt
        {
            textField.inputView = ridePicker
            textField.text = rides[0]
        }
        
    }
    
    
    
    // assign rows in pickerview
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        // Set Rows Count
        switch pickerView.tag {
        case 1:
            return boats.count
        case 2:
            return ports.count
        case 3:
            return timeFrom.count
        case 4:
            return timeTo.count
        case 5:
            return games.count
        case 6:
            return rides.count
        default:
            return 1
        }
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // rows title like male,female
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //Setting Row Data
        switch pickerView.tag {
        case 1:
            return boats[row]
        case 2:
            return self.ports[row]
        case 3:
            return timeFrom[row]
        case 4:
            return self.timeTo[row]
        case 5:
            return games[row]
        case 6:
            return self.rides[row]
        default:
            return "N/A"
        } 
        
    }
    
    // action on each row select
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Setting Row Data
        switch pickerView.tag {
        case 1:
            boatTxt.text = boats[row]
      
        case 2:
            portTxt.text = self.ports[row]
        case 3:
            timeFromTxt.text = timeFrom[row]
      
        case 4:
            timeToTx.text = self.timeTo[row]
        case 5:
            gameTypeTxt.text = games[row]
      
        case 6:
            rideTypeTxt.text = self.rides[row]
        default:
            return
        }
    }
    
   
}
