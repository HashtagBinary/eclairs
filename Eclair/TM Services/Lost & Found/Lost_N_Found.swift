//
//  Lost_N_Found.swift
//  Eclair
//
//  Created by iOS Indigo on 10/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class Lost_N_Found: UIViewController {

    @IBOutlet weak var titletxt: UITextField!
    @IBOutlet weak var selectImgVw: RectangleView!
    @IBOutlet weak var imgContainerVw: RectangleView!
    @IBOutlet weak var selectedImg: UIImageView!
    @IBOutlet weak var descriptiontxt: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    var descriptionPlaceholder = "Enter Lost Items Description"
    var placeDesc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        submitButton.cornerRadius = 10
        let imgTap = UITapGestureRecognizer(target: self, action: #selector(self.handleImageTap(_:)))
        selectImgVw.addGestureRecognizer(imgTap)
        
        descriptiontxt.text = descriptionPlaceholder
        descriptiontxt.textColor = UIColor.lightGray
        descriptiontxt.delegate = self
    }
    @objc func handleImageTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
      showGalleryOpt()
    }
    @IBAction func submitapi(_ sender: Any) {
        if titletxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please enter title first")
            return
        }
        else if descriptiontxt.text == self.descriptionPlaceholder
        {
            self.placeDesc = ""
            self.alert(title: "", message: "Are you sure you want to submit the request?")
        }
        else
        {
            self.placeDesc = self.descriptiontxt.text ?? ""
            self.alert(title: "", message: "Are you sure you want to submit the request?")
        }
    }
    func alert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
             
             let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
             })
             alert.addAction(cancel)
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
               self.submitAPI(desc: self.placeDesc)
               
            })
            alert.addAction(ok)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    
    func submitAPI(desc : String){
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let tokenString = UserDefaults.standard.string(forKey: "token") ?? ""
        let Auth_header : HTTPHeaders = [ "Authorization" : "Bearer \(tokenString)",
                                          "Content-type": "multipart/form-data",
                                          "Content-Disposition" : "form-data"
        ]
        let parameters: [String: String] = [
            
            "Description": desc,
            "ECode":Constants.ecode,
            "Title":self.titletxt.text ?? ""
        ]
        print("paramerter \(parameters)")
            AF.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                let NicImgData = self.selectedImg.image?.jpegData(compressionQuality: 1)
                if let data = NicImgData{
                    multipartFormData.append(data, withName: "image", fileName: "imagename.jpg", mimeType: "image/jpeg")
                }
                else
                {
                    Utilities.showAlert("", message: "Getting some error. Please try again later")
                    return
                }

            }, to:Constants.ServerAPI.ksaveLostNFound,headers: Auth_header).response{ response in
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                if(response.data != nil){
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                    let jsonData = response.data!
                    let json = try? JSON(data: jsonData)
                    let respns = json?["response"].bool ?? false
                    if respns == false
                    {
                     let message = json?["msg"].string ?? ""
                     Utilities.showAlert("", message: message)
                     return
                    }
                    else
                    {
                     let message = json?["msg"].string ?? ""
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToHome"), object: nil)
                     Utilities.showAlert("", message: message)
                     return
                    }
                    print("Json data for saving images \(json)")
                }
                else{
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                     print(response.error?.localizedDescription)
                }
            }

        }
}
extension Lost_N_Found : UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = descriptionPlaceholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func showGalleryOpt(){
        
        let alert = UIAlertController(title: "Upload Picture", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
            self.getPhotoFromLib()
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.getPhotoFromCam()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getPhotoFromLib(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func getPhotoFromCam(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage{
           
                self.selectedImg.image = image
                self.imgContainerVw.isHidden = false
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
