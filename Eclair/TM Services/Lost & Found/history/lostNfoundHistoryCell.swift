//
//  lostNfoundHistoryCell.swift
//  Eclair
//
//  Created by Indigo on 19/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class lostNfoundHistoryCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var disclbl: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var dateTimelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
