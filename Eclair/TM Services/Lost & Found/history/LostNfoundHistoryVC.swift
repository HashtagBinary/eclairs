//
//  LostNfoundHistoryVC.swift
//  Eclair
//
//  Created by Indigo on 19/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LostNfoundHistoryVC: UIViewController {

    @IBOutlet weak var tblvw: UITableView!
    var history = [complaints]()
    var villaHistory = [villaMaintenanceModelData]()
    var isVilla = false
    private let refreshControl = UIRefreshControl()
    var skip = 0
    var limit = 10
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        isVilla = UserDefaults.standard.bool(forKey: "isVilla")
        if isVilla == false
        {
            HistoryList()
        }
        else
        {
            HistoryListOfVilla()
        }        
    }
    @objc private func refreshData(_ sender: Any) {
        
        self.skip = 0
        if isVilla == false
        {
            history.removeAll()
            HistoryList()
        }
        else
        {
            villaHistory.removeAll()
            HistoryListOfVilla()
        }
        self.tblvw.reloadData()
        self.refreshControl.endRefreshing()
    }
    func HistoryListOfVilla()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params :Parameters = [
            "id" : Constants.ecode,
            "skip" : self.skip,
            "take" : self.limit
        ]
        print("url \(Constants.ServerAPI.kGetTokens)")
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kGetMaintananceHistory, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(villaMaintenanceModel.self, from: data!)

             if(model?.response == true)
             {
                if model?.data?.count ?? -1 <= 0 && self.skip <= 0
                {
                    Utilities.showAlert("", message: "No Data Found")
                    return
                }
                else if model?.data?.count ?? -1 <= 0 && self.skip > 0
                {
                    return
                }
                else
                {
                    self.villaHistory += model?.data ?? []
                    self.tblvw.reloadData()
                }
             }
             else
             {
                Utilities.showAlert("", message: "Error: \(model?.msg ?? "server Error")")
             }
      }
}
    func HistoryList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params :Parameters = [
            "ecode" : Constants.ecode,
            "skip" : skip,
            "take" : limit
        ]
        print("url \(Constants.ServerAPI.kGetTokens)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kHistoryLostNFound, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(lostNfoundHistoryModel.self, from: data!)

             if(model?.response == true)
             {
                if model?.complaints?.count ?? -1 <= 0 && self.skip <= 0
                {
                    Utilities.showAlert("", message: "No Data Found")
                    return
                }
                else if model?.complaints?.count ?? -1 <= 0 && self.skip > 0
                {
                    return
                }
                else
                {
                    self.history += model?.complaints ?? []
                    self.tblvw.reloadData()
                }
             }
             else
             {
                 Utilities.showAlert("", message: "Error: \(model?.error ?? "server Error")")
             }
        }
    }
}
extension LostNfoundHistoryVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isVilla == false
        {
            return history.count
        }
       else
        {
            return villaHistory.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! lostNfoundHistoryCell
        cell.disclbl.numberOfLines = 0
        if isVilla == false
        {
            cell.titlelbl.text = history[indexPath.row].Title ?? ""
            cell.disclbl.text = history[indexPath.row].Description
            let status = history[indexPath.row].Status ?? ""
            let url = history[indexPath.row].Photo ?? "image_loading"
            cell.img.image = UIImage.init(named: "image_loading")
            cell.img.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.img.image = data
            }
            cell.statusButton.setTitle("Status : \(status)", for: .normal)
            cell.dateTimelbl.text = history[indexPath.row].StrDate ?? ""
        }
        else
        {
            cell.titlelbl.text = villaHistory[indexPath.row].DeptName ?? ""
            cell.disclbl.text = villaHistory[indexPath.row].ItemName
            let status = villaHistory[indexPath.row].Status ?? ""
            cell.dateTimelbl.text = history[indexPath.row].StrDate ?? ""

            let url = villaHistory[indexPath.row].Picture ?? "image_loading"
            cell.img.image = UIImage.init(named: "image_loading")
            cell.img.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.img.image = data
            }
            var updatedStatus = ""
            if status == "a"
            {
                updatedStatus = "Approved"
            }
            else if status == "c"
            {
                updatedStatus = "Cancelled"
            }
            else if status == "p"
            {
                updatedStatus = "Pending"
            }
            else if status == "s"
            {
                updatedStatus = "Submitted"
            }
            else if status == "r"
            {
                updatedStatus = "Rejected"
            }
            else
            {
                updatedStatus = "Not Defined"
            }
            cell.statusButton.setTitle("Status : \(updatedStatus)", for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isVilla == false
        {
            if indexPath.row == self.history.count - 1 {
                    skip += limit
                    self.HistoryList()
            }
        }
        else
        {
            if indexPath.row == self.villaHistory.count - 1 {
                    skip += limit
                    self.HistoryListOfVilla()
            }
        }
    }
}

