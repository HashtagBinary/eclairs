//
//  GuestListVC.swift
//  Eclair
//
//  Created by iOS Indigo on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class GuestListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    var guestList = [guestListModelData]()
    var limit = 10
    var skip = 0
    var emptySlots = Constants.guestSlots
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getGuestList()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return guestList.count
        }
        else
        {
            let count = Constants.guestSlots - self.guestList.count
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! subTenantsCell
            cell.titleLbl.text = self.guestList[indexPath.row].ecode
            cell.namelbl.text = self.guestList[indexPath.row].name
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptySlotsCell") as! subTenantsCell
            return cell
        }
    }
    
    func getGuestList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kGetGuestList+"\(Constants.ecode)", parameters: nil) {  (data:Data?, error:NSError?)  in
            if error != nil {
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(guestListModel.self, from: data!)
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                if (model.response ?? false) {
                    self.guestList = model.data ?? []
                }
                self.tableview.reloadData()
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
}
