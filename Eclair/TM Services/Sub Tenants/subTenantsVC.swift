//
//  subTenantsVC.swift
//  Eclair
//
//  Created by iOS Indigo on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class subTenantsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableview: UITableView!
    var tenantsList = [TenantsListModelData]()
    var limit = 10
    var skip = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTenantsList()
        // Do any additional setup after loading the view.
    
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return tenantsList.count
        }
        else
        {
            let empty = Constants.tenantsSlots - tenantsList.count
            return empty
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! subTenantsCell
            cell.titleLbl.text = tenantsList[indexPath.row].EclairCode
            cell.namelbl.text = tenantsList[indexPath.row].FullName
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptySlotsCell") as! subTenantsCell
            return cell
        }
    }
    func getTenantsList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kGetTenantsList+"\(Constants.ecode)", parameters: nil) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
              }
            do{
                let model = try JSONDecoder().decode(TenantsListModel.self, from: data!)
                if (model.response ?? false) {
                    self.tenantsList = model.data ?? []
                }
                self.tableview.reloadData()
                } catch let error as NSError {
                print(error)
            }
        }
    }
}
