//
//  LevelingVC.swift
//  Eclair
//
//  Created by iOS Indigo on 23/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LevelingVC: UIViewController {

    @IBOutlet weak var tableVw: UITableView!
    var levelModelList = [LevelingModelLevel]()
    var currentLevelID = -1
    var levelPercentage = 0
    var currentProgressXp = -1
    var levelModel: LevelingModel?
    var levelReward = [LevelingRewardSettings]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
    }
    override func viewDidAppear(_ animated: Bool) {
        
        getLevelsData()
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LevelAnimationVC") as! LevelAnimationVC
//        newViewController.levelid = 30
//        self.present(newViewController, animated: true, completion: nil)
        
    }
    
    func getLevelsData()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params : Parameters = ["ecode":Constants.ecode]
        print("Params \(params)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kGetlevels, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("json \(json ?? [])")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(LevelingModel.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            if (model != nil) {
                self.levelModel = model
                let seasons = model?.seasons
                self.currentLevelID = model?.currentlevelId ?? -1
                self.levelPercentage = model?.levelProgress ?? 0
                self.levelModelList = seasons?[0].levels ?? []
                self.levelReward = model?.rewardsSettings ?? []
                self.levelModelList.reverse()
                self.tableVw.reloadData()
                print("rewards \(self.levelReward)")
                let index = self.levelModelList.firstIndex { $0.title == self.currentLevelID }
                if (index != nil) {
                    let indexPath = IndexPath(row: index!, section: 0)
                    self.tableVw.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
                if self.levelModelList.count > 0
                {
                    let lastLevel = self.levelModelList[0].title ?? -1
                    if self.currentLevelID == lastLevel && self.levelPercentage == 100
                    {
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LevelAnimationVC") as! LevelAnimationVC
                        newViewController.levelid = self.currentLevelID
                        self.present(newViewController, animated: true, completion: nil)
                    }
                }
            }
       }
  }
}

extension LevelingVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return levelModelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LevelingCell
        
        cell.ProgressStackBox.removeAllArrangedSubviews()
        if (self.levelPercentage > 100) {
            self.levelPercentage = 100
        }
        cell.statusImage.isHidden = true
        cell.percentagelbl.isHidden = true
        cell.ProgressStackBox.isHidden = true
        cell.LevelLbl.text = "Level \(levelModelList[indexPath.row].title ?? 0)"
        //cell.requiredXpLBL.text = "0 / \(levelModelList[indexPath.row].requiredXP ?? 0) XP"
        if indexPath.row == 0
        {
            cell.requiredXpLBL.text = ""
            cell.neededlbl.text = ""
        }
        else
        {
            cell.requiredXpLBL.text = "0 / \(levelModelList[indexPath.row - 1].requiredXP ?? 0) XP"
            if self.currentLevelID == levelModelList[indexPath.row].title {
                cell.bgImage.image = UIImage(named: "level_bg2")
                if self.levelPercentage == 100 {
                    cell.statusImage.isHidden = false
                    cell.ProgressStackBox.isHidden = false
                    cell.requiredXpLBL.text = "\(levelModelList[indexPath.row - 1].requiredXP ?? 0) / \(levelModelList[indexPath.row - 1].requiredXP ?? 0)"
                    cell.statusImage.image = UIImage(named: "complete_icon")
                    cell.neededlbl.text = "COMPLETED"
                }
                else {
                    cell.percentagelbl.isHidden = false
                    cell.ProgressStackBox.isHidden = false
                    cell.neededlbl.text = ""
                    cell.percentagelbl.text = "\(levelModel?.currentProgressXp ?? 0) XP"
                    cell.requiredXpLBL.text = "\(levelModel?.currentXp ?? 0) / \(levelModelList[indexPath.row - 1].requiredXP ?? 0) XP"
                }
                
                if self.levelPercentage > 0 {
                    let constraintPerc = (CGFloat(self.levelPercentage) / 100.0)
                    let view1 = UIView()
                    view1.backgroundColor = Constants.hexStringToUIColor(hex: "5817B2")
                    view1.widthAnchor.constraint(equalToConstant: 100).isActive = true
                    cell.ProgressStackBox.addArrangedSubview(view1)
                    view1.widthAnchor.constraint(equalTo: cell.ProgressStackBox.widthAnchor, multiplier: constraintPerc).isActive = true
                    
                    let view2 = UIView()
                    view2.backgroundColor = UIColor.clear
                    view2.widthAnchor.constraint(equalToConstant: 100).isActive = true
                    cell.ProgressStackBox.addArrangedSubview(view2)
                    view2.widthAnchor.constraint(equalTo: cell.ProgressStackBox.widthAnchor, multiplier: (1 - constraintPerc)).isActive = true
                }
            }
            else if self.currentLevelID > levelModelList[indexPath.row].title ?? 0 {
                cell.statusImage.isHidden = false
                cell.statusImage.image = UIImage(named: "complete_icon")
                cell.requiredXpLBL.text = "\(levelModelList[indexPath.row - 1].requiredXP ?? 0) / \(levelModelList[indexPath.row - 1].requiredXP ?? 0) XP"
                cell.neededlbl.text = "COMPLETED"
                cell.bgImage.image = UIImage(named: "level_bg2")
            }
            else {
                cell.statusImage.isHidden = false
                cell.statusImage.image = UIImage(named: "lock")
                cell.bgImage.image = UIImage(named: "level_bg")
                cell.neededlbl.text = ""
            }
        }
        cell.headerLabel.text = "-"
        let id = levelModelList[indexPath.row].title
        let reward = self.levelReward.first {$0.levelNo == id}
        if reward != nil
        {
            cell.headerLabel.text = reward?.title ?? "-"
        }
        return cell
    }
}
