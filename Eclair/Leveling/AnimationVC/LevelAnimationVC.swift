//
//  LevelAnimationVC.swift
//  Eclair
//
//  Created by Zohaib Naseer on 7/6/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class LevelAnimationVC: UIViewController {

    @IBOutlet weak var animationImg: UIImageView!
    @IBOutlet weak var levelLbl: UILabel!
    @IBOutlet weak var completedLbl: UILabel!
    @IBOutlet weak var levelTitlelbl: UILabel!
    var levelid = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        levelLbl.text = "\(levelid)"
        let gifImage = UIImage.gifImageWithName("levelGifAnimation")
        self.animationImg.image = gifImage
    }
    @IBAction func skipbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
