//
//  LevelingCell.swift
//  Eclair
//
//  Created by iOS Indigo on 23/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class LevelingCell: UITableViewCell {
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var containerVw: UIView!
    
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var percentagelbl: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var LevelLbl: UILabel!
    @IBOutlet weak var requiredXpLBL: UILabel!
    @IBOutlet weak var ProgressStackBox: UIStackView!
    @IBOutlet weak var neededlbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
