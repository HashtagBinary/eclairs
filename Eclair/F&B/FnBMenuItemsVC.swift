//
//  FnBMenuItemsVC.swift
//  Eclair
//
//  Created by Sohail on 08/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Kingfisher

class FnBMenuItemsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var summaryBoxView: SummaryBoxView!
    
    var departmentModel: FnBMenuDepartment?
    var categoryModel: FnBMenuCategory!
    var arrayList = [FnBMenuItem]()
    
    var dbHelper = DBHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.categoryModel.title
        
        setupViews()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "FnBJuiceMenuItemTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBJuiceMenuItemTableViewCell")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSummaryBoxClick))
        summaryBoxView.addGestureRecognizer(tap)
        summaryBoxView.isHidden = true
        
        fetchData()
    }
    override func viewWillAppear(_ animated: Bool) {
        updateCartSummaryBox()
    }
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
        
    }
    
    func setupViews() {
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    @objc func onSummaryBoxClick(sender: SummaryBoxView) {
        let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBCartViewController") as? FnBCartViewController

        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    @objc func addToCart(sender: NumberButton) {
//        dbHelper.updateSpaProductItem(id: sender.tag, quantity: sender.value, catId: self.spaProductCategory.id!)
//        updateCartSummaryBox()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return 1
        }
        else {
            return self.arrayList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            if (departmentModel!.isJuice!) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FnBJuiceMenuItemTableViewCell", for: indexPath) as! FnBJuiceMenuItemTableViewCell
                
                return cell
            }
            else {
                return UITableViewCell()
            }
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FnBMenuItemTableViewCell
            
            cell.titleLabel.text = self.arrayList[indexPath.row].title
            cell.descriptionLabel.text = self.arrayList[indexPath.row].description
            cell.priceLabel.text = "\(self.arrayList[indexPath.row].price ?? 0) \(Constants.CURRENCY_SYMBOL)"
            
            //cell.imgView.sd_setImage(with: URL(string: self.arrayList[indexPath.row].image ?? ""), placeholderImage: UIImage(named: "image_loading"))
            
            let url = arrayList[indexPath.item].image ?? "image_loading"
            cell.imgView.image = UIImage.init(named: "image_loading")
            cell.imgView.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.imgView.image = data
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            if departmentModel!.isJuice == false {
                return 0
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 0) {
            if (departmentModel!.isJuice!) {
                let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBJuiceViewController") as! FnBJuiceViewController

                vc.department_id = departmentModel?.id ?? -1
                self.navigationController!.pushViewController(vc, animated: true)
            }
        }
        else {
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuIngredientVC") as! FnBMenuIngredientVC

            vc.itemModel = arrayList[indexPath.row]
            self.navigationController!.pushViewController(vc, animated: true)
        }
    }
    
    func fetchData()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let parameters = [
            "id" : categoryModel?.id,
        ]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kGetFnBItemsById, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let model = try? JSONDecoder().decode(FnBMenuItems.self, from: data!)
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            
            self.arrayList = model!.menuItems
            self.tableView.reloadData()
        }
    }
    
    func updateCartSummaryBox() {
        let fnbTotal = FnB_Home.calculateFnbPrice()
        summaryBoxView.updateValue(count: fnbTotal.count, total: fnbTotal.total)
        summaryBoxView.isHidden = (fnbTotal.count <= 0) ? true : false
    }
}

class FnBMenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgView.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
