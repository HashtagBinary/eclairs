//
//  MyGlassVC.swift
//  Eclair
//
//  Created by Indigo on 15/04/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MyGlassVC: UIViewController {
    var emptySlots = 5
    var glassList = [JSON]()
    var addedFlavours = [FnBJuiceFlavor]()
    var myFlavours = [FnBJuiceFlavor]()
    var delegate : JuiceGlassUpdateDelegate?
    var selectedItem : FnBJuiceGlassItem?
    var isSave = false
    var selectedSlotId = 0
    
    @IBOutlet weak var tblvw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getGlass()
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isBeingDismissed {
            delegate!.updateGlass()
            delegate?.updateCheckedImages()
        }
    }
    @IBAction func cancelbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func okbtn(_ sender: Any) {
        
        juiceSelectedFlavours = selectedItem?.flavours ?? []
        juiceSelectedIceId = selectedItem?.ice_id ?? -1
        juiceSelectedBlendId = selectedItem?.blend_id ?? -1
        juiceSelectedGlassId = selectedItem?.glass_id ?? -1
        juiceSelectedSweetenerId = selectedItem?.sweetner_id ?? -1
        juiceSelectedSyrupID = selectedItem?.syrup_id ?? -1
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- api calling
    
    func getGlass() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBGetGlasses, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            let response = json?["response"].bool ?? false
            if response == false
            {
                Utilities.showAlert("", message: json?["error"].string ?? "Data Not Found")
                return
            }
            else
            {
                self.glassList = json?["glass"].arrayValue ?? []
                self.emptySlots -= self.glassList.count
                self.tblvw.reloadData()
            }
        }
    }
    func saveGlassData()
    {
        if juiceSelectedFlavours.count == 1 {
            juiceSelectedFlavours[0].percentage = 100.0
        }
        
        let item = juiceIngredients?.juiceGlass.first { $0.id == juiceSelectedGlassId }
        var total: Float = 0
        if (item != nil) {
            total = item?.price ?? 0
        }
        
        let cartItems = FnBJuiceCartItem(glass_id: juiceSelectedGlassId, ice_id: juiceSelectedIceId,
                                        sweetner_id: juiceSelectedSweetenerId, blend_id: juiceSelectedBlendId,syrup_id: juiceSelectedSyrupID,
                                        flavours: juiceSelectedFlavours, totalprice: Double(total))
        let cartItem = FnBJuiceGlassItem(glass_id: juiceSelectedGlassId, ice_id: juiceSelectedIceId,
                                         sweetner_id: juiceSelectedSweetenerId, blend_id: juiceSelectedBlendId,syrup_id: juiceSelectedSyrupID,
                                         flavours: juiceSelectedFlavours, totalprice: Double(total))
        let jsonData = try? JSONEncoder().encode(cartItem)
        let json = String(data: jsonData!, encoding: String.Encoding.utf8)
        self.saveGlassApi(glassData: json ?? "")
    }
    
    
    func saveGlassApi(glassData : String) {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        var id = ""
        if self.selectedSlotId != 0
        {
            id = "\(self.selectedSlotId)"
        }
        let parameters = [
            "id":id,
            "ecode" : Constants.ecode,
            "glass" : glassData
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBSaveGlass, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            let response = json?["response"].bool ?? false
            if response == true
            {
                self.dismiss(animated: true, completion: nil)
                Utilities.showAlert("", message: "Juice glass saved successfully")
            }
            else
            {
                Utilities.showAlert("", message: json?["error"].string ?? "Server Error")
            }
            print("json response of save glass \(json)")
        }
    }
    
    
    
    func removeGlassApi(id : String) {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let parameters = [
            "id":id
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBRemoveGlass, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            let response = json?["response"].bool ?? false
            
            if response == true
            {
                
//                Utilities.showAlert("", message: "Juice glass deleted successfully")
                self.showToast(message: "Juice glass deleted successfully")
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                Utilities.showAlert("", message: json?["error"].string ?? "Server Error")
            }
        }
    }
    
    func alertOption(message:String,tag:Int)
    {
        let refreshAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
          print("Handle Ok logic here")
            if tag == 1{
                
            }
            else
            {
                self.selectedSlotId = 0
            }
            self.saveGlassData()
          }))

        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
          print("Handle Cancel Logic here")
          }))

        present(refreshAlert, animated: true, completion: nil)
    }
    @objc func RemovePressed(sender: UIButton){
        print(sender.tag)
        
        let id = self.glassList[sender.tag]["id"].int ?? 0
        
        
        
        let refreshAlert = UIAlertController(title: "Confirmation", message: "Are you sure you want to delete the glass?", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
          print("Handle Ok logic here")
           
            self.removeGlassApi(id: "\(id)")
          }))

        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
          print("Handle Cancel Logic here")
          }))

        present(refreshAlert, animated: true, completion: nil)
        
        
        
        
        
        
        
        
    }
}
extension MyGlassVC:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return self.glassList.count
        }
        else
        {
            return emptySlots
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MyGlassCell
        
        if indexPath.section == 0
        {
            let datas = self.glassList[indexPath.row]["glass"].string ?? ""
            let f = try? JSONDecoder().decode(FnBJuiceGlassItem.self, from: datas.data(using: .utf8)!)
            let data = datas.data(using: .utf8)
            
            do {
                let str = try JSONDecoder().decode(FnBJuiceGlassItem.self, from: datas.data(using: .utf8)!)
                    print("parsed data is")
                    print(str)
               }
               catch let caught as NSError
               {
                print("error is \(caught)")
               }
            
            
            cell.juiceGlass.juiceFlavors = f?.flavours ?? []
            cell.juiceGlass.juiceFlavors.reverse()
            cell.juiceGlass.UpdateFlavors()
            cell.glassNumberlbl.text = "\(indexPath.row + 1)"
            cell.removeButton.isHidden = false
            cell.removeButton.tag = indexPath.row
            cell.removeButton.addTarget(self, action: #selector(RemovePressed), for: .touchUpInside)
            
            
            
            
            
            
            //MARK:- blend_ID
            let blend_id = f?.blend_id
            let item = juiceIngredients?.juiceBlendTimes.first { $0.id == blend_id
            }
            cell.blendTimelbl.text = item?.title
            //MARK:- glass_id
            let glass_id = f?.glass_id
            let Glasses = juiceIngredients?.juiceGlass.first { $0.id == glass_id
            }
            cell.glassSizelbl.text = Glasses?.title
            //MARK:- sweetner
            let sweetner_id = f?.sweetner_id
            let sweetner = juiceIngredients?.juiceSweetner.first { $0.id == sweetner_id
            }
            cell.sweetnerlbl.text = sweetner?.title
            //MARK:- ice_cube
            let ice_id = f?.ice_id
            let ices = juiceIngredients?.juiceIce.first { $0.id == ice_id
            }
            cell.iceCubelbl.text = ices?.title
            
            //MARK:- syrup
            let syrup_id = f?.syrup_id
            let syrupes = juiceIngredients?.juiceSyrups.first { $0.id == syrup_id
            }
            cell.syruplbl.text = syrupes?.title
            return cell
        }
        else
        {
            cell.removeButton.isHidden = true
            cell.glassNumberlbl.text = "Empty Slot"
            cell.juiceGlass.juiceFlavors = []
            cell.juiceGlass.UpdateFlavors()
            cell.blendTimelbl.text = ""
            cell.glassSizelbl.text = ""
            cell.iceCubelbl.text = ""
            cell.sweetnerlbl.text = ""
            cell.syruplbl.text = ""
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSave == false
        {
            if indexPath.section == 0
            {
                let datas = self.glassList[indexPath.row]["glass"].string ?? ""
                let f = try? JSONDecoder().decode(FnBJuiceGlassItem.self, from: datas.data(using: .utf8)!)
                selectedItem = f
                selectedItem?.flavours.reverse()
            }
            else
            {
                juiceSelectedFlavours = []
                juiceSelectedIceId = -1
                juiceSelectedBlendId = -1
                juiceSelectedGlassId = -1
                juiceSelectedSweetenerId = -1
                juiceSelectedSyrupID = -1
            }
        }
        else
        {
            if indexPath.section == 0
            {
                self.selectedSlotId = self.glassList[indexPath.row]["id"].int ?? 0
                self.alertOption(message: "Are you sure you want replace this juice slot?", tag: 1)
            }
            else
            {
                self.alertOption(message: "Are you sure you want to save the juice on this slot?", tag: 2)
            }
        }
    }
}
