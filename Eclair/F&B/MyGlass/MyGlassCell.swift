//
//  MyGlassCell.swift
//  Eclair
//
//  Created by Indigo on 15/04/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class MyGlassCell: UITableViewCell {

    @IBOutlet weak var juiceGlass: JuiceGlassSmall!
    @IBOutlet weak var glassNumberlbl: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var glassSizelbl: UILabel!
    @IBOutlet weak var blendTimelbl: UILabel!
    @IBOutlet weak var sweetnerlbl: UILabel!
    @IBOutlet weak var iceCubelbl: UILabel!
    @IBOutlet weak var syruplbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
