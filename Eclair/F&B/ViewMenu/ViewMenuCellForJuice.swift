//
//  ViewMenuCellForJuice.swift
//  Eclair
//
//  Created by Indigo on 10/03/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class ViewMenuCellForJuice: UITableViewCell {
    @IBOutlet weak var tableStack: UIStackView!
    @IBOutlet weak var juiceGlassVw: JuiceGlassSmall!
    @IBOutlet weak var viewIngredientsButton: UIButton!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var titlelbl: UILabel!
    var titleback = ""
    var price = 0.0
    var previusDataOfJuice = [String:Any]()
    
    
    var tblData = [[String:Any]]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tblVw.delegate = self
        tblVw.dataSource = self
//        self.tblVw.estimatedRowHeight = 44
//        self.tblVw.rowHeight = UITableView.automaticDimension
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
extension ViewMenuCellForJuice : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? FlavourCellForJuice
        
        switch indexPath.row {
        case 0:
            cell?.titlelbl.text = "Glass Size"
            let glassId = previusDataOfJuice["glass_id"] as? Int
            let glassitem = departmentsModel.juiceGlasses.first { $0.id == glassId }
            if glassitem != nil
            {
                cell?.valuelbl.text = glassitem?.title
            }
        case 1:
            cell?.titlelbl.text = "Sweetner"
            let sweetnerId = previusDataOfJuice["sweetner_id"] as? Int
            let seetneritem = departmentsModel.juiceSweetners.first { $0.id == sweetnerId }
            if seetneritem != nil
            {
                cell?.valuelbl.text = seetneritem?.title
            }
        case 2:
            cell?.titlelbl.text = "Ice Cube"
            let iceId = previusDataOfJuice["ice_id"] as? Int
            let iceitem = departmentsModel.juiceIces.first { $0.id == iceId }
            if iceitem != nil
            {
                cell?.valuelbl.text = iceitem?.title
            }
            
        case 3:
            cell?.titlelbl.text = "Blend Time"
            let blendId = previusDataOfJuice["blend_id"] as? Int
            let blenditem = departmentsModel.juiceBlendTimes.first { $0.id == blendId }
            if blenditem != nil
            {
                cell?.valuelbl.text = blenditem?.title
            }
        case 4:
            cell?.titlelbl.text = "syrup"
            let syrupId = previusDataOfJuice["syrupId"] as? Int
            let syrupitem = departmentsModel.juiceBlendTimes.first { $0.id == syrupId }
            if syrupitem != nil
            {
                cell?.valuelbl.text = syrupitem?.title
            }
        case 5:
            cell?.titlelbl.text = "Glass Price"
            let price = previusDataOfJuice["totalprice"] as? Int
//            let priceitem = departmentsModel.juiceBlendTimes.first { $0.id == priceId }
//            if priceitem != nil
//            {
            cell?.valuelbl.text = "SR \(price ?? 0)"
           // }
        default:
            cell?.titlelbl.text = "Not Assigned"
        }
        
        
        
//        cell?.ingredients = ingredients[indexPath.row]["menuIngredients"] as? [[String:Any]] ?? []
//        cell?.menuItems = self.menuItems
//        cell?.titleback = self.titleback + "- \(indexPath.row)"
//        cell?.totalPrice = ingredients[indexPath.row]["totalprice"] as? Double ?? 0.0
//        cell?.subtbl.reloadData()
        //self.titlelbl.text = "\(titleback) - \(indexPath.row)"
        
        return cell!
    }
}
