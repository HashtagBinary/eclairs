//
//  ViewMenuCell.swift
//  Eclair
//
//  Created by Indigo on 11/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class ViewMenuCell: UITableViewCell {

    @IBOutlet weak var tableStack: UIStackView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewIngredientsButton: UIButton!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var titlelbl: UILabel!
    var titleback = ""
    var price = 0.0
    var ingredients = [[String:Any]]()
    var menuItems = [FnBMenuIngredient]()
    
    var tblData = [[String:Any]]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tblVw.delegate = self
        tblVw.dataSource = self
        self.tblVw.estimatedRowHeight = 400
        self.tblVw.rowHeight = UITableView.automaticDimension
        
        print("menuitmes \(menuItems)")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension ViewMenuCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SubingredientsCell
        cell?.ingredients = ingredients[indexPath.row]["menuIngredients"] as? [[String:Any]] ?? []
        cell?.menuItems = self.menuItems
        cell?.titleback = self.titleback + "- \(indexPath.row + 1)"
//        cell?.totalPrice = ingredients[indexPath.row]["totalprice"] as? Double ?? 0.0
        
        if let price = ingredients[indexPath.row]["totalprice"]
        {
            let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
            cell?.totalPrice = inDecimal.toDouble() ?? 0.0
        }
        else
        {
            cell?.totalPrice = 0.0
        }
        cell?.subtbl.reloadData()
        //self.titlelbl.text = "\(titleback) - \(indexPath.row)"
        
        return cell!
    }

    
}
