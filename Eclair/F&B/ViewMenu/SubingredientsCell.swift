//
//  SubingredientsCell.swift
//  Eclair
//
//  Created by Indigo on 11/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class SubingredientsCell: UITableViewCell {

 
    @IBOutlet weak var subtbl: UITableView!
    @IBOutlet weak var titlelbl: UILabel!
    var ingredients = [[String:Any]]()
    var menuItems = [FnBMenuIngredient]()
    var titleback = ""
    var totalPrice = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        subtbl.delegate = self
        subtbl.dataSource = self
//        subtbl.estimatedRowHeight = 70
//        subtbl.rowHeight = UITableView.automaticDimension
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension SubingredientsCell : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1
        {
            return 1
        }
        else
        {
            return ingredients.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.titlelbl.text = titleback
        if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "priceCell") as? subListOfIngredeintsCell
            cell?.valuelbl.text = "\(totalPrice) SR"
            cell?.titlelbl.text = "Ingredient Additional Cost"
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? subListOfIngredeintsCell
            
            let id = ingredients[indexPath.row]["id"] as? Int
            let qty = ingredients[indexPath.row]["quantity"] as? Int ?? 0
            
            let item = menuItems.first {$0.id == id}
            if item != nil
            {
                cell?.titlelbl.text = item?.title
                if item?.type == 2
                {
                    let subIng = item?.subIngredients
                    if subIng?.isEmpty == false
                    {
                        cell?.valuelbl.text = subIng?[0].title
                    }
                }
                else if item?.type == 0
                {
                    if qty <= 0
                    {
                        cell?.valuelbl.text = "NO"
                    }
                    else
                    {
                        cell?.valuelbl.text = "YES"
                    }
                    
                }
                else if item?.type == 1
                {
                    cell?.valuelbl.text = "\(qty) QUANTITY"
                    if qty <= 0
                    {
                        cell?.valuelbl.text = "NO"
                    }
                }
            }
            return cell ?? UITableViewCell()
        }
        
    }
    
    
}
