//
//  ViewMenuVC.swift
//  Eclair
//
//  Created by Indigo on 11/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit


class ViewMenuVC: UIViewController {
    struct fnbData {
        var isOpen : Bool
        var datas : [String:Any]
    }
    
    @IBOutlet weak var tblVw: UITableView!
    var previusData = [[String:Any]]()
    var previusDataOfJuice = [[String:Any]]()
    var updatedData = [fnbData]()
    var updatedDataForJuice = [fnbData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("prevoius data \(previusData)")
        getfnbMenuITems()
        self.tblVw.estimatedRowHeight = 690
        self.tblVw.rowHeight = UITableView.automaticDimension
        
        
        // Do any additional setup after loading the view.
        ActivityIndicatorSingleton
            .StartActivity(myView: self.view)
        self.updatedData.removeAll()
        for i in 0..<self.previusData.count
        {
            self.updatedData.append(fnbData.init(isOpen: false, datas: previusData[i]))
        }
        for j in 0..<self.previusDataOfJuice.count
        {
            self.updatedDataForJuice.append(fnbData.init(isOpen: false, datas: previusDataOfJuice[j]))
        }
        self.tblVw.reloadData()
        ActivityIndicatorSingleton.StopActivity(myView: self.view)
    }
    @IBAction func okbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func getfnbMenuITems()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kGetFnBMenuItems)
        { (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            let model = try? JSONDecoder().decode(FnBMenuDepartments.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            
            if (model != nil) {
                departmentsModel = model!
                self.tblVw.reloadData()
            }
        }
    }
}
extension ViewMenuVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return updatedDataForJuice.count
        }
        else
        {
            return updatedData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellforJuice") as? ViewMenuCellForJuice
            if updatedDataForJuice[indexPath.row].isOpen == false
            {
                cell?.tableStack.arrangedSubviews[1].isHidden = true
            }
            else
            {
                cell?.tableStack.arrangedSubviews[1].isHidden = false
            }
            
            cell?.titlelbl.text = "Mix Juice"
            cell?.viewIngredientsButton.cornerRadius = 8
            cell?.viewIngredientsButton.borderColor = .black
            cell?.viewIngredientsButton.borderWidth = 1
            cell?.previusDataOfJuice = updatedDataForJuice[indexPath.row].datas
            cell?.viewIngredientsButton.tag = indexPath.row
            cell?.viewIngredientsButton.addTarget(self, action: #selector(click_On_ButtonJuice(sender:)), for: .touchUpInside)
            cell?.tblVw.reloadData()
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ViewMenuCell
            if updatedData[indexPath.row].isOpen == false
            {
                cell?.tableStack.arrangedSubviews[1].isHidden = true
            }
            else
            {
                cell?.tableStack.arrangedSubviews[1].isHidden = false
            }
            let id = updatedData[indexPath.row].datas["id"] as? Int
            let menuitem = departmentsModel.menuItems.first { $0.id == id }
            if menuitem != nil
            {
                cell?.titlelbl.text = menuitem?.title
                cell?.ingredients.removeAll()
                cell?.ingredients = updatedData[indexPath.row].datas["menu_ingredients_list"] as? [[String:Any]] ?? []
                cell?.titleback = menuitem?.title ?? ""
                cell?.img.circleCorner = true
                cell?.viewIngredientsButton.cornerRadius = 8
                cell?.viewIngredientsButton.borderColor = .black
                cell?.viewIngredientsButton.borderWidth = 1
                let url = menuitem?.image ?? "image_loading"
                cell?.img.image = UIImage.init(named: "image_loading")
                
                cell?.img.downloadImage(with: url)
                { (data:UIImage?, error:NSError?) in
                    cell?.img.image = data
                }
                cell?.menuItems = menuitem?.menu_ingredients ?? []
                print("menuitems selected \(cell?.menuItems)")
                cell?.viewIngredientsButton.tag = indexPath.row
                cell?.viewIngredientsButton.addTarget(self, action: #selector(click_On_Button(sender:)), for: .touchUpInside)
                cell?.tblVw.reloadData()
            }
            return cell!
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    @objc func click_On_Button(sender: UIButton){
    
        if updatedData[sender.tag].isOpen == false
        {
            updatedData[sender.tag].isOpen = true
           // self.tblVw.rowHeight = UITableView.automaticDimension
            self.tblVw.reloadData()
        }
        else
        {
            updatedData[sender.tag].isOpen = false
            //self.tblVw.rowHeight = UITableView.automaticDimension
            self.tblVw.reloadData()
        }
    }
    @objc func click_On_ButtonJuice(sender: UIButton){
    
        if updatedDataForJuice[sender.tag].isOpen == false
        {
            updatedDataForJuice[sender.tag].isOpen = true
            self.tblVw.rowHeight = UITableView.automaticDimension
            self.tblVw.reloadData()
        }
        else
        {
            updatedDataForJuice[sender.tag].isOpen = false
            self.tblVw.rowHeight = UITableView.automaticDimension
            self.tblVw.reloadData()
        }
    }
}
