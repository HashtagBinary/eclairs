//
//  FlavourCellForJuice.swift
//  Eclair
//
//  Created by Indigo on 10/03/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class FlavourCellForJuice: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var valuelbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
