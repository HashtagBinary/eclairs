//
//  FnB_Home.swift
//  Eclair
//
//  Created by iOS Indigo on 24/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

var departmentsModel = FnBMenuDepartments()
var GselectedTableId = -1
var GselectedAddress: String? = nil
var GselectedPaymentType: String? = nil
var GselectedPreOrderTime: String? = nil
var GselectedTip: Float = 0
var GpaymentTag = -1
//var paymentType: String? = nil
var GvoucherAmount: Float = 0
var GisVoucherApplied = false
var GisVoucher = false
var Gvoucher_remaining : Float = 0
var Gvoucher_Code = ""
var Gvoucher_apiAmount:Float = 0




class FnB_Home: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var monthCollection: UICollectionView!
    @IBOutlet weak var dayCollection: UICollectionView!
    @IBOutlet weak var weekCollection: UICollectionView!
    @IBOutlet weak var menuCollection: UICollectionView!
    @IBOutlet weak var trendLbl: UILabel!
    @IBOutlet weak var menuLbl: UILabel!
    @IBOutlet weak var majorStack: UIStackView!
    @IBOutlet weak var summaryBoxView: SummaryBoxView!
    
    var trendsModel = FnBTrendsModel()
    var dbHelper = DBHelperFnB()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupVw()
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSummaryBoxClick))
        summaryBoxView.addGestureRecognizer(tap)
        summaryBoxView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateCartSummaryBox()
//        getfnbTrendITems()
//        getfnbMenuITems()
        trendBtn(nil)
        self.apiCallsWithQueue()
    }
    
    
    func apiCallsWithQueue(){
       let globalQueue = DispatchQueue.global()
       globalQueue.sync {
           self.getfnbTrendITems()
       }
       globalQueue.sync {
          self.getfnbMenuITems()
       }
    }
    
    
    
    func setupVw()
    {
        self.dayCollection.register(UINib(nibName: "FnBTrendingItemViewCell", bundle: nil), forCellWithReuseIdentifier: "FnBTrendingItemViewCell")
        
        self.dayCollection.delegate = self
        self.dayCollection.dataSource = self
        self.weekCollection.register(UINib(nibName: "FnBTrendingItemViewCell", bundle: nil), forCellWithReuseIdentifier: "FnBTrendingItemViewCell")
        self.weekCollection.delegate = self
        self.weekCollection.dataSource = self
        //custom collectionViewCell
        self.monthCollection.register(UINib(nibName: "FnBTrendingItemViewCell", bundle: nil), forCellWithReuseIdentifier: "FnBTrendingItemViewCell")
        self.monthCollection.delegate = self
        self.monthCollection.dataSource = self
        self.menuCollection.register(UINib(nibName: "FnBTrendingItemViewCell", bundle: nil), forCellWithReuseIdentifier: "FnBTrendingItemViewCell")
        self.menuCollection.delegate = self
        self.menuCollection.dataSource = self
    }
    
    @objc func onSummaryBoxClick(sender: SummaryBoxView) {
        let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBCartViewController") as? FnBCartViewController

        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    @IBAction func trendBtn(_ sender: Any?) {
        self.majorStack.arrangedSubviews[1].isHidden = true
        self.majorStack.arrangedSubviews[0].isHidden = false
        
        self.trendLbl.alpha = 1
        self.menuLbl.alpha = 0.5
    }
    
    @IBAction func menuBtn(_ sender: Any?) {
        self.majorStack.arrangedSubviews[0].isHidden = true
        self.majorStack.arrangedSubviews[1].isHidden = false
        
        self.trendLbl.alpha = 0.5
        self.menuLbl.alpha = 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count: Int = 0;
        
        if(collectionView == self.dayCollection)
        {
            count = self.trendsModel.dayTrend.count
        }
        else if(collectionView == self.weekCollection)
        {
            count = self.trendsModel.weekTrend.count
        }
        else if(collectionView == self.monthCollection)
        {
            count = self.trendsModel.monthTrend.count
        }
        else if(collectionView == self.menuCollection)
        {
            count = departmentsModel.depts.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = self.dayCollection.dequeueReusableCell(withReuseIdentifier: "FnBTrendingItemViewCell", for: indexPath) as! FnBTrendingItemViewCell
        
        if(collectionView == self.dayCollection)
        {
            cell = self.dayCollection.dequeueReusableCell(withReuseIdentifier: "FnBTrendingItemViewCell", for: indexPath) as! FnBTrendingItemViewCell
            
            let url = self.trendsModel.dayTrend[indexPath.item].image ?? "image_loading"
            cell.image_imageView.image = UIImage.init(named: "image_loading")
            
            cell.image_imageView.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.image_imageView.image = data
            }
            
            cell.title_label.text = self.trendsModel.dayTrend[indexPath.item].title ?? ""
        }
        else if(collectionView == self.weekCollection)
        {
            cell = self.weekCollection.dequeueReusableCell(withReuseIdentifier: "FnBTrendingItemViewCell", for: indexPath) as! FnBTrendingItemViewCell
            
            let url = self.trendsModel.weekTrend[indexPath.item].image ?? "image_loading"
            cell.image_imageView.image = UIImage.init(named: "image_loading")
            cell.image_imageView.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.image_imageView.image = data
            }
            cell.title_label.text = self.trendsModel.weekTrend[indexPath.item].title ?? ""
        }
        else if(collectionView == self.monthCollection)
        {
            cell = self.monthCollection.dequeueReusableCell(withReuseIdentifier: "FnBTrendingItemViewCell", for: indexPath) as! FnBTrendingItemViewCell
            let url = self.trendsModel.monthTrend[indexPath.item].image ?? "image_loading"
            cell.image_imageView.image = UIImage.init(named: "image_loading")
            cell.image_imageView.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.image_imageView.image = data
            }
            cell.title_label.text = self.trendsModel.monthTrend[indexPath.item].title ?? ""
        }
        else if(collectionView == self.menuCollection)
        {
            cell = self.menuCollection.dequeueReusableCell(withReuseIdentifier: "FnBTrendingItemViewCell", for: indexPath) as! FnBTrendingItemViewCell
            let img = departmentsModel.depts[indexPath.item].image ?? "image_loading"
            cell.title_label.text = departmentsModel.depts[indexPath.item].title ?? ""
            cell.image_imageView.downloadImage(with: img)
            { (data:UIImage?, error:NSError?) in
                cell.image_imageView.image = data
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let depid = (dbHelper.getDepartment()?.value(forKey: "id") as? Int) ?? -1
        print("department id \(depid)")
        if collectionView != self.menuCollection {
            var trendItem = FnBMenuItem()
            
            if collectionView == self.dayCollection {
                trendItem = self.trendsModel.dayTrend[indexPath.item]
            }
            else if collectionView == self.weekCollection {
                trendItem = self.trendsModel.weekTrend[indexPath.item]
            }
            else if collectionView == self.monthCollection {
                trendItem = self.trendsModel.monthTrend[indexPath.item]
            }
            
            let department = FnBMenuDepartment(id: trendItem.departmentId, title: String(), image: String(), isSheeshaAvailable: trendItem.isSheeshaAvailable, isJuice: false)
            
            if depid != -1 && depid == department.id {
                let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuIngredientVC") as! FnBMenuIngredientVC
                
                vc.itemModel = trendItem
                self.navigationController!.pushViewController(vc, animated: true)
            }
            else {
                let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
                
                vc.isTrend = true
                vc.menuitem = trendItem
                vc.departmentModel = department
                self.navigationController!.pushViewController(vc, animated: true)
            }
        }
        else {
            let department = departmentsModel.depts[indexPath.row]
            if depid != -1 && depid == department.id {
                let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuCategoriesVC") as! FnBMenuCategoriesVC
                
                vc.departmentModel = department
                self.navigationController!.pushViewController(vc, animated: true)
            }
            else {
                let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
                
                vc.departmentModel = department
                self.navigationController!.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getfnbMenuITems()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kGetFnBMenuItems)
        { (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            let json = try? JSON(data:data!)
            print("json \(json)")
            
            let model = try? JSONDecoder().decode(FnBMenuDepartments.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            
            if (model != nil) {
                departmentsModel = model!
                self.menuCollection.reloadData()
                self.updateCartSummaryBox()
            }
        }
    }
    
    func getfnbTrendITems()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kGetFnBItems)
        { (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            let model = try? JSONDecoder().decode(FnBTrendsModel.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            
            if (model != nil) {
                self.trendsModel = model!
                self.dayCollection.reloadData()
                self.weekCollection.reloadData()
                self.monthCollection.reloadData()
            }
            //self.getfnbMenuITems()
        }
    }
    
    func updateCartSummaryBox() {
        let fnbTotal = FnB_Home.calculateFnbPrice()
        summaryBoxView.updateValue(count: fnbTotal.count, total: fnbTotal.total)
        summaryBoxView.isHidden = (fnbTotal.count <= 0) ? true : false
    }
}

extension FnB_Home: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == menuCollection
        {
            return CGSize(width: UIScreen.main.bounds.width / 2, height: UIScreen.main.bounds.width / 2)
        }
        else
        {
            return CGSize(width: UIScreen.main.bounds.width / 2 - 20, height: UIScreen.main.bounds.width / 2)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) //.zero
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension FnB_Home {
    static func calculateFnbPrice() -> FnBCalculationModel {
        var itemCount = 0
        var total: Float = 0
        
        let dbHelper = DBHelperFnB()
        
        dbHelper.getAllFnbItems()?.forEach({ (item) in
            let id = item.value(forKey: "id") as? Int
            let menuitem = departmentsModel.menuItems.first { $0.id == id }
            if (menuitem != nil) {
                let item_price = menuitem?.price ?? 0
                let item_quantity = (item.value(forKey: "value") as? Int) ?? 0
                var ingredient_total: Float = 0
                menuitem?.menu_ingredients.forEach({ (menuIngredient) in
                    let ingredient_type = menuIngredient.type
                    for qid in 1...item_quantity {
                        let dbIngredient = dbHelper.getFnbIngredientById(id: menuIngredient.id ?? -1, quantityId: qid)
                        if (dbIngredient != nil) {
                            let ingredient_quantity = (dbIngredient?.value(forKey: "value") as? Int) ?? 0
                            
                            if (ingredient_type == Constants.CHECKBOX && ingredient_quantity == 1 && menuIngredient.defaultValue == 0) {
                                ingredient_total += menuIngredient.price ?? 0
                            }
                            else if (ingredient_type == Constants.CHECKBOX && ingredient_quantity == 0 && menuIngredient.defaultValue == 1) {
                                ingredient_total -= menuIngredient.price ?? 0
                            }
                            else if (ingredient_type == Constants.COUNTER && ingredient_quantity > (menuIngredient.defaultValue ?? 0)) {
                                let ee = ingredient_quantity - (menuIngredient.defaultValue ?? 0)
                                ingredient_total += (menuIngredient.price ?? 0) * Float(ee)
                            }
                            else if (ingredient_type == Constants.CHOICE && menuIngredient.defaultValue != ingredient_quantity) {
                                let index = menuIngredient.subIngredients.firstIndex{$0.subIngId == ingredient_quantity}
                                if index != nil {
                                    ingredient_total += (menuIngredient.subIngredients[index!].price ?? 0)
                                }
                            }
                        }
                    }
                })
                
                total += item_price * Float(item_quantity) + ingredient_total
                itemCount += item_quantity
            }
        })
        
        dbHelper.getAllJuiceItems()?.forEach({ (obj) in
            let json = obj.value(forKey: "json") as? String
            
            if (json != nil) {
                let data = json!.data(using: .utf8)!
                
                let model = try? JSONDecoder().decode(FnBJuiceCartItem.self, from: data)
                if (model != nil) {
                    let juiceGlass = departmentsModel.juiceGlasses.first { $0.id == model?.glass_id }
                    if (juiceGlass != nil) {
                        total += juiceGlass?.price ?? 0
                        itemCount += 1
                    }
                }
            }
        })
        
        return FnBCalculationModel(total: total, count: itemCount)
    }
}

struct FnBCalculationModel {
    var total: Float
    var count: Int
}
