//
//  TipViewController.swift
//  Eclair
//
//  Created by Sohail on 17/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TipViewController: UIViewController {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var logoOmg: UIImageView!
    @IBOutlet weak var amountTxt: BorderTextField!
    
    var delegate : TipDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoOmg.circleCorner = true
    }
    
    @IBAction func cancelbtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButton(_ sender: Any) {
        if (amountTxt.text != nil && !amountTxt.text!.trimString().isEmpty) {
            delegate!.addTip(amount: Float(amountTxt.text!)!)
            dismiss(animated: true, completion: nil)
        }
    }
}
