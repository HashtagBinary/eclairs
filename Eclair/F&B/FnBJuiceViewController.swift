//
//  FnBJuiceViewController.swift
//  Eclair
//
//  Created by Sohail on 12/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

var juiceIngredients: JuiceIngredients?
var juiceSelectedFlavours = [FnBJuiceFlavor]()
var juiceSelectedGlassId = -1
var juiceSelectedBlendId = -1
var juiceSelectedSweetenerId = -1
var juiceSelectedIceId = -1
var juiceSelectedSyrupID = -1


class FnBJuiceViewController: UIViewController, JuiceGlassUpdateDelegate {
    
    
       
    @IBOutlet weak var glassSizeImg: UIImageView!
    @IBOutlet weak var flavorImg: UIImageView!
    @IBOutlet weak var sweetnerImg: UIImageView!
    @IBOutlet weak var blendTime: UIImageView!
    @IBOutlet weak var iceImg: UIImageView!
    @IBOutlet weak var syrupimg: UIImageView!
    @IBOutlet var choiceButtons: Array<UIView>!
    @IBOutlet weak var juiceGlass: JuiceGlass!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var numberButton: UIView!
    @IBOutlet weak var summaryBoxView: SummaryBoxView!
    @IBOutlet weak var glassSizelbl: UILabel!
    @IBOutlet weak var blendTimelbl: UILabel!
    @IBOutlet weak var sweetnerlbl: UILabel!
    @IBOutlet weak var iceCube: UILabel!
    @IBOutlet weak var syrup: UILabel!
    @IBOutlet weak var updateVw: UIView!
    @IBOutlet weak var addVw: UIView!
    @IBOutlet weak var saveVw: UIView!
    @IBOutlet weak var updateInnerVw: UIView!
    @IBOutlet weak var minusButton: UIButton!
    
    var department_id: Int = -1
    var dbHelper = DBHelperFnB()
    var isSave = false
    var juiceID = -1
    var currentQuantity = 0
    var isFromDetail = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       if isFromDetail == false
       {
            self.reset_values()
            juiceIngredients = JuiceIngredients()
            juiceSelectedFlavours = []
            addVw.isHidden = false
            updateVw.isHidden = true
        }
        else
       {
            addVw.isHidden = true
            updateVw.isHidden = false
       }
        choiceButtons.forEach { (button) in
            let tap = UITapGestureRecognizer(target: self, action: #selector(onChoiceClick))
            button.addGestureRecognizer(tap)
        }
        //numberButton.addTarget(self, action: #selector(onAddToCart(sender:)), for: .valueChanged)
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSummaryBoxClick))
        summaryBoxView.addGestureRecognizer(tap)
        summaryBoxView.isHidden = true
        saveVw.borderWidth = 1
        saveVw.borderColor = UIColor.init(named: "ColorPrimary")
        updateInnerVw.cornerRadius = 8.0
        updateInnerVw.borderWidth = 1
        updateInnerVw.borderColor = UIColor.init(named: "ColorPrimary")
        saveVw.cornerRadius = 8.0
        numberButton.cornerRadius = 8.0
    }
    func reset_values()
    {
        juiceSelectedFlavours.removeAll()
        juiceSelectedGlassId = -1
        juiceSelectedBlendId = -1
        juiceSelectedSweetenerId = -1
        juiceSelectedIceId = -1
        juiceSelectedSyrupID = -1
        juiceGlass.iceImg.isHidden = true
        juiceGlass.sugarImg.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        let juiceCartList = dbHelper.getAllJuiceItems()
        if (juiceCartList != nil) {
            self.currentQuantity = juiceCartList!.count
            if self.currentQuantity <= 0
            {
                self.minusButton.isEnabled = false
            }
        }
        
        updateCartSummaryBox()
    }
    override func viewDidAppear(_ animated: Bool) {
        fetchData()
    }
    
    @IBAction func addToCartAction(_ sender: Any) {
        self.onAddToCart()
    }
    @IBAction func removeFromButton(_ sender: Any) {
        self.onRemoveFromCart()
    }
    @IBAction func updatebtn(_ sender: Any) {
       
            if (isValidate()) {
                if juiceSelectedFlavours.count == 1 {
                    juiceSelectedFlavours[0].percentage = 100.0
                }
                
                let item = juiceIngredients?.juiceGlass.first { $0.id == juiceSelectedGlassId }
                var total: Float = 0
                if (item != nil) {
                    total = item?.price ?? 0
                }
                
                let cartItem = FnBJuiceCartItem(glass_id: juiceSelectedGlassId, ice_id: juiceSelectedIceId,
                                                sweetner_id: juiceSelectedSweetenerId, blend_id: juiceSelectedBlendId,syrup_id: juiceSelectedSyrupID,
                                                flavours: juiceSelectedFlavours, totalprice: Double(total))
                
                let jsonData = try? JSONEncoder().encode(cartItem)
                let json = String(data: jsonData!, encoding: String.Encoding.utf8)
               // dbHelper.setJuice(id: numberButton.currentValue, json: json!)
                dbHelper.updateJuice(id: juiceID, json: json!)
                showToast(message: "Item updated successfully")
                self.navigationController?.popViewController(animated: true)
//            }
//            else {
//                numberButton.setPreValue()
//            }
        }
        updateCartSummaryBox()
    }
    @objc func onSummaryBoxClick(sender: SummaryBoxView) {
        let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBCartViewController") as? FnBCartViewController
        if isFromDetail == true
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.navigationController!.pushViewController(vc!, animated: true)
        }
    }
    
    func updateCheckedImages() {
        isSave = false
        print("juiceSelectedSweetenerId \(juiceSelectedSweetenerId) and juiceSelectedIceId \(juiceSelectedIceId)")
        if juiceSelectedGlassId != -1
        {
            glassSizeImg.image = UIImage(named: "icon_success")
            let item = juiceIngredients?.juiceGlass.first { $0.id == juiceSelectedGlassId
            }
            if item != nil
            {
                self.glassSizelbl.text = item?.title
            }
        }
        if juiceSelectedBlendId != -1
        {
            blendTime.image = UIImage(named: "icon_success")
            let item = juiceIngredients?.juiceBlendTimes.first { $0.id == juiceSelectedBlendId
            }
            if item != nil
            {
                self.blendTimelbl.text = item?.title
            }
        }
        if juiceSelectedSweetenerId != -1
        {
            sweetnerImg.image = UIImage(named: "icon_success")
            
            let item = juiceIngredients?.juiceSweetner.first { $0.id == juiceSelectedSweetenerId
            }
            if item != nil
            {
                self.sweetnerlbl.text = item?.title
            }
            
            if juiceSelectedSweetenerId == 15
            {
                juiceGlass.sugarImg.isHidden = true
            }
            else
            {
                juiceGlass.sugarImg.isHidden = false
            }
        }
        if juiceSelectedIceId != -1
        {
            iceImg.image = UIImage(named: "icon_success")
            
            let item = juiceIngredients?.juiceIce.first { $0.id == juiceSelectedIceId
            }
            if item != nil
            {
                self.iceCube.text = item?.title
            }
            
            if juiceSelectedIceId == 15
            {
                juiceGlass.iceImg.isHidden = true
            }
            else
            {
                juiceGlass.iceImg.isHidden = false
            }
        }
        if juiceSelectedSyrupID != -1
        {
            syrupimg.image = UIImage(named: "icon_success")
            let item = juiceIngredients?.juiceSyrups.first { $0.id == juiceSelectedSyrupID
            }
            if item != nil
            {
                self.syrup.text = item?.title
            }
        }
        if juiceSelectedFlavours.count > 0
        {
            flavorImg.image = UIImage(named: "icon_success")
        }
        if juiceSelectedFlavours.count == 0
        {
            flavorImg.image = UIImage(named: "")
        }
        
        print("selected arrays \(juiceSelectedFlavours.count)")
    }
    func updateGlass() {
        juiceGlass.UpdateFlavors()
        
        if (juiceSelectedGlassId != -1) {
            let item = juiceIngredients?.juiceGlass.first { $0.id == juiceSelectedGlassId
            }
            if (item != nil) {
                totalPriceLabel.text = "\(item?.price ?? 0) \(Constants.CURRENCY_SYMBOL)"
            }
        }
    }
    
    func onAddToCart() {
        
            if (isValidate()) {
                if juiceSelectedFlavours.count == 1 {
                    juiceSelectedFlavours[0].percentage = 100.0
                }
                
                let item = juiceIngredients?.juiceGlass.first { $0.id == juiceSelectedGlassId }
                var total: Float = 0
                if (item != nil) {
                    total = item?.price ?? 0
                }
                self.currentQuantity += 1
                self.minusButton.isEnabled = true
                let cartItem = FnBJuiceCartItem(glass_id: juiceSelectedGlassId, ice_id: juiceSelectedIceId,
                                                sweetner_id: juiceSelectedSweetenerId, blend_id: juiceSelectedBlendId,syrup_id: juiceSelectedSyrupID,
                                                flavours: juiceSelectedFlavours, totalprice: Double(total))
                
                let jsonData = try? JSONEncoder().encode(cartItem)
                let json = String(data: jsonData!, encoding: String.Encoding.utf8)
                dbHelper.setJuice(id: self.currentQuantity, json: json!)
                
                showToast(message: "Item added successfully")
            }
          updateCartSummaryBox()
    }
    
    func onRemoveFromCart() {

        dbHelper.removeJuice(id: self.currentQuantity)
        self.currentQuantity -= 1
        if self.currentQuantity <= 0
        {
            self.minusButton.isEnabled = false
        }
        updateCartSummaryBox()
        showToast(message: "Item Removed successfully")
    }
    
    
//    @objc func onAddToCart(sender: NumberButton) {
//        print("number button type on add to cart \(numberButton.type) ")
//        if (numberButton.type == 1) {
//            if (isValidate()) {
//                if juiceSelectedFlavours.count == 1 {
//                    juiceSelectedFlavours[0].percentage = 100.0
//                }
//
//                let item = juiceIngredients?.juiceGlass.first { $0.id == juiceSelectedGlassId }
//                var total: Float = 0
//                if (item != nil) {
//                    total = item?.price ?? 0
//                }
//
//                let cartItem = FnBJuiceCartItem(glass_id: juiceSelectedGlassId, ice_id: juiceSelectedIceId,
//                                                sweetner_id: juiceSelectedSweetenerId, blend_id: juiceSelectedBlendId,syrup_id: juiceSelectedSyrupID,
//                                                flavours: juiceSelectedFlavours, totalprice: Double(total))
//
//                let jsonData = try? JSONEncoder().encode(cartItem)
//                let json = String(data: jsonData!, encoding: String.Encoding.utf8)
//                dbHelper.setJuice(id: numberButton.currentValue, json: json!)
//
//                showToast(message: "Item added successfully")
//            }
//            else {
//                sender.setPreValue()
//            }
//        }
//        else if (numberButton.type == -1) {
//            dbHelper.removeJuice(id: numberButton.preValue)
//            showToast(message: "Item removed successfully")
//        }
//        updateCartSummaryBox()
//    }
    
    @objc func onChoiceClick(sender: AnyObject) {
//        performSegue(withIdentifier: "choiceSegue", sender: sender)
        print("sender tag = \(sender.tag)")
        let BtnTag = (sender as AnyObject).view.tag
        if BtnTag == 4
        {
            performSegue(withIdentifier: "myGlass", sender: sender)
        }
        else
        {
            performSegue(withIdentifier: "choiceSegue", sender: sender)
        }
    }
    
    @IBAction func saveglassbtn(_ sender: Any) {
        
       if self.isValidate() == true
       {
        self.isSave = true
        self.performSegue(withIdentifier: "myGlass", sender: self)
       }
       else
       {
        self.isSave = false
       }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "choiceSegue") {
            let vc = segue.destination as! FnBJuiceIngredientViewController
            vc.type = (sender as AnyObject).view.tag
            vc.delegate = self
        }
       else if (segue.identifier == "myGlass") {
            let vc = segue.destination as! MyGlassVC
            //vc.type = (sender as AnyObject).view.tag
            vc.delegate = self
            vc.isSave = self.isSave
        }
    }
    
    func fetchData() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let parameters = [
            "id" : department_id,
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kGetFnBJuiceIngredients, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let model = try? JSONDecoder().decode(JuiceIngredients.self, from: data!)
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            juiceIngredients = model
            self.juiceGlass.UpdateFlavors()
            if self.isFromDetail == true
            {
                self.updateCheckedImages()
                self.updateGlass()
            }
        }
        self.getGlass()
        
    }
    func getGlass() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBGetGlasses, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            let response = json?["response"].bool ?? false
            if response == false
            {
                Utilities.showAlert("", message: json?["error"].string ?? "Data Not Found")
                return
            }
            else
            {
                let GlassMain = json?["glass"] ?? []
                let datas = GlassMain[0]["glass"].string ?? ""
                let f = try? JSONDecoder().decode(FnBJuiceCartItem.self, from: datas.data(using: .utf8)!)
                self.updateGlass()
            }
        }
    }
    
    func saveGlass(glassData : String) {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
            "glass" : glassData
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBSaveGlass, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json response of save glass \(json)")
        }
    }
    
    func isValidate() -> Bool {
        if (juiceSelectedGlassId == -1) {
            Utilities.showAlert("Error", message: "Please select glass size!")
            
            return false
        }
        else if (juiceSelectedFlavours.count <= 0) {
            Utilities.showAlert("Error", message: "Please select fruit!")
            return false
        }
        else if (juiceSelectedSweetenerId == -1) {
            Utilities.showAlert("Error", message: "Please select sweetener!")
            return false
        }
        else if (juiceSelectedIceId == -1) {
            Utilities.showAlert("Error", message: "PPlease select ice!")
            return false
        }
        else if (juiceSelectedBlendId == -1) {
            Utilities.showAlert("Error", message: "Please select blend time!")
            return false
        }
        else if (juiceSelectedSyrupID == -1) {
            Utilities.showAlert("Error", message: "Please select syrup!")
            return false
        }
        return true
    }
    
    func updateCartSummaryBox() {
        let fnbTotal = FnB_Home.calculateFnbPrice()
        summaryBoxView.updateValue(count: fnbTotal.count, total: fnbTotal.total)
        summaryBoxView.isHidden = (fnbTotal.count <= 0) ? true : false
    }
}

protocol JuiceGlassUpdateDelegate {
    func updateGlass()
    func updateCheckedImages()
}
