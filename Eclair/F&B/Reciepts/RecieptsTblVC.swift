//
//  RecieptsTblVC.swift
//  Eclair
//
//  Created by Zohaib Naseer on 6/17/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import PDFKit
import FirebaseFirestore
import PDFGenerator

class RecieptsTblVC: UITableViewController {
    @IBOutlet var headerVw: UIView!
    @IBOutlet var footerVw: UIView!
    @IBOutlet weak var invoiceNoLbl: UILabel!
    @IBOutlet weak var ecodeLbl: UILabel!
    @IBOutlet weak var addresslbl: UILabel!
    @IBOutlet weak var orderDatelbl: UILabel!
    @IBOutlet weak var subtotLbl: UILabel!
    @IBOutlet weak var vatlbl: UILabel!
    @IBOutlet weak var tiplbl: UILabel!
    @IBOutlet weak var netTotlbl: UILabel!
    @IBOutlet weak var vatTitlelbl: UILabel!
    
    
    var jsonData = Dictionary<String, AnyObject>()
    var menuItems = [Dictionary<String, AnyObject>]()
    var payment_detail = [Dictionary<String, AnyObject>]()
    var pdfURL: URL!
    var pdfVw = PDFView()
    var BarButtonItem = UIBarButtonItem()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        BarButtonItem = UIBarButtonItem(image: UIImage(named: "option"), style: .done, target: self, action: #selector(clickButton))
        self.navigationItem.rightBarButtonItem  = BarButtonItem
        
        
        tableView.tableHeaderView = self.headerVw
        tableView.tableFooterView = footerVw
        self.headerVw.width = self.view.width
        self.footerVw.width = self.view.width
        print("json data \(jsonData)")
        
        let invoiceID = jsonData["order_id"] as? String ?? ""
        let ecode = jsonData["ecode"] as? String ?? ""
        let address = jsonData["address"] as? String ?? ""
        let orderDate = jsonData["datetime_new"] as? String
        let vatPercentag = jsonData["vatpercentage"] as? Int ?? 0
        let tip = jsonData["tip"] as? Double ?? 0.0
        
        var subtot = 0.0
        var total = 0.0
        var vatAmount = 0.0
        if let sub = jsonData["subtotal"]
        {
            let inDecimal = String(format: "%.2f", "\(sub)".toDouble() ?? 0.0)
            subtot = inDecimal.toDouble() ?? 0.0
        }
        if let tot = jsonData["total"]
        {
            let inDecimal = String(format: "%.2f", "\(tot)".toDouble() ?? 0.0)
            total = inDecimal.toDouble() ?? 0.0
        }
        if let vat = jsonData["vat"]
        {
            let inDecimal = String(format: "%.2f", "\(vat)".toDouble() ?? 0.0)
            vatAmount = inDecimal.toDouble() ?? 0.0
        }
        if orderDate?.isEmpty == false
        {
            let converted = Utilities.convertDateFormate(forStringDate: "\(orderDate!)", currentFormate: "yyyy-MM-dd' 'HH:mm:ss.SSS", newFormate: Constants.DateFormate)
            self.orderDatelbl.text = converted
        }
        else
        {
            self.orderDatelbl.text = ""
        }
        invoiceNoLbl.text = "#\(invoiceID)"
        ecodeLbl.text = ecode
        addresslbl.text = address
        subtotLbl.text = "\(subtot) \(Constants.CURRENCY_SYMBOL)"
        vatTitlelbl.text = "VAT \(vatPercentag)%"
        vatlbl.text = "\(vatAmount) \(Constants.CURRENCY_SYMBOL)"
        tiplbl.text = "\(tip) \(Constants.CURRENCY_SYMBOL)"
        netTotlbl.text = "\(total) \(Constants.CURRENCY_SYMBOL)"
        menuItems = jsonData["menuitems"] as? [Dictionary<String, AnyObject>] ?? []
        payment_detail = jsonData["payment_detail"] as? [Dictionary<String, AnyObject>] ?? []
        tableView.reloadData()
        self.pdfDataWithTableView(tableView: tableView)
        
    }

    @objc func clickButton(){
        if let documet = PDFDocument(url: pdfURL)
        {
            sharePDF(documet)
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return menuItems.count
        }
        else if section == 1
        {
            return 2
        }
        else
        {
            return payment_detail.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! ReceiptItemCell
            if indexPath.row != 0
            {
                cell.ItemNameTitlelbl.isHidden = true
                cell.rateTitlelbl.isHidden = true
                cell.qtyTitlelbl.isHidden = true
                cell.amountTitlelbl.isHidden = true
                cell.itemTitleHeight.constant = 0.0
                cell.rateHeight.constant = 0.0
                cell.qtyHeight.constant = 0.0
                cell.amountHeight.constant = 0.0
            }
            else
            {
                cell.ItemNameTitlelbl.isHidden = false
                cell.rateTitlelbl.isHidden = false
                cell.qtyTitlelbl.isHidden = false
                cell.amountTitlelbl.isHidden = false
                cell.itemTitleHeight.constant = 17.0
                cell.rateHeight.constant = 17.0
                cell.qtyHeight.constant = 17.0
                cell.amountHeight.constant = 17.0
            }
            
            cell.qtylbl.text = "\(menuItems[indexPath.row]["quantity"] as? Int ?? 0)"
//            cell.amountlbl.text = "\(menuItems[indexPath.row]["totalprice"] as? Double ?? 0.0) \(Constants.CURRENCY_SYMBOL)"
            
            if let price = menuItems[indexPath.row]["totalprice"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.amountlbl.text = "\(inDecimal) \(Constants.CURRENCY_SYMBOL)"
            }
            else
            {
                cell.amountlbl.text = "-"
            }
            
            
            
            let id = menuItems[indexPath.row]["id"] as? Int
            let menuitem = departmentsModel.menuItems.first { $0.id == id }
            if menuitem != nil
            {
                cell.menuItemlbl.text = menuitem?.title ?? ""
                cell.ratelbl.text = "\(menuitem?.price ?? 0.0) \(Constants.CURRENCY_SYMBOL)"
            }
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as! ReceiptPaymentCell
            if indexPath.row == 0
            {
                cell.titlelbl.text = "Total Items"
                cell.valuelbl.text = "\(jsonData["item_count"] as? Int ?? 0)"
            }
            else
            {
                cell.titlelbl.text = "Payment By"
                cell.valuelbl.text = jsonData["paymenttype"] as? String
            }
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath) as! ReceiptTransactionCell
            if indexPath.row != 0
            {
                cell.tranIDTitlelbl.isHidden = true
                cell.cardTypeTitlelbl.isHidden = true
                cell.amountTitlelbl.isHidden = true
            }
            else
            {
                cell.tranIDTitlelbl.isHidden = false
                cell.cardTypeTitlelbl.isHidden = false
                cell.amountTitlelbl.isHidden = false
            }
            
            
            cell.transactionlbl.text = "\(payment_detail[indexPath.row]["id"] as? String ?? "")"
            cell.cardTypelbl.text = payment_detail[indexPath.row]["paymentmethod"] as? String ?? ""
//            cell.amountlbl.text = "\(payment_detail[indexPath.row]["amount"] as? Double ?? 0.0) \(Constants.CURRENCY_SYMBOL)"
            
            if let price = payment_detail[indexPath.row]["amount"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.amountlbl.text = "\(inDecimal) \(Constants.CURRENCY_SYMBOL)"
            }
            else
            {
                cell.amountlbl.text = "-"
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pdfDataWithTableView(tableView: tableView)
    }
    func sharePDF(_ filePDF: PDFDocument) {
        if let pdfData = filePDF.dataRepresentation() {
            let objectsToShare = [pdfData]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.barButtonItem = BarButtonItem
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func pdfDataWithTableView(tableView: UITableView) {
      let priorBounds = tableView.bounds
        
        print("priorBounds \(priorBounds)")
        
      let fittedSize = tableView.sizeThatFits(CGSize(
        width: priorBounds.size.width,
        height: tableView.contentSize.height
      ))
        print("fitter size \(fittedSize)")
      tableView.bounds = CGRect(
        x: 0, y: 0,
        width: fittedSize.width,
        height: fittedSize.height
      )
        print("tableview.bouds \(tableView.bounds)")
      let pdfPageBounds = CGRect(
        x :0, y: 0,
        width: tableView.frame.width,
        height: self.view.frame.height
      )
        print("pdfPageBounds \(pdfPageBounds)")
        
      let pdfData = NSMutableData()
      UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)

      var pageOriginY: CGFloat = 0
      while pageOriginY < fittedSize.height {
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        pageOriginY += pdfPageBounds.size.height
        tableView.contentOffset = CGPoint(x: 0, y: 0)   // move "renderer"
      }
      UIGraphicsEndPDFContext()

      tableView.bounds = priorBounds //priorBounds
      var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
      docURL = docURL.appendingPathComponent("myDocument.pdf")
      pdfData.write(to: docURL as URL, atomically: true)

        self.pdfURL = docURL
        if let document = PDFDocument(url: pdfURL) {
            pdfVw.document = document
            pdfVw.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.pdfVw)
            pdfVw.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            pdfVw.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
            pdfVw.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            pdfVw.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            pdfVw.displayDirection = .vertical
            pdfVw.autoScales = true
            
        }
    }
}
