//
//  FnBJuiceIngredientViewController.swift
//  Eclair
//
//  Created by Sohail on 12/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class FnBJuiceIngredientViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var delegate : JuiceGlassUpdateDelegate?
    
    var juiceRemainingFlavours = [FnBJuiceFlavor]()
    
    var type = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "FnBJuiceAddFlavorTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBJuiceAddFlavorTableViewCell")
        tableView.register(UINib(nibName: "FnBJuiceMixFlavorTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBJuiceMixFlavorTableViewCell")
        tableView.register(UINib(nibName: "FnBJuiceItemTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBJuiceItemTableViewCell")
        tableView.register(UINib(nibName: "syrupFnbItemCells", bundle: nil), forCellReuseIdentifier: "syrupFnbItemCells")
        //syrupFnbItemCell
        switch type {
        case 0:
            titleLabel.text = "Select Juice Glass Size"
        case 1:
            titleLabel.text = "Add Flavors"
            getFlavorList()
        case 2:
            titleLabel.text = "Mix Flavors"
        case 3:
            titleLabel.text = "Select Blend Time"
        case 5:
            titleLabel.text = "Select Sweetener"
        case 6:
            titleLabel.text = "Select Ice Cubes"
        default:
            titleLabel.text = ""
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isBeingDismissed {
            delegate!.updateGlass()
            delegate?.updateCheckedImages()
        }
    }
    
    func getFlavorList() {
        juiceRemainingFlavours = []
        juiceIngredients?.juiceFlavours.forEach({ (flavor) in
            let index = juiceSelectedFlavours.firstIndex { $0.id == flavor.id }
            if (index == nil) {
                juiceRemainingFlavours.append(flavor)
            }
        })
        tableView.reloadData()
    }
    
    @IBAction func onDone(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onAddFlavor(sender: AnyObject) {
        let flavorId = sender.view.tag
        
        let index = juiceRemainingFlavours.firstIndex { $0.id == flavorId }
        var flavor = juiceRemainingFlavours.first { $0.id == flavorId }
        if (flavor != nil && index != nil) {
            flavor?.percentage = 100.0
            juiceSelectedFlavours.append(flavor!)
            self.juiceRemainingFlavours.remove(at: index!)
            tableView.deleteRows(at: [IndexPath(row: index!, section: 0)], with: .automatic)
        }
    }
    
    @objc func onRemoveFlavor(sender: UIButton) {
        let flavorId = sender.tag
        
        let index = juiceSelectedFlavours.firstIndex { $0.id == flavorId }
        if (index != nil) {
            juiceSelectedFlavours.remove(at: index!)
            tableView.deleteRows(at: [IndexPath(row: index!, section: 0)], with: .automatic)
        }
        
        if juiceSelectedFlavours.count == 1 {
            juiceSelectedFlavours[0].percentage = 100.0
            tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
        }
    }
    
    @objc func sliderValueChanged(sender: UISlider) {
        let flavorId = sender.tag
        
        let index = juiceSelectedFlavours.firstIndex { $0.id == flavorId }
        if (index != nil) {
            var flavor = juiceSelectedFlavours[index!]
            flavor.percentage = Double(sender.value)
            juiceSelectedFlavours[index!] = flavor
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch type {
        case 0:
            return juiceIngredients!.juiceGlass.count
        case 1:
            return juiceRemainingFlavours.count
        case 2:
            return juiceSelectedFlavours.count
        case 3:
            return juiceIngredients!.juiceBlendTimes.count
        case 5:
            return juiceIngredients!.juiceSweetner.count
        case 6:
            return juiceIngredients!.juiceIce.count
        case 7:
            return juiceIngredients!.juiceSyrups.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch type {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBJuiceItemTableViewCell", for: indexPath) as! FnBJuiceItemTableViewCell
            
            let model = juiceIngredients!.juiceGlass[indexPath.row]
            
            cell.titleLabel.text = model.title
            cell.descriptionLabel.text = model.description
            cell.priceLabel.text = "\(model.price ?? 0) \(Constants.CURRENCY_SYMBOL)"
            
            cell.imgSuccess.isHidden = (model.id == juiceSelectedGlassId) ? false : true
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBJuiceAddFlavorTableViewCell", for: indexPath) as! FnBJuiceAddFlavorTableViewCell
            
            let model = juiceRemainingFlavours[indexPath.row]
            
            cell.titleLabel.text = model.flavour
            cell.bgView.backgroundColor = Constants.hexStringToUIColor(hex: model.colorCode ?? "FFFFFF")
            
            cell.bgView.tag = model.id ?? -1
            let tap = UITapGestureRecognizer(target: self, action: #selector(onAddFlavor))
            cell.bgView.addGestureRecognizer(tap)
            
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBJuiceMixFlavorTableViewCell", for: indexPath) as! FnBJuiceMixFlavorTableViewCell
            
            let model = juiceSelectedFlavours[indexPath.row]
            
            cell.title.text = "\(model.flavour ?? "") (\(Int(model.percentage ?? 100.0))%)"
            cell.slider.tintColor = Constants.hexStringToUIColor(hex: model.colorCode ?? "FFFFFF")
            cell.slider.value = Float(model.percentage ?? 100.0)
            
            cell.slider.tag = model.id ?? -1
            cell.slider.addTarget(self, action: #selector(sliderValueChanged(sender:)), for: .valueChanged)
            
            cell.removeButton.tag = model.id ?? -1
            cell.removeButton.addTarget(self, action: #selector(onRemoveFlavor(sender:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBJuiceItemTableViewCell", for: indexPath) as! FnBJuiceItemTableViewCell
            
            let model = juiceIngredients!.juiceBlendTimes[indexPath.row]
            
            cell.titleLabel.text = model.title
            cell.descriptionLabel.text = model.description
            cell.priceLabel.isHidden = true
            
            cell.imgSuccess.isHidden = (model.id == juiceSelectedBlendId) ? false : true
            
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBJuiceItemTableViewCell", for: indexPath) as! FnBJuiceItemTableViewCell
            
            let model = juiceIngredients!.juiceSweetner[indexPath.row]
            
            cell.titleLabel.text = model.title
            cell.descriptionLabel.text = model.description
            cell.priceLabel.isHidden = true
            
            cell.imgSuccess.isHidden = (model.id == juiceSelectedSweetenerId) ? false : true
            
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBJuiceItemTableViewCell", for: indexPath) as! FnBJuiceItemTableViewCell
            
            let model = juiceIngredients!.juiceIce[indexPath.row]
            
            cell.titleLabel.text = model.title
            cell.descriptionLabel.text = model.description
            cell.priceLabel.isHidden = true
            
            cell.imgSuccess.isHidden = (model.id == juiceSelectedIceId) ? false : true
            //syrupFnbItemCell
            return cell
            
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "syrupFnbItemCells", for: indexPath) as! syrupFnbItemCells
            
            let model = juiceIngredients!.juiceSyrups[indexPath.row]
            
            cell.titleLbl.text = model.title
            cell.descriptionlbl.text = model.description
            
            
            cell.img.tintColor = Constants.hexStringToUIColor(hex: model.color ?? "FFFFFF")

            return cell
            
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch type {
        case 0:
            juiceSelectedGlassId = juiceIngredients!.juiceGlass[indexPath.row].id ?? -1
            dismiss(animated: true, completion: nil)
        case 3:
            juiceSelectedBlendId = juiceIngredients!.juiceBlendTimes[indexPath.row].id ?? -1
            dismiss(animated: true, completion: nil)
        case 5:
            juiceSelectedSweetenerId = juiceIngredients!.juiceSweetner[indexPath.row].id ?? -1
            dismiss(animated: true, completion: nil)
        case 6:
            juiceSelectedIceId = juiceIngredients!.juiceIce[indexPath.row].id ?? -1
            dismiss(animated: true, completion: nil)
            
        case 7:
            juiceSelectedSyrupID = juiceIngredients!.juiceSyrups[indexPath.row].id ?? -1
            dismiss(animated: true, completion: nil)
        default:
            print("")
        }
    }
}
