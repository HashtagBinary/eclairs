//
//  OrderInstructionViewController.swift
//  Eclair
//
//  Created by Sohail on 22/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class OrderInstructionViewController: UIViewController {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var logoOmg: UIImageView!
    @IBOutlet weak var instructionTxt: UITextView!
    
    var delegate : SubItemDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoOmg.circleCorner = true
        instructionTxt.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0).cgColor
        instructionTxt.layer.borderWidth = 1.0
        instructionTxt.layer.cornerRadius = 8
        
        let instructionsData = DBHelperFnB().getInstructions()?.value(forKey: "data") as? String
        if (instructionsData != nil) {
            instructionTxt.text = instructionsData
        }
    }
    
    @IBAction func cancelbtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButton(_ sender: Any) {
        delegate!.action(type: 3, data: instructionTxt.text ?? "")
        dismiss(animated: true, completion: nil)
    }
}
