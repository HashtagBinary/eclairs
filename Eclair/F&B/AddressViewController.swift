//
//  AddressViewController.swift
//  Eclair
//
//  Created by iOS Indigo on 30/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class AddressViewController: UIViewController {
    
    @IBOutlet var stackButtons: Array<RadioButton>!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var villaView: UIStackView!
    @IBOutlet weak var takeAwayView: UIStackView!
    @IBOutlet weak var villaTextField: UITextField!
    @IBOutlet weak var note: UILabel!
    
    var delegate : UpdationDelegate?
    
    var arrayList = [TableModel]()
    var dbHelper = DBHelperFnB()
    
    var departmentModel: FnBMenuDepartment?
    var menuitem: FnBMenuItem?
    var isTrend: Bool = false
    var isCart: Bool = false
    
    var selectedType = 0
    var selectedTableId = -1
    var selectedTable = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            let size = CGSize(width: collectionView!.bounds.width / 4, height: 100)
            layout.itemSize = size
            layout.invalidateLayout()
        }
        
        unCheckAllRadioButton()
        collectionView.isHidden = false
        stackButtons[0].setValue(isChecked: true)
        
        stackButtons.forEach { (button) in
            button.addTarget(self, action:#selector(radioButtonClicked(sender:)), for: UIControl.Event.touchUpInside)
        }
        
        note.text = Constants.kAddressNote
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let depid = (dbHelper.getDepartment()?.value(forKey: "id") as? Int) ?? -1
        print("department id on address controller \(depid)")
        if (depid != -1 && depid != departmentModel?.id) {
            let alert = UIAlertController(title: "Cart Confirmation", message: "By adding items from different outlets, the items you have added to your cart will be cleared.", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                self.dbHelper.emptyFnbCart()
                self.fetchData()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
        }
        else {
            fetchData()
        }
    }
    
    @objc func radioButtonClicked(sender: RadioButton) {
        unCheckAllRadioButton()
        sender.setValue(isChecked: true)
        
        selectedType = sender.tag
        
        switch sender.tag {
        case 0:
            collectionView.isHidden = false
        case 1:
            villaView.isHidden = false
        case 2:
            takeAwayView.isHidden = false
        default:
            break
        }
    }
    
    func unCheckAllRadioButton() {
        stackButtons.forEach { (button) in
            button.setValue(isChecked: false)
        }
        
        collectionView.isHidden = true
        villaView.isHidden = true
        takeAwayView.isHidden = true
    }
    
    func fetchData() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let parameters = [
            "id" : departmentModel?.id ?? -1,
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kAddressTables, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let model = try? JSONDecoder().decode(TablesResponse.self, from: data!)
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            
            if model!.isTableOnly {
                self.stackButtons[0].isEnabled = true
                self.stackButtons[1].isEnabled = false
                self.stackButtons[2].isEnabled = false
            }
            else {
                self.stackButtons[0].isEnabled = true
                self.stackButtons[1].isEnabled = true
                self.stackButtons[2].isEnabled = true
            }
            
            self.arrayList = model!.tables
            self.collectionView.reloadData()
        }
    }
    
    @IBAction func onConfirmClick(_ sender: UIButton) {
        if selectedType == 0 {
            if (selectedTableId == -1) {
                Utilities.showAlert("", message: "Please select a table to continue.")
                return
            }
            GselectedTableId = selectedTableId
            GselectedAddress = "\(selectedTable)"
        }
        else if selectedType == 1 {
            if (villaTextField.text == nil || villaTextField.text?.trimWhiteSpaces() == "") {
                Utilities.showAlert("", message: "Please enter villa number to continue.")
                return
            }
            GselectedTableId = -1
            GselectedAddress = "Villa \(villaTextField.text!)"
        }
        else if selectedType == 2 {
            GselectedTableId = -1
            GselectedAddress = "Take Away"
        }
        
        dbHelper.setDepartment(id: departmentModel!.id ?? -1, isSheesha: departmentModel!.isSheeshaAvailable ?? false, isJuice: departmentModel!.isJuice ?? false)
        
        if (isTrend) {
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuIngredientVC") as! FnBMenuIngredientVC
            
            vc.itemModel = self.menuitem
            self.navigationController!.pushViewController(vc, animated: true)
        }
        else if (isCart) {
            delegate?.updateSummary()
            self.navigationController!.popViewController(animated: true)
        }
        else {
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuCategoriesVC") as! FnBMenuCategoriesVC
            
            vc.departmentModel = self.departmentModel
            self.navigationController!.pushViewController(vc, animated: true)
        }
    }
}

extension AddressViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrayList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AddressTableCollectionView
        
        cell.bgView.backgroundColor = (self.selectedTableId == arrayList[indexPath.row].id) ? UIColor(named: "ColorTextDark") : UIColor.white
        cell.titleLabel.textColor = (self.selectedTableId == arrayList[indexPath.row].id) ? UIColor.white : UIColor(named: "ColorTextDark")
        cell.titleLabel.text = arrayList[indexPath.row].title
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedTableId = arrayList[indexPath.row].id
        self.selectedTable = arrayList[indexPath.row].title
        self.collectionView.reloadData()
    }
}

struct TablesResponse: Codable {
    var response: Bool
    var error: String
    var isTableOnly: Bool
    var tables = [TableModel]()
}

struct TableModel: Codable {
    var id: Int
    var title: String
}
