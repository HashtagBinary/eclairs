//
//  FnBMenuCategoriesVC.swift
//  Eclair
//
//  Created by Sohail on 08/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class FnBMenuCategoriesVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var summaryBoxView: SummaryBoxView!
    
    var arrayList = [FnBMenuCategory]()
    var departmentModel: FnBMenuDepartment?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = departmentModel?.title
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
                layout.minimumLineSpacing = 0
                layout.minimumInteritemSpacing = 4
                layout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
                let size = CGSize(width:(collectionView!.bounds.width - 20)/2, height: 200)
                layout.itemSize = size
                layout.invalidateLayout()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSummaryBoxClick))
        summaryBoxView.addGestureRecognizer(tap)
        summaryBoxView.isHidden = true
        
        fetchData()
    }
    override func viewWillAppear(_ animated: Bool) {
        updateCartSummaryBox()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @objc func onSummaryBoxClick(sender: SummaryBoxView) {
        let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBCartViewController") as? FnBCartViewController

        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    func fetchData() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let parameters = [
            "id" : departmentModel?.id ?? -1,
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kGetFnBCategoriesById, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let model = try? JSONDecoder().decode(FnBMenuCategories.self, from: data!)
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            
            self.arrayList = model!.categories
            self.collectionView.reloadData()
        }
    }
    
    func updateCartSummaryBox() {
        let fnbTotal = FnB_Home.calculateFnbPrice()
        summaryBoxView.updateValue(count: fnbTotal.count, total: fnbTotal.total)
        summaryBoxView.isHidden = (fnbTotal.count <= 0) ? true : false
    }
}

extension FnBMenuCategoriesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrayList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SpaCategoryCollectionViewCell
        
        cell.titleLabel.text = arrayList[indexPath.row].title
        
        //cell.imageView.sd_setImage(with: URL(string: arrayList[indexPath.row].image ?? ""), placeholderImage: UIImage(named: "image_loading"))
        
        cell.imageView.image = UIImage.init(named: "image_loading")
        
        let url = arrayList[indexPath.item].image ?? "image_loading"
        cell.imageView.image = UIImage.init(named: "image_loading")
        cell.imageView.downloadImage(with: url)
        { (data:UIImage?, error:NSError?) in
            cell.imageView.image = data
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuItemsVC") as? FnBMenuItemsVC

        vc?.departmentModel = self.departmentModel
        vc?.categoryModel = arrayList[indexPath.row]
        self.navigationController!.pushViewController(vc!, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width / 2 - 10, height: self.view.frame.width / 2 - 10)
    }
}
