//
//  FnBCartDetailsViewController.swift
//  Eclair
//
//  Created by Sohail on 11/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

var Gitem: CartFnbItem?

class FnBCartDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var menuItem: FnBMenuItem?
    
    var dbHelper = DBHelperFnB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
//        tableView.rowHeight = UITableView.automaticDimension;
//        tableView.estimatedRowHeight = 300.0;
        
        tableView.register(UINib(nibName: "FnBCartDetailItemTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBCartDetailItemTableViewCell")
        
       
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        let cartObj = dbHelper.getFnbItemById(id: menuItem?.id ?? -1)
        if (cartObj != nil) {
            Gitem = CartFnbItem(menuitemId: cartObj?.value(forKey: "id") as! Int, menuitemQuantity: cartObj?.value(forKey: "value") as! Int)
        }
        else {
            Gitem = CartFnbItem(menuitemId: -1, menuitemQuantity: 0)
        }
        cartToArray()
        self.tableView.reloadData()
    }
    
    @IBAction func addbtn(_ sender: Any) {
        
        //FnBMenuIngredientVC
        
        let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuIngredientVC") as? FnBMenuIngredientVC
        vc?.itemModel = self.menuItem
        vc?.isFromDetail = true
        self.navigationController!.pushViewController(vc!, animated: true)
    }
    func cartToArray() {
        menuItem?.menu_ingredients.forEach({ (menuIngredient) in
            for i in 0...(Gitem!.menuitemQuantity - 1) {
                var cartIngredient = CartIngredient()
                
                let cartObj = dbHelper.getFnbIngredientById(id: menuIngredient.id ?? -1, quantityId: i + 1)
                if (cartObj != nil) {
                    cartIngredient = CartIngredient(
                        quantityId: cartObj!.value(forKey: "quantityId") as? Int,
                        ingredientId: cartObj!.value(forKey: "id") as? Int,
                        ingredientQuantity: cartObj!.value(forKey: "value") as? Int)
                }
                if (Gitem!.ingredientLists.count > i) {
                    var cartIngredientList = Gitem!.ingredientLists[i]
                    cartIngredientList.ingredientList.append(cartIngredient)
                    Gitem!.ingredientLists[i] = cartIngredientList
                }
                else {
                    var cartIngredientList = CartIngredientList()
                    cartIngredientList.ingredientList.append(cartIngredient)
                    Gitem!.ingredientLists.insert(cartIngredientList, at: i)
                }
            }
        })
    }
    
    func updateArrayToDB() {
        deleteIngredients()
        if (Gitem!.menuitemQuantity <= 0) {
            dbHelper.deleteFnBItemById(id: menuItem?.id ?? -1)
            self.navigationController!.popViewController(animated: true)
        }
        else {
            dbHelper.updateFnbItem(id: menuItem?.id ?? -1,
                                   catId: menuItem?.categoryId ?? -1, value: Gitem!.menuitemQuantity)
            
            let cartIngredientLists = Gitem!.ingredientLists
            var i = 1
            cartIngredientLists.forEach { (ingredientList) in
                ingredientList.ingredientList.forEach { (ingredient) in
                    dbHelper.updateFnbIngredient(id: ingredient.ingredientId ?? -1,
                                                 itemId: Gitem!.menuitemId, quantityId: i,
                                                 value: ingredient.ingredientQuantity ?? 0)
                }
                i = i + 1
            }
            cartToArray()
        }
    }
    
    func deleteIngredients() {
        let cartObj = dbHelper.getFnbItemById(id: menuItem?.id ?? -1)
        if (cartObj != nil) {
            let item_quantity = cartObj!.value(forKey: "value") as! Int
            menuItem?.menu_ingredients.forEach({ (menuIngredient) in
                for qid in 1...item_quantity {
                    dbHelper.deleteFnbIngredientById(id: menuIngredient.id ?? -1, quantityId: qid)
                }
            })
        }
    }
    
    @objc func removeSubItem(sender: UIButton) {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to remove this item from the cart.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.showToast(message: "Item removed successfully")
            Gitem!.removeItem(position: sender.tag)
        
            self.updateArrayToDB()
            self.tableView.reloadData()
        }))
        present(alert, animated: true, completion: nil)
    }
}

extension FnBCartDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Gitem!.menuitemQuantity
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FnBCartDetailItemTableViewCell", for: indexPath) as! FnBCartDetailItemTableViewCell
        
        cell.itemModel = menuItem
        cell.qid = indexPath.row
        
        cell.titleLabel.text = "\(menuItem?.title ?? "-") \(indexPath.row + 1)"
        cell.totalItemPrice.text = "\(menuItem?.price ?? 0.0) \(Constants.CURRENCY_SYMBOL)"
        cell.tableView.reloadData()
        
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(removeSubItem(sender:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
