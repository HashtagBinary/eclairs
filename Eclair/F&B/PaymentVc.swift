//
//  PaymentVc.swift
//  Eclair
//
//  Created by iOS Indigo on 29/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class PaymentVc: UIViewController {

    @IBOutlet weak var cardRadio: UIImageView!
    @IBOutlet weak var spanRadio: UIImageView!
    @IBOutlet weak var ainCoinRadio: UIImageView!
    @IBOutlet weak var settleToVillaRadio: UIImageView!
    @IBOutlet weak var cardVw: UIView!
    @IBOutlet weak var spanVw: UIView!
    @IBOutlet weak var coinVw: UIView!
    @IBOutlet weak var villaVw: UIView!
    @IBOutlet weak var stack: UIStackView!
    var delegate : PaymentDelegate?
    var tag : Int?
    var type = ""
    var id : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapCard = UITapGestureRecognizer(target: self, action: #selector(self.handleTapCard(_:)))
        cardVw.addGestureRecognizer(tapCard)
        
        let tapSpan = UITapGestureRecognizer(target: self, action: #selector(self.handleTapSpan(_:)))
        spanVw.addGestureRecognizer(tapSpan)
        
        let tapCoin = UITapGestureRecognizer(target: self, action: #selector(self.handleTapCoin(_:)))
        coinVw.addGestureRecognizer(tapCoin)
        
        let tapVilla = UITapGestureRecognizer(target: self, action: #selector(self.handleTapVilla(_:)))
        villaVw.addGestureRecognizer(tapVilla)
    }
    override func viewWillAppear(_ animated: Bool) {
        stack.arrangedSubviews[1].isHidden = false
        stack.arrangedSubviews[0].isHidden = true
        stack.arrangedSubviews[2].isHidden = true
        stack.arrangedSubviews[3].isHidden = true
        let ecode = Constants.ecode
//        let type = ecode.prefix(1)
//        if type == "t"
//        {
//            if Constants.kIsMainMember == true
//            {
//                stack.arrangedSubviews[3].isHidden = false
//                stack.arrangedSubviews[1].isHidden = false
//                
//            }
//        }
//        else if type == "m"
//        {
//            if Constants.kAccountType == "Special Membership"
//            {
//                stack.arrangedSubviews[3].isHidden = false
//                stack.arrangedSubviews[1].isHidden = false
//            }
//        }
    }
    @objc func handleTapCard(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.type = "Credit Card"
        cardRadio.image = UIImage(named: "radioChecked")
        spanRadio.image = UIImage(named: "radioUnchecked")
        ainCoinRadio.image = UIImage(named: "radioUnchecked")
        settleToVillaRadio.image = UIImage(named: "radioUnchecked")
    }
    @objc func handleTapSpan(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.type = "Span"
        cardRadio.image = UIImage(named: "radioUnchecked")
        spanRadio.image = UIImage(named: "radioChecked")
        ainCoinRadio.image = UIImage(named: "radioUnchecked")
        settleToVillaRadio.image = UIImage(named: "radioUnchecked")
    }
    @IBAction func donebtn(_ sender: Any) {
        self.delegate?.getType(type: self.type, id: self.id ?? -1, tag: self.tag ?? -1)
        self.dismiss(animated: true, completion: nil)
    }
    @objc func handleTapCoin(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.type = "Ain Coin"
        cardRadio.image = UIImage(named: "radioUnchecked")
        spanRadio.image = UIImage(named: "radioUnchecked")
        ainCoinRadio.image = UIImage(named: "radioChecked")
        settleToVillaRadio.image = UIImage(named: "radioUnchecked")
    }
    @objc func handleTapVilla(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.type = "Settle to Villa"
        cardRadio.image = UIImage(named: "radioUnchecked")
        spanRadio.image = UIImage(named: "radioUnchecked")
        ainCoinRadio.image = UIImage(named: "radioUnchecked")
        settleToVillaRadio.image = UIImage(named: "radioChecked")
    }
}
