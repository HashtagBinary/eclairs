//
//  FnBMenuIngredientVC.swift
//  Eclair
//
//  Created by Sohail on 08/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Kingfisher

var ingredientCart = [Int : Int]()

class FnBMenuIngredientVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numberButton: UIView!
    @IBOutlet weak var summaryBoxView: SummaryBoxView!
    @IBOutlet weak var quantitylbl: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    
    var itemModel: FnBMenuItem?
    var totalPrice: Float = 0
    var isFromDetail = false
    var dbHelper = DBHelperFnB()
    var currentQuantity = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ingredientCart = [Int : Int]()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 44.0;
        
        tableView.register(UINib(nibName: "FnBType0TableViewCell", bundle: nil), forCellReuseIdentifier: "FnBType0TableViewCell")
        tableView.register(UINib(nibName: "FnBType1TableViewCell", bundle: nil), forCellReuseIdentifier: "FnBType1TableViewCell")
        tableView.register(UINib(nibName: "FnBType2TableViewCell", bundle: nil), forCellReuseIdentifier: "FnBType2TableViewCell")
        
        setupViews()
        
        //numberButton.addTarget(self, action: #selector(addToCart(sender:)), for: .valueChanged)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSummaryBoxClick))
        summaryBoxView.addGestureRecognizer(tap)
        summaryBoxView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifyUpdate(_:)), name: NSNotification.Name(rawValue: "updateFnBIngredientPrice"), object: nil)
        tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        updateCartSummaryBox()
    }
    override func viewDidAppear(_ animated: Bool) {
        initData()
        updateTotalPrice()
    }
    
    func setupViews() {
        imgView.cornerRadius = 8
    }
    
    func initData() {
        titleLabel.text = itemModel?.title
        descriptionLabel.text = itemModel?.description
        caloriesLabel.text = "\(itemModel?.calories ?? 0.0) cal"
        priceLabel.text = "\(itemModel?.price ?? 0) \(Constants.CURRENCY_SYMBOL)"
        
        
        self.imgView.image = UIImage.init(named: "image_loading")
        let url = itemModel?.image ?? "image_loading"
        self.imgView.downloadImage(with: url)
        { (data:UIImage?, error:NSError?) in
            self.imgView.image = data
        }
        let cartItem = dbHelper.getFnbItemById(id: itemModel?.id ?? -1)
        //numberButton.setValue(newValue: (cartItem != nil) ? cartItem?.value(forKey: "value") as! Int : 0)
        self.currentQuantity = (cartItem != nil) ? cartItem?.value(forKey: "value") as! Int : 0
        self.quantitylbl.text = "Already Added : \(self.currentQuantity)"
        if self.currentQuantity <= 0
        {
            self.minusButton.isEnabled = false
        }
        else
        {
            self.minusButton.isEnabled = true
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModel?.menu_ingredients.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let ingredientModel = itemModel?.menu_ingredients[indexPath.row]
        
        ingredientCart[ingredientModel?.id ?? -1] = ingredientModel?.defaultValue ?? 0
        
        if ingredientModel?.type == Constants.CHECKBOX {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBType0TableViewCell", for: indexPath) as! FnBType0TableViewCell
            
            cell.titleLabel.text = ingredientModel?.title
            cell.priceLabel.text = "\(ingredientModel?.price ?? 0) \(Constants.CURRENCY_SYMBOL)"
            cell.button.setValue(isChecked: (ingredientModel?.defaultValue == 0) ? false : true)
            
            cell.button.tag = ingredientModel?.id ?? -1
            cell.button.addTarget(self, action: #selector(updateCheckBoxCart(sender:)), for: .valueChanged)
            
            return cell
        }
        else if ingredientModel?.type == Constants.COUNTER {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBType1TableViewCell", for: indexPath) as! FnBType1TableViewCell
            
            cell.titleLabel.text = ingredientModel?.title
            cell.priceLabel.text = "\(ingredientModel?.price ?? 0) \(Constants.CURRENCY_SYMBOL)"
            cell.button.setValue(newValue: ingredientModel?.defaultValue ?? 0)
            cell.button.maxValue = ingredientModel?.max ?? 100
            
            cell.button.tag = ingredientModel?.id ?? -1
            cell.button.addTarget(self, action: #selector(updateSpinnerCart(sender:)), for: .valueChanged)
            
            return cell
        }
        else if ingredientModel?.type == Constants.CHOICE {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBType2TableViewCell", for: indexPath) as! FnBType2TableViewCell
            
            cell.ingId = ingredientModel?.id ?? -1
            cell.titleLabel.text = ingredientModel?.title
            
            var radioList = [RadioModel]()
            ingredientModel?.subIngredients.forEach({ (item) in
                let model = RadioModel(subId: item.subIngId!, title: item.title!, price: item.price!, defaultValue: ingredientModel?.defaultValue ?? 0)
                radioList.append(model)
            })
            cell.setData(dataList: radioList)
            
            if (ingredientModel?.defaultValue == 0 && (ingredientModel?.subIngredients.count ?? 0) > 0) {
                ingredientCart[ingredientModel?.id ?? -1] = ingredientModel?.subIngredients[0].subIngId
            }
            
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    @IBAction func addToCartAction(_ sender: Any) {
        self.addToCart()
    }
    
    @IBAction func removeFromCartAction(_ sender: Any) {
        if self.currentQuantity <= 0
        {
            return
        }
        else
        {
            self.currentQuantity -= 1
            if self.currentQuantity <= 0
            {
                self.minusButton.isEnabled = false
            }
            else
            {
                self.minusButton.isEnabled = true
            }
            self.quantitylbl.text = "Already Added : \(self.currentQuantity)"
            ingredientCart.forEach { (arg0) in
                let (key, value) = arg0
                dbHelper.updateFnbIngredient(id: key, itemId: itemModel?.id ?? -1, quantityId: self.currentQuantity, value: value)
            }
            
            dbHelper.updateFnbItem(id: itemModel?.id ?? -1, catId: itemModel?.categoryId ?? -1, value: self.currentQuantity)
            updateCartSummaryBox()
            showToast(message: "Item Removed successfully")
        }
    }
    func addToCart() {
        
        self.currentQuantity += 1
        self.minusButton.isEnabled = true
        self.quantitylbl.text = "Already Added : \(self.currentQuantity)"
        ingredientCart.forEach { (arg0) in
            let (key, value) = arg0
            dbHelper.updateFnbIngredient(id: key, itemId: itemModel?.id ?? -1, quantityId: self.currentQuantity, value: value)
        }
        
        dbHelper.updateFnbItem(id: itemModel?.id ?? -1, catId: itemModel?.categoryId ?? -1, value: self.currentQuantity)
        updateCartSummaryBox()
        showToast(message: "Item added successfully")
    }
    
    
//    @objc func addToCart(sender: NumberButton) {
//
//        ingredientCart.forEach { (arg0) in
//            let (key, value) = arg0
//            dbHelper.updateFnbIngredient(id: key, itemId: itemModel?.id ?? -1, quantityId: self.currentQuantity, value: value)
//        }
//
//        dbHelper.updateFnbItem(id: itemModel?.id ?? -1, catId: itemModel?.categoryId ?? -1, value: self.currentQuantity)
//        updateCartSummaryBox()
//
//        if sender.type == 1 {
//            showToast(message: "Item added successfully")
//        }
//        else if sender.type == -1 {
//            showToast(message: "Item removed successfully")
//        }
//    }
    
    @objc func updateCheckBoxCart(sender: CheckBox) {
        ingredientCart[sender.tag] = (sender.isChecked) ? 1 : 0
        updateTotalPrice()
        updateCartSummaryBox()
    }
    
    @objc func updateSpinnerCart(sender: NumberButton) {
        ingredientCart[sender.tag] = sender.currentValue
        updateTotalPrice()
        updateCartSummaryBox()
    }
    
    
    @objc func notifyUpdate(_ notification: NSNotification) {
        self.updateTotalPrice()
        updateCartSummaryBox()
    }
    
    @objc func onSummaryBoxClick(sender: SummaryBoxView) {
        let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBCartViewController") as? FnBCartViewController
        if isFromDetail == true
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.navigationController!.pushViewController(vc!, animated: true)
        }
    }
    
    func updateTotalPrice() {
        totalPrice = itemModel?.price ?? 0
        
        itemModel?.menu_ingredients.forEach({ (ingredient) in
            let key = ingredientCart[ingredient.id!]
            
            if (ingredient.type == Constants.CHECKBOX && key == 1 && ingredient.defaultValue == 0) {
                totalPrice += (ingredient.price ?? 0)
            }
            else if (ingredient.type == Constants.CHECKBOX && key == 0 && ingredient.defaultValue == 1) {
                totalPrice -= (ingredient.price ?? 0)
            }
            else if (ingredient.type == Constants.COUNTER && (key ?? 0) > (ingredient.defaultValue ?? 0)) {
                let ee = (key ?? 0) - (ingredient.defaultValue ?? 0)
                totalPrice += (ingredient.price ?? 0) * Float(ee)
            }
            else if (ingredient.type == Constants.CHOICE && ingredient.defaultValue != key) {
                let index = ingredient.subIngredients.firstIndex{$0.subIngId == key}
                if index != nil {
                    totalPrice += (ingredient.subIngredients[index!].price ?? 0)
                }
            }
        })

        totalPriceLabel.text = "\(totalPrice) \(Constants.CURRENCY_SYMBOL)"
    }
    
    func updateCartSummaryBox() {
        let fnbTotal = FnB_Home.calculateFnbPrice()
        summaryBoxView.updateValue(count: fnbTotal.count, total: fnbTotal.total)
        summaryBoxView.isHidden = (fnbTotal.count <= 0) ? true : false
    }
}
