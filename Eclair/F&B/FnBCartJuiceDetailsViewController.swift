//
//  FnBCartJuiceDetailsViewController.swift
//  Eclair
//
//  Created by Sohail on 25/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBCartJuiceDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var fnBJuiceCartItem: FnBJuiceCartItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 60;
        
        tableView.register(UINib(nibName: "CartSubIngredientTableViewCell", bundle: nil), forCellReuseIdentifier: "CartSubIngredientTableViewCell")
    }
}

extension FnBCartJuiceDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartSubIngredientTableViewCell", for: indexPath) as! CartSubIngredientTableViewCell
        
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "GLASS SIZE"
            let value = departmentsModel.juiceGlasses.first { $0.id == fnBJuiceCartItem!.glass_id }
            cell.valueLabel.text = value?.title ?? "-"
        case 1:
            cell.titleLabel.text = "SWEETENER"
            let value = departmentsModel.juiceSweetners.first { $0.id == fnBJuiceCartItem!.sweetner_id }
            cell.valueLabel.text = value?.title ?? "-"
        case 2:
            cell.titleLabel.text = "ICE CUBE"
            let value = departmentsModel.juiceIces.first { $0.id == fnBJuiceCartItem!.ice_id }
            cell.valueLabel.text = value?.title ?? "-"
        case 3:
            cell.titleLabel.text = "BLEND TIME"
            let value = departmentsModel.juiceBlendTimes.first { $0.id == fnBJuiceCartItem!.blend_id }
            cell.valueLabel.text = value?.title ?? "-"
        case 4:
            cell.titleLabel.text = "Syrup"
            let value = departmentsModel.juiceSyrups.first { $0.id == fnBJuiceCartItem!.syrup_id }
            cell.valueLabel.text = value?.title ?? "-"
        default:
            break
        }
        
        return cell
    }
}
