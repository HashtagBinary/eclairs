//
//  FnBCartViewController.swift
//  Eclair
//
//  Created by Sohail on 11/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import FirebaseFirestore
import SwiftyJSON

class FnBCartViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dbHelper = DBHelperFnB()
//    var selectedTip: Float = 0
//    var paymentTag = -1
//    var paymentType: String? = nil
//    var voucherAmount: Float = 0
//    var isVoucherApplied = false
//    var voucher_remaining : Float = 0
//    var voucher_Code = ""
//    var voucher_apiAmount:Float = 0
    
    var delegate : JuiceGlassUpdateDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 44.0;
        
        tableView.register(UINib(nibName: "FnBCartItemsTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBCartItemsTableViewCell")
        tableView.register(UINib(nibName: "FnBSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBSummaryTableViewCell")
        self.getVoucher()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    func getCartList() -> [Any] {
        var cartList = [Any]()
        
        dbHelper.getAllFnbItems()?.forEach({ (obj) in
            let id = obj.value(forKey: "id") as? Int
            
            var item = departmentsModel.menuItems.first { $0.id == id }
            if (item != nil) {
                item!.qty = obj.value(forKey: "value") as? Int
                cartList.append(item!)
            }
        })
        
        dbHelper.getAllJuiceItems()?.forEach({ (obj) in
            let id = obj.value(forKey: "id") as? Int
            let json = obj.value(forKey: "json") as? String
            
            if (json != nil) {
                let data = json!.data(using: .utf8)!
                
                var model = try? JSONDecoder().decode(FnBJuiceCartItem.self, from: data)
                if (model != nil) {
                    model!.id = id ?? -1
                    cartList.append(model!)
                }
            }
        })
        
        return cartList
    }
    
    func removeItem(id: Int) {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to remove this item.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.dbHelper.deleteFnBItemById(id: id)
            self.tableView.reloadData()
            self.showToast(message: "Item removed successfully")
            let item = self.dbHelper.getAllFnbItems()
            if item?.count ?? -1 <= 0
            {
                print("item count is \(item?.count)")
                self.dbHelper.emptyFnbCart()
                GselectedTip = 0
                GpaymentTag = -1
               //var paymentType: String? = nil
                GvoucherAmount = 0
                GisVoucherApplied = false
                Gvoucher_remaining = 0
                Gvoucher_Code = ""
                Gvoucher_apiAmount = 0
                self.navigationController!.popToRootViewController(animated: true)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func removeJuiceItem(id: Int) {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to remove this item.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.dbHelper.removeJuice(id: id)
            self.tableView.reloadData()
            self.showToast(message: "Item removed successfully")
            
            let item = self.dbHelper.getAllJuiceItems()
            if item?.count ?? -1 <= 0
            {
                print("item count is \(item?.count)")
                self.dbHelper.emptyFnbCart()
                GselectedTip = 0
                GpaymentTag = -1
               //var paymentType: String? = nil
                GvoucherAmount = 0
                GisVoucherApplied = false
                Gvoucher_remaining = 0
                Gvoucher_Code = ""
                Gvoucher_apiAmount = 0
                self.navigationController!.popToRootViewController(animated: true)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "tipSegue") {
            let vc = segue.destination as! TipViewController
            vc.delegate = self
        }
        else if (segue.identifier == "instructionSegue") {
            let vc = segue.destination as! OrderInstructionViewController
            vc.delegate = self
        }
        else if (segue.identifier == "payment") {
            let vc = segue.destination as! PaymentVc
            vc.delegate = self
            vc.tag = GpaymentTag
        }
        else if (segue.identifier == "getVoucher") {
            let vc = segue.destination as! VoucherVC
            vc.delegate = self
            vc.isFnb = true
        }
    }
    
    @objc func addTip(sender: UIButton) {
        performSegue(withIdentifier: "tipSegue", sender: self)
    }
    
    @objc func removeTip(sender: UIButton) {
        GselectedTip = 0
        tableView.reloadData()
    }
    
    @objc func setPreorder(sender: UIButton) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 250)
        let pickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        if #available(iOS 13.4, *) {
            pickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
        }
        pickerView.datePickerMode = .time
        vc.view.addSubview(pickerView)
        
        
        
        let alertController = UIAlertController(title: "Select Preorder Time", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.setValue(vc, forKey: "contentViewController")
        alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: { (action: UIAlertAction!) in
            let date = pickerView.date
            let strTime = date.dateStringWith(strFormat: "hh:mm a")
            GselectedPreOrderTime = strTime
            self.tableView.reloadData()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true)
    }
    
    @objc func removePreorder(sender: UIButton) {
        GselectedPreOrderTime = nil
        tableView.reloadData()
    }
    
    @objc func onAddress(sender: Any) {
        let obj = dbHelper.getDepartment()
        if (obj != nil) {
            let department = FnBMenuDepartment(id: (obj!.value(forKey: "id") as! Int), title: String(), image: String(), isSheeshaAvailable: (obj!.value(forKey: "isSheesha") as! Bool), isJuice: (obj!.value(forKey: "isJuice") as! Bool))
            
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
            
            vc.isTrend = false
            vc.isCart = true
            vc.departmentModel = department
            vc.delegate = self
            self.navigationController!.pushViewController(vc, animated: true)
        }
        else {
            Utilities.showAlert("Form Error", message: "No department is selected!")
        }
    }
    
    @objc func onInstruction(sender: Any) {
        performSegue(withIdentifier: "instructionSegue", sender: self)
    }
    
    @objc func clearAll(sender: UIButton) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to remove all items from the cart.", preferredStyle: UIAlertController.Style.alert)
        

        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.dbHelper.emptyFnbCart()
            GselectedTip = 0
            GpaymentTag = -1
           //var paymentType: String? = nil
            GvoucherAmount = 0
            GisVoucherApplied = false
            Gvoucher_remaining = 0
            Gvoucher_Code = ""
            Gvoucher_apiAmount = 0
            self.navigationController!.popToRootViewController(animated: true)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func checkOut(sender: UIButton)
    {
        let department = dbHelper.getDepartment()
        if (department == nil) {
            Utilities.showAlert("Form Error", message: "No department is selected!")
            return
        }
        
        if (GselectedTableId == -1 && GselectedAddress == nil) {
            Utilities.showAlert("Form Error", message: "Please select address to continue!")
            return
        }
        
        if (GselectedPaymentType == nil) || (GselectedPaymentType == "")  {
            Utilities.showAlert("Form Error", message: "Please select payment type to continue!")
            return
        }

        
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to checkout?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            self.checkoutApiCall()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func checkoutApiCall()
    {
        let department = dbHelper.getDepartment()
        if (department == nil) {
            Utilities.showAlert("Form Error", message: "No department is selected!")
            return
        }
        
        if (GselectedTableId == -1 && GselectedAddress == nil) {
            Utilities.showAlert("Form Error", message: "Please select address to continue!")
            return
        }
        
        if (GselectedPaymentType == nil) || (GselectedPaymentType == "")  {
            Utilities.showAlert("Form Error", message: "Please select payment type to continue!")
            return
        }
        
        var items = [Int]()
        
        dbHelper.getAllFnbItems()?.forEach({ (item) in
            items.append(item.value(forKey: "id") as! Int)
        })
      
        if (items.count <= 0 && (dbHelper.getAllJuiceItems()?.count ?? 0) <= 0) {
            print("Items are \(items)")
            Utilities.showAlert("Form Error", message: "Please select an item to continue!")
            return
        }
        
        let fnbTotal = FnB_Home.calculateFnbPrice()
        let subtotal: Float = fnbTotal.total
        var newSubTot = subtotal
        
        if GisVoucherApplied == true
        {
            Gvoucher_remaining = subtotal - GvoucherAmount
            if Gvoucher_remaining <= 0
            {
                newSubTot = 0
            }
            else
            {
                newSubTot = Gvoucher_remaining
                Gvoucher_remaining = 0
            }
        }
        
        
        // Tax Calculation
        let vtax = newSubTot * Constants.kFnBVatPercentage / 100
        let total = vtax + newSubTot + GselectedTip
        
        let jsonObject: NSMutableDictionary = NSMutableDictionary()

        jsonObject.setValue(GselectedTableId, forKey: "tableId")
        jsonObject.setValue((GselectedTableId == -1) ? true : false, forKey: "isVilla")
        jsonObject.setValue(department!.value(forKey: "id") as! Int, forKey: "departmentId")
        jsonObject.setValue(items, forKey: "menuitems")

        let jsonData: NSData
        var jsonString = String()

        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String

        } catch _ {
            print ("JSON Failure")
        }

        
        if Constants.kAccountType.trimString().lowercased() == "hospitality" || Constants.kAccountType.trimString().lowercased() == "owner"
        {
            GselectedPaymentType = "Settle by Ledger"
        }
         
        
        let parameters = [
            "ecode" : Constants.ecode,
            "paymentType" : GselectedPaymentType,
            "checkoutData" : jsonString,
            "checkoutFrom" : "fnb",
            "totalBill" : total,
            "vat":vtax,
            "discount": 0,
            "totalNetBill":subtotal,
            "voucherCode":Gvoucher_Code,
            "address":GselectedAddress ?? "",
            "tip":"",
            "transactionId":""
        ] as [String: Any]
        
        print("parameter \(parameters)")
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kFnBGetNewOrderId, parameters: parameters) {  (data:Data?, error:NSError?)  in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            do {
                let model = try JSONDecoder().decode(FnBCheckoutResponse.self, from: data!)
                if (model.response) {
                    self.submitOrder(order_id: model.orderId!)
                }
                else {
                    ActivityIndicatorSingleton.StopActivity(myView: self.view)
                    Utilities.showAlert("Response Error", message: model.error ?? "")
                }
                self.tableView.reloadData()
                
            } catch let error as NSError {
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                print(error)
            }
        }
    }
    
    func submitOrder(order_id: Int) {
        let department = dbHelper.getDepartment()!
        let dbHelper = DBHelperFnB()
        var menuItemArrayList = [Any]()
        
        dbHelper.getAllFnbItems()?.forEach({ (item) in
            let id = item.value(forKey: "id") as? Int
            let menuitem = departmentsModel.menuItems.first { $0.id == id }
            if (menuitem != nil) {
                let item_quantity = (item.value(forKey: "value") as? Int) ?? 0
                
                var item_totalprice: Float = 0
                
                var fnBDataMenuItem = FnBCheckOutMenuItem(id: menuitem!.id!, quantity: item_quantity, totalprice: item_totalprice)
                
                var menuIngredientsArrayList = [FnBCheckOutMenuIngredients]()
                
                for qid in 1...item_quantity {
                    
                    item_totalprice += menuitem!.price ?? 0
                    
                    var fnBDataMenuIngredients = [FnBCheckOutMenuIngredient]()
                    var ingredient_total: Float = 0
                    
                    menuitem?.menu_ingredients.forEach({ (menuIngredient) in
                        let ingredient_type = menuIngredient.type
                        
                        let dbIngredient = dbHelper.getFnbIngredientById(id: menuIngredient.id ?? -1, quantityId: qid)
                        if (dbIngredient != nil) {
                            let ingredient_quantity = (dbIngredient?.value(forKey: "value") as? Int) ?? 0
                            var ingredient_item_price: Float = 0
                            
                            if (ingredient_type == Constants.CHECKBOX && ingredient_quantity == 1 && menuIngredient.defaultValue == 0) {
                                ingredient_total += menuIngredient.price ?? 0
                                ingredient_item_price = menuIngredient.price ?? 0
                            }
                            else if (ingredient_type == Constants.CHECKBOX && ingredient_quantity == 0 && menuIngredient.defaultValue == 1) {
                                ingredient_total -= menuIngredient.price ?? 0
                                ingredient_item_price = -(menuIngredient.price ?? 0)
                            }
                            else if (ingredient_type == Constants.COUNTER && ingredient_quantity > (menuIngredient.defaultValue ?? 0)) {
                                let ee = ingredient_quantity - (menuIngredient.defaultValue ?? 0)
                                ingredient_total += (menuIngredient.price ?? 0) * Float(ee)
                                ingredient_item_price = (menuIngredient.price ?? 0) * Float(ee)
                            }
                            else if (ingredient_type == Constants.CHOICE && menuIngredient.defaultValue != ingredient_quantity) {
                                let index = menuIngredient.subIngredients.firstIndex{$0.subIngId == ingredient_quantity}
                                if index != nil {
                                    ingredient_total += (menuIngredient.subIngredients[index!].price ?? 0)
                                    ingredient_item_price = (menuIngredient.subIngredients[index!].price ?? 0)
                                }
                            }
                            
                            let ingredient = FnBCheckOutMenuIngredient(id: menuIngredient.id!, quantity: ingredient_quantity, totalprice: ingredient_item_price)
                            fnBDataMenuIngredients.append(ingredient)
                            item_totalprice += ingredient_item_price
                        }
                    })
                    
                    var fnBDataMenuIngredients1 = FnBCheckOutMenuIngredients(totalprice: ingredient_total)
                    fnBDataMenuIngredients1.menuIngredients = fnBDataMenuIngredients
                    menuIngredientsArrayList.append(fnBDataMenuIngredients1)
                }
                
                fnBDataMenuItem.menu_ingredients_list = menuIngredientsArrayList
                fnBDataMenuItem.totalprice = item_totalprice
                
                
                let jsonData = try? JSONEncoder().encode(fnBDataMenuItem)
                let jsonObject = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
                menuItemArrayList.append(jsonObject!)
            }
        })
        
        var juiceArrayList = [Any]()
        dbHelper.getAllJuiceItems()?.forEach({ (obj) in
            let json = obj.value(forKey: "json") as? String
            
            if (json != nil) {
                let jsonData = json!.data(using: .utf8)!
                let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: [])
                if jsonObject != nil {
                    juiceArrayList.append(jsonObject!)
                }
            }
        })
        
        let fnbTotal = FnB_Home.calculateFnbPrice()
        let itemCount = fnbTotal.count
        let subtotal: Float = fnbTotal.total
        var newSubTot = subtotal
        
        if GisVoucherApplied == true
        {
            Gvoucher_remaining = subtotal - GvoucherAmount
            if Gvoucher_remaining <= 0
            {
                newSubTot = 0
            }
            else
            {
                newSubTot = Gvoucher_remaining
                Gvoucher_remaining = 0
            }
        }
        let vtax = newSubTot * Constants.kFnBVatPercentage / 100
        let total = vtax + newSubTot + GselectedTip
        let map = [
            "ecode":  Constants.ecode,
            "department_id": department.value(forKey: "id") as! Int,
            "isdepartment_sheesha": department.value(forKey: "isSheesha") as! Bool,
            "address": GselectedAddress ?? "N/A",
            "menuitems": menuItemArrayList,
            "juiceitems": juiceArrayList,
            "paymenttype": GselectedPaymentType ?? "",
            "subtype": Constants.kAccountType,
            "item_count": itemCount,
            "voucher_total":total,
            "voucher_amount":Gvoucher_apiAmount,
            "isvoucher": GisVoucherApplied,
            "voucher_code":Gvoucher_Code,
            "voucher_remaining":Gvoucher_remaining,
            "subtotal": subtotal,
            "vat": vtax,
            "vatpercentage": Constants.kFnBVatPercentage,
            "tip": GselectedTip,
            "total": total,
            "instructions": (dbHelper.getInstructions()?.value(forKey: "data") as? String) ?? "N/A",
            "ispreorder": (GselectedPreOrderTime != nil) ? true : false,
            "preordertime": GselectedPreOrderTime ?? "N/A",
            "datetime_new": FieldValue.serverTimestamp(),
            "status": "n",
            "device": "Iphone"
        ] as [String : Any]
        
        Firestore.firestore().collection(Constants.ServerAPI.FS_FNB_URL).document(String(order_id)).setData(map) { err in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                
                 GselectedTip = 0
                 GpaymentTag = -1
                 GvoucherAmount = 0
                 GisVoucherApplied = false
                 Gvoucher_remaining = 0
                 Gvoucher_Code = ""
                 Gvoucher_apiAmount = 0
                 GisVoucher = false
                            
                
                let alert = UIAlertController(title: "Success", message: "Your Order has been submitted successfully!", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                    self.dbHelper.emptyFnbCart()
                    self.navigationController!.popToRootViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getVoucher() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBGetVoucherCode, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let json = try? JSON(data:data!)
            let response = json?["response"].bool ?? false
            if response == true
            {
                let voucherCode = json?["voucherCode"].string ?? ""
                if voucherCode.isEmpty == false
                {
                    Gvoucher_Code = voucherCode
                    GisVoucher = true
                    self.tableView.reloadData()
                }
                else
                {
                    GisVoucher = false
                    Gvoucher_Code = ""
                    GisVoucherApplied = false
                    GvoucherAmount = 0
                }
            }
        }
    }
}

extension FnBCartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBCartItemsTableViewCell", for: indexPath) as! FnBCartItemsTableViewCell
            
            cell.delegate = self
            cell.cartList = getCartList()
            cell.tableView.reloadData()
            cell.tableViewHeightContraint.constant = cell.tableView.contentSize.height
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBSummaryTableViewCell", for: indexPath) as! FnBSummaryTableViewCell
            
            let instructionsData = dbHelper.getInstructions()?.value(forKey: "data") as? String
            if (instructionsData != nil) {
                cell.instructionsLabel.text = instructionsData
            }
            else {
                cell.instructionsLabel.text = "There is no instructions defined."
            }
            
            let fnbTotal = FnB_Home.calculateFnbPrice()
            let itemCount = fnbTotal.count
            let subtotal: Float = fnbTotal.total
            var newSubTot = subtotal
            
            
            if GisVoucher == true
            {
                cell.voucherVw.isHidden = false
                cell.removeVoucherVw.isHidden = true
                cell.voucherDiscountVw.isHidden = true
                if GisVoucherApplied == true
                {
                    cell.voucherVw.isHidden = true
                    cell.voucherDiscountVw.isHidden = false
                    cell.removeVoucherVw.isHidden = false
                    cell.appledTagVoucherVw.isHidden = false
                    Gvoucher_remaining = subtotal - GvoucherAmount
                    
                    if Gvoucher_remaining <= 0
                    {
                        newSubTot = 0
                        GselectedPaymentType = "Settle by Voucher"
                    }
                    else
                    {
                        newSubTot = Gvoucher_remaining
                        GselectedPaymentType = ""
                    }
                }
                else
                {
                    cell.appledTagVoucherVw.isHidden = true
                    cell.voucherDiscountVw.isHidden = true
                    cell.voucherVw.isHidden = false
                    cell.removeVoucherVw.isHidden = true
                }
            }
            else
            {
                cell.appledTagVoucherVw.isHidden = true
                cell.voucherDiscountVw.isHidden = true
                cell.voucherVw.isHidden = true
                cell.removeVoucherVw.isHidden = true
            }

            
            // Tax Calculation
            let vtax = newSubTot * Constants.kFnBVatPercentage / 100
            let total = vtax + newSubTot + GselectedTip
            
            cell.voucherDisAmountlbl.text = "-\(Gvoucher_apiAmount) \(Constants.CURRENCY_SYMBOL)"
            cell.totalItemsLabel.text = String(format: "%02d", itemCount)
            cell.subAmountLabel.text = "\(subtotal) \(Constants.CURRENCY_SYMBOL)"
            cell.vatTitleLabel.text = "\(Constants.kFnBVatLabel) (\(Constants.kFnBVatPercentage)%)"
            cell.vatLabel.text = "\(vtax) \(Constants.CURRENCY_SYMBOL)"
            cell.totalAmountLabel.text = "\(total) \(Constants.CURRENCY_SYMBOL)"
            
            if (GselectedTip <= 0) {
                cell.tipAmountBox.isHidden = true
                cell.removeTipBox.isHidden = true
                cell.addTipBox.isHidden = false
            }
            else {
                GselectedPaymentType = ""
                cell.tipAmountBox.isHidden = false
                cell.removeTipBox.isHidden = false
                cell.addTipBox.isHidden = true
                cell.tipAmountLabel.text = "\(GselectedTip) \(Constants.CURRENCY_SYMBOL)"
            }
            cell.preOrderBox.isHidden = true
            if (GselectedPreOrderTime == nil) {
                cell.preOrderBox.isHidden = true
                cell.removePreOrderBox.isHidden = true
            }
            else {
                cell.preOrderBox.isHidden = true
                cell.removePreOrderBox.isHidden = false
                
                cell.preOrderLabel.text = GselectedPreOrderTime!
            }

            cell.addressLabel.text = GselectedAddress ?? ""
            cell.paymentLabel.text = GselectedPaymentType ?? ""
            
            cell.removeTipButton.addTarget(self, action: #selector(removeTip(sender:)), for: .touchUpInside)
            cell.tipButton.addTarget(self, action: #selector(addTip(sender:)), for: .touchUpInside)
            cell.clearButton.addTarget(self, action: #selector(clearAll(sender:)), for: .touchUpInside)
            cell.checkOutButton.addTarget(self, action: #selector(checkOut(sender:)), for: .touchUpInside)
            cell.voucherButton.addTarget(self, action: #selector(voucherTapped(sender:)), for: .touchUpInside)
            cell.removeVoucherButton.addTarget(self, action: #selector(removeVoucherTapped(sender:)), for: .touchUpInside)
            
            cell.preOrderButton.addTarget(self, action: #selector(setPreorder(sender:)), for: .touchUpInside)
            cell.removePreOrderButton.addTarget(self, action: #selector(removePreorder(sender:)), for: .touchUpInside)
            
            var tap = UITapGestureRecognizer(target: self, action: #selector(onAddress))
            cell.addressBox.addGestureRecognizer(tap)
            
            tap = UITapGestureRecognizer(target: self, action: #selector(onInstruction(sender:)))
            cell.instructionsBox.addGestureRecognizer(tap)
            
            tap = UITapGestureRecognizer(target: self, action: #selector(paymentTapped(sender:)))
            cell.paymentBox.addGestureRecognizer(tap)

            return cell
            
        default:
            return UITableViewCell()
        }
    }
    @objc func paymentTapped(sender: UIStackView) {

        if GselectedPaymentType != "Settle by Voucher"
        {
            self.performSegue(withIdentifier: "payment", sender: self)
        }
    }
    @objc func voucherTapped(sender: UIStackView) {
        
        self.performSegue(withIdentifier: "getVoucher", sender: self)
    }
    @objc func removeVoucherTapped(sender: UIStackView) {
        GisVoucherApplied = false
//        let fnbTotal = FnB_Home.calculateFnbPrice()
//        let itemCount = fnbTotal.count
//        let subtotal: Float = fnbTotal.total
//        let newSubTot = subtotal
//
//        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! FnBSummaryTableViewCell
//        cell.removeVoucherVw.isHidden = true
//        cell.voucherDiscountVw.isHidden = true
//        cell.voucherVw.isHidden = false
//        cell.appledTagVoucherVw.isHidden = true
//
//        GselectedPaymentType = ""
//        cell.paymentLabel.text = GselectedPaymentType ?? ""
//        let vtax = newSubTot * Constants.kFnBVatPercentage / 100
//        let total = vtax + newSubTot + GselectedTip
//
//        cell.voucherDisAmountlbl.text = "-\(Gvoucher_apiAmount) \(Constants.CURRENCY_SYMBOL)"
//        cell.totalItemsLabel.text = String(format: "%02d", itemCount)
//        cell.subAmountLabel.text = "\(subtotal) \(Constants.CURRENCY_SYMBOL)"
//        cell.vatTitleLabel.text = "\(Constants.kFnBVatLabel) (\(Constants.kFnBVatPercentage)%)"
//        cell.vatLabel.text = "\(vtax) \(Constants.CURRENCY_SYMBOL)"
//        cell.totalAmountLabel.text = "\(total) \(Constants.CURRENCY_SYMBOL)"
        tableView.reloadData()
    }
}

extension FnBCartViewController: SubItemDelegate, TipDelegate, UpdationDelegate ,PaymentDelegate{
    func getType(type: String, id: Int, tag: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? FnBSummaryTableViewCell
        cell?.paymentLabel.text = type
        GselectedPaymentType = type
    }
        
    func action(type: Int, data: Any) {
        if (type == 0) {
            let menuItem = data as! FnBMenuItem
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBCartDetailsViewController") as? FnBCartDetailsViewController
            vc!.menuItem = menuItem
            self.navigationController!.pushViewController(vc!, animated: true)
        }
        else if (type == 1) {
            removeItem(id: data as! Int)
        }
        else if (type == 2) {
            removeJuiceItem(id: data as! Int)
        }
        else if (type == 3) {
            dbHelper.setInstructions(data: data as! String)
            tableView.reloadData()
        }
        else if (type == 4) {
            let fnBJuiceCartItem = data as? FnBJuiceCartItem
            print("fnb juice cart item \(fnBJuiceCartItem)")
            
            juiceSelectedFlavours = fnBJuiceCartItem?.flavours ?? []
            juiceSelectedIceId = fnBJuiceCartItem?.ice_id ?? -1
            juiceSelectedBlendId = fnBJuiceCartItem?.blend_id ?? -1
            juiceSelectedGlassId = fnBJuiceCartItem?.glass_id ?? -1
            juiceSelectedSweetenerId = fnBJuiceCartItem?.sweetner_id ?? -1
            juiceSelectedSyrupID = fnBJuiceCartItem?.syrup_id ?? -1
            
            let depid = (dbHelper.getDepartment()?.value(forKey: "id") as? Int) ?? -1
            
            
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBJuiceViewController") as! FnBJuiceViewController
            vc.isFromDetail = true
            vc.department_id = depid
            vc.juiceID = fnBJuiceCartItem?.id ?? -1
            self.navigationController!.pushViewController(vc, animated: true)
        }
        else if type == 5
        {
            //juice
            
//            let fnBJuiceCartItem = data as? FnBJuiceCartItem
//            print("fnb juice cart item \(fnBJuiceCartItem)")
//
//            juiceSelectedFlavours = fnBJuiceCartItem?.flavours ?? []
//            juiceSelectedIceId = fnBJuiceCartItem?.ice_id ?? -1
//            juiceSelectedBlendId = fnBJuiceCartItem?.blend_id ?? -1
//            juiceSelectedGlassId = fnBJuiceCartItem?.glass_id ?? -1
//            juiceSelectedSweetenerId = fnBJuiceCartItem?.sweetner_id ?? -1
//            juiceSelectedSyrupID = fnBJuiceCartItem?.syrup_id ?? -1
//
//            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBJuiceViewController") as! FnBJuiceViewController
//              let depid = (dbHelper.getDepartment()?.value(forKey: "id") as? Int) ?? -1
//              vc.department_id = depid
//              vc.juiceID = fnBJuiceCartItem?.id ?? -1
//              vc.isFromDetail = true
//            self.navigationController!.pushViewController(vc, animated: true)
            
            
            
            let fnBJuiceCartItem = data as? FnBJuiceCartItem
            print("fnb juice cart item \(fnBJuiceCartItem)")
            
            juiceSelectedFlavours = fnBJuiceCartItem?.flavours ?? []
            juiceSelectedIceId = fnBJuiceCartItem?.ice_id ?? -1
            juiceSelectedBlendId = fnBJuiceCartItem?.blend_id ?? -1
            juiceSelectedGlassId = fnBJuiceCartItem?.glass_id ?? -1
            juiceSelectedSweetenerId = fnBJuiceCartItem?.sweetner_id ?? -1
            juiceSelectedSyrupID = fnBJuiceCartItem?.syrup_id ?? -1
            
            let depid = (dbHelper.getDepartment()?.value(forKey: "id") as? Int) ?? -1
            
            
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBJuiceViewController") as! FnBJuiceViewController
            vc.isFromDetail = true
            vc.department_id = depid
            vc.juiceID = fnBJuiceCartItem?.id ?? -1
            self.navigationController!.pushViewController(vc, animated: true)
            
            
        }
        else if type == 6
        {
            //fnbItem
            let menuItem = data as! FnBMenuItem
            let vc = UIStoryboard.init(name: "FnB", bundle: Bundle.main).instantiateViewController(withIdentifier: "FnBMenuIngredientVC") as? FnBMenuIngredientVC
            vc?.itemModel = menuItem
            vc?.isFromDetail = true
            self.navigationController!.pushViewController(vc!, animated: true)
        }
    }
    
    func addTip(amount: Float) {
        GselectedTip = amount
        tableView.reloadData()
    }
    
    func updateSummary() {
        tableView.reloadData()
    }
}
//
extension FnBCartViewController: VoucherDelegate
{
    func getVoucherCode(amount: String, voucherCode: String) {
        print("amount \(amount) and voucher Code \(voucherCode)")
        Gvoucher_apiAmount = Float(amount) ?? 0
        GvoucherAmount = Gvoucher_apiAmount
        //Gvoucher_Code = voucherCode
        GisVoucherApplied = true
        updateSummary()
    }
}
//extension FnBCartViewController:JuiceGlassUpdateDelegate
//{
//    func updateGlass() {
//        print("Update glass")
//    }
//
//    func updateCheckedImages() {
//        print("images")
//    }
//
//
//}

protocol SubItemDelegate {
    func action(type: Int, data: Any)
}

protocol TipDelegate {
    func addTip(amount: Float)
}

protocol UpdationDelegate {
    func updateSummary()
}

protocol PaymentDelegate {
    func getType(type:String,id:Int,tag:Int)
}

extension Date {
    func dateStringWith(strFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
        dateFormatter.dateFormat = strFormat
        return dateFormatter.string(from: self)
    }
}
