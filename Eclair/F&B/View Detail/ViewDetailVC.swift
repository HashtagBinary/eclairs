//
//  ViewDetailVC.swift
//  Eclair
//
//  Created by Indigo on 11/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import PDFGenerator
import WebKit


class ViewDetailVC: UIViewController {

    @IBOutlet var headerVw: UIView!
    @IBOutlet weak var tblVw: UITableView!
    
    
    var privousData : historyData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblVw.tableHeaderView = headerVw
        tblVw.cornerRadius = 12
        
        //headerVw.frame = CGRect(x)
        // Do any additional setup after loading the view.
    }

    @IBAction func okbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension ViewDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ViewDetailCell
        switch indexPath.row {
        case 0:
            cell?.titlelbl.text = "Order ID"
            cell?.valuelbl.text = "#\(privousData?.orderID ?? "")"
        case 1:
            cell?.titlelbl.text = "Address"
            cell?.valuelbl.text = privousData?.jsonData["address"] as? String
        case 2:
            cell?.titlelbl.text = "Instructions"
            cell?.valuelbl.text = privousData?.jsonData["instructions"] as? String
        case 3:
            cell?.titlelbl.text = "Order Date & Time"
            cell?.valuelbl.text = privousData?.jsonData["datetime_new"] as? String
        case 4:
            cell?.titlelbl.text = "Payment Type"
            cell?.valuelbl.text = privousData?.jsonData["paymenttype"] as? String
        case 5:
            cell?.titlelbl.text = "Pre Order"
            let status = privousData?.jsonData["ispreorder"] as? Bool ?? false
            if status == false
            {
                cell?.valuelbl.text = "No"
            }
            else
            {
                cell?.valuelbl.text = "Yes"
            }
        case 6:
            cell?.titlelbl.text = "Pre Order Time"
            cell?.valuelbl.text = privousData?.jsonData["preordertime"] as? String
        case 7:
            cell?.titlelbl.text = "Total Price"
            
//            var price = privousData?.jsonData["total"] as? Double ?? 0.0
//            var strPrice = "\(price)"
//            if price == 0
//            {
//                strPrice = privousData?.jsonData["total"] as? String ?? "0.0"
//            }
//            cell?.valuelbl.text = "\(strPrice) SR"
            if let price = privousData?.jsonData["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell?.valuelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell?.valuelbl.text = "-"
            }
            
      
            
        default:
            break
        }
        return cell!
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        //self.pdfDataWithTableViewNew(tableView: self.tblVw)
//        self.performSegue(withIdentifier: "pdf", sender: self)
//    }
}

