//
//  ViewDetailCell.swift
//  Eclair
//
//  Created by Indigo on 11/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class ViewDetailCell: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var valuelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
