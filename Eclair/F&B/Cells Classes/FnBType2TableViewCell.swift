//
//  FnBType2TableViewCell.swift
//  Eclair
//
//  Created by Sohail on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBType2TableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    var radioList = [RadioModel]()
    var buttonList = [RadioItem]()
    
    var ingId: Int = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(dataList: [RadioModel]) {
        self.radioList = dataList
        
        buttonList = []
        stackView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        
        var isAnyDefaultValue = -1
        
        radioList.forEach { (obj) in
            let radioItem = RadioItem()
            radioItem.setTitlePrice(title: obj.title, price: obj.price)
            radioItem.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
            
            radioItem.radioButton.id = obj.subId
            radioItem.radioButton.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
            
            radioItem.setValue(isChecked: (obj.subId == obj.defaultValue) ? true : false)
            
            if (obj.subId == obj.defaultValue) {
                isAnyDefaultValue = obj.subId
            }
            
            stackView.addArrangedSubview(radioItem)
            buttonList.append(radioItem)
        }
        
        if (radioList.count > 0) {
            if (radioList[0].defaultValue == 0 || isAnyDefaultValue == -1) {
                buttonList[0].setValue(isChecked: true)
            }
        }
    }
    
    @objc func buttonClicked(sender: RadioButton) {
        unCheckAllRadioButton()
        sender.setValue(isChecked: true)
        ingredientCart[ingId] = sender.id
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFnBIngredientPrice"), object: nil)
    }
    
    func unCheckAllRadioButton() {
        buttonList.forEach { (button) in
            button.setValue(isChecked: false)
        }
    }
}

struct RadioModel {
    var subId: Int
    var title: String
    var price: Float
    var defaultValue: Int
}
