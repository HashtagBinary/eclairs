//
//  ReceiptPaymentCell.swift
//  Eclair
//
//  Created by Zohaib Naseer on 6/21/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class ReceiptPaymentCell: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var valuelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
