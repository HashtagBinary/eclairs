//
//  FnBSummaryTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 11/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBSummaryTableViewCell: UITableViewCell {
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var totalItemsLabel: UILabel!
    @IBOutlet weak var subAmountLabel: UILabel!
    @IBOutlet weak var vatTitleLabel: UILabel!
    @IBOutlet weak var vatLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var instructionsBox: UIView!
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var addressBox: UIStackView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var paymentBox: UIStackView!
    @IBOutlet weak var paymentLabel: UILabel!
    
    @IBOutlet weak var addTipBox: UIView!
    @IBOutlet weak var tipButton: UIButton!
    @IBOutlet weak var removeTipBox: UIView!
    @IBOutlet weak var removeTipButton: UIButton!
    @IBOutlet weak var tipAmountBox: UIStackView!
    @IBOutlet weak var tipAmountLabel: UILabel!
    @IBOutlet weak var preOrderBox: UIView!
    @IBOutlet weak var preOrderButton: UIButton!
    @IBOutlet weak var preOrderLabel: UILabel!
    @IBOutlet weak var removePreOrderBox: UIView!
    @IBOutlet weak var removePreOrderButton: UIButton!
    @IBOutlet weak var voucherVw: UIView!
    @IBOutlet weak var voucherButton: UIButton!
    @IBOutlet weak var voucherDiscountVw: UIStackView!
    @IBOutlet weak var voucherDisAmountlbl: UILabel!
    @IBOutlet weak var removeVoucherVw: UIView!
    @IBOutlet weak var removeVoucherButton: UIButton!
    @IBOutlet weak var appledTagVoucherVw: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    func setupViews() {
        
        instructionsBox.borderWidth = 1
        instructionsBox.cornerRadius = 4
        instructionsBox.borderColor = UIColor.init(named: "ColorTextLight")
        
        tipButton.borderWidth = 1
        tipButton.cornerRadius = 20
        tipButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        removeTipButton.borderWidth = 1
        removeTipButton.cornerRadius = 20
        removeTipButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        preOrderButton.borderWidth = 1
        preOrderButton.cornerRadius = 20
        preOrderButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        removePreOrderButton.borderWidth = 1
        removePreOrderButton.cornerRadius = 20
        removePreOrderButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        voucherButton.borderWidth = 1
        voucherButton.cornerRadius = 20
        voucherButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        removeVoucherButton.borderWidth = 1
        removeVoucherButton.cornerRadius = 20
        removeVoucherButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        clearButton.borderWidth = 1
        clearButton.cornerRadius = 8
        clearButton.borderColor = UIColor.init(named: "ColorTextDark")
        
        
        
        checkOutButton.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
