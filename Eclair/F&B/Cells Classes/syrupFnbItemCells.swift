//
//  syrupFnbItemCells.swift
//  Eclair
//
//  Created by iOS Indigo on 31/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class syrupFnbItemCells: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionlbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
