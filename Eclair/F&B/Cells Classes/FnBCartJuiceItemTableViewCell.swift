//
//  FnBCartJuiceItemTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 16/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBCartJuiceItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var juiceGlass: JuiceGlassSmall!
    
    @IBOutlet weak var addItemButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        juiceGlass.cornerRadius = 12
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
