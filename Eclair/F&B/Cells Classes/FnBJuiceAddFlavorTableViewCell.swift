//
//  FnBJuiceAddFlavorTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 13/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBJuiceAddFlavorTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOpacity = 0.8
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.masksToBounds = false
        bgView.layer.cornerRadius = 12
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
