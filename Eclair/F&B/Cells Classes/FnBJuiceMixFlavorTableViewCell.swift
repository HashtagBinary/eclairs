//
//  FnBJuiceMixFlavorTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 13/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBJuiceMixFlavorTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOpacity = 0.8
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.masksToBounds = false
        bgView.layer.cornerRadius = 12
        
        slider.minimumValue = 1
        slider.maximumValue = 100
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
