//
//  FnBCartDetailItemTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 11/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBCartDetailItemTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var totalItemPrice: UILabel!
    var itemModel: FnBMenuItem?
    var qid: Int = -1
    
    var dbHelper = DBHelperFnB()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.dataSource = self
        tableView.delegate = self
        self.tableViewHeightContraint.constant = self.tableView.contentSize.height
        
        tableView.register(UINib(nibName: "CartSubIngredientTableViewCell", bundle: nil), forCellReuseIdentifier: "CartSubIngredientTableViewCell")
    }
}

extension FnBCartDetailItemTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModel?.menu_ingredients.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartSubIngredientTableViewCell", for: indexPath) as! CartSubIngredientTableViewCell
        
        let ingredientModel = itemModel?.menu_ingredients[indexPath.row]
        let cartIngredient = dbHelper.getFnbIngredientById(id: ingredientModel?.id ?? -1, quantityId: qid + 1)
        
        cell.titleLabel.text = ingredientModel?.title
        
        
        if (cartIngredient != nil) {
            let cartValue = cartIngredient?.value(forKey: "value") as! Int
            
            if ingredientModel?.type == Constants.CHECKBOX {
                let value = (cartValue == 1) ? "YES" : "NO"
                cell.valueLabel.text = "\(value)"
                cell.pricelbl.text = "0.0 \(Constants.CURRENCY_SYMBOL)"
                if ingredientModel?.price ?? 0 > 0
                {
                    cell.pricelbl.text = "\(ingredientModel?.price ?? 0.0) \(Constants.CURRENCY_SYMBOL)"
                }
            }
            else if ingredientModel?.type == Constants.COUNTER {
                cell.valueLabel.text = "\(cartValue) Quantity"
                cell.pricelbl.text = "0.0 \(Constants.CURRENCY_SYMBOL)"
                if cartValue > 0
                {
                    cell.pricelbl.text = "\(ingredientModel?.price ?? 0.0) \(Constants.CURRENCY_SYMBOL)"
                }
                
            }
            else if ingredientModel?.type == Constants.CHOICE {
                
                let subIng = ingredientModel?.subIngredients.first { $0.subIngId == cartValue }
                if (subIng != nil) {
                    cell.valueLabel.text = subIng?.title
                    cell.pricelbl.text = "0.0 \(Constants.CURRENCY_SYMBOL)"
                    if subIng?.price ?? 0 > 0
                    {
                        cell.pricelbl.text = "\(subIng?.price ?? 0.0) \(Constants.CURRENCY_SYMBOL)"
                    }
                }
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
}
