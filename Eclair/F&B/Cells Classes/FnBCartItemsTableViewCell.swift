//
//  FnBCartItemsTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 11/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Kingfisher

class FnBCartItemsTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightContraint: NSLayoutConstraint!
    
    var delegate : SubItemDelegate?
    
    var cartList = [Any]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "FnBCartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBCartItemTableViewCell")
        
        tableView.register(UINib(nibName: "FnBCartJuiceItemTableViewCell", bundle: nil), forCellReuseIdentifier: "FnBCartJuiceItemTableViewCell")
    }
    
    @objc func viewDetail(sender: UIButton) {
        delegate!.action(type: 0, data: cartList[sender.tag])
    }
    
    @objc func viewJuiceDetail(sender: UIButton) {
        delegate!.action(type: 4, data: cartList[sender.tag] as! FnBJuiceCartItem)
    }
    
    @objc func removeItem(sender: UIButton) {
        delegate!.action(type: 1, data: (cartList[sender.tag] as! FnBMenuItem).id ?? -1)
    }
    
    @objc func removeJuiceItem(sender: UIButton) {
        delegate!.action(type: 2, data: (cartList[sender.tag] as! FnBJuiceCartItem).id)
    }
    @objc func AddJuiceItem(sender: UIButton) {
        delegate!.action(type: 5, data: cartList[sender.tag] as! FnBJuiceCartItem)
        //juice controller
    }
    @objc func AddFnbItem(sender: UIButton) {
        delegate!.action(type: 6, data: cartList[sender.tag] as! FnBMenuItem)
        //Fnb Item controller
    }
}

extension FnBCartItemsTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = cartList[indexPath.row] as? FnBMenuItem
        
        if model != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBCartItemTableViewCell", for: indexPath) as! FnBCartItemTableViewCell
            
            cell.titleLabel.text = model!.title
            cell.descriptionLabel.text = model!.description
            cell.priceLabel.text = "\((model!.price ?? 0) * Float(model?.qty ?? 0)) \(Constants.CURRENCY_SYMBOL)"
            cell.quantityLabel.text = String(model!.qty ?? 0)
            cell.iconImage.image = UIImage.init(named: "image_loading")
            let url = model?.image ?? ""
            cell.iconImage.downloadImage(with: url)
            { (data:UIImage?, error:NSError?) in
                cell.iconImage.image = data
            }
            cell.viewDetailsButton.tag = indexPath.row
            cell.viewDetailsButton.addTarget(self, action: #selector(viewDetail(sender:)), for: .touchUpInside)
            
            cell.removeButton.tag = indexPath.row
            cell.removeButton.addTarget(self, action: #selector(removeItem(sender:)), for: .touchUpInside)
            cell.addItemButton.tag = indexPath.row
            cell.addItemButton.addTarget(self, action: #selector(AddFnbItem(sender:)), for: .touchUpInside)
           
            return cell
        }
        else {
            let juiceItem = cartList[indexPath.row] as? FnBJuiceCartItem
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FnBCartJuiceItemTableViewCell", for: indexPath) as! FnBCartJuiceItemTableViewCell
            
            cell.titleLabel.text = "Juice \(juiceItem?.id ?? 0)"
            
            let juiceGlass = departmentsModel.juiceGlasses.first { $0.id == juiceItem?.glass_id ?? -1 }
            if (juiceGlass != nil) {
                cell.priceLabel.text = "\(juiceGlass?.price ?? 0) \(Constants.CURRENCY_SYMBOL)"
            }
            
            cell.juiceGlass.juiceFlavors = juiceItem!.flavours
            cell.juiceGlass.UpdateFlavors()
            
            cell.viewDetailsButton.tag = indexPath.row
            cell.viewDetailsButton.addTarget(self, action: #selector(viewJuiceDetail(sender:)), for: .touchUpInside)
            
            cell.removeButton.tag = indexPath.row
            cell.removeButton.addTarget(self, action: #selector(removeJuiceItem(sender:)), for: .touchUpInside)
            
            cell.addItemButton.tag = indexPath.row
            cell.addItemButton.addTarget(self, action: #selector(AddJuiceItem(sender:)), for: .touchUpInside)
            return cell
        }
        
    }
}
