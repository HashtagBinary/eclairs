//
//  FnBTrendingItemViewCell.swift
//  Eclair
//
//  Created by Bilal on 8/28/19.
//  Copyright © 2019 Indigo. All rights reserved.
//

import UIKit

class FnBTrendingItemViewCell: UICollectionViewCell {

    @IBOutlet var image_imageView: UIImageView!
    
    @IBOutlet var title_label: UILabel!
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet var viewDetails_view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cardView.layer.cornerRadius = 12
        cardView.clipsToBounds = true
        cardView.layer.masksToBounds = false
        cardView.layer.shadowRadius = 3
        cardView.layer.shadowOpacity = 0.6
        cardView.layer.shadowOffset = CGSize.zero
        cardView.layer.shadowColor = UIColor.lightGray.cgColor
        
        image_imageView.cornerRadius = 12
    }
}
