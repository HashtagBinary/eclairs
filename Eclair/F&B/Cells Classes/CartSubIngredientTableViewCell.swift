//
//  CartSubIngredientTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 25/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class CartSubIngredientTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var pricelbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
