//
//  ReceiptTransactionCell.swift
//  Eclair
//
//  Created by Zohaib Naseer on 6/21/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class ReceiptTransactionCell: UITableViewCell {

    @IBOutlet weak var tranIDTitlelbl: UILabel!
    @IBOutlet weak var cardTypeTitlelbl: UILabel!
    @IBOutlet weak var amountTitlelbl: UILabel!
    @IBOutlet weak var transactionlbl: UILabel!
    @IBOutlet weak var cardTypelbl: UILabel!
    @IBOutlet weak var amountlbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
