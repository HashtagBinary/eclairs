//
//  FnBJuiceItemTableViewCell.swift
//  Eclair
//
//  Created by Sohail on 15/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBJuiceItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imgSuccess: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
