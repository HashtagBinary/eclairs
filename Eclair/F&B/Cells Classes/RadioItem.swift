//
//  RadioItem.swift
//  Eclair
//
//  Created by Sohail on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class RadioItem: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var radioButton: RadioButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("RadioItem", owner: self, options: nil)
        contentView.fixInView(self)
    }
    
    func setTitlePrice(title: String, price: Float) {
        titleLabel.text = title
        priceLabel.text = "\(price) \(Constants.CURRENCY_SYMBOL)"
        priceLabel.isHidden = (price <= 0) ? true : false
    }
    
    func setValue(isChecked: Bool) {
        radioButton.setValue(isChecked: isChecked)
    }
}
