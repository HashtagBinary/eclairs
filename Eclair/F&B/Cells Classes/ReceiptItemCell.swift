//
//  ReceiptItemCell.swift
//  Eclair
//
//  Created by Zohaib Naseer on 6/21/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class ReceiptItemCell: UITableViewCell {

    @IBOutlet weak var ItemNameTitlelbl: UILabel!
    @IBOutlet weak var rateTitlelbl: UILabel!
    @IBOutlet weak var qtyTitlelbl: UILabel!
    @IBOutlet weak var amountTitlelbl: UILabel!
    @IBOutlet weak var menuItemlbl: UILabel!
    @IBOutlet weak var ratelbl: UILabel!
    @IBOutlet weak var qtylbl: UILabel!
    @IBOutlet weak var amountlbl: UILabel!
    @IBOutlet weak var itemTitleHeight: NSLayoutConstraint!
    
    @IBOutlet weak var amountHeight: NSLayoutConstraint!
    @IBOutlet weak var qtyHeight: NSLayoutConstraint!
    @IBOutlet weak var rateHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
