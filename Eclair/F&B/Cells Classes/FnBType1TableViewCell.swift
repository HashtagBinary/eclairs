//
//  FnBType1TableViewCell.swift
//  Eclair
//
//  Created by Sohail on 09/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class FnBType1TableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var button: NumberButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
