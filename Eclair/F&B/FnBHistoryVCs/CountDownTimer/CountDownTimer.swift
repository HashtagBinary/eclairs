//
//  CountDownTimer.swift
//  TimerTesting
//
//  Created by iOS Indigo on 05/01/2021.
//

import UIKit
import CircleSlider

@objc class CountDownTimer: UIControl {
    
    var timer: Timer?
    //public var endTimeMillis: Int64 = 0
    private var timerValue = UILabel()
    var endTimes:Int64?
    var duration : Int64?
    
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.horizontal
        stackView.distribution  = UIStackView.Distribution.fillEqually
        stackView.alignment = UIStackView.Alignment.fill
        stackView.spacing   = 2
        return stackView
    }()
    
    var myview = UIView()
    private var circleVw = UIView()
    private var percentageLabel = UILabel()
    
    var circleSlider: CircleSlider!
  
    private var minValue: Float = 20
    private var maxValue: Float = 100
    private var sliderOptions: [CircleSliderOption] {
        return [
            CircleSliderOption.barColor(UIColor(red: 127 / 255, green: 244 / 255, blue: 23 / 255, alpha: 1)),
            CircleSliderOption.thumbColor(UIColor(red: 127 / 255, green: 185 / 255, blue: 204 / 255, alpha: 1)),
            CircleSliderOption.trackingColor(UIColor(red: 78 / 255, green: 136 / 255, blue: 185 / 255, alpha: 1)),
            CircleSliderOption.barWidth(5),
            CircleSliderOption.startAngle(270),
            CircleSliderOption.maxValue(self.maxValue),
            CircleSliderOption.minValue(self.minValue),
            CircleSliderOption.thumbImage(UIImage(named: "thumb_image_1"))
        ]
    }
    private var progressOptions: [CircleSliderOption] {
        return [
            .barColor(UIColor(red: 255 / 255, green: 190 / 255, blue: 190 / 255, alpha: 1)),
            .trackingColor(UIColor(red: 159 / 255, green: 0 / 255, blue: 0 / 255, alpha: 1)),
            .barWidth(5),
            .sliderEnabled(true)
        ]
    }
    
    
    
    @objc required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupViews()
        //buildCircleSlider()
    }
    
    func setupViews() {
        //self.removeFromSuperview()
        
        
        self.addSubview(self.stackView)
        self.timerValue.text = "00:00:00"
        self.myview.addSubview(self.timerValue)
        for v in stackView.subviews {
            v.removeFromSuperview()
        }
        
        
        stackView.addArrangedSubview(circleVw)
        stackView.addArrangedSubview(myview)
        addSubview(stackView)
    }
    private func buildCircleSlider() {
        
        
        //myview.backgroundColor = .brown
        //circleVw.backgroundColor = .blue
        circleSlider = CircleSlider(frame: circleVw.bounds, options: sliderOptions)
        circleSlider?.addTarget(self, action: #selector(valueChange(sender:)), for: .valueChanged)
        circleVw.addSubview(circleSlider!)
        circleSlider.addSubview(self.percentageLabel)
        circleSlider.delegate = self
        
    }
    @objc func valueChange(sender: CircleSlider) {
        print(sender.value)
    }
    
    func calculateTime() {
        let millisec = self.endTimes! - currentTimeMillis()
        
        if millisec <= 0
        {
            self.percentageLabel.text = "100%"
            self.circleSlider.value = 100
            return
        }
        
        var totalsec = millisec / 1000
        
        let day = Int(floor(Double(millisec) / 86400000.0))
        totalsec = totalsec % (3600 * 24)
        
        let hour = Int(totalsec / 3600 )
        totalsec = totalsec % 3600
        
        let min = Int(totalsec / 60)
        totalsec = totalsec % 60
        
        let sec = totalsec
        
        let seconds = String(format: "%02d", sec)
        let minutes = String(format: "%02d", min)
        let hours = String(format: "%02d", hour)
        let days = String(format: "%02d", day)
        //d1.updateText("\(days):\(hours):\(minutes):\(seconds)")
        //return "\(days) : \(hours) : \(minutes) : \(seconds)"
       // print("Event: \(day) \(hour) \(min) \(sec)")//
        var timeis = "\(days):\(hours):\(minutes):\(seconds)"
        if day <=  0
        {
            timeis = "\(hours):\(minutes):\(seconds)"
        }
        if hour <= 0
        {
            timeis = "\(minutes):\(seconds)"
        }
        
        self.timerValue.text = timeis
        let difference = (self.duration ?? 0) - (millisec)
        let percentage = ((difference * 100) / (duration ?? 0))
        //print("percentage = \(percentage)")
        self.circleSlider.value = Float(Int(percentage))
        self.percentageLabel.text = "\(Int(percentage))%"
    }
    
    public override func layoutSubviews() {
        stackView.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height)
        self.timerValue.frame = self.myview.frame
        self.buildCircleSlider()
        self.circleVw.addSubview(circleSlider)
        circleVw.frame = CGRect(x: stackView.arrangedSubviews[0].bounds.origin.x - 0, y: stackView.arrangedSubviews[0].bounds.origin.y - 0, width: 120, height: 120)
        self.timerValue.frame = CGRect(x: stackView.arrangedSubviews[1].bounds.origin.x - 0, y: stackView.arrangedSubviews[1].bounds.origin.y - 0, width: 120, height: 120)
        //circleVw.center = stackView.arrangedSubviews[0].center
        circleSlider.frame = self.circleVw.bounds
        self.percentageLabel.frame = circleSlider.bounds
        self.percentageLabel.textAlignment = .center
        self.percentageLabel.text = "0%"
        
    }
    public func setTime(duration:Int64,endTime:Int64)
    {
        
    }
    
    public func startTimer() {
        
        setupViews()
        
        timer?.invalidate()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            if self?.endTimes == nil {
                return
            }
            
            self!.calculateTime()
        }
    }
    
    func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
}
extension CountDownTimer: CircleSliderDelegate {
    func didStartChangeValue() {
        //delegateLabel.text = "didStartChangeValue"
    }
    
    func didReachedMaxValue() {
       // delegateLabel.text = "didReachedMaxValue"
    }
    
}
