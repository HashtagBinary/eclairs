//
//  FnbHistoryVC.swift
//  Eclair
//
//  Created by iOS Indigo on 03/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import CircleSlider
import FirebaseFirestore
import SwiftyJSON

struct historyData {
    var orderID : String
    var jsonData : [String : Any]
}
class FnbHistoryVC: UIViewController {
    
//    struct historyData {
//        var orderID : String
//        var jsonData : [String : Any]
//    }
    let db = Firestore.firestore()

    var dictArr = [historyData]()
    var selectedData : historyData?
    var menuItem = [[String:Any]]()
    var juiceItem = [[String:Any]]()
    var jsondataOfClosed = [Dictionary<String, AnyObject>]()
    var selectedClosedData = Dictionary<String, AnyObject>()
    var dataOf = "progress"
    let limit = 10
    var skip = 0
    
    
    @IBOutlet weak var tbleVw: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        self.dictArr.removeAll()
        self.tbleVw.reloadData()
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        db.collection(Constants.ServerAPI.FS_FNB_URL).whereField("ecode", isEqualTo: "\(Constants.ecode)").getDocuments
        { querySnapshot, error in
            // ...
            guard let snapshot = querySnapshot else {
                        print("Error fetching snapshots: \(error!)")
                        ActivityIndicatorSingleton.StopActivity(myView: self.view)
                        return
                    }
                    snapshot.documentChanges.forEach { diff in
                        let josnData = diff.document.data() as? [String : Any]
                        let orderid = diff.document.documentID
                        print("order id \(orderid)")
                        self.dictArr.append(historyData.init(orderID: diff.document.documentID, jsonData: josnData ?? ["":""]))
                    }
            print("dict array \(self.dictArr)")
            self.dictArr.reverse()
            self.tbleVw.reloadData()
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
}
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        db.collection(Constants.ServerAPI.FS_FNB_URL).whereField("ecode", isEqualTo: "\(Constants.ecode)")
            .addSnapshotListener { querySnapshot, error in
                // ...
                guard let snapshot = querySnapshot else {
                            print("Error fetching snapshots: \(error!)")
                            ActivityIndicatorSingleton.StopActivity(myView: self.view)
                            return
                        }
                        snapshot.documentChanges.forEach { diff in
                            let josnData = diff.document.data() as? [String : Any]
                            let orderid = diff.document.documentID
                            for i in 0..<self.dictArr.count
                            {
                                if self.dictArr[i].orderID == orderid
                                {
                                    self.dictArr[i].jsonData = josnData ?? ["":""]
                                }
                            }
                        }
                print("dict array \(self.dictArr)")
                self.tbleVw.reloadData()
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
    }
 }
    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        self.skip = 0
        self.jsondataOfClosed.removeAll()
        self.tbleVw.reloadData()
        self.refreshControl.endRefreshing()
        self.fetchClosedOrders()
    }
    @IBAction func segmentaction(_ sender: Any) {
        if self.segment.selectedSegmentIndex == 0
        {
            dataOf = "progress"
            self.dictArr.removeAll()
            self.jsondataOfClosed.removeAll()
            self.tbleVw.reloadData()
            ActivityIndicatorSingleton.StartActivity(myView: self.view)
            db.collection(Constants.ServerAPI.FS_FNB_URL).whereField("ecode", isEqualTo: "\(Constants.ecode)").getDocuments
            { querySnapshot, error in
                // ...
                guard let snapshot = querySnapshot else {
                            print("Error fetching snapshots: \(error!)")
                            return
                        }
                        snapshot.documentChanges.forEach { diff in
                            let josnData = diff.document.data() as? [String : Any]
                            let orderid = diff.document.documentID
                            self.dictArr.append(historyData.init(orderID: diff.document.documentID, jsonData: josnData ?? ["":""]))
                        }
                print("dict array \(self.dictArr)")
                self.tbleVw.reloadData()
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
    }
        }
        else
        {
            dataOf = "closed"
            self.dictArr.removeAll()
            self.tbleVw.reloadData()
            self.jsondataOfClosed.removeAll()
            self.skip = 0
            self.fetchClosedOrders()
        }
    }
    
    func fetchClosedOrders() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
            "take": self.limit,
            "skip":self.skip
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kFnBGetClosedOrders, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let json = try? JSON(data:data!)
            let orderjson = json?["orderJson"].array ?? []
            var jsondataTemp = [Dictionary<String, AnyObject>]()
            if json?["response"].bool ?? false == false
            {
                Utilities.showAlert("", message: json?["error"].string ?? "Response Error")
                return
            }
            for i in 0..<orderjson.count
            {
                let str = orderjson[i].string ?? ""
                var dictonary:NSDictionary?
                if let data = str.data(using: .utf8) {
                     do {
                        dictonary =  try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String:AnyObject] as NSDictionary?
                            if let myDictionary = dictonary
                              {
                                jsondataTemp.append(myDictionary as! Dictionary<String, AnyObject>)
                              }
                            } catch let error as NSError {
                            print(error)
                         }
                    }
              }
            
            self.jsondataOfClosed += jsondataTemp
            if self.jsondataOfClosed.count <= 0 && self.skip <= 0
            {
                Utilities.showAlert("", message: "No Data Found")
                return
            }
            else if jsondataTemp.count <= 0 && self.skip > 0
            {
                return
            }
            else
            {
                self.tbleVw.reloadData()
            }
            self.tbleVw.reloadData()
        }
    }

        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewDetail"
        {
            let vc = segue.destination as! ViewDetailVC
            vc.privousData = self.selectedData
        }
        else if segue.identifier == "showMenu"
        {
            let vc = segue.destination as! ViewMenuVC
            vc.previusData = self.menuItem
            vc.previusDataOfJuice = self.juiceItem
        }
        else if segue.identifier == "pdf"
        {
            let vc = segue.destination as! RecieptsTblVC
            vc.jsonData = self.selectedClosedData
        }
    }
}
extension FnbHistoryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataOf == "progress"
        {
            return dictArr.count
        }
        else
        {
            return jsondataOfClosed.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
      if dataOf == "progress"
      {
        if dictArr[indexPath.row].jsonData["status"] as? String == "n"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "onTheWay") as! StatusCell
            
            if segment.selectedSegmentIndex == 1
            {
                cell.receiptsVw.isHidden = false
            }
            else
            {
                cell.receiptsVw.isHidden = true
            }
            
            tableView.rowHeight = 280
            cell.statusLbl.text = "Pending"
            cell.orderIdlbl.text = dictArr[indexPath.row].orderID
            cell.addresslbl.text = dictArr[indexPath.row].jsonData["address"] as? String
            let date = dictArr[indexPath.row].jsonData["datetime_new"] as? FirebaseFirestore.Timestamp
            if date != nil
            {
                let converted = Utilities.convertDateFormate(forStringDate: "\(date!.dateValue())", currentFormate: "yyyy-MM-dd' 'HH:mm:ssZ", newFormate: Constants.DateFormate)
                cell.dateTimelbl.text = converted
            }
            else
            {
                cell.dateTimelbl.text = ""
            }
//            let price = dictArr[indexPath.row].jsonData["total"] as? Double ?? 0.0
//            let inDecimal = String(format: "%.2f", price)
//            cell.pricelbl.text = "\(inDecimal) SR"
            
            
            if let price = dictArr[indexPath.row].jsonData["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.receiptButton.tag = indexPath.row
            cell.receiptButton.addTarget(self, action: #selector(viewReceiptClick(sender:)), for: .touchUpInside)
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            return cell
        }
        else if dictArr[indexPath.row].jsonData["status"] as? String == "a"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "onTheWay") as! StatusCell
            
            if segment.selectedSegmentIndex == 1
            {
                cell.receiptsVw.isHidden = false
            }
            else
            {
                cell.receiptsVw.isHidden = true
            }
            
            tableView.rowHeight = 280
            cell.statusLbl.text = "Pending"
            cell.orderIdlbl.text = dictArr[indexPath.row].orderID
            cell.addresslbl.text = dictArr[indexPath.row].jsonData["address"] as? String
            let date = dictArr[indexPath.row].jsonData["datetime_new"] as? FirebaseFirestore.Timestamp
            if date != nil
            {
                let converted = Utilities.convertDateFormate(forStringDate: "\(date!.dateValue())", currentFormate: "yyyy-MM-dd' 'HH:mm:ssZ", newFormate: Constants.DateFormate)
                cell.dateTimelbl.text = converted
            }
            else
            {
                cell.dateTimelbl.text = ""
            }
//            let price = dictArr[indexPath.row].jsonData["total"] as? Double ?? 0.0
//            let inDecimal = String(format: "%.2f", price)
//            cell.pricelbl.text = "\(inDecimal) SR"
            if let price = dictArr[indexPath.row].jsonData["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.receiptButton.tag = indexPath.row
            cell.receiptButton.addTarget(self, action: #selector(viewReceiptClick(sender:)), for: .touchUpInside)
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            return cell
        }
        else if dictArr[indexPath.row].jsonData["status"] as? String == "d"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "onTheWay") as! StatusCell
            if segment.selectedSegmentIndex == 1
            {
                cell.receiptsVw.isHidden = false
            }
            else
            {
                cell.receiptsVw.isHidden = true
            }
            tableView.rowHeight = 280
            cell.statusLbl.text = "On the way"
            cell.orderIdlbl.text = dictArr[indexPath.row].orderID
            cell.addresslbl.text = dictArr[indexPath.row].jsonData["address"] as? String
            let date = dictArr[indexPath.row].jsonData["datetime_new"] as? FirebaseFirestore.Timestamp
            if date != nil
            {
                let converted = Utilities.convertDateFormate(forStringDate: "\(date!.dateValue())", currentFormate: "yyyy-MM-dd' 'HH:mm:ssZ", newFormate: Constants.DateFormate)
                cell.dateTimelbl.text = converted
            }
            else
            {
                cell.dateTimelbl.text = ""
            }
//            let price = dictArr[indexPath.row].jsonData["total"] as? Double ?? 0.0
//            let inDecimal = String(format: "%.2f", price)
//            cell.pricelbl.text = "\(inDecimal) SR"
            if let price = dictArr[indexPath.row].jsonData["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.receiptButton.tag = indexPath.row
            cell.receiptButton.addTarget(self, action: #selector(viewReceiptClick(sender:)), for: .touchUpInside)
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            return cell
        }
        else if dictArr[indexPath.row].jsonData["status"] as? String == "p"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "timer") as! TimerCell
            tableView.rowHeight = 404
            cell.statusLbl.text = "Preparing"
            cell.orderIdLBl.text = dictArr[indexPath.row].orderID
            cell.addresslbl.text = dictArr[indexPath.row].jsonData["address"] as? String
            let date = dictArr[indexPath.row].jsonData["datetime_new"] as? FirebaseFirestore.Timestamp
            if date != nil
            {
                let converted = Utilities.convertDateFormate(forStringDate: "\(date!.dateValue())", currentFormate: "yyyy-MM-dd' 'HH:mm:ssZ", newFormate: Constants.DateFormate)
                cell.dateNTimelbl.text = converted
            }
            else
            {
                cell.dateNTimelbl.text = ""
            }
//            let price = dictArr[indexPath.row].jsonData["total"] as? Double ?? 0.0
//            let inDecimal = String(format: "%.2f", price)
//            cell.pricelbl.text = "\(inDecimal) SR"
            if let price = dictArr[indexPath.row].jsonData["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            let preparingDate = dictArr[indexPath.row].jsonData["datetime_preparing"] as? FirebaseFirestore.Timestamp
            let duration = dictArr[indexPath.row].jsonData["duration"] as? Int ?? 0
            let inMilies = preparingDate?.dateValue()
            let total = Int64(inMilies?.toMillis() ?? 0) + Int64(duration)
            cell.countDownVw.isHidden = false
            cell.countDownVw.endTimes = Int64(total)
            cell.countDownVw.duration = Int64(duration)
            cell.countDownVw.startTimer()
            return cell
        }
        else if dictArr[indexPath.row].jsonData["status"] as? String == "r"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "timer") as! TimerCell
            tableView.rowHeight = 404
            cell.statusLbl.text = "Ready"
            cell.orderIdLBl.text = dictArr[indexPath.row].orderID
            cell.addresslbl.text = dictArr[indexPath.row].jsonData["address"] as? String
            let date = dictArr[indexPath.row].jsonData["datetime_new"] as? FirebaseFirestore.Timestamp
            if date != nil
            {
                let converted = Utilities.convertDateFormate(forStringDate: "\(date!.dateValue())", currentFormate: "yyyy-MM-dd' 'HH:mm:ssZ", newFormate: Constants.DateFormate)
                cell.dateNTimelbl.text = converted
            }
            else
            {
                cell.dateNTimelbl.text = ""
            }
//            let price = dictArr[indexPath.row].jsonData["total"] as? Double ?? 0.0
//            let inDecimal = String(format: "%.2f", price)
//            cell.pricelbl.text = "\(inDecimal) SR"
            if let price = dictArr[indexPath.row].jsonData["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            cell.countDownVw.stackView.arrangedSubviews[1].isHidden = true
            cell.countDownVw.endTimes = Int64(0)
            cell.countDownVw.duration = Int64(0)
            cell.countDownVw.startTimer()
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "onTheWay") as! StatusCell
            if segment.selectedSegmentIndex == 1
            {
                cell.receiptsVw.isHidden = false
            }
            else
            {
                cell.receiptsVw.isHidden = true
            }
            tableView.rowHeight = 280
            cell.statusLbl.text = "Not Defined"
            cell.orderIdlbl.text = dictArr[indexPath.row].orderID
            cell.addresslbl.text = dictArr[indexPath.row].jsonData["address"] as? String
            let date = dictArr[indexPath.row].jsonData["datetime_new"] as? FirebaseFirestore.Timestamp
            if date != nil
            {
                let converted = Utilities.convertDateFormate(forStringDate: "\(date!.dateValue())", currentFormate: "yyyy-MM-dd' 'HH:mm:ssZ", newFormate: Constants.DateFormate)
                cell.dateTimelbl.text = converted
            }
            else
            {
                cell.dateTimelbl.text = ""
            }
//            let price = dictArr[indexPath.row].jsonData["total"] as? Double ?? 0.0
//            let inDecimal = String(format: "%.2f", price)
//            cell.pricelbl.text = "\(inDecimal) SR"
            if let price = dictArr[indexPath.row].jsonData["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.receiptButton.tag = indexPath.row
            cell.receiptButton.addTarget(self, action: #selector(viewReceiptClick(sender:)), for: .touchUpInside)
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            return cell
        }
      }
      else
      {
        if jsondataOfClosed[indexPath.row]["status"] as? String == "e"
        {
            if #available(iOS 10.0, *) {
                tableView.refreshControl = refreshControl
            } else {
                tableView.addSubview(refreshControl)
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "onTheWay") as! StatusCell
            if segment.selectedSegmentIndex == 1
            {
                cell.receiptsVw.isHidden = false
            }
            else
            {
                cell.receiptsVw.isHidden = true
            }
            tableView.rowHeight = 280
            cell.statusLbl.text = "Delivered"
            cell.orderIdlbl.text = jsondataOfClosed[indexPath.row]["order_id"] as? String ?? ""
            cell.addresslbl.text = jsondataOfClosed[indexPath.row]["address"] as? String
            let date = jsondataOfClosed[indexPath.row]["datetime_new"] as? String ?? ""
            if date.isEmpty == false
            {
                let converted = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd' 'HH:mm:ss.SSS", newFormate: Constants.DateFormate)
                cell.dateTimelbl.text = converted
            }
            else
            {
                cell.dateTimelbl.text = ""
            }
            if let price = jsondataOfClosed[indexPath.row]["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            
            
            
            
            
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.receiptButton.tag = indexPath.row
            cell.receiptButton.addTarget(self, action: #selector(viewReceiptClick(sender:)), for: .touchUpInside)
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "onTheWay") as! StatusCell
            if segment.selectedSegmentIndex == 1
            {
                cell.receiptsVw.isHidden = false
            }
            else
            {
                cell.receiptsVw.isHidden = true
            }
            tableView.rowHeight = 280
            cell.statusLbl.text = "Not Defined"
            cell.orderIdlbl.text = jsondataOfClosed[indexPath.row]["order_id"] as? String ?? ""
            cell.addresslbl.text = jsondataOfClosed[indexPath.row]["address"] as? String
            let date = jsondataOfClosed[indexPath.row]["datetime_new"] as? String ?? ""
            if date.isEmpty == false
            {
                let converted = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd' 'HH:mm:ss.SSS", newFormate: Constants.DateFormate)
                cell.dateTimelbl.text = converted
            }
            else
            {
                cell.dateTimelbl.text = ""
            }
            if let price = jsondataOfClosed[indexPath.row]["total"]
            {
                let inDecimal = String(format: "%.2f", "\(price)".toDouble() ?? 0.0)
                cell.pricelbl.text = "\(inDecimal) SR"
            }
            else
            {
                cell.pricelbl.text = "-"
            }
            cell.viewMenuButton.tag = indexPath.row
            cell.viewDetailButton.tag = indexPath.row
            cell.receiptButton.tag = indexPath.row
            cell.receiptButton.addTarget(self, action: #selector(viewReceiptClick(sender:)), for: .touchUpInside)
            cell.viewMenuButton.addTarget(self, action: #selector(viewMenuClick(sender:)), for: .touchUpInside)
            cell.viewDetailButton.addTarget(self, action: #selector(viewDetailClick(sender:)), for: .touchUpInside)
            return cell
        }
      }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if segment.selectedSegmentIndex == 1
        {
            if indexPath.row == self.jsondataOfClosed.count - 1 { //
                    skip += limit
                    self.fetchClosedOrders()
            }
        }
    }
    @objc func viewMenuClick(sender: UIButton){
        if dataOf == "progress"
        {
            self.menuItem = self.dictArr[sender.tag].jsonData["menuitems"] as! [[String : Any]]
            self.juiceItem = self.dictArr[sender.tag].jsonData["juiceitems"] as! [[String : Any]]
        }
        else
        {
            self.menuItem = self.jsondataOfClosed[sender.tag]["menuitems"] as! [[String : Any]]
            self.juiceItem = self.jsondataOfClosed[sender.tag]["juiceitems"] as! [[String : Any]]
        }
        self.performSegue(withIdentifier: "showMenu", sender: self)
        
    }
    @objc func viewDetailClick(sender: UIButton){
    
        if dataOf == "progress"
        {
            self.selectedData = self.dictArr[sender.tag]
        }
        else
        {
            let history = historyData.init(orderID: self.jsondataOfClosed[sender.tag]["order_id"] as? String ?? "", jsonData: self.jsondataOfClosed[sender.tag] as [String : Any])
            self.selectedData = history
        }
        self.performSegue(withIdentifier: "viewDetail", sender: self)
    }
    @objc func viewReceiptClick(sender: UIButton){
        self.selectedClosedData = self.jsondataOfClosed[sender.tag]
        self.getfnbMenuITems()
    }
    //MARK:- API for get all items list
    
    func getfnbMenuITems()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.getCallwithoutParameters(serviceName: Constants.ServerAPI.kGetFnBMenuItems)
        { (data:Data?, error:NSError?) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            let model = try? JSONDecoder().decode(FnBMenuDepartments.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: "Data Not Found")
                return
            }
            if (model != nil) {
                departmentsModel = model!
                self.performSegue(withIdentifier: "pdf", sender: self)
            }
        }
    }
}

