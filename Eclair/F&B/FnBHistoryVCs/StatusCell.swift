//
//  StatusCell.swift
//  Eclair
//
//  Created by iOS Indigo on 03/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class StatusCell: UITableViewCell {

    @IBOutlet weak var viewMenuVw: UIView!
    @IBOutlet weak var viewDetailVw: UIView!
    @IBOutlet weak var statusVw: UIView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var orderIdlbl: UILabel!
    @IBOutlet weak var addresslbl: UILabel!
    @IBOutlet weak var dateTimelbl: UILabel!
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var viewMenuButton: UIButton!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var receiptsVw: UIView!
    @IBOutlet weak var receiptButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        statusVw.circleCorner = true
        viewMenuVw.cornerRadius = 5
        viewDetailVw.cornerRadius = 5
        receiptsVw.cornerRadius = 5
        receiptsVw.borderColor = UIColor.init(named: "ColorTextDark")
        receiptsVw.borderWidth = 1
        statusVw.borderColor = UIColor.init(named: "ColorTextDark")
        statusVw.borderWidth = 1
        viewMenuVw.borderColor = UIColor.init(named: "ColorTextDark")
        viewMenuVw.borderWidth = 1
        viewDetailVw.borderColor = UIColor.init(named: "ColorTextDark")
        viewDetailVw.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
