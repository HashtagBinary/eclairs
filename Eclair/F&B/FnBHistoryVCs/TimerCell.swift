//
//  TimerCell.swift
//  Eclair
//
//  Created by iOS Indigo on 03/01/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import CircleSlider


class TimerCell: UITableViewCell {

    
    @IBOutlet weak var countDownVw: CountDownTimer!
    @IBOutlet weak var viewMenuVw: UIView!
    @IBOutlet weak var viewDetailVw: UIView!
    @IBOutlet weak var statusVw: UIView!
    @IBOutlet weak var progressVw: UIView!
    @IBOutlet weak var orderIdLBl: UILabel!
    @IBOutlet weak var addresslbl: UILabel!
    @IBOutlet weak var dateNTimelbl: UILabel!
    @IBOutlet weak var viewMenuButton: UIButton!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var pricelbl: UILabel!
    var limit = 50.0
    var counter = 0.0
    
    //MARK:- countDown Timer
    

    
    //MARK:- circle slider working
    
//    var circleSlider: CircleSlider!
//   // private var timer: Timer?
//    private var minValue: Float = 20
//    private var maxValue: Float = 100
//    private var sliderOptions: [CircleSliderOption] {
//        return [
//            CircleSliderOption.barColor(UIColor(red: 127 / 255, green: 244 / 255, blue: 23 / 255, alpha: 1)),
//            CircleSliderOption.thumbColor(UIColor(red: 127 / 255, green: 185 / 255, blue: 204 / 255, alpha: 1)),
//            CircleSliderOption.trackingColor(UIColor(red: 78 / 255, green: 136 / 255, blue: 185 / 255, alpha: 1)),
//            CircleSliderOption.barWidth(5),
//            CircleSliderOption.startAngle(0),
//            CircleSliderOption.maxValue(self.maxValue),
//            CircleSliderOption.minValue(self.minValue),
//            CircleSliderOption.thumbImage(UIImage(named: "thumb_image_1"))
//        ]
//    }
//    private var progressOptions: [CircleSliderOption] {
//        return [
//            .barColor(UIColor(red: 255 / 255, green: 190 / 255, blue: 190 / 255, alpha: 1)),
//            .trackingColor(UIColor(red: 159 / 255, green: 0 / 255, blue: 0 / 255, alpha: 1)),
//            .barWidth(5),
//            .sliderEnabled(true)
//        ]
//    }
    
    //MARK:- circle slider work ending
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //buildCircleSlider(uiview: self.progressVw)
        
        
        statusVw.circleCorner = true
        viewMenuVw.cornerRadius = 5
        viewDetailVw.cornerRadius = 5
        
        statusVw.borderColor = UIColor.init(named: "ColorTextDark")
        statusVw.borderWidth = 1
        viewMenuVw.borderColor = UIColor.init(named: "ColorTextDark")
        viewMenuVw.borderWidth = 1
        viewDetailVw.borderColor = UIColor.init(named: "ColorTextDark")
        viewDetailVw.borderWidth = 1
        
        
    }
//    @objc func fire(timer _: Timer) {
//        circleSlider.value += 0.5
//        counter += 0.5
//        if counter == limit
//        {
//            timer?.invalidate()
//        }
//        //changeButtonImage(circleSlider.status)
//    }
    
//    private func buildCircleSlider(uiview : UIView) {
//        circleSlider = CircleSlider(frame: uiview.bounds, options: sliderOptions)
//        circleSlider?.addTarget(self, action: #selector(valueChange(sender:)), for: .valueChanged)
//        uiview.addSubview(circleSlider!)
//        circleSlider.delegate = self
//    }
//    @objc func valueChange(sender: CircleSlider) {
//        print(sender.value)
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- countdownSettings
}
    
//extension TimerCell: CircleSliderDelegate {
//    func didStartChangeValue() {
//        //delegateLabel.text = "didStartChangeValue"
//    }
//
//    func didReachedMaxValue() {
//       // delegateLabel.text = "didReachedMaxValue"
//    }
//
//}
