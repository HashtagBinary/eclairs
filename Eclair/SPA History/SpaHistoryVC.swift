//
//  SpaHistoryVC.swift
//  Eclair
//
//  Created by Indigo on 13/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SpaHistoryVC: UIViewController {

    @IBOutlet weak var tblvw: UITableView!
    var historyList = [SpaHistoryModelList]()
    var selectedValue : SpaHistoryModelList?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        HistoryList()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail"
        {
            let vc = segue.destination as! HistoryDetailVC
            vc.selectedValue = self.selectedValue
        }
    }
    //MARK:- API
    
    func HistoryList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params : Parameters = [
            "ecode":Constants.ecode,
        "skip":"0",
        "take":"50"
        ]
        
        print("spa history \(Constants.ServerAPI.kSpaGetHistory)")
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kSpaGetHistory, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(SpaHistoryModel.self, from: data!)

             if(model?.response == true)
             {
                self.historyList = model?.list ?? []
                self.tblvw.reloadData()
                if self.historyList.isEmpty == true
                {
                    Utilities.showAlert("", message: "No History Found")
                    return
                }
             }
             else
             {
                 Utilities.showAlert("", message: "Error: \(model?.message ?? "server Error")")
             }
        }
    }
}
extension SpaHistoryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SpaHistoryCell
        cell.titlelbl.text = "Service Title"
        cell.timelbl.text = historyList[indexPath.row].timeslot
        cell.datelbl.text = historyList[indexPath.row].date
        cell.amountlbl.text = "\(historyList[indexPath.row].total ?? 0) SR"
        cell.viewDetailButton.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        cell.viewDetailButton.tag = indexPath.row
        cell.viewDetailButton.borderColor = .darkText
        cell.viewDetailButton.borderWidth = 1
        cell.viewDetailButton.circleCorner = true
        return cell
    }
    
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        self.selectedValue = self.historyList[buttonTag]
        self.performSegue(withIdentifier: "detail", sender: self)
    }
}
