//
//  HistoryDetailVC.swift
//  Eclair
//
//  Created by Indigo on 13/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class HistoryDetailVC: UIViewController {

    @IBOutlet weak var spaServiceType: UILabel!
    @IBOutlet weak var spaCategorylbl: UILabel!
    @IBOutlet weak var subCategorylbl: UILabel!
    @IBOutlet weak var itemlbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var additionalTimeLbl: UILabel!
    @IBOutlet weak var Description: UILabel!
    @IBOutlet weak var totalPricelbl: UILabel!
    
    var selectedValue : SpaHistoryModelList?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupVw()
    }
    func setupVw()
    {
        itemlbl.text = selectedValue?.items?.itemname
        datelbl.text = selectedValue?.date
        timelbl.text = selectedValue?.timeslot
        additionalTimeLbl.text = selectedValue?.additionaltime
        Description.text = selectedValue?.items?.description
        totalPricelbl.text = "\(selectedValue?.total ?? 0) SR"
        spaServiceType.text = selectedValue?.items?.type
        spaCategorylbl.text = selectedValue?.items?.catname
        subCategorylbl.text = selectedValue?.items?.subcatname
    }
}
