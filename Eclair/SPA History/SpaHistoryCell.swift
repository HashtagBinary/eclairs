//
//  SpaHistoryCell.swift
//  Eclair
//
//  Created by Indigo on 13/02/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit

class SpaHistoryCell: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var amountlbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
