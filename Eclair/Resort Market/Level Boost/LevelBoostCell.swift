//
//  LevelBoostCell.swift
//  Eclair
//
//  Created by iOS Indigo on 16/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class LevelBoostCell: UITableViewCell {

    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var amountButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
