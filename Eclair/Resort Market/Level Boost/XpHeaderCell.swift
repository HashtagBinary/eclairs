//
//  XpHeaderCell.swift
//  Eclair
//
//  Created by iOS Indigo on 17/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class XpHeaderCell: UITableViewCell {

    @IBOutlet weak var requestTokenbtn: UIButton!
    @IBOutlet weak var byTokenBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
