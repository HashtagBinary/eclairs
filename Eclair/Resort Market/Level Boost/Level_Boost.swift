//
//  Level_Boost.swift
//  Eclair
//
//  Created by iOS Indigo on 16/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class Level_Boost: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var xpBoostList = [XpBoostModelXpBoost]()
    var superTokensList = [XpBoostModelToken]()
    var requiredTokensList = [XpBoostModelTokensList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        getXpBoost()
    }
    func openAlert(xpamount:String,xpBoostID : String)
    {
        let alert = UIAlertController(title: "", message: "Are you sure you want to Boost \(xpamount) XP?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.buyXpBoost(xpBoostId: xpBoostID)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- API Calling
    
    func buyXpBoost(xpBoostId : String)
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params : Parameters = ["ecode":Constants.ecode,
                                   "purchaseType":"1",
                                   "xpBoostId":xpBoostId]
        print("Params \(params)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kBuyXPByEcode, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let response = json?["response"].bool ?? false
            if response == true
            {
                Utilities.showAlert("", message: json?["message"].string ?? "Done")
            }
            else {
                Utilities.showAlert("", message: "\(json?["error"].string ?? "")")
            }
      }
}
    
    
    func getXpBoost()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        let params : Parameters = ["ecode":Constants.ecode]
        print("Params \(params)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kGetXpBoost, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(XpBoostModel.self, from: data ?? Data())
            if model?.response == false
            {
                Utilities.showAlert("", message: model?.message ?? "Data Not Found")
                return
            }
            self.xpBoostList = model?.xpBoost ?? []
            self.superTokensList = model?.superTokenRequirments?.tokens ?? []
            self.requiredTokensList = model?.tokensList ?? []
            self.tableview.reloadData()
    }
}
}
extension Level_Boost : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return xpBoostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "LevelBoostCell") as! LevelBoostCell
        cell.labelAmount.text = "\(xpBoostList[indexPath.row].rate ?? 0) Crystal"
        cell.amountButton.setTitle("Buy \(xpBoostList[indexPath.row].xps ?? 0) XP", for: .normal)
        cell.amountButton.cornerRadius = 10
        cell.amountButton.tag = indexPath.row
        cell.amountButton.addTarget(self, action: #selector(buy(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func buy(sender: UIButton){
        openAlert(xpamount: "\(xpBoostList[sender.tag].xps ?? 0)", xpBoostID: "\(xpBoostList[sender.tag].xpBoostId ?? 0)")
    }
    @objc func byToken(sender: UIButton){
        self.performSegue(withIdentifier: "required", sender: self)
    }
}
