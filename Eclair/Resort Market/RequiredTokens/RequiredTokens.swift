//
//  RequiredTokens.swift
//  Eclair
//
//  Created by iOS Indigo on 17/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import SwiftyJSON

class RequiredTokens: UIViewController {

    @IBOutlet weak var tblVw: UITableView!
    
    var tokens = [GetTokensList]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tokenList()
    }
    //MARK:- API Calling
    func tokenList()
    {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let params :Parameters = [
            "ecode" : Constants.ecode
        ]
        print("url \(Constants.ServerAPI.kGetTokens)")
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kGetTokens, parameters: params) { (data:Data?, error:NSError?) in
            if error != nil {
                   print(error?.localizedDescription ?? "Error")
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            let json = try? JSON(data: data!)
            print("Json \(json)")
            let decoder = JSONDecoder()
            let model = try? decoder.decode(GetTokens.self, from: data!)
     
             if(model?.response == true)
             {
                self.tokens = model?.tokensList ?? []
                self.tblVw.reloadData()
             }
             else
             {
                 Utilities.showAlert("", message: "Error: \(model?.error ?? "server Error")")
             }
        }
   }
}
extension RequiredTokens : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tokens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! RequiredTokenCell
        
        cell.img.image = UIImage.init(named: "image_loading")
        let url = tokens[indexPath.row].imageUrl ?? "image_loading"
        cell.img.downloadImage(with: url)
        { (data:UIImage?, error:NSError?) in
            cell.img.image = data
        }
        cell.namelbl.text = tokens[indexPath.row].tokenType ?? ""
        cell.tokenType.text = "\(tokens[indexPath.row].quantity ?? 0)"
        return cell
    }
}
