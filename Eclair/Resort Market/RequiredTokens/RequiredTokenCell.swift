//
//  RequiredTokenCell.swift
//  Eclair
//
//  Created by iOS Indigo on 17/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class RequiredTokenCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var tokenType: UILabel!
    @IBOutlet weak var quantityVw: UIView!
    @IBOutlet weak var containerVw: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerVw.layer.cornerRadius = 12
        containerVw.clipsToBounds = true
        containerVw.layer.masksToBounds = false
        containerVw.layer.shadowRadius = 3
        containerVw.layer.shadowOpacity = 0.6
        containerVw.layer.shadowOffset = CGSize.zero
        containerVw.layer.shadowColor = UIColor.lightGray.cgColor
        quantityVw.roundCorners([.topRight, .bottomRight], radius: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
