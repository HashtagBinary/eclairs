//

//
//  Created by Eclair on 9/11/20.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire
import SwiftyJSON
import NavigationDrawer


class CustomCell: UITableViewCell , UIScrollViewDelegate{
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var backimg: UIImageView!
    @IBOutlet weak var leadingEdge: NSLayoutConstraint!
    
}

class NavigationDrawerController: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var stackVw: UIStackView!
    @IBOutlet weak var logoutStack: UIStackView!
    @IBOutlet weak var userImgInStack: UIImageView!
    @IBOutlet weak var userNameStack: UILabel!
    @IBOutlet weak var guidelinesBox: UIView!
    
    var interactor:Interactor? = nil
    var arrayHeader = [0,1,0,0,0,0,0,1]
    var headerValues = ["","","General Services","T/M Services","Activity History","Achievements & Rewards","Resort Market","",""]
    var headerImgs = ["","","resort-services","TMServices","recent-activity","achievements","resortMarket","",""]
//    var menuItmesImgs = [
//        [""],
//        ["check-in"],
//        ["f&b","spa","water-activity","boutique","sunbed","events","navLost_found"],
//        ["sub-tm-list","guestList","request-form","guestRequest","guestReinvitation","maintenance"],
//        ["f&b","spa","water-activity","boutique","voucher","events","guestRequest","guestReinvitation","maintenance","navLost_found"],
//        ["Leaderboard","Level","quest","Gong","Inventory","Title","Token"],
//        ["crystal","auction-house","voucher","XP","Garden"],
//        ["settings"],
//        ["help"]
//    ]
//
//    var menuItems = [
//        [""],
//        ["Check In/Out QR"],
//        ["F&B","SPA","Marina","Boutique","Sunbed","Event Reservation","Lost & Found"],
//        ["Sub Tenant List","Guest List","Sub Tenant Form", "Guest Form","Invite Guest","Villa Maintenance"],
//        ["F&B","SPA","Marina","Boutique",
//         "Vouchers","Event Reservation","Guest Form","Invite Guest","Villa Maintenance","Lost & Found"],
//        ["Leaderboard","Level","Quest","Platinum Gong","Inventory","Titles","Tokens"],
//        ["Crystal","Auction House","Vouchers","XP Boost","Garden"],
//        ["Settings"],
//        ["Help"]
//    ]
    
    
    var menuItmesImgs = [
        [""],
        ["check-in"],
        ["f&b","spa","water-activity","boutique","sunbed","events","navLost_found"],
        ["sub-tm-list","guestList","request-form","guestRequest","guestReinvitation","maintenance"],
        ["f&b","spa","water-activity","boutique",
         //"voucher",
         "events","maintenance","navLost_found"],
        ["Leaderboard","Level","Title","Token","trader"],
        ["crystal","auction-house",
         //"voucher",
         "XP"],
        ["settings"],
        ["help"]
    ]
    
    var menuItems = [
        [""],
        ["Check In/Out QR"],
        ["F&B","SPA","Marina","Boutique","Sunbed","Event Reservation","Lost & Found"],
        ["Sub Tenant List","Guest List","Sub Tenant Form", "Guest Form","Invite Guest","Villa Maintenance"],
        ["F&B","SPA","Marina","Boutique",
         //"Vouchers",
         "Event Reservation","Villa Maintenance","Lost & Found"],
        ["Leaderboard","Level","Titles","Tokens","The Trader"],
        ["Crystal","Auction House",
         //"Vouchers",
         "XP Boost"],
        ["Settings"],
        ["Help"]
    ]

    var memberTypes = ""
    var memberFlag = true
    var dropDownSelection = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if Constants.ecode.isEmpty == false
        {
            let memberType = Constants.ecode.prefix(1)
            self.memberTypes = String(memberType)
            switch memberType {
            case "m":
                print("member")
                
                if Constants.kIsMainMember == true
                {
                     menuItmesImgs = [
                        [""],
                        ["check-in"],
                        ["f&b","spa","water-activity","boutique","sunbed","events","navLost_found"],
                        ["sub-tm-list","guestList","request-form","guestRequest","guestReinvitation"],
                        ["f&b","spa","water-activity","boutique",
                         //"voucher",
                         "request-form","guestRequest","guestReinvitation","events","navLost_found"],
                        ["Leaderboard","Level","Title","Token","trader"],
                        ["crystal","auction-house",
                         //"voucher",
                         "XP"],
                        ["settings"],
                        ["help"]
                    ]
                    
                    menuItems = [
                        [""],
                        ["Check In/Out QR"],
                        ["F&B","SPA","Marina","Boutique","Sunbed","Event Reservation","Lost & Found"],
                        ["Sub Member List","Guest List","Sub Member Form", "Guest Form","Invite Guest"],
                        ["F&B","SPA","Marina","Boutique",
                         //"Vouchers",
                         "Sub Member Form","Guest Form","Invite Guest","Event Reservation","Lost & Found"],
                        ["Leaderboard","Level","Titles","Tokens","The Trader"],
                        ["Crystal","Auction House",
                         //"Vouchers",
                         "XP Boost"],
                        ["Settings"],
                        ["Help"]
                    ]
                }
                else
                {
                     menuItmesImgs = [
                        [""],
                        ["check-in"],
                        ["f&b","spa","water-activity","boutique","sunbed","events","navLost_found"],
                        ["guestList","guestRequest","guestReinvitation","maintenance"],
                        ["f&b","spa","water-activity","boutique","voucher","guestRequest","guestReinvitation","events","maintenance","navLost_found"],
                        ["Leaderboard","Level","Title","Token","trader"],
                        ["crystal","auction-house",
                         //"voucher",
                         "XP"],
                        ["settings"],
                        ["help"]
                    ]
                    
                     menuItems = [
                        [""],
                        ["Check In/Out QR"],
                        ["F&B","SPA","Marina","Boutique","Sunbed","Event Reservation","Lost & Found"],
                        ["Guest List", "Guest Form","Invite Guest","Villa Maintenance"],
                        ["F&B","SPA","Marina","Boutique",
                         //"Vouchers",
                         "Guest Form","Invite Guest","Event Reservation","Villa Maintenance","Lost & Found"],
                        ["Leaderboard","Level","Titles","Tokens","The Trader"],
                        ["Crystal","Auction House",
                         //"Vouchers",
                         "XP Boost"],
                        ["Settings"],
                        ["Help"]
                    ]
                }
                
            case "g":
                print("Guest")
                
                self.arrayHeader = [0,1,0,0,0,0,1]
                self.headerValues = ["","","General Services","Activity History","Achievements & Rewards","Resort Market","",""]
                self.headerImgs = ["","","resort-services","recent-activity","achievements","resortMarket","",""]
                
                
                 menuItmesImgs = [
                    [""],
                    ["check-in"],
                    ["f&b","spa","water-activity","boutique","sunbed","navLost_found"],
                    ["f&b","spa","water-activity","boutique",
                     //"voucher",
                     "navLost_found"],
                    ["Leaderboard","Level","Title","Token","trader"],
                    ["crystal","auction-house",
                     //"voucher",
                     "XP"],
                    ["settings"]//,
                    //["help"]
                ]
                
                menuItems = [
                    [""],
                    ["Check In/Out QR"],
                    ["F&B","SPA","Marina","Boutique","Sunbed","Lost & Found"],
                    ["F&B","SPA","Marina","Boutique",
                     //"Vouchers",
                     "Lost & Found"],
                    ["Leaderboard","Level","Titles","Tokens","The Trader"],
                    ["Crystal","Auction House",
                     //"Vouchers",
                     "XP Boost"],
                    ["Settings"]//
                    //,
                    //["Help"]
                ]
                
                
            case "t":
                print("Tenant")
                
                if Constants.kIsMainMember == true
                {
                    menuItmesImgs = [
                        [""],
                        ["check-in"],
                        ["f&b","spa","water-activity","boutique","sunbed","events","navLost_found"],
                        ["sub-tm-list","guestList","request-form","guestRequest","guestReinvitation","maintenance"],
                        ["f&b","spa","water-activity","boutique",
                         //"voucher",
                         "request-form","guestRequest","guestReinvitation","events","maintenance","navLost_found"],
                        ["Leaderboard","Level","Title","Token","trader"],
                        ["crystal","auction-house",
                         //"voucher",
                         "XP"],
                        ["settings"],
                        ["help"]
                    ]
                    
                    menuItems = [
                        [""],
                        ["Check In/Out QR"],
                        ["F&B","SPA","Marina","Boutique","Sunbed","Event Reservation","Lost & Found"],
                        ["Sub Tenant List","Guest List","Sub Tenant Form", "Guest Form","Invite Guest","Villa Maintenance"],
                        ["F&B","SPA","Marina","Boutique",
                         //"Vouchers",
                         "Sub Tenant Form","Guest Form","Invite Guest","Event Reservation","Villa Maintenance","Lost & Found"],
                        ["Leaderboard","Level","Titles","Tokens","The Trader"],
                        ["Crystal","Auction House",
                         //"Vouchers",
                         "XP Boost"],
                        ["Settings"],
                        ["Help"]
                    ]
                }
                else
                {
                    menuItmesImgs = [
                        [""],
                        ["check-in"],
                        ["f&b","spa","water-activity","boutique","sunbed","navLost_found"],
                        ["guestList","guestRequest","guestReinvitation","maintenance"],
                        ["f&b","spa","water-activity","boutique",
                         //"voucher",
                         "guestRequest","guestReinvitation","maintenance","navLost_found"],
                        ["Leaderboard","Level","Title","Token","trader"],
                        ["crystal","auction-house",
                         //"voucher",
                         "XP"],
                        ["settings"],
                        ["help"]
                    ]
                    
                    menuItems = [
                        [""],
                        ["Check In/Out QR"],
                        ["F&B","SPA","Marina","Boutique","Sunbed","Lost & Found"],
                        ["Guest List", "Guest Form","Invite Guest","Villa Maintenance"],
                        ["F&B","SPA","Marina","Boutique",
                         //"Vouchers",
                         "Guest Form","Invite Guest","Villa Maintenance","Lost & Found"],
                        ["Leaderboard","Level","Titles","Tokens","The Trader"],
                        ["Crystal","Auction House",
                         //"Vouchers",
                         "XP Boost"],
                        ["Settings"],
                        ["Help"]
                    ]
                }
                
            default:
                print("default")
            }
        }
       
        self.tableview.reloadData()
        
        guidelinesBox.isHidden = true
        
        stackVw.arrangedSubviews[1].isHidden = true
        stackVw.arrangedSubviews[0].isHidden = false
        let headerNib = UINib.init(nibName: "HeaderVwFile", bundle: Bundle.main)
        tableview.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderVwFile")
        
        self.logoutStack.addShadow(offset: CGSize(width: 0, height: -5), color: UIColor.black .withAlphaComponent(0.5), radius: 0.0, opacity: 0.5)
        self.userImgInStack.circleCorner = true
        
        let headerNibTbl = UINib.init(nibName: "SideMenuHeader", bundle: Bundle.main)
        tableview.register(headerNibTbl, forHeaderFooterViewReuseIdentifier: "SideMenuHeader")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleLogout(_:)))
        self.logoutStack.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.profilePicUpdate(notification:)), name: NSNotification.Name(rawValue: "profilePicUpdate"), object: nil)
        
    }
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func profilePicUpdate(notification: NSNotification)
    {
        tableview.reloadData()
        
    }
    
    @objc func handleLogout(_ sender: UITapGestureRecognizer? = nil) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            ActivityIndicatorSingleton.StartActivity(myView: self.view)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                ActivityIndicatorSingleton.StopActivity(myView: self.view)
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "RootNavigationController") as! UINavigationController
                UIApplication.shared.keyWindow?.rootViewController = viewController;
                
                self.dismiss(animated: false, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:- tableview delegate methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0
        {
            let tblHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SideMenuHeader") as? SideMenuHeader
            tblHeader?.userTypelbl.text = Utilities.displayMemberType(ecode: Constants.ecode)
            tblHeader?.userNamelbl.text = Constants.ecode
            self.userNameStack.text = UserDefaults.standard.string(forKey: "fullName") ?? ""
            tblHeader?.profileImg.circleCorner = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            tblHeader?.profileImg.isUserInteractionEnabled = true
            tblHeader?.profileImg.addGestureRecognizer(tapGestureRecognizer)
            tblHeader?.dropDownButton.addTarget(self, action: #selector(dropDownTapped(sender:)), for: .touchUpInside)
            if dropDownSelection == false
            {
                tblHeader?.dropDownButton.setImage(UIImage(named: "dropDownFill"), for: .normal)
            }
            else
            {
                tblHeader?.dropDownButton.setImage(UIImage(named: "upArrowFill"), for: .normal)
            }
            
            let picture = UserDefaults.standard.string(forKey: "profileImg") ?? ""
            if picture.isEmpty == true
            {
                self.userImgInStack.image = UIImage(named: "userimg")
            }
            else
            {
                if let decodedData = Data(base64Encoded: picture, options: .ignoreUnknownCharacters) {
                let image = UIImage(data: decodedData)
                    tblHeader?.profileImg.image = image
                    self.userImgInStack.image = image
                }
            }
            return tblHeader
        }
        else
        {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderVwFile") as? HeaderVwFile
            if arrayHeader[section] == 1
            {
                headerView?.dropDownIcon.image = UIImage(named: "icon_arrow_up")
            }
            else
            {
                headerView?.dropDownIcon.image = UIImage(named: "icon_arrow_down")//
            }
            headerView?.icon.image = UIImage(named: self.headerImgs[section])
            headerView?.titleLbl.text = self.headerValues[section]
            headerView?.dropDownButton.tag = section
            headerView?.dropDownButton.addTarget(self, action: #selector(tapSection(sender:)), for: .touchUpInside)
            return headerView
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //userCell
        if dropDownSelection == false
        {
            if memberFlag == false
            {
                return 1
            }
            else
            {
                return arrayHeader.count
            }
        }
        else
        {
            if memberFlag == false
            {
                return 1
            }
            else
            {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dropDownSelection == false
        {
            if self.arrayHeader[section] == 0
            {
                return 0
            }
            else
            {
                return self.menuItems[section].count
            }
        }
        else
        {
            return 1
        }
    }
    
    // 7
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if dropDownSelection == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
            cell.lbl_title.text = self.menuItems[indexPath.section][indexPath.row]
            cell.lbl_title.textColor = .white
            cell.backimg.image = UIImage(named: self.menuItmesImgs[indexPath.section][indexPath.row])
            if self.memberTypes == "g"
            {
                if indexPath.section == 1 || indexPath.section == 6
                {
                    cell.leadingEdge.constant = 0.0
                }
                else
                {
                    cell.leadingEdge.constant = 30.0
                }
            }
            else
            {
                if indexPath.section == 1 || indexPath.section == 7 || indexPath.section == 8
                {
                    cell.leadingEdge.constant = 0.0
                }
                else
                {
                    cell.leadingEdge.constant = 30.0
                }
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! CustomCell
            cell.lbl_title.text = Utilities.displayMemberType(ecode: Constants.ecode)
            cell.lbl_title.textColor = .black
            cell.backimg.image = UIImage(named: "userimg")
            return cell
        }
    }
    
    @objc func dropDownTapped(sender: UIButton)
    {
        if self.dropDownSelection == false
        {
            self.dropDownSelection = true
            self.tableview.reloadData()
        }
        else
        {
            self.dropDownSelection = false
            self.tableview.reloadData()
        }
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToProfile"), object: nil)
    }
    
    @objc func tapSection(sender: UIButton) {
        self.arrayHeader[sender.tag] = (self.arrayHeader[sender.tag] == 0) ? 1 : 0
        self.tableview.reloadSections([sender.tag], with: .fade)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if dropDownSelection == false
        {
            if self.memberTypes == "g"
            {
                if indexPath.section == 0
                {
                    return 190
                }
                else if indexPath.section == 1 || indexPath.section == 6
                {
                    return 60
                }
                else
                {
                    return 50
                }
            }
            else
            {
                if indexPath.section == 0
                {
                    return 190
                }
                else if indexPath.section == 1 || indexPath.section == 7 || indexPath.section == 8
                {
                    return 60
                }
                else
                {
                    return 50
                }
            }
        }
        else
        {
            return 80
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if dropDownSelection == false
        {
            if self.memberTypes == "g"
            {
                if section == 0
                {
                    return 170
                }
                else if section == 1 || section == 6
                {
                    return 0
                }
                else
                {
                    return 60
                }
            }
            else
            {
                if section == 0
                {
                    return 170
                }
                else if section == 1 || section == 7 || section == 8
                {
                    return 0
                }
                else
                {
                    return 60
                }
            }
        }
        else
        {
            if section == 0
            {
                return 170
            }
            else
            {
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let Vw = UIView()
        Vw.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1)
        Vw.backgroundColor = UIColor.init(named: "ColorNavMenuLine")
        return Vw
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    @IBAction func memberbtn(_ sender: Any) {
    }
    @IBAction func profilebtn(_ sender: Any) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
        
        let cell = self.tableview.cellForRow(at: indexPath) as! CustomCell
        if cell.lbl_title.text?.trimString()  == "My Orders"{
            //self.sideMenuController?.toggle()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TabBarSelection"), object: nil)
        }
        else if cell.lbl_title.text?.trimString()  == "Check In/Out QR"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 5])
        }
        else if cell.lbl_title.text?.trimString()  == "Crystal"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 6])
        }
        
        else if cell.lbl_title.text?.trimString()  == "SPA" && headerValues[indexPath.section] ==
                    "General Services"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 8])
        }
        else if cell.lbl_title.text?.trimString()  == "SPA Products"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 9])
        }
        else if cell.lbl_title.text?.trimString()  == "Sunbed"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 10])
        }
        else if cell.lbl_title.text?.trimString()  == "Sunbed Beverages"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 11])
        }
        else if cell.lbl_title.text?.trimString()  == "Event Reservation" && headerValues[indexPath.section] ==
                    "General Services"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 12])
        }
        
        else if cell.lbl_title.text?.trimString()  == "Marina"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 13])
        }
        else if cell.lbl_title.text?.trimString()  == "Sub Tenant List"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 14])
        }
        else if cell.lbl_title.text?.trimString()  == "Sub Member List"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 35])
        }
        else if cell.lbl_title.text?.trimString()  == "Guest List"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 15])
        }
        else if headerValues[indexPath.section] == "T/M Services" && cell.lbl_title.text?.trimString()  == "Sub Tenant Form"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 16])
        }
       else if headerValues[indexPath.section] == "T/M Services" && cell.lbl_title.text?.trimString()  == "Sub Member Form"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 34])
        }
        else if headerValues[indexPath.section] == "T/M Services" && cell.lbl_title.text?.trimString()  == "Guest Form"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 17])
        }
        else if headerValues[indexPath.section] == "T/M Services" && cell.lbl_title.text?.trimString()  == "Invite Guest"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 18])
        }
        else if cell.lbl_title.text?.trimString()  == "Villa Maintenance" && headerValues[indexPath.section] != "Activity History"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 19])
        }
        
        else if headerValues[indexPath.section] == "General Services" && cell.lbl_title.text?.trimString()  == "Lost & Found"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 20])
        }
        else if cell.lbl_title.text?.trimString()  == "Level"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 21])
        }
        else if cell.lbl_title.text?.trimString()  == "Quest"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 22])
        }
        else if cell.lbl_title.text?.trimString()  == "Auction House"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 23])
        }
        else if cell.lbl_title.text?.trimString()  == "Vouchers"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 24])
        }
        else if cell.lbl_title.text?.trimString()  == "XP Boost"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 25])
        }
        else if cell.lbl_title.text?.trimString()  == "Boutique"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 26])
        }
        //
        
        else if headerValues[indexPath.section] == "Activity History" && cell.lbl_title.text?.trimString()  == "F&B"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 27])
        }
        else if cell.lbl_title.text?.trimString()  == "F&B" && headerValues[indexPath.section] != "Activity History"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 7])
        }
        
        else if cell.lbl_title.text?.trimString()  == "Platinum Gong"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 28])
        }
        else if cell.lbl_title.text?.trimString()  == "Inventory"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 29])
        }
        else if cell.lbl_title.text?.trimString()  == "Titles"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 30])
        }
        else if cell.lbl_title.text?.trimString()  == "Tokens"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 31])
        }
        else if cell.lbl_title.text?.trimString()  == "Garden"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 32])
        }
        else if cell.lbl_title.text?.trimString()  == "Settings"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 33])
        }
    //
        else if cell.lbl_title.text?.trimString()  == "Leaderboard"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 36])
        }
        
        else if headerValues[indexPath.section] == "Activity History" && cell.lbl_title.text?.trimString()  == "Lost & Found"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 37])
        }
        else if headerValues[indexPath.section] != "T/M Services" && cell.lbl_title.text?.trimString()  == "Invite Guest"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 38])
        }
        else if headerValues[indexPath.section] != "T/M Services" && cell.lbl_title.text?.trimString()  == "Sub Tenant Form"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 39])
        }
       else if headerValues[indexPath.section] != "T/M Services" && cell.lbl_title.text?.trimString()  == "Sub Member Form"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 40])
        }
        else if headerValues[indexPath.section] != "T/M Services" && cell.lbl_title.text?.trimString()  == "Guest Form"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 41])
        }
        else if cell.lbl_title.text?.trimString()  == "SPA" && headerValues[indexPath.section] !=
                    "General Services"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 42])
        }
        else if cell.lbl_title.text?.trimString()  == "Villa Maintenance" && headerValues[indexPath.section] == "Activity History"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 43])
        }
        else if cell.lbl_title.text?.trimString()  == "Event Reservation" && headerValues[indexPath.section] !=
                    "General Services"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 44])
        }
        //
        else if cell.lbl_title.text?.trimString()  == "Auction Invoice"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 45])
        }
        else if cell.lbl_title.text?.trimString()  == "The Trader"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeTab"), object: nil, userInfo: ["id": 46])
        }
    }
    
}
