//
//  SideMenuHeader.swift
//  Eclair
//
//  Created by iOS Indigo on 19/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class SideMenuHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userNamelbl: UILabel!
    @IBOutlet weak var userTypelbl: UILabel!
    @IBOutlet weak var userVw: UIView!
    @IBOutlet weak var dropDownButton: UIButton!
}
