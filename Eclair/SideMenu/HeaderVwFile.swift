//
//  HeaderVwFile.swift
//  Eclair
//
//  Created by iOS Indigo on 02/11/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit

class HeaderVwFile: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var dropDownIcon: UIImageView!
    @IBOutlet weak var icon: UIImageView!
    

}
