//
//  FilterVC.swift
//  Eclair
//
//  Created by Zohaib Naseer on 7/28/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit


class FilterVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var startVw: UIView!
    @IBOutlet weak var endVw: UIView!
    @IBOutlet weak var startDatetxt: UITextField!
    @IBOutlet weak var endDatetxt: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    let datePicker = UIDatePicker()
    var selectedDate = ""
    var fromDate = ""
    var toDate = ""
    var isFromDate = false
    var delegate : dateFilterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startVw.cornerRadius = 12
        startVw.borderColor = UIColor.init(named: "ColorLine")
        startVw.borderWidth = 1
        endVw.cornerRadius = 12
        endVw.borderColor = UIColor.init(named: "ColorLine")
        endVw.borderWidth = 1
//        cancelButton.borderColor = UIColor.init(named: "ColorTextDark")
//        cancelButton.borderWidth = 1
        // Do any additional setup after loading the view.
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == startDatetxt
        {
            isFromDate = true
            textField.resignFirstResponder()
            datePickerSettings(textfield: self.startDatetxt)
        }
        else
        {
            isFromDate = false
            textField.resignFirstResponder()
            datePickerSettings(textfield: self.endDatetxt)
        }
    }

    @IBAction func applyAction(_ sender: Any) {
    
        if self.startDatetxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Please Select Start Date")
            return
        }
//        else if self.endDatetxt.text?.isEmpty == true
//        {
//            Utilities.showAlert("", message: "Please Select End Date")
//            return
//        }
        else
        {
            if self.startDatetxt.text?.isEmpty == false && self.endDatetxt.text?.isEmpty == false
            {
                let startDate = Utilities.getDateFromString(strDate: self.startDatetxt.text ?? "", currentFormate: Constants.DateFormateWithoutTime)
                
                let endDate = Utilities.getDateFromString(strDate: self.endDatetxt.text ?? "", currentFormate: Constants.DateFormateWithoutTime)
                
                if startDate > endDate
                {
                    Utilities.showAlert("", message: "End date must be greater than or equal to start date")
                    return
                }
                else
                {
                    print("Date in true formate")
                    self.delegate?.getSelectedDate(fromDate: fromDate, toDate: toDate, isFilter: true)
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else
            {
                self.delegate?.getSelectedDate(fromDate: fromDate, toDate: toDate, isFilter: true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func cancelbtn(_ sender: Any) {
        self.delegate?.getSelectedDate(fromDate: "", toDate: "", isFilter: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    func datePickerSettings(textfield:UITextField)
    {
        let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "DatePickerVc") as! DatePickerVc
            vc.delegate = self
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
    }
}
extension FilterVC : GetDateDelegate
{
    func getSelectedDate(date: String) {

        self.selectedDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: Constants.DateFormateWithoutTime)
        if isFromDate == true
        {
            self.startDatetxt.text = self.selectedDate
            self.fromDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "yyyy-MM-dd")
        }
        else
        {
            self.endDatetxt.text = self.selectedDate
            self.toDate = Utilities.convertDateFormate(forStringDate: date, currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "yyyy-MM-dd")
        }
    }
}

