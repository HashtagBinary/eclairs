//
//  TransferCoinReceiptVC.swift
//  Eclair
//
//  Created by Indigo on 28/04/2021.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TransferCoinReceiptVC: UIViewController {

    @IBOutlet weak var senderNamelbl: UILabel!
    @IBOutlet weak var senderCodelbl: UILabel!
    @IBOutlet weak var recieverNamelbl: UILabel!
    @IBOutlet weak var recieverCodelbl: UILabel!
    @IBOutlet weak var amountlbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var transferButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    var senderName = ""
    var senderCode = ""
    var receiverName = ""
    var receiverCode = ""
    var amount = ""
    var date = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVw()
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func transferbtn(_ sender: Any) {
        self.alertView()
    }
    @IBAction func cancelbtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupVw()
    {
        var currentDate = Date()
        print("current date is \(currentDate)")
       // yyyy-MM-dd HH:mm:ss Z
        let strDate = Utilities.convertDateFormate(forStringDate: "\(currentDate)", currentFormate: "yyyy-MM-dd HH:mm:ss Z", newFormate: "dd MMM, yyyy")
        self.date = strDate
        
        
        transferButton.cornerRadius = 10
        cancelButton.cornerRadius = 10
        cancelButton.borderColor = UIColor.init(named: "ColorPrimary")
        cancelButton.borderWidth = 1
        senderNamelbl.text = UserDefaults.standard.string(forKey: "fullName") ?? ""
        senderCodelbl.text = senderCode
        recieverNamelbl.text = receiverName
        recieverCodelbl.text = receiverCode
        amountlbl.text = amount
        datelbl.text = date
    }
    
    func transerCoin() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
  
        let parameters = [
            "SenderCode" : receiverCode,
            "RecieverCode":self.receiverCode,
            "Amount":self.amount,
            "Description":""
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kWalletTransferCoin, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let json = try? JSON(data:data!)
            print("json of transfer coin \(json)")
            let response = json?["response"].bool ?? false
            let message = json?["msg"].string ?? ""
            if response == false
            {
                Utilities.showAlert("", message: message)
                return
            }
            else
            {
                self.navigationController?.popToRootViewController(animated: true)
                Utilities.showAlert("", message: message)
            }
        }
    }
   
    func alertView()
    {
        let refreshAlert = UIAlertController(title: "Crystal", message: "Are you sure you want to transfer your crystal?", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
            self.transerCoin()
        }))

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }
    
}
