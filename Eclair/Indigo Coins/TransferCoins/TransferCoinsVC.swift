//
//  TransferCoinsVC.swift
//  Eclair
//
//  Created by iOS Indigo on 15/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TransferCoinsVC: UIViewController {

    @IBOutlet weak var transferButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var eclairTxt: UITextField!
    @IBOutlet weak var amounttxt: UITextField!
    
    var isEcodeValid = false
    var recieverName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        transferButton.cornerRadius = 10
        cancelButton.cornerRadius = 10
        cancelButton.borderColor = UIColor.init(named: "ColorPrimary")
        cancelButton.borderWidth = 1
        // Do any additional setup after loading the view.
    }
    @IBAction func trnasferbtn(_ sender: Any) {
        if self.eclairTxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Enter Reciever's Ecode")
            return
        }
        else if self.amounttxt.text?.isEmpty == true
        {
            Utilities.showAlert("", message: "Enter Amount")
            return
        }

        else
        {
            self.generateOtp()
        }
    }
    @IBAction func cancelbtn(_ sender: Any) {
        
            if self.eclairTxt.text?.isEmpty == true
            {
                Utilities.showAlert("", message: "Enter Reciever's Ecode")
                return
            }
            else if self.amounttxt.text?.isEmpty == true
            {
                Utilities.showAlert("", message: "Enter Amount")
                return
            }

            else
            {
                let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "verifyOTPVC") as! verifyOTPVC
                vc.modalPresentationStyle = .overFullScreen
                vc.receiverEcode = self.eclairTxt.text ?? ""
                vc.isNewOtp = false
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
    }
    //MARK:- api calling
    
    func generateOtp() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : self.eclairTxt.text ?? "",
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kWalletGenrateOTP, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json otp \(json)")
            let response = json?["response"].bool ?? false
            if response == false
            {
                Utilities.showAlert("", message: json?["msg"].string ?? "Data Not Found")
                return
            }
            else
            {
                self.alertVeiw()
            }
        }
    }
    func alertVeiw()
    {
        let refreshAlert = UIAlertController(title: "OTP", message: "OTP sent to User", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
            
            let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "verifyOTPVC") as! verifyOTPVC
            vc.modalPresentationStyle = .overFullScreen
            vc.receiverEcode = self.eclairTxt.text ?? ""
            vc.isNewOtp = true
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
}
extension TransferCoinsVC : otpVerificationDelegation{
    func otpVerify(isVerified: Bool, ecode: String, name: String) {
        print("isVerified \(isVerified) name \(name)")
        
        let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "TransferCoinReceiptVC") as! TransferCoinReceiptVC
        vc.amount = self.amounttxt.text ?? ""
        vc.senderCode = Constants.ecode
        vc.receiverName = name
        vc.receiverCode = ecode
        self.navigationController!.pushViewController(vc, animated: true)
    }
}
protocol otpVerificationDelegation {
    func otpVerify(isVerified:Bool,ecode:String,name:String)
}

