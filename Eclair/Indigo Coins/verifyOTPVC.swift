//
//  verifyOTPVC.swift
//  Eclair
//
//  Created by Zohaib Naseer on 8/6/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON


class verifyOTPVC: UIViewController {

    @IBOutlet weak var txt1: UITextField!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var txt3: UITextField!
    @IBOutlet weak var txt4: UITextField!
    @IBOutlet weak var txt5: UITextField!
    @IBOutlet weak var txt6: UITextField!
    @IBOutlet weak var timerlblWidth: NSLayoutConstraint!
    @IBOutlet weak var timerlbl: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    
    var otpTimer: Timer?
    var timeCounter = 60
    var isNewOtp = false
    var receiverEcode = ""
    var delegate : otpVerificationDelegation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timerlbl.text = ""
        timerlblWidth.constant = 0.0
    
        txt1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for:UIControl.Event.editingChanged)
        if isNewOtp == true
        {
            self.startTimer()
        }
    }
    
    @IBAction func cancelbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func resendbtn(_ sender: Any) {
        self.generateOtp()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        otpTimer?.invalidate()
    }
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case txt1:
                txt2.becomeFirstResponder()
            case txt2:
                txt3.becomeFirstResponder()
            case txt3:
                txt4.becomeFirstResponder()
            case txt4:
                txt5.becomeFirstResponder()
            case txt5:
                txt6.becomeFirstResponder()
            case txt6:
                txt6.resignFirstResponder()
                
                if txt1.text?.isEmpty == false && txt2.text?.isEmpty == false && txt3.text?.isEmpty == false &&
                    txt4.text?.isEmpty == false &&
                    txt5.text?.isEmpty == false &&
                    txt6.text?.isEmpty == false
                {
                    let otp = "\(txt1.text ?? "")\(txt2.text ?? "")\(txt3.text ?? "")\(txt4.text ?? "")\(txt5.text ?? "")\(txt6.text ?? "")"
                    self.verifyOtp(otp: otp)
                }
                
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case txt1:
                txt1.becomeFirstResponder()
            case txt2:
                txt1.becomeFirstResponder()
            case txt3:
                txt2.becomeFirstResponder()
            case txt4:
                txt3.becomeFirstResponder()
            case txt5:
                txt4.becomeFirstResponder()
            case txt6:
                txt5.becomeFirstResponder()
                
            default:
                break
            }
        }
        if text?.count ?? 0 > 1{
            textField.text = ""
        }
    }
    
    func startTimer() {
        otpTimer?.invalidate() //cancels it if already running
        otpTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerDidFire(_:)), userInfo: nil, repeats: true)
    }
    
    @objc func timerDidFire(_ timer: Timer) {

        if timeCounter != 0
        {
            timeCounter -= 1
            resendButton.isEnabled = false
            resendButton.setTitleColor(UIColor.lightGray, for: .normal)
            timerlbl.text = "\(timeCounter)"
            timerlblWidth.constant = 30.0
        }
        else
        {
            timer.invalidate()
            resendButton.setTitleColor(UIColor.white, for: .normal)
            resendButton.isEnabled = true
            timerlblWidth.constant = 0.0
            timerlbl.text = ""
        }
       }
    
    
    //MARK:- api calling
    
    
    func generateOtp() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let name = UserDefaults.standard.string(forKey: "fullName") ?? ""
        let parameters = [
            "ecode" : self.receiverEcode,
            "senderName" : name
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kWalletGenrateOTP, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json otp \(json)")
            let response = json?["response"].bool ?? false
            if response == false
            {
                Utilities.showAlert("", message: json?["msg"].string ?? "Data Not Found")
                return
            }
            else
            {
                self.txt1.text = ""
                self.txt2.text = ""
                self.txt3.text = ""
                self.txt4.text = ""
                self.txt5.text = ""
                self.txt6.text = ""
                self.startTimer()
                Utilities.showAlert("OTP", message: "OTP sent to user")
            }
        }
    }
    
    
    func verifyOtp(otp:String) {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : self.receiverEcode,
            "otpCode": otp
        ] as [String: Any]
        print("parameters \(parameters)")
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kWalletVerifyOTP, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            let response = json?["response"].bool ?? false
            if response == false
            {
                Utilities.showAlert("", message: json?["msg"].string ?? "Data Not Found")
                return
            }
            else
            {
                if json?["IsVerified"].bool ?? false == true
                {
                    self.delegate?.otpVerify(isVerified: true, ecode: json?["ECode"].string ?? "", name: json?["Name"].string ?? "")
                    self.dismiss(animated: true, completion: nil)
                }
                else
                {
                    Utilities.showAlert("OTP", message: json?["msg"].string ?? "Not Verified")
                    return
                }
            }
        }
    }
    
}

