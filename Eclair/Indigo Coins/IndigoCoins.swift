//
//  IndigoCoins.swift
//  Eclair
//
//  Created by iOS Indigo on 10/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import JJFloatingActionButton


class IndigoCoins: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var upperVw: UIView!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var coinsLabel: UILabel!
    @IBOutlet weak var grainsLabel: UILabel!
    @IBOutlet weak var lastCreditLabel: UILabel!
    @IBOutlet weak var lastDebitLabel: UILabel!
    @IBOutlet weak var floatingButton: JJFloatingActionButton!
    @IBOutlet weak var stackFirstVw: UIView!
    @IBOutlet weak var stackSecondvw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        topView.applyGradient(colours: [.black,.darkGray])
        topView.layer.zPosition = -10
        upperVw.cornerRadius = 10
        historyButton.circleCorner = true
        

        
        
        let item = floatingButton.addItem()
        item.titleLabel.text = "Transfer Crystal"
        item.imageView.image = UIImage(named: "Transfer")
        item.buttonColor = UIColor.init(named: "ColorTextDark")!
        item.buttonImageColor = UIColor.init(named: "ColorTextDark")!
        //item.buttonImageColor = CGSize(width: 30, height: 30)
        item.action = { item in
            // Do something
            let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "TransferCoinsVC") as? TransferCoinsVC
            
            self.navigationController!.pushViewController(vc!, animated: true)
        }
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.stackAction))
        let gesturetwo = UITapGestureRecognizer(target: self, action:  #selector(self.stackAction))
        self.stackFirstVw.addGestureRecognizer(gesture)
        self.stackSecondvw.addGestureRecognizer(gesturetwo)
        
//        floatingButton.addItem(title: "Transfer Coins", image: UIImage(named: "Transfer")) { item in
//
//            //
//            item.buttonColor = UIColor.init(named: "ColorTextDark")!
//            item.tintColor = UIColor.init(named: "ColorTextDark")!
//            item.buttonImageColor = UIColor.init(named: "ColorTextDark")!
//            
//            let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "TransferCoinsVC") as? TransferCoinsVC
//            
//            self.navigationController!.pushViewController(vc!, animated: true)
//        }
    }
    @objc func stackAction(sender : UITapGestureRecognizer) {
        // Do what you want
        let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "CoinStatementVC") as? CoinStatementVC
        vc?.title = "Crystal Statement"
        self.navigationController!.pushViewController(vc!, animated: true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        fetchData()
    }
    
    func fetchData() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
        ] as [String: Any]
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kGetWalletDashboard, parameters: parameters, method: .post) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            let model = try? JSONDecoder().decode(EwalletModel.self, from: data!)
            if model?.response == false
            {
                Utilities.showAlert("", message: model?.error ?? "Response error")
                return
            }
            else {
                self.coinsLabel.text = String(format: "%.2f", model?.totalCoins ?? 0)
                self.grainsLabel.text = String(model?.totalGrains ?? 0)
                self.lastDebitLabel.text = String(format: "%.2f", model?.lastDebit ?? 0)
                self.lastCreditLabel.text = String(format: "%.2f", model?.lastCredit ?? 0)
            }
        }
    }

    @IBAction func historybtn(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "CoinStatementVC") as? CoinStatementVC
        vc?.title = "Crystal Statement"
        self.navigationController!.pushViewController(vc!, animated: true)
    }
}

 
