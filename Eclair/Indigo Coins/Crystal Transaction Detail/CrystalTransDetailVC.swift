//
//  CrystalTransDetailVC.swift
//  Eclair
//
//  Created by Zohaib Naseer on 8/25/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import PDFKit

class CrystalTransDetailVC: UIViewController {

    @IBOutlet weak var referenceIdlbl: UILabel!
    @IBOutlet weak var ecodelbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var typelbl: UILabel!
    @IBOutlet weak var amountlbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var generateVw: UIView!
    @IBOutlet weak var pdfContainerVw: UIView!
    @IBOutlet weak var sapraterVw: UIView!
    
    var selectedData : EwalletFilterList?
    var pdfVw = PDFView()
    var BarButtonItem = UIBarButtonItem()
    var pdfURL : URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupVw()
    }
    func setupVw()
    {
        ecodelbl.text = selectedData?.Ecode
        descLbl.text = selectedData?.Description
        referenceIdlbl.text = "#\(selectedData?.CTID ?? 0)"
        datelbl.text = selectedData?.StrDate
        if selectedData?.Credit ?? 0 <= 0
        {
            amountlbl.textColor = UIColor.init(named: "ColorPurple")
            amountlbl.text = "\(selectedData?.Debit ?? 0) IC"
            typelbl.text = "Debit"
        }
        else
        {
            amountlbl.textColor = UIColor.init(named: "ColorGreen")
            amountlbl.text = "\(selectedData?.Credit ?? 0) IC"
            typelbl.text = "Credit"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.rightBarButtonItem = .none
        self.generateVw.isHidden = false
        self.sapraterVw.isHidden = false
    }
    
    @IBAction func gneratebtn(_ sender: Any) {
        BarButtonItem = UIBarButtonItem(image: UIImage(named: "option"), style: .done, target: self, action: #selector(clickButton))
        self.navigationItem.rightBarButtonItem  = BarButtonItem
        self.generateVw.isHidden = true
        self.sapraterVw.isHidden = true
        self.createPDF(from: self.view)
    }
    @objc func clickButton(){
        if let documet = PDFDocument(url: pdfURL)
        {
            sharePDF(documet)
        }
    }
    func sharePDF(_ filePDF: PDFDocument) {
        if let pdfData = filePDF.dataRepresentation() {
            let objectsToShare = [pdfData]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.barButtonItem = BarButtonItem
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    func createPDF(from view: UIView) {
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let outputFileURL = documentDirectory.appendingPathComponent("MyPDF.pdf")
            print("URL:", outputFileURL) // When running on simulator, use the given path to retrieve the PDF file
            self.pdfURL = outputFileURL
            let pdfRenderer = UIGraphicsPDFRenderer(bounds: view.bounds)

            do {
                try pdfRenderer.writePDF(to: outputFileURL, withActions: { context in
                    context.beginPage()
                    view.layer.render(in: context.cgContext)
                })
            } catch {
                print("Could not create PDF file: \(error)")
            }
        if let document = PDFDocument(url: outputFileURL) {
            pdfVw.document = document
            pdfVw.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.pdfVw)
            pdfVw.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            pdfVw.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
            pdfVw.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            pdfVw.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            pdfVw.displayDirection = .vertical
            pdfVw.autoScales = true
        }
    }
}
