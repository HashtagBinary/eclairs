//
//  CoinStatementVC.swift
//  Eclair
//
//  Created by iOS Indigo on 13/12/2020.
//  Copyright © 2020 Indigo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CoinStatementVC: UIViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    //var historyList = [EwalletFilterModel]()
    var filterHistoryModelList = [EwalletFilterList]()
    let take = 10
    var skip = 0
    private let refreshControl = UIRefreshControl()
    var isFilterApplied = false
    var fromDate = ""
    var toDate = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupvw()
    }
    func setupvw()
    {
        
        let button = UIButton()
        button.frame = CGRect(x: self.view.frame.size.width - 90, y: self.view.frame.height - 200, width: 60, height: 60)
        button.backgroundColor = UIColor.init(named: "ColorTextDark")
        button.setImage(UIImage(named: "pdfImage"), for: .normal)
        
        button.setTitle("", for: .normal)
        button.tintColor = .white
        button.circleCorner = true
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(filterTapped))
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        fetchData()
    }
    func creatButton()
    {
        
    }
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        
        if self.filterHistoryModelList.count <= 0
        {
            Utilities.showAlert("", message: "No records found to create the pdf")
            return
        }
        
        let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "CrystalPDFVC") as! CrystalPDFVC
        vc.selectValuesOfWallet = filterHistoryModelList
        vc.isFilterApplied = self.isFilterApplied
        vc.fromDate = self.fromDate
        vc.toDate = self.toDate
        self.navigationController?.pushViewController(vc, animated: true)
     }
    @objc func filterTapped()
    {
        let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc private func refreshData(_ sender: Any) {
        if isFilterApplied == false
        {
            self.skip = 0
            self.filterHistoryModelList.removeAll()
            self.tblVw.reloadData()
            self.refreshControl.endRefreshing()
            fetchData()
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    //MARK:- api call
    
    func fetchData() {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.requestWithHeader(serviceName: Constants.ServerAPI.kWalletFilterHistory+"?ecode=\(Constants.ecode)&take=\(self.take)&skip=\(self.skip)", parameters: nil, method: .get) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json of crystal history \(json)")
            let model = try? JSONDecoder().decode(EwalletFilterModel.self, from: data!)
            self.filterHistoryModelList += model?.list ?? []
        
            if self.filterHistoryModelList.count <= 0 && self.skip <= 0
            {
                Utilities.showAlert("", message: json?["error"].string ?? "Response error")
                return
            }
            else if model?.list.count ?? 0 <= 0 && self.skip > 0
            {
                return
            }
            else
            {
                self.tblVw.reloadData()
            }
        }
    }
    
    func fetchFilterData(fromDate:String,toDate:String) {
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        let parameters = [
            "ecode" : Constants.ecode,
            "fromDate":fromDate,
            "ToDate":toDate
        ] as [String: Any]
        
        
        var url = Constants.ServerAPI.kWalletFilterHistory+"?ecode=\(Constants.ecode)&fromDate=\(fromDate)&ToDate=\(toDate)"
        if toDate.isEmpty == true
        {
            url = Constants.ServerAPI.kWalletFilterHistory+"?ecode=\(Constants.ecode)&fromDate=\(fromDate)"
        }
        
        AlamorfireSingleton.requestWithHeader(serviceName: url, parameters: nil, method: .get) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            let json = try? JSON(data:data!)
            print("json of crystal history \(json)")
            do{
                let model = try JSONDecoder().decode(EwalletFilterModel.self, from: data!)
                if model.response == false
                {
                    Utilities.showAlert("", message: model.error ?? "")
                    return
                }
                else
                {
                    self.filterHistoryModelList = model.list
                    self.tblVw.reloadData()
                }
            }
            catch{
                Utilities.showAlert("Error", message: error.localizedDescription)
            }
        }
    }
}
extension CoinStatementVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterHistoryModelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CoinStatementCell
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        cell.datelbl.text = self.filterHistoryModelList[indexPath.row].StrDate ?? ""
        if self.filterHistoryModelList[indexPath.row].Credit ?? 0 <= 0
        {
            cell.titleLbl.text = "\(self.filterHistoryModelList[indexPath.row].Debit ?? 0.0) IC"
            cell.titleLbl.textColor = UIColor.init(named: "ColorPurple")
            cell.img.image = UIImage(named: "History-debit")
        }
        else
        {
            cell.titleLbl.text = "\(self.filterHistoryModelList[indexPath.row].Credit ?? 0.0) IC"
            cell.titleLbl.textColor = UIColor.init(named: "ColorGreen")
            cell.img.image = UIImage(named: "History-credit")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if skip == 0
        {
            if isFilterApplied == false
            {
                let animation = AnimationFactory.makeSlideIn(duration: 0.5, delayFactor: 0.05)
                let animator = Animator(animation: animation)
                animator.animate(cell: cell, at: indexPath, in: tableView)
            }
        }
       
        if isFilterApplied == false
        {
            if indexPath.row == self.filterHistoryModelList.count - 1 {
                skip += take
                self.fetchData()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Forms", bundle: Bundle.main).instantiateViewController(withIdentifier: "CrystalTransDetailVC") as! CrystalTransDetailVC
        vc.title = "Transaction Detail"
        vc.selectedData = self.filterHistoryModelList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension CoinStatementVC : dateFilterDelegate
{
    func getSelectedDate(fromDate: String, toDate: String ,isFilter:Bool) {
        self.fromDate = Utilities.convertDateFormate(forStringDate: fromDate, currentFormate: "yyyy-MM-dd", newFormate: Constants.DateFormateWithoutTime)
        self.toDate = Utilities.convertDateFormate(forStringDate: toDate, currentFormate: "yyyy-MM-dd", newFormate: Constants.DateFormateWithoutTime)
        print("from \(fromDate) to \(toDate)")
        self.filterHistoryModelList.removeAll()
        self.skip = 0
        if isFilter == true
        {
            self.isFilterApplied = true
            self.fetchFilterData(fromDate: fromDate, toDate: toDate)
        }
        else
        {
            self.isFilterApplied = false
            self.fetchData()
        }
    }
}

protocol dateFilterDelegate {
    func getSelectedDate(fromDate:String,toDate:String ,isFilter:Bool)
}
