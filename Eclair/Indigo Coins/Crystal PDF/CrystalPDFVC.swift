//
//  CrystalPDFVC.swift
//  Eclair
//
//  Created by Zohaib Naseer on 8/13/21.
//  Copyright © 2021 Indigo. All rights reserved.
//

import UIKit
import PDFKit

class CrystalPDFVC: UITableViewController {

    @IBOutlet var headerVw: UIView!
    @IBOutlet var footerVw: UIView!
    @IBOutlet var tblVw: UITableView!
    @IBOutlet weak var fromToDatelbl: UILabel!
    @IBOutlet weak var dateVw: UIView!
    @IBOutlet weak var statementPeriodVw: UIView!
    
    
    var selectValuesOfWallet = [EwalletFilterList]()
    var BarButtonItem = UIBarButtonItem()
    var pdfURL: URL!
    var pdfVw = PDFView()
    var fromDate = ""
    var toDate = ""
    var isFilterApplied = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BarButtonItem = UIBarButtonItem(image: UIImage(named: "option"), style: .done, target: self, action: #selector(clickButton))
        self.navigationItem.rightBarButtonItem  = BarButtonItem
        
        
        tableView.tableHeaderView = self.headerVw
        tableView.tableFooterView = footerVw
        self.headerVw.width = self.view.width
        self.footerVw.width = self.view.width
        self.dateVw.cornerRadius = 12
        self.dateVw.borderColor = UIColor.init(named: "ColorTextDark")
        self.dateVw.borderWidth = 1
        if isFilterApplied == true
        {
            //statementPeriodVw.isHidden = false
            self.fromToDatelbl.text = "\(self.fromDate)  To  \(self.toDate)"
        }
        else
        {
            //statementPeriodVw.isHidden = true
            self.fromToDatelbl.text = "Top \(selectValuesOfWallet.count) Records"
        }
        
        
        self.pdfDataWithTableView(tableView: tblVw)
    }
    
    @objc func clickButton(){
        if let documet = PDFDocument(url: pdfURL)
        {
            sharePDF(documet)
        }
    }
    func sharePDF(_ filePDF: PDFDocument) {
        if let pdfData = filePDF.dataRepresentation() {
            let objectsToShare = [pdfData]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.barButtonItem = BarButtonItem
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    // MARK: - Table view data source

    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectValuesOfWallet.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ReceiptItemCell
        if indexPath.row != 0
        {
            cell.ItemNameTitlelbl.isHidden = true
            cell.rateTitlelbl.isHidden = true
            cell.qtyTitlelbl.isHidden = true
            cell.amountTitlelbl.isHidden = true
            cell.itemTitleHeight.constant = 0.0
            cell.rateHeight.constant = 0.0
            cell.qtyHeight.constant = 0.0
            cell.amountHeight.constant = 0.0
        }
        else
        {
            cell.ItemNameTitlelbl.isHidden = false
            cell.rateTitlelbl.isHidden = false
            cell.qtyTitlelbl.isHidden = false
            cell.amountTitlelbl.isHidden = false
            cell.itemTitleHeight.constant = 17.0
            cell.rateHeight.constant = 17.0
            cell.qtyHeight.constant = 17.0
            cell.amountHeight.constant = 17.0
        }
        cell.menuItemlbl.text = "\(indexPath.row + 1)."
        if self.selectValuesOfWallet[indexPath.row].Credit ?? -1 <= 0
        {
            cell.ratelbl.text = "\(self.selectValuesOfWallet[indexPath.row].Debit ?? 0.0) IC"
            cell.ratelbl.textColor = UIColor.init(named: "ColorPurple")
            cell.qtylbl.text = "Debit"
        }
        else
        {
            cell.ratelbl.text = "\(self.selectValuesOfWallet[indexPath.row].Credit ?? 0.0) IC"
            cell.ratelbl.textColor = UIColor.init(named: "ColorGreen")
            cell.qtylbl.text = "Credit"
        }
        cell.amountlbl.text = selectValuesOfWallet[indexPath.row].StrDate ?? ""
        return cell
        
    }

    func pdfDataWithTableView(tableView: UITableView) {
      let priorBounds = tableView.bounds
        
        print("priorBounds \(priorBounds)")
        
      let fittedSize = tableView.sizeThatFits(CGSize(
        width: priorBounds.size.width,
        height: tableView.contentSize.height
      ))
        print("fitter size \(fittedSize)")
      tableView.bounds = CGRect(
        x: 0, y: 0,
        width: fittedSize.width,
        height: fittedSize.height
      )
        print("tableview.bouds \(tableView.bounds)")
      let pdfPageBounds = CGRect(
        x :0, y: 0,
        width: tableView.frame.width,
        height: self.view.frame.height
      )
        print("pdfPageBounds \(pdfPageBounds)")
        
      let pdfData = NSMutableData()
      UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)

      var pageOriginY: CGFloat = 0
      while pageOriginY < fittedSize.height {
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        pageOriginY += pdfPageBounds.size.height
        tableView.contentOffset = CGPoint(x: 0, y: 0)   // move "renderer"
      }
      UIGraphicsEndPDFContext()

      tableView.bounds = priorBounds //priorBounds
      var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
      docURL = docURL.appendingPathComponent("myDocument.pdf")
      pdfData.write(to: docURL as URL, atomically: true)

        self.pdfURL = docURL
        if let document = PDFDocument(url: pdfURL) {
            pdfVw.document = document
            pdfVw.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.pdfVw)
            pdfVw.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            pdfVw.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
            pdfVw.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            pdfVw.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            pdfVw.displayDirection = .vertical
            pdfVw.autoScales = true
        }
    }
}
