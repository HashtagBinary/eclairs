//
//  ViewController.swift
//  Eclair IOS
//
//  Created by Sohail on 01/11/2020.
//

import UIKit
import SwiftyJSON
import Alamofire

class EventFormViewController: UIViewController {
    
    @IBOutlet weak var timer: EventCountDownTimer!
    @IBOutlet weak var timerBox: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var eventModel: EventModel?
    var selectedUnit: EventUnitModel?
    var selectedEcodes = [EventEcodeModel]()
    var selectedExtraEcodes = [EventEcodeModel]()
    var selectedNumberButtonValue = 0
    let unitPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        unitPickerView.delegate = self
        unitPickerView.dataSource = self
        tableView.register(UINib(nibName: "EventFormTableViewCell", bundle: nil), forCellReuseIdentifier: "EventFormTableViewCell")
        tableView.register(UINib(nibName: "EventSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "EventSummaryTableViewCell")
        
        setupViews()
        fetchEventRequest()
    }
    func setupViews() {
        timerBox.shadowColor = UIColor.black
        timerBox.shadowOffset = CGSize(width: 0, height: 1)
        timerBox.shadowOpacity = 0.1
        timerBox.shadowRadius = 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @objc func selectUnit(sender: UITextField) {
        self.resetData()
        self.selectedUnit = self.eventModel?.unitList[unitPickerView.selectedRow(inComponent: 0)]
        tableView.reloadData()
    }
    
    @objc func extraUnitChanged(sender: NumberButton) {
        selectedNumberButtonValue = sender.currentValue
        tableView.reloadData()
    }
    
    @objc func bookUnitClick(sender: UIButton) {
        sender.tag = 0
        performSegue(withIdentifier: "eventEcodePicker", sender: sender)
    }
    
    @objc func bookExtraUnitClick(sender: UIButton) {
        sender.tag = 1
        performSegue(withIdentifier: "eventEcodePicker", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "eventEcodePicker") {
            let button = sender as! UIButton
            if (button.tag == 0) {
                let vc = segue.destination as! EventEcodePickerViewController
                vc.isExtraUnit = false
                vc.subUnitsCount = selectedUnit?.quantity ?? 0
                vc.ecodeList = self.selectedEcodes
            }
            else
            {
                let vc = segue.destination as! EventEcodePickerViewController
                vc.isExtraUnit = true
                vc.subUnitsCount = selectedNumberButtonValue
                vc.ecodeList = self.selectedExtraEcodes
            }
        }
    }
    
    func update() {
        tableView.reloadData()
    }
    
    func resetData() {
        selectedEcodes = [EventEcodeModel]()
        selectedExtraEcodes = [EventEcodeModel]()
        selectedNumberButtonValue = 0
    }
    
    func fetchEventRequest()
    {
        let parameters = [
            "type" : "Gym",
        ]
        print("parameter \(parameters)")
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.getCall(serviceName: Constants.ServerAPI.kEvent, parameters: parameters) {  (data:Data?, error:NSError?)  in
            print("api url \(Constants.ServerAPI.kEvent)")
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
                }
             do{
                let json = try? JSON(data:data!)
                print("json data of event \(json)")
                let model = try JSONDecoder().decode(EventResponseModel.self, from: data!)
                if (model.response) {
                    self.eventModel = model.data
                    let endDate = self.eventModel?.eventEnd!.split(separator: "T")
                    let input = "\(endDate?[0] ?? "0") \(self.eventModel?.endTime ?? "") +05:00"
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss xxxxx"
                    let date: Date? = dateFormatter.date(from:input)
                    let endTimeMillis = date?.millisecondsSince1970 ?? 0
                    self.timer.startTimer(endTime: endTimeMillis)
                }
                else
                {
                    let message = model.msg ?? "Server Error"
                    let refreshAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToHome"), object: nil)
                    }))
                    self.present(refreshAlert, animated: true, completion: nil)
                }
                self.tableView.reloadData()
               } catch let error as NSError {
                print(error)
            }
        }
    }
    
    @objc func checkOut(sender: UIButton) {
        
        if selectedUnit == nil || selectedEcodes.count == 0 {
            Utilities.showAlert("Checkout Error", message: "Please select all required fields!")
            return
        }
            var selectedEcodesJson = ""
            var extraUnitEcodesJson = ""
        do {
            var jsonData = try JSONEncoder().encode(self.selectedEcodes)
            selectedEcodesJson = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
            jsonData = try JSONEncoder().encode(self.selectedExtraEcodes)
            extraUnitEcodesJson = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
           }
        catch {
            print(error)
        }
        let parameters = [
            "eventId" : self.eventModel?.eventId ?? -1,
            "unitId" : self.selectedUnit?.id ?? -1,
            "unitName" : self.selectedUnit?.name ?? -1,
            "unitAmount" : self.selectedUnit?.rate ?? 0.0,
            "totalUnits" : self.selectedUnit?.quantity ?? 0,
            "extraUnits" : self.selectedNumberButtonValue,
            "extraUnitsAmount" : self.eventModel?.extraUnitsAmount ?? 0.0,
            "vat" : self.eventModel?.vat ?? 0.0,
            "totalAmount" : self.eventModel?.totalAmount ?? 0.0,
            "selectedEcodes" : selectedEcodesJson,
            "extraUnitEcodes" : extraUnitEcodesJson,
            "ecode" : Constants.ecode,
        ] as [String : Any]
        
        print("parameter \(parameters)")
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kEventSave, parameters: parameters) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
               }
            do {
                let model = try JSONDecoder().decode(EventResponseModel.self, from: data!)
                if (model.response) {
                    Utilities.showAlert("Success", message: model.msg ?? "")
                }
                self.tableView.reloadData()
             }  catch let error as NSError {
                print(error)
            }
        }
    }
}

extension EventFormViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return eventModel?.unitList.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return eventModel?.unitList[row].name
    }
}
