//
//  EventFormTableViewCell.swift
//  Eclair IOS
//
//  Created by Sohail on 01/11/2020.
//

import UIKit

class EventFormTableViewCell: UITableViewCell {

    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var subUnitsAvailableLabel: UILabel!
    @IBOutlet weak var subUnitAmountLabel: UILabel!
    @IBOutlet weak var extraUnitAmountLabel: UILabel!
    @IBOutlet weak var bookUnitButton: UIButton!
    @IBOutlet weak var bookExtraUnitButton: UIButton!
    @IBOutlet weak var unitTextField: UITextField!
    @IBOutlet weak var bookUnitBox: UIStackView!
    @IBOutlet weak var bookUnitLine: UIView!
    @IBOutlet weak var bookExtraUnitBox: UIStackView!
    @IBOutlet weak var bookExtraUnitLine: UIView!
    @IBOutlet weak var selectedEcodesLabel: UILabel!
    @IBOutlet weak var selectedExtraEcodesLabel: UILabel!
    @IBOutlet weak var extraUnitNumberButton: NumberButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
    
    func setupViews() {
        unitTextField.paddingRightCustom = 35
        
        containerView.cornerRadius = 12
        containerView.shadowColor = UIColor.black
        containerView.shadowOffset = CGSize(width: 0, height: 0)
        containerView.shadowOpacity = 0.1
        containerView.shadowRadius = 3
        
        bookUnitButton.borderWidth = 1
        bookUnitButton.cornerRadius = 8
        bookUnitButton.borderColor = UIColor.init(named: "ColorPrimary")
        
        bookExtraUnitButton.borderWidth = 1
        bookExtraUnitButton.cornerRadius = 8
        bookExtraUnitButton.borderColor = UIColor.init(named: "ColorPrimary")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
