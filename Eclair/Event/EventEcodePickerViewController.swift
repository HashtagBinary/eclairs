//
//  EventEcodePickerViewController.swift
//  Eclair IOS
//
//  Created by Sohail on 03/11/2020.
//

import UIKit
import SwiftyJSON

class EventEcodePickerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var subUnitsCount: Int = 5
    var isExtraUnit: Bool = false
    
    var ecodeList = [EventEcodeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func setupViews() {
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
        noteLabel.text = Constants.kEventEcodePickerNote
    }
    
    @IBAction func onDone(_ sender: UIButton) {
        if let presenter = presentingViewController as? EventFormViewController {
            if (isExtraUnit) {
                presenter.selectedExtraEcodes = ecodeList
            }
            else {
                presenter.selectedEcodes = ecodeList
            }
            presenter.update()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onEcodeClick(sender: UIButton) {
        let alert = UIAlertController(title: "Select Option", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Remove Ecode", style: .default , handler:{ (UIAlertAction)in
            if (sender.tag <= self.ecodeList.count) {
                self.ecodeList.remove(at: sender.tag)
                self.tableView.reloadData()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceView = self.view // works for both iPhone & iPad
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text != nil && !textField.text!.isEmpty) {
            verifyEcode(ecode: textField.text!)
        }
    }
    
    func verifyEcode(ecode: String) {
        let index = self.ecodeList.firstIndex{$0.code == ecode}
        if (index != nil) {
            Utilities.showAlert("Ecode Error", message: "Already exist!")
            return
        }
        
        let parameters = [
            "ecode" : ecode,
        ]
        print("parameter \(parameters)")
        
        ActivityIndicatorSingleton.StartActivity(myView: self.view)
        
        AlamorfireSingleton.postCallwithHeader(serviceName: Constants.ServerAPI.kVerifyEcode, parameters: parameters) {  (data:Data?, error:NSError?)  in
            ActivityIndicatorSingleton.StopActivity(myView: self.view)
            
            if error != nil {
                print(error?.localizedDescription ?? "Error")
                Utilities.showAlert(Constants.ErrorMessage.kTitle  , message: error?.localizedDescription ?? Constants.ErrorMessage.kNoInternetConnectionMessage)
                return
            }
            
            if (data == nil) {
                return
            }
            
            do {
                let json = try? JSON(data: data!)
                
                if let response = json?["response"].boolValue, response == true {
                    let index = self.ecodeList.firstIndex{$0.code == ecode}
                    if (index == nil) {
                        self.ecodeList.append(EventEcodeModel(code: ecode, name: json?["name"].stringValue))
                        self.tableView.reloadData()
                    }
                }
                else {
                    Utilities.showAlert("Ecode Error", message: "Invalid ecode!")
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subUnitsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventEcodeTableViewCell", for: indexPath) as! EventEcodeTableViewCell
        
        cell.ecodeTextField.delegate = self
        
        if (indexPath.row < ecodeList.count) {
            cell.successBox.isHidden = false
            cell.ecodeTextField.text = ecodeList[indexPath.row].code
            cell.nameLabel.text = ecodeList[indexPath.row].name
            cell.ecodeTextField.isEnabled = false
            cell.ecodeActionButton.isHidden = false
        }
        else {
            cell.successBox.isHidden = true
            cell.ecodeTextField.text = ""
            cell.nameLabel.text = ""
            cell.ecodeTextField.isEnabled = true
            cell.ecodeActionButton.isHidden = true
        }
        
        if (indexPath.row <= ecodeList.count) {
            cell.ecodeTextField.isEnabled = true
            cell.ecodeTextField.backgroundColor = UIColor.white
        }
        else {
            cell.ecodeTextField.isEnabled = false
            cell.ecodeTextField.backgroundColor = UIColor.init(named: "ColorLine")
        }
        
        cell.ecodeActionButton.addTarget(self, action: #selector(onEcodeClick(sender:)), for: .touchUpInside)
        cell.ecodeActionButton.tag = indexPath.row
        
        return cell
    }
}
