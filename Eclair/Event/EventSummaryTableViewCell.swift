//
//  EventSummaryTableViewCell.swift
//  Eclair IOS
//
//  Created by Sohail on 01/11/2020.
//

import UIKit

class EventSummaryTableViewCell: UITableViewCell {

    
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var totalUnitsLabel: UILabel!
    @IBOutlet weak var selectedEcodesLabel: UILabel!
    @IBOutlet weak var extraEcodesLabel: UILabel!
    @IBOutlet weak var unitAmountLabel: UILabel!
    @IBOutlet weak var extraUnitTotalLabel: UILabel!
    @IBOutlet weak var vatTitleLabel: UILabel!
    @IBOutlet weak var vatLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var selectedEcodesBox: UIView!
    @IBOutlet weak var extraEcodesBox: UIView!
    @IBOutlet weak var selectedEcodesButton: UIButton!
    @IBOutlet weak var extraEcodesButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    func setupViews() {
        selectedEcodesBox.borderWidth = 1
        selectedEcodesBox.cornerRadius = 4
        selectedEcodesBox.borderColor = UIColor.init(named: "ColorLine")
        
        extraEcodesBox.borderWidth = 1
        extraEcodesBox.cornerRadius = 4
        extraEcodesBox.borderColor = UIColor.init(named: "ColorLine")
        
        checkOutButton.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
