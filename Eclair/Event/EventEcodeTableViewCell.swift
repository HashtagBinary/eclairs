//
//  EventEcodeTableViewCell.swift
//  Eclair IOS
//
//  Created by Sohail on 03/11/2020.
//

import UIKit

class EventEcodeTableViewCell: UITableViewCell {

    @IBOutlet weak var ecodeActionButton: UIButton!
    @IBOutlet weak var ecodeTextField: UITextField!
    @IBOutlet weak var successBox: UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
