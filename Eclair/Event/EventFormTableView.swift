//
//  ViewController.swift
//  Eclair IOS
//
//  Created by Sohail on 01/11/2020.
//

import UIKit

extension EventFormViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventFormTableViewCell", for: indexPath) as! EventFormTableViewCell
            
            cell.eventTitle.text = self.eventModel?.eventName
            
            cell.subUnitsAvailableLabel.text = String(self.selectedUnit?.quantity ?? 0)
            cell.subUnitAmountLabel.text = "\(self.selectedUnit?.rate ?? 0) \(Constants.CURRENCY_SYMBOL)"
            cell.extraUnitAmountLabel.text = "\(self.selectedUnit?.extraRate ?? 0) \(Constants.CURRENCY_SYMBOL)"
            
            if (self.selectedEcodes.count > 0) {
                cell.selectedEcodesLabel.text = "\(self.selectedEcodes.count) \((self.selectedEcodes.count == 1) ? "ecode" : "ecodes") selected"
            }
            else {
                cell.selectedEcodesLabel.text = ""
            }
            
            if (self.selectedExtraEcodes.count > 0) {
                cell.selectedExtraEcodesLabel.text = "\(self.selectedExtraEcodes.count) \((self.selectedExtraEcodes.count == 1) ? "ecode" : "ecodes") selected"
            }
            else {
                cell.selectedExtraEcodesLabel.text = ""
            }
            
            if (self.selectedUnit != nil) {
                cell.bookUnitBox.isHidden = false
                cell.bookUnitLine.isHidden = false
                cell.unitTextField.text = self.selectedUnit!.name
                cell.extraUnitNumberButton.maxValue = self.selectedUnit!.extra!
            }
            else {
                cell.bookUnitBox.isHidden = true
                cell.bookUnitLine.isHidden = true
                cell.unitTextField.text = "Select Unit"
                cell.extraUnitNumberButton.maxValue = 0
            }
            
            cell.extraUnitNumberButton.setValue(newValue: selectedNumberButtonValue)
            
            if (selectedNumberButtonValue > 0) {
                cell.bookExtraUnitBox.isHidden = false
                cell.bookExtraUnitLine.isHidden = false
            }
            else {
                cell.bookExtraUnitBox.isHidden = true
                cell.bookExtraUnitLine.isHidden = true
            }
            
            cell.unitTextField.inputView = unitPickerView
            cell.unitTextField.addHideinputAccessoryView()
            cell.unitTextField.addTarget(self, action: #selector(selectUnit(sender:)), for: .valueChanged)
           
            cell.bookUnitButton.addTarget(self, action: #selector(bookUnitClick(sender:)), for: .touchUpInside)
            cell.bookExtraUnitButton.addTarget(self, action: #selector(bookExtraUnitClick(sender:)), for: .touchUpInside)
            
            cell.extraUnitNumberButton.addTarget(self, action: #selector(extraUnitChanged(sender:)), for: .valueChanged)
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventSummaryTableViewCell", for: indexPath) as! EventSummaryTableViewCell
            
            let totalUnits = (self.selectedUnit?.quantity ?? 0) + self.selectedNumberButtonValue
            cell.totalUnitsLabel.text = String(format: "%02d", totalUnits)
            
            var ecodes = ""
            self.selectedEcodes.forEach { (x) in
                ecodes = ecodes + x.code!.uppercased() + ", "
            }
            cell.selectedEcodesLabel.text = ecodes
            
            var extraEcodes = ""
            self.selectedExtraEcodes.forEach { (x) in
                extraEcodes = extraEcodes + x.code!.uppercased() + ", "
            }
            cell.extraEcodesLabel.text = extraEcodes
            
            let unitTotal = (self.selectedUnit?.rate ?? 0) * Float(self.selectedUnit?.quantity ?? 0)
            let extraUnitTotal = Float(self.selectedNumberButtonValue) * Float(self.selectedUnit?.extraRate ?? 0)
            
            cell.unitAmountLabel.text = "\(unitTotal) \(Constants.CURRENCY_SYMBOL)"
            cell.extraUnitTotalLabel.text = "\(extraUnitTotal) \(Constants.CURRENCY_SYMBOL)"
            
            let subtotal = unitTotal +  extraUnitTotal

            // Tax Calculation
            let vtax = subtotal * Constants.kEventVatPercentage / 100
            let event_total = vtax + subtotal
            
            self.eventModel?.extraUnitsAmount = extraUnitTotal
            self.eventModel?.vat = vtax
            self.eventModel?.totalAmount = event_total

            cell.vatTitleLabel.text = "\(Constants.kEventVatLabel) (\(Constants.kEventVatPercentage)%)"
            cell.vatLabel.text = "\(vtax) \(Constants.CURRENCY_SYMBOL)"
            cell.totalAmountLabel.text = "\(event_total) \(Constants.CURRENCY_SYMBOL)"
            
            cell.selectedEcodesButton.addTarget(self, action: #selector(bookUnitClick(sender:)), for: .touchUpInside)
            cell.extraEcodesButton.addTarget(self, action: #selector(bookExtraUnitClick(sender:)), for: .touchUpInside)
            cell.checkOutButton.addTarget(self, action: #selector(checkOut(sender:)), for: .touchUpInside)
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension UITextField {

    func addHideinputAccessoryView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButton(sender:)))

        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        self.inputAccessoryView = toolBar
    }
    
    @objc func doneButton(sender: Any) {
        sendActions(for: .valueChanged)
        self.endEditing(true)
    }
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
