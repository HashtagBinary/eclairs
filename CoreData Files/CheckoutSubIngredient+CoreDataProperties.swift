//
//  CheckoutSubIngredient+CoreDataProperties.swift
//  
//
//  Created by Bilal on 6/9/20.
//
//

import Foundation
import CoreData


extension CheckoutSubIngredient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CheckoutSubIngredient> {
        return NSFetchRequest<CheckoutSubIngredient>(entityName: "CheckoutSubIngredient")
    }

    @NSManaged public var id: Int32
    @NSManaged public var subingid: Int32
    @NSManaged public var checkoutingredient_id: Int32
    @NSManaged public var localdb_checkoutingredient_id: Int32
    @NSManaged public var quantity: Int32

}
