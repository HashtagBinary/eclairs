//
//  State+CoreDataProperties.swift
//  
//
//  Created by Bilal on 9/2/20.
//
//

import Foundation
import CoreData


extension State {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<State> {
        return NSFetchRequest<State>(entityName: "State")
    }

    @NSManaged public var fnb_address: String?
    @NSManaged public var fnb_address_type: Int32
    @NSManaged public var fnb_department_id: Int32
    @NSManaged public var id: Int32
    @NSManaged public var user_id: Int32
    @NSManaged public var fnb_department_is_sheesha_available: Bool

}
