//
//  CheckoutIngredient+CoreDataProperties.swift
//  
//
//  Created by iOS Indigo on 18/03/2020.
//
//

import Foundation
import CoreData


extension CheckoutIngredient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CheckoutIngredient> {
        return NSFetchRequest<CheckoutIngredient>(entityName: "CheckoutIngredient")
    }

    @NSManaged public var checkoutitem_id: Int32
    @NSManaged public var id: Int32
    @NSManaged public var ingredient_id: Int32
    @NSManaged public var localdb_checkoutitem_id: Int32
    @NSManaged public var price: Double
    @NSManaged public var quantity: Double
    @NSManaged public var type: Int32
    @NSManaged public var user_id: Int32

}
