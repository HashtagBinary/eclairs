//
//  CheckoutItem+CoreDataProperties.swift
//  
//
//  Created by iOS Indigo on 18/03/2020.
//
//

import Foundation
import CoreData


extension CheckoutItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CheckoutItem> {
        return NSFetchRequest<CheckoutItem>(entityName: "CheckoutItem")
    }

    @NSManaged public var id: Int32
    @NSManaged public var item_id: Int32
    @NSManaged public var quantity: Double
    @NSManaged public var user_id: Int32

}
